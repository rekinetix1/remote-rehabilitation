import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './containers/App';
import * as serviceWorker from './serviceWorker';
import {ConnectedRouter} from "connected-react-router";
import {Provider} from "react-redux";
import store from "./redux/store";
import {history} from "./redux/reducers/rootReducer";
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from './axios-api';
import ErrorBoundary from "./containers/ErrorBoundary";

axios.interceptors.request.use(req => {
    try {
        req.headers['Token'] = store.getState().users.user ?
            store.getState().users.user.token : '';
    } catch (e) {
        throw new Error(e);
    }
    return req;
});


const app = (
  <Provider store={store}>
    <ErrorBoundary>
      <ConnectedRouter history={history}>
        <App />
      </ConnectedRouter>
    </ErrorBoundary>
  </Provider>
);

ReactDOM.render(app, document.getElementById('root'));
serviceWorker.unregister();
