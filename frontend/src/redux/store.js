import {createStore, applyMiddleware, compose} from "redux";
import thunkMiddleware from "redux-thunk";
import rootReducer from "./reducers/rootReducer";
import {routerMiddleware} from "connected-react-router";
import {history} from "./reducers/rootReducer";
import { saveToLocalStorage, loadFromLocalStorage } from "./localStorage";

const middleware = [
    thunkMiddleware,
    routerMiddleware(history)
];

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const enhancers = composeEnhancers(applyMiddleware(...middleware));

const defaultState = loadFromLocalStorage();

const store = createStore(rootReducer, defaultState, enhancers);

store.subscribe(() => {
    saveToLocalStorage({
        users: {
          user: store.getState().users.user
        }
    });
});

export default store;