import { 
    CREATE_INDIVIDUAL_PROGRAM_FAILURE, CREATE_INDIVIDUAL_PROGRAM_SUCCESS, 
    DELETE_INDIVIDUAL_PROGRAM_FAILURE, DELETE_INDIVIDUAL_PROGRAM_REQUEST, 
    FETCH_INDIVIDUAL_PROGRAMS_FAILURE, FETCH_INDIVIDUAL_PROGRAMS_REQUEST, 
    FETCH_INDIVIDUAL_PROGRAMS_SUCCESS, FETCH_INDIVIDUAL_PROGRAM_REQUEST,
    FETCH_INDIVIDUAL_PROGRAM_SUCCESS, FETCH_INDIVIDUAL_PROGRAM_FAILURE,
    CREATE_INDIVIDUAL_PROGRAM_REQUEST, UPDATE_INDIVIDUAL_PROGRAM_REQUEST,
    UPDATE_INDIVIDUAL_PROGRAM_SUCCESS, UPDATE_INDIVIDUAL_PROGRAM_FAILURE 
} from "../actionTypes/individualProgramsTypes"

export const fetchIndividualProgramsRequest = () => {
    return {
        type: FETCH_INDIVIDUAL_PROGRAMS_REQUEST
    }
};

export const fetchIndividualProgramsSuccess = (data) => {
    return {
        type: FETCH_INDIVIDUAL_PROGRAMS_SUCCESS,
        payload: data
    }
};

export const fetchIndividualProgramsFailure = (error) => {
    return {
        type: FETCH_INDIVIDUAL_PROGRAMS_FAILURE,
        payload: error
    }
};

export const fetchIndividualProgramRequest = () => {
    return {
        type: FETCH_INDIVIDUAL_PROGRAM_REQUEST
    }
};

export const fetchIndividualProgramSuccess = (data) => {
    return {
        type: FETCH_INDIVIDUAL_PROGRAM_SUCCESS,
        payload: data
    }
};

export const fetchIndividualProgramFailure = (error) => {
    return {
        type: FETCH_INDIVIDUAL_PROGRAM_FAILURE,
        payload: error
    }
};

export const createIndividualProgramRequest = () => {
    return {
        type: CREATE_INDIVIDUAL_PROGRAM_REQUEST
    }
};

export const createIndividualProgramSuccess = () => {
    return {
        type: CREATE_INDIVIDUAL_PROGRAM_SUCCESS
    }
};

export const createIndividualProgramFailure = (error) => {
    return {
        type: CREATE_INDIVIDUAL_PROGRAM_FAILURE,
        payload: error
    }
};

export const updateIndividualProgramRequest = () => {
    return {
        type: UPDATE_INDIVIDUAL_PROGRAM_REQUEST
    }
};

export const updateIndividualProgramSuccess = () => {
    return {
        type: UPDATE_INDIVIDUAL_PROGRAM_SUCCESS
    }
};

export const updateIndividualProgramFailure = (error) => {
    return {
        type: UPDATE_INDIVIDUAL_PROGRAM_FAILURE
    }
};

export const deleteIndividualProgramRequest = () => {
    return {
        type: DELETE_INDIVIDUAL_PROGRAM_REQUEST
    }
};

export const deleteIndividualProgramSuccess = () => {
    return {
        type: DELETE_INDIVIDUAL_PROGRAM_REQUEST
    }
};

export const deleteIndividualProgramFailure = (error) => {
    return {
        type: DELETE_INDIVIDUAL_PROGRAM_FAILURE
    }
};
