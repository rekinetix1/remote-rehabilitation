import {
  FETCH_MUSCLE_REQUEST,
  FETCH_MUSCLES_SUCCESS, FETCH_MUSCLES_FAILURE,
  FETCH_MUSCLE_SUCCESS, FETCH_MUSCLE_FAILURE
} from "../actionTypes/musclesActionTypes";

export const fetchMusclesRequest = () => {
  return {
    type: FETCH_MUSCLE_REQUEST
  }
};

export const fetchMusclesSuccess = (muscles) => {
  return {
    type: FETCH_MUSCLES_SUCCESS,
    payload: muscles
  }
};

export const fetchMusclesFailure = (error) => {
  return {
    type: FETCH_MUSCLES_FAILURE,
    payload: error
  }
};

export const fetchMuscleSuccess = (muscle) => {
  return {
    type: FETCH_MUSCLE_SUCCESS,
    payload: muscle
  }
};

export const fetchMuscleFailure = (error) => {
  return {
    type: FETCH_MUSCLE_FAILURE,
    payload: error
  }
};