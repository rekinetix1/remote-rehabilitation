import {
  FETCH_EQUIPMENT_FAILURE,
  FETCH_EQUIPMENT_REQUEST,
  FETCH_EQUIPMENT_SUCCESS, FETCH_SINGLE_EQUIPMENT_FAILURE, FETCH_SINGLE_EQUIPMENT_SUCCESS
} from "../actionTypes/equipmentActionTypes";

export const fetchEquipmentRequest = () => {
  return {
    type: FETCH_EQUIPMENT_REQUEST
  }
};

export const fetchEquipmentSuccess = (equipment) => {
  return {
    type: FETCH_EQUIPMENT_SUCCESS,
    payload: equipment
  }
};

export const fetchEquipmentFailure = (error) => {
  return {
    type: FETCH_EQUIPMENT_FAILURE,
    payload: error
  }
};

export const fetchSingleEquipmentSuccess = (data) => {
  return {
    type: FETCH_SINGLE_EQUIPMENT_SUCCESS,
    payload: data
  }
};

export const fetchSingleEquipmentFailure = (error) => {
  return {
    type: FETCH_SINGLE_EQUIPMENT_FAILURE,
    payload: error
  }
};