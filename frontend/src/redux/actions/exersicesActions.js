import { 
    DELETE_EXERCISE_FAILURE, DELETE_EXERCISE_REQUEST, DELETE_EXERCISE_SUCCESS,
    FETCH_EXERCISES_FAILURE, FETCH_EXERCISES_REQUEST, FETCH_EXERCISES_SUCCESS, 
    FETCH_EXERCISE_FAILURE, FETCH_EXERCISE_REQUEST, FETCH_EXERCISE_SUCCESS, 
    POST_EXERCISE_FAILURE, POST_EXERCISE_REQUEST, POST_EXERCISE_SUCCESS, 
    UPDATE_EXERCISE_FAILURE, UPDATE_EXERCISE_REQUEST, UPDATE_EXERCISE_SUCCESS
} from "../actionTypes/exercisesActionTypes"

export const fetchExercisesRequest = () => {
    return {
        type: FETCH_EXERCISES_REQUEST
    }
};

export const fetchExercisesSuccess = (exercises) => {
    return {
        type: FETCH_EXERCISES_SUCCESS,
        payload: exercises
    }
};

export const fetchExercisesFailure = (error) => {
    return {
        type: FETCH_EXERCISES_FAILURE
    }
};

export const fetchExerciseRequest = () => {
    return {
        type: FETCH_EXERCISE_REQUEST
    }
};

export const fetchExerciseSuccess = (exercise) => {
    return {
        type: FETCH_EXERCISE_SUCCESS,
        payload: exercise
    }
};

export const fetchExerciseFailure = (error) => {
    return {
        type: FETCH_EXERCISE_FAILURE,
        payload: error
    }
};

export const postExerciseRequest = () => {
    return {
        type: POST_EXERCISE_REQUEST
    }
};

export const postExerciseSuccess = () => {
    return {
        type: POST_EXERCISE_SUCCESS
    }
};

export const postExerciseFailure = (error) => {
    return {
        type: POST_EXERCISE_FAILURE, error
    }
};

export const updateExerciseRequest = () => {
    return {
        type: UPDATE_EXERCISE_REQUEST
    }
};

export const updateExerciseSuccess = () => {
    return {
        type: UPDATE_EXERCISE_SUCCESS
    }
};

export const updateExerciseFailure = () => {
    return {
        type: UPDATE_EXERCISE_FAILURE
    }
};

export const deleteExerciseRequest = () => {
    return {
        type: DELETE_EXERCISE_REQUEST
    }
};

export const deleteExerciseSuccess = () => {
    return {
        type: DELETE_EXERCISE_SUCCESS
    }
};

export const deleteExerciseFailure = (error) => {
    return {
        type: DELETE_EXERCISE_FAILURE,
        payload: error
    }
};