import {
  START_FAVORITES_REQUEST,
  DELETE_FAVORITES_SUCCESS,
  DELETE_FAVORITES_FAILURE,
  CREATE_FAVORITES_SUCCESS,
  CREATE_FAVORITES_FAILURE,
  FETCH_FAVORITES_SUCCESS,
  FETCH_FAVORITES_FAILURE
} from "../actionTypes/favoritesActionTypes";

export const startFavoritesRequest = () => {
  return {
    type: START_FAVORITES_REQUEST
  };
};

export const fetchFavoritesSuccess = favorites => {
  return {
    type: FETCH_FAVORITES_SUCCESS,
    payload: favorites
  };
};

export const fetchFavoritesFailure = error => {
  return {
    type: FETCH_FAVORITES_FAILURE,
    payload: error
  };
};

export const deleteFavoritesSuccess = () => {
  return {
    type: DELETE_FAVORITES_SUCCESS,
  };
};

export const deleteFavoritesFailure = error => {
  return {
    type: DELETE_FAVORITES_FAILURE,
    payload: error
  };
};

export const createFavoritesFailure = error => {
  return {
    type: CREATE_FAVORITES_FAILURE,
    payload: error
  };
};

export const createFavoritesSuccess = favorite => {
  return {
    type: CREATE_FAVORITES_SUCCESS,
    payload: favorite
  };
};
