import { 
    FETCH_OPTIONS_REQUEST, FETCH_OPTIONS_SUCCESS, FETCH_OPTIONS_FAILURE, 
    FETCH_OPTION_REQUEST, FETCH_OPTION_SUCCESS, FETCH_OPTION_FAILURE, 
    POST_OPTION_SUCCESS, POST_OPTION_FAILURE, UPDATE_OPTION_REQUEST, 
    UPDATE_OPTION_SUCCESS, UPDATE_OPTION_FAILURE, DELETE_OPTION_REQUEST, 
    POST_OPTION_REQUEST, DELETE_OPTION_SUCCESS, DELETE_OPTION_FAILURE 
} from "../actionTypes/optionsActionsTypes";

export const fetchOptionsRequest = () => {
    return {
        type: FETCH_OPTIONS_REQUEST
    }
};

export const fetchOptionsSuccess = (options) => {
    return {
        type: FETCH_OPTIONS_SUCCESS,
        payload: options
    }
};

export const fetchOptionsFailure = (error) => {
    return {
        type: FETCH_OPTIONS_FAILURE,
        payload: error
    }
};

export const fetchSingleOptionRequest = () => {
    return {
        type: FETCH_OPTION_REQUEST
    }
};

export const fetchSingleOptionSuccess = (option) => {
    return {
        type: FETCH_OPTION_SUCCESS,
        payload: option
    }
};

export const fetchSingleOptionFailure = (error) => {
    return {
        type: FETCH_OPTION_FAILURE,
        payload: error
    }
};

export const postOptionRequest = () => {
    return {
        type: POST_OPTION_REQUEST
    }
};

export const postOptionSuccess = () => {
    return {
        type: POST_OPTION_SUCCESS
    }
};

export const postOptionFailure = (error) => {
    return {
        type: POST_OPTION_FAILURE,
        payload: error
    }
};

export const updateOptionRequest = () => {
    return {
        type: UPDATE_OPTION_REQUEST
    }
};

export const updateOptionSuccess = () => {
    return {
        type: UPDATE_OPTION_SUCCESS
    }
};

export const updateOptionFailure = (error) => {
    return {
        type: UPDATE_OPTION_FAILURE,
        payload: error
    }
};

export const deleteOptionRequest = () => {
    return {
        type: DELETE_OPTION_REQUEST
    }
};

export const deleteOptionSuccess = () => {
    return {
        type: DELETE_OPTION_SUCCESS
    }
};

export const deleteOptionFailure = (error) => {
    return {
        type: DELETE_OPTION_FAILURE,
        payload: error
    }
};
