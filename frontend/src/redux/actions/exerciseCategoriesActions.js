import {
  FETCH_EXERCISE_CATEGORIES_FAILURE,
  FETCH_EXERCISE_CATEGORIES_REQUEST,
  FETCH_EXERCISE_CATEGORIES_SUCCESS, FETCH_EXERCISE_CATEGORY
} from "../actionTypes/exerciseCategoriesActionsTypes";

export const fetchExerciseCategoriesRequest = () => {
  return {
    type: FETCH_EXERCISE_CATEGORIES_REQUEST
  }
};

export const fetchExerciseCategoriesSuccess = (data) => {
  return {
    type: FETCH_EXERCISE_CATEGORIES_SUCCESS,
    payload: data
  }
};

export const fetchExerciseCategoriesFailure = (error) => {
  return {
    type: FETCH_EXERCISE_CATEGORIES_FAILURE,
    payload: error
  }
};

export const fetchExerciseCategorySuccess = (data) => {
  return {
    type: FETCH_EXERCISE_CATEGORY,
    payload: data
  }
};

// export const fetchExerciseCategories = () => {
//   return dispatch => {
//     dispatch(fetchExerciseCategoriesRequest());
//     axios.get("/exercise_categories")
//       .then(response => {
//         dispatch(fetchExerciseCategoriesSuccess(response.data));
//       })
//       .catch(error => {
//         console.log(error);
//         dispatch(fetchExerciseCategoriesFailure(error));
//       })
//   }
// };
//
// export const fetchExerciseCategory = (id) => {
//   return dispatch => {
//     dispatch(fetchExerciseCategoriesRequest());
//     return axios.get(`/exercise_categories/${id}`)
//       .then(response => {
//         dispatch(fetchExerciseCategorySuccess(response.data));
//         return response.data;
//       })
//       .catch(error => {
//         console.log(error);
//       })
//   }
// };
//
// export const createExerciseCategory = data => {
//   return dispatch => {
//     axios.post("/exercise_categories", data)
//       .then(() => {
//         dispatch(fetchExerciseCategories());
//       })
//       .catch(error => {
//         console.log(error);
//       })
//   }
// };
//
// export const deleteExerciseCategory = id => {
//   return dispatch => {
//     axios.delete(`/exercise_categories/${id}`)
//       .then(() => {
//         dispatch(fetchExerciseCategories());
//       })
//       .catch(error => {
//         console.log(error);
//       })
//   }
// };
//
// export const updateExerciseCategory = (id, data) => {
//   return dispatch => {
//     axios.put(`/exercise_categories/${id}`, data)
//       .then(() => {
//         dispatch(fetchExerciseCategories());
//       })
//       .catch(error => {
//         console.log(error);
//       })
//   }
// };