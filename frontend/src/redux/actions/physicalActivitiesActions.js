import { 
    DELETE_ACTIVITY_FAILURE, DELETE_ACTIVITY_REQUEST, DELETE_ACTIVITY_SUCCESS, 
    FETCH_ACTIVITIES_FAILURE, FETCH_ACTIVITIES_REQUEST, FETCH_ACTIVITIES_SUCCESS, 
    FETCH_ACTIVITY_FAILURE, FETCH_ACTIVITY_REQUEST, FETCH_ACTIVITY_SUCCESS, 
    POST_ACTIVITY_FAILURE, POST_ACTIVITY_REQUEST, POST_ACTIVITY_SUCCESS, 
    UPDATE_ACTIVITY_FAILURE, UPDATE_ACTIVITY_REQUEST, UPDATE_ACTIVITY_SUCCESS 
} from "../actionTypes/physicalActivitiesActionTypes"

export const fetchActivitiesRequest = () => {
    return {
        type: FETCH_ACTIVITIES_REQUEST
    }
};

export const fetchActivitiesSuccess = (activities) => {
    return {
        type: FETCH_ACTIVITIES_SUCCESS,
        payload: activities
    }
};

export const fetchActivitiesFailure = (error) => {
    return {
        type: FETCH_ACTIVITIES_FAILURE,
        payload: error
    }
};

export const fetchActivityRequest = () => {
    return {
        type: FETCH_ACTIVITY_REQUEST
    }
};

export const fetchActivitySuccess = (activity) => {
    return {
        type: FETCH_ACTIVITY_SUCCESS,
        payload: activity
    }
};

export const fetchActivityFailure = (error) => {
    return {
        type: FETCH_ACTIVITY_FAILURE,
        payload: error
    }
};

export const postActivityRequest = () => {
    return {
        type: POST_ACTIVITY_REQUEST
    }
};

export const postActivitySuccess = () => {
    return {
        type: POST_ACTIVITY_SUCCESS
    }
};

export const postActivityFailure = (error) => {
    return {
        type: POST_ACTIVITY_FAILURE,
        payload: error
    }
};

export const updateActivityRequest = () => {
    return {
        type: UPDATE_ACTIVITY_REQUEST
    }
};

export const updateActivitySuccess = () => {
    return {
        type: UPDATE_ACTIVITY_SUCCESS
    }
};

export const updateActivityFailure = (error) => {
    return {
        type: UPDATE_ACTIVITY_FAILURE,
        payload: error
    }
};

export const deleteActivityRequest = () => {
    return {
        type: DELETE_ACTIVITY_REQUEST
    }
};

export const deleteActivitySuccess = () => {
    return {
        type: DELETE_ACTIVITY_SUCCESS
    }
};

export const deleteActivityFailure = (error) => {
    return {
        type: DELETE_ACTIVITY_FAILURE,
        payload: error
    }
};