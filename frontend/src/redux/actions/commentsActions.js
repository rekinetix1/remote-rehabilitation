
import {
    FETCH_HISTORY_FAILURE,
} from "../actionTypes/browsingHistoryActionTypes";
import {
    DELETE_COMMENT_SUCCESS,
    FETCH_COMMENTS_REQUEST,
    FETCH_COMMENTS_SUCCESS
} from "../actionTypes/commentsAcrtionTypes";
import {DELETE_EXERCISE_SUCCESS} from "../actionTypes/exercisesActionTypes";

export const fetchCommentsRequest = () => {
    return {
        type: FETCH_COMMENTS_REQUEST
    }
};

export const fetchCommentsSuccess = (history) => {
    return {
        type: FETCH_COMMENTS_SUCCESS,
        payload: history
    }
};

export const fetchCommentsFailure = (error) => {
    return {
        type: FETCH_HISTORY_FAILURE,
        payload: error
    }
};

export const deleteCommentSuccess = () => {
    return {
        type: DELETE_COMMENT_SUCCESS
    }
};