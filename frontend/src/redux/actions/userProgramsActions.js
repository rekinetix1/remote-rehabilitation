import { 
    POST_USER_PROGRAM_REQUEST, POST_USER_PROGRAM_SUCCESS, POST_USER_PROGRAM_FAILURE, 
    FETCH_USER_PROGRAMS_REQUEST, FETCH_USER_PROGRAMS_SUCCESS, FETCH_USER_PROGRAMS_FAILURE, 
    UPDATE_USER_PROGRAM_REQUEST, UPDATE_USER_PROGRAM_SUCCESS, UPDATE_USER_PROGRAM_FAILURE, 
    DELETE_USER_PROGRAM_SUCCESS, DELETE_USER_PROGRAM_REQUEST, DELETE_USER_PROGRAM_FAILURE,
    FETCH_CURRENT_USER_PROGRAM_REQUEST, FETCH_CURRENT_USER_PROGRAM_SUCCESS, FETCH_CURRENT_USER_PROGRAM_FAILURE 
} from "../actionTypes/userProgramsActionsTpes";
import axios from "../../axios-api";

export const postUserProgramRequest = () => {
    return {
        type: POST_USER_PROGRAM_REQUEST
    }
};

export const postUserProgramSuccess = (data) => {
    return {
        type: POST_USER_PROGRAM_SUCCESS,
        payload: data
    }
};

export const postUserProgramFailure = (error) => {
    return {
        type: POST_USER_PROGRAM_FAILURE,
        payload: error
    }
};

export const fetchUserProgramsRequest = () => {
    return {
        type: FETCH_USER_PROGRAMS_REQUEST
    }
};

export const fetchUserProgramsSuccess = (data) => {
    return {
        type: FETCH_USER_PROGRAMS_SUCCESS,
        payload: data
    }
};

export const fetchUserProgramsFailure = (error) => {
    return {
        type: FETCH_USER_PROGRAMS_FAILURE,
        payload: error
    }
};

export const fetchCurrentUserProgramRequest = () => {
    return {
        type: FETCH_CURRENT_USER_PROGRAM_REQUEST
    }
};

export const fetchCurrentUserProgramSuccess = (userProgram) => {
    return {
        type: FETCH_CURRENT_USER_PROGRAM_SUCCESS,
        payload: userProgram
    }
};

export const fetchCurrentUserProgramFailure = (error) => {
    return {
        type: FETCH_CURRENT_USER_PROGRAM_FAILURE,
        payload: error
    }
};

export const updateUserProgramRequest = () => {
    return {
        type: UPDATE_USER_PROGRAM_REQUEST
    }
};

export const updateUserProgramSuccess = (data) => {
    return {
        type: UPDATE_USER_PROGRAM_SUCCESS,
        payload: data
    }
};

export const updateUserProgramFailure = (error) => {
    return {
        type: UPDATE_USER_PROGRAM_FAILURE,
        payload: error
    }
};

export const deleteUserProgramRequest = () => {
    return {
        type: DELETE_USER_PROGRAM_REQUEST
    }
};

export const deleteUserProgramSuccess = () => {
    return {
        type: DELETE_USER_PROGRAM_SUCCESS
    }
};

export const deleteUserProgramFailure = (error) => {
    return {
        type: DELETE_USER_PROGRAM_FAILURE,
        payload: error
    }
};

export const postUserProgram = (data) => {
    return dispatch => {
        dispatch(postUserProgramRequest());
        axios.post("/user_programs", data)
        .then(() => {
            dispatch(postUserProgramSuccess());
        })
        .catch(error => {
            dispatch(postUserProgramFailure(error));
        });
    } 
};
