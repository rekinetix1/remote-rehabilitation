import {
    FETCH_PROGRAM_CATEGORIES_FAILURE, FETCH_PROGRAM_CATEGORIES_REQUEST,
    FETCH_PROGRAM_CATEGORIES_SUCCESS,
    FETCH_PROGRAM_CATEGORY
} from "../actionTypes/programCategoriesActionsTypes";
import axios from "../../axios-api";
import {push} from "connected-react-router";
import {NotificationManager} from "react-notifications";
import {fetchExerciseCategoriesFailure} from "./exerciseCategoriesActions";

const fetchProgramCategoriesRequest = () => {
    return {
        type: FETCH_PROGRAM_CATEGORIES_REQUEST
    }
};

const fetchProgramCategoriesSuccess = (data) => {
    return {
        type: FETCH_PROGRAM_CATEGORIES_SUCCESS,
        payload: data
    }
};

const fetchProgramCategoriesFailure = (error) => {
    return {
        type: FETCH_PROGRAM_CATEGORIES_FAILURE,
        payload: error
    }
};

const fetchProgramCategorySuccess = (data) => {
    return {
        type: FETCH_PROGRAM_CATEGORY,
        payload: data
    }
};

export const fetchProgramCategories = () => {
    return dispatch => {
        dispatch(fetchProgramCategoriesRequest());
        axios.get("/program_categories")
            .then(response => {
                dispatch(fetchProgramCategoriesSuccess(response.data));
            })
            .catch(error => {
                dispatch(fetchProgramCategoriesFailure(error));
            })
    }
};

export const fetchProgramCategory = (id) => {
    return dispatch => {
        dispatch(fetchProgramCategoriesRequest());
        return axios.get(`/program_categories/${id}`)
            .then(response => {
                dispatch(fetchProgramCategorySuccess(response.data));
                return response.data;
            })
            .catch(error => {
                console.log(error);
            })
    }
};

export const createProgramCategory = data => {
    return dispatch => {
        axios.post("/program_categories", data)
            .then(() => {
                dispatch(fetchProgramCategories());
                dispatch(push("/exercise-categories"));
                NotificationManager.success("Успешно создано");
            })
            .catch(error => {
                    if (error.response && error.response.data) {
                        dispatch(fetchProgramCategoriesFailure(error.response.data));
                    }
                }
            )
    }
};

export const deleteProgramCategory = id => {
    return dispatch => {
        axios.delete(`/program_categories/${id}`)
            .then(() => {
                dispatch(fetchProgramCategories());
            })
            .catch(error => {
                console.log(error);
            })
    }
};

export const updateProgramCategory = (id, data) => {
    return dispatch => {
        axios.put(`/program_categories/${id}`, data)
            .then(() => {
                dispatch(fetchProgramCategories());
            })
            .catch(error => {
                console.log(error);
            })
    }
};