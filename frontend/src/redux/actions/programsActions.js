import {
    FETCH_PROGRAMS_FAILURE,
    FETCH_PROGRAMS_REQUEST,
    FETCH_PROGRAMS_SUCCESS,
    FETCH_USER_PROGRAMS_SUCCESS,
    START_PROGRAMS_REQUEST,
    DELETE_PROGRAM_SUCCESS,
    DELETE_PROGRAM_FAILURE,
    CREATE_PROGRAM_SUCCESS,
    CREATE_PROGRAM_FAILURE,
    FETCH_SINGLE_PROGRAM_SUCCESS,
    FETCH_SINGLE_PROGRAM_FAILURE,
    UPDATE_PROGRAM_SUCCESS,
    UPDATE_PROGRAM_FAILURE
} from "../actionTypes/programsActionsTypes";
import axios from "../../axios-api";

const fetchProgramsRequest = () => {
    return {
        type: FETCH_PROGRAMS_REQUEST
    };
};

export const fetchPrograms = () => {
    return dispatch => {
        dispatch(fetchProgramsRequest());
        axios.get("/programs")
            .then(response => {
                dispatch(fetchProgramsSuccess(response.data));
            }).catch(error => {
                dispatch(fetchProgramsFailure(error));
            });
    };
};


const fetchUserPrograms = (programs) => {
    return {
        type: FETCH_USER_PROGRAMS_SUCCESS,
        programs
    };
}

export const getUserPrograms = (id) => {
    return dispatch => {
        axios.get(`/user_programs/?user=${id}`)
            .then((res) => {
                if (res.data) {
                    dispatch(fetchUserPrograms(res.data))
                }
            })
            .catch(error => {
                console.log(error);
            })
    }
}

export const startProgramsRequest = () => {
  return {
    type: START_PROGRAMS_REQUEST
  };
};

export const fetchProgramsSuccess = programs => {
  return {
      type: FETCH_PROGRAMS_SUCCESS,
      programs: programs
  };
};

export const fetchProgramsFailure = error => {
  return {
      type: FETCH_PROGRAMS_FAILURE,
      error
  };
};

export const deleteProgramSuccess = programs => {
  return {
      type: DELETE_PROGRAM_SUCCESS,
  };
};

export const fetchSingleProgramSuccess = (program) => {
  return {
    type: FETCH_SINGLE_PROGRAM_SUCCESS,
    payload: program
  }
};

export const fetchSingleProgramFailure = (program) => {
  return {
      type: FETCH_SINGLE_PROGRAM_FAILURE,
      payload: program
  }
};

export const deleteProgramFailure = error => {
  return {
    type: DELETE_PROGRAM_FAILURE,
    payload: error
  };
};

export const createProgramSuccess = programs => {
  return {
    type: CREATE_PROGRAM_SUCCESS,
    programs
  };
};

export const createProgramFailure = error => {
  return {
    type: CREATE_PROGRAM_FAILURE,
    payload: error
  };
};

export const updateProgramSuccess = programs => {
  return {
    type: UPDATE_PROGRAM_SUCCESS,
    programs
  };
};

export const updateProgramFailure = error => {
  return {
    type: UPDATE_PROGRAM_FAILURE,
    payload: error
  };
};
