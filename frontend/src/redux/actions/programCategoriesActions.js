
import { 
    FETCH_PROGRAM_CATEGORIES_FAILURE, FETCH_PROGRAM_CATEGORIES_REQUEST, 
    FETCH_PROGRAM_CATEGORIES_SUCCESS, 
    FETCH_PROGRAM_CATEGORY
} from "../actionTypes/programCategoriesActionsTypes";
  
  export const fetchProgramCategoriesRequest = () => {
    return {
      type: FETCH_PROGRAM_CATEGORIES_REQUEST
    }
  };
  
  export const fetchProgramCategoriesSuccess = (data) => {
    return {
      type: FETCH_PROGRAM_CATEGORIES_SUCCESS,
      payload: data
    }
  };
  
  export const fetchProgramCategoriesFailure = (error) => {
    return {
      type: FETCH_PROGRAM_CATEGORIES_FAILURE,
      payload: error
    }
  }; 
  
  export const fetchProgramCategorySuccess = (data) => {
    return {
      type: FETCH_PROGRAM_CATEGORY,
      payload: data
    }
  };
  
  