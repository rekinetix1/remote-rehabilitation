import { 
    FETCH_PAINS_REQUEST, FETCH_PAINS_SUCCESS, FETCH_PAINS_FAILURE, 
    FETCH_SINGLE_PAIN_REQUEST, FETCH_SINGLE_PAIN_SUCCESS, FETCH_SINGLE_PAIN_FAILURE, 
    POST_PAIN_REQUEST, POST_PAIN_SUCCESS, POST_PAIN_FAILURE, 
    UPDATE_PAIN_REQUEST, UPDATE_PAIN_FAILURE, UPDATE_PAIN_SUCCESS,
    DELETE_PAIN_REQUEST, DELETE_PAIN_SUCCESS, DELETE_PAIN_FAILURE 
} from "../actionTypes/painsActionTypes"

export const fetchPainsRequest = () => { 
    return {
        type: FETCH_PAINS_REQUEST
    }
};

export const fetchPainsSuccess = (pains) => {
    return {
        type: FETCH_PAINS_SUCCESS,
        payload: pains  
    }
};

export const fetchPainsFailure = (error) => {
    return {
        type: FETCH_PAINS_FAILURE,
        payload: error  
    }
};

export const fetchSinglePainRequest = () => {
    return {
        type: FETCH_SINGLE_PAIN_REQUEST
    }
};

export const fetchSinglePainSuccess = (pain) => {
    return {
        type: FETCH_SINGLE_PAIN_SUCCESS,
        payload: pain 
    }
};

export const fetchSinglePainFailure = (error) => {
    return {
        type: FETCH_SINGLE_PAIN_FAILURE,
        payload: error  
    }
};

export const postPainRequest = () => {
    return {
        type: POST_PAIN_REQUEST
    }
};

export const postPainSuccess = () => {
    return {
        type: POST_PAIN_SUCCESS
    }
};

export const postPainFailure = (error) => {
    return {
        type: POST_PAIN_FAILURE,
        payload: error  
    }
};

export const updatePainRequest = () => {
    return {
        type: UPDATE_PAIN_REQUEST
    }
};

export const updatePainSuccess = () => {
    return {
        type: UPDATE_PAIN_SUCCESS
    }
};

export const updatePainFailure = (error) => {
    return {
        type: UPDATE_PAIN_FAILURE,
        payload: error  
    }
};

export const deletePainRequest = () => {
    return {
        type: DELETE_PAIN_REQUEST
    }
};

export const deletePainSuccess = () => {
    return {
        type: DELETE_PAIN_SUCCESS
    }
};

export const deletePainFailure = (error) => {
    return {
        type: DELETE_PAIN_FAILURE,
        payload: error  
    }
};