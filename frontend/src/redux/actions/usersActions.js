import {
  LOGIN_USER_REQUEST,
  LOGIN_USER_FAILURE,
  LOGIN_USER_SUCCESS,
  REGISTER_USER_FAILURE,
  REGISTER_USER_SUCCESS,
  REGISTER_USER_REQUEST,
  LOGOUT_USER_REQUEST,
  LOGOUT_USER_SUCCESS,
  LOGOUT_USER_FAILURE,
  FETCH_USERS_REQUEST,
  FETCH_USERS_SUCCESS,
  FETCH_USERS_FAILURE,
  FETCH_USER_REQUEST,
  FETCH_USER_SUCCESS,
  FETCH_USER_FAILURE,
  UPDATE_USER_REQUEST,
  UPDATE_USER_SUCCESS,
  UPDATE_USER_FAILURE,
  DELETE_USER_REQUEST,
  DELETE_USER_SUCCESS,
  DELETE_USER_FAILURE,
  UPDATE_USER_PROFILE_SUCCESS,
  VERIFY_USER_REQUEST,
  VERIFY_USER_SUCCESS,
  VERIFY_USER_FAILURE,
  CHECK_USER_IN_BASE_REQUEST,
  CHECK_USER_IN_BASE_SUCCESS,
  CHECK_USER_IN_BASE_FAILURE,
  RESET_USER_PASSWORD_REQUEST,
  RESET_USER_PASSWORD_SUCCESS,
  RESET_USER_PASSWORD_FAILURE
} from "../actionTypes/usersActionsTypes";

export const loginUserRequest = () => {
  return {
    type: LOGIN_USER_REQUEST
  } 
};

export const loginUserSuccess = user => {
  return {
    type: LOGIN_USER_SUCCESS, 
    user
  };
};

export const loginUserFailure = error => {
  return {
    type: LOGIN_USER_FAILURE,
    error
  };
};

export const logoutUserRequest = () => {
  return {type: LOGOUT_USER_REQUEST}
};

export const logoutUserSuccess = () => {
  return {type: LOGOUT_USER_SUCCESS}
};

export const logoutUserError = error => {
  return {type: LOGOUT_USER_FAILURE, error}
};

export const registerUserRequest = () => {
  return {type: REGISTER_USER_REQUEST}
};
export const registerUserSuccess = () => {
  return {type: REGISTER_USER_SUCCESS};
};
export const registerUserFailure = error => {
  return {type: REGISTER_USER_FAILURE, error};
};

export const fetchUsersRequest = () => {
  return {
    type: FETCH_USERS_REQUEST
  }
};

export const fetchUsersSuccess = (users) => {
  return {
    type: FETCH_USERS_SUCCESS,
    users
  }
};

export const fetchUsersFailure = (error) => {
  return {
    type: FETCH_USERS_FAILURE,
    error
  }
};

export const fetchUserRequest = () => {
  return {
    type: FETCH_USER_REQUEST
  }
};

export const fetchUserSuccess = (currentUser) => {
  return {
    type: FETCH_USER_SUCCESS,
    currentUser
  }
};

export const fetchUserFailure = (error) => {
  return {
    type: FETCH_USER_FAILURE,
    error
  }
};

export const updateUserRequest = () => {
  return {
    type: UPDATE_USER_REQUEST
  }
};

export const updateUserSuccess = () => {
  return {
    type: UPDATE_USER_SUCCESS
  }
};

export const updateUserFailure = (error) => {
  return {
    type: UPDATE_USER_FAILURE,
    error
  }
};

export const deleteUserRequest = () => {
  return {
    type: DELETE_USER_REQUEST
  }
};

export const deleteUserSuccess = () => {
  return {
    type: DELETE_USER_SUCCESS
  }
};

export const deleteUserFailure = (error) => {
  return {
    type: DELETE_USER_FAILURE,
    error
  }
};

export const updateUserProfileSuccess = (user) => {
  return {
    type: UPDATE_USER_PROFILE_SUCCESS, user
  }
};

export const fetchVerifyRequest = () => {
  return {
    type: VERIFY_USER_REQUEST
  }
};

export const fetchVerifySuccess = () => {
  return {
    type: VERIFY_USER_SUCCESS
  }
};

export const fetchVerifyFailure = (error) => {
  return {
    type: VERIFY_USER_FAILURE,
    error
  }
};



export const fetchCheckUserRequest = () => {
  return {
    type: CHECK_USER_IN_BASE_REQUEST
  }
};
export const fetchCheckUserSuccess = (data) => {
  return {
    type:CHECK_USER_IN_BASE_SUCCESS,
    data
  }
};

export const fetchCheckUserFailure = (error) => {
  return {
    type: CHECK_USER_IN_BASE_FAILURE,
    error
  }
};

export const fetchResetPasswordRequest = () => {
  return {
    type: RESET_USER_PASSWORD_REQUEST
  }
};
export const fetchResetPasswordSuccess = () => {

  return {
    type:RESET_USER_PASSWORD_SUCCESS,
  }
};

export const fetchResetPasswordFailure = (error) => {
  return {
    type: RESET_USER_PASSWORD_FAILURE,
    error
  }
};
