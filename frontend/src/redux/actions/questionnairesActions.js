import {
    FETCH_QUESTIONNAIRES_REQUEST, FETCH_QUESTIONNAIRES_FAILURE, FETCH_QUESTIONNAIRES_SUCCESS,
    FETCH_USER_QUESTIONNAIRE_SUCCESS, FETCH_USER_QUESTIONNAIRE_FAILURE, UPDATE_QUESTIONNAIRE_SUCCESS,
    UPDATE_QUESTIONNAIRE_FAILURE, CREATE_QUESTIONNAIRE_SUCCESS, CREATE_QUESTIONNAIRE_FAILURE
} from "../actionTypes/questionnairesActionsTypes";



export const fetchQuestionnairesRequest = () => {
    return {
        type: FETCH_QUESTIONNAIRES_REQUEST
    }
};

export const fetchQuestionnairesSuccess = (data) => {
    return {
        type: FETCH_QUESTIONNAIRES_SUCCESS,
        payload: data
    }
};

export const fetchQuestionnairesFailure = (error) => {
    return {
        type: FETCH_QUESTIONNAIRES_FAILURE,
        payload: error
    }
};

export const fetchUserQuestionnaireSuccess = (questionnaire) => {
    return {
        type: FETCH_USER_QUESTIONNAIRE_SUCCESS,
        payload: questionnaire
    }
};

export const fetchUserQuestionnaireFailure = (error) => {
    return {
        type: FETCH_USER_QUESTIONNAIRE_FAILURE,
        payload: error
    }
};

export const updateUserQuestionnaireSuccess = (questionnaire) => {
    return {
        type: UPDATE_QUESTIONNAIRE_SUCCESS,
        payload: questionnaire
    }
};

export const updateUserQuestionnaireFailure = (error) => {
    return {
        type: UPDATE_QUESTIONNAIRE_FAILURE,
        payload: error
    }
};

export const createQuestionnaireSuccess = (data) => {
    return {
        type: CREATE_QUESTIONNAIRE_SUCCESS,
        payload: data
    }
};

export const createQuestionnaireFailure = (error) => {
    return {
        type: CREATE_QUESTIONNAIRE_FAILURE,
        payload: error
    }
};


