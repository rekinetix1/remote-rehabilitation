
import {
    FETCH_HISTORY_FAILURE,
    FETCH_HISTORY_REQUEST,
    FETCH_HISTORY_SUCCESS
} from "../actionTypes/browsingHistoryActionTypes";

export const fetchHistoryRequest = () => {
    return {
        type: FETCH_HISTORY_REQUEST
    }
};

export const fetchHistorySuccess = (history) => {
    return {
        type: FETCH_HISTORY_SUCCESS,
        payload: history
    }
};

export const fetchHistoryFailure = (error) => {
    return {
        type: FETCH_HISTORY_FAILURE,
        payload: error
    }
};
