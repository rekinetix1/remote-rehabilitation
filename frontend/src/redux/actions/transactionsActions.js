import { 
    FETCH_TRANSACTIONS_REQUEST, FETCH_TRANSACTIONS_FAILURE, FETCH_TRANSACTIONS_SUCCESS, 
    FETCH_SINGLE_TRANSACTION_REQUEST, FETCH_SINGLE_TRANSACTION_SUCCESS, 
    FETCH_SINGLE_TRANSACTION_FAILURE, UPDATE_SINGLE_TRANSACTION_REQUEST, 
    UPDATE_SINGLE_TRANSACTION_SUCCESS, UPDATE_SINGLE_TRANSACTION_FAILURE, 
    POST_SINGLE_TRANSACTION_REQUEST, POST_SINGLE_TRANSACTION_SUCCESS, POST_SINGLE_TRANSACTION_FAILURE 
} from "../actionTypes/transactionsActionTypes";

export const fetchTransactionsRequest = () => {
    return {
        type: FETCH_TRANSACTIONS_REQUEST
    }
};

export const fetchTransactionsSuccess = (transactions) => {
    return {
        type: FETCH_TRANSACTIONS_SUCCESS,
        payload: transactions
    }
};

export const fetchTransactionsFailure = (error) => {
    return {
        type: FETCH_TRANSACTIONS_FAILURE,
        payload: error
    }
};

export const postSingleTransactionRequest = () => {
    return {
        type: POST_SINGLE_TRANSACTION_REQUEST
    }
};

export const postSingleTransactionSuccess = (data) => {
    return {
        type: POST_SINGLE_TRANSACTION_SUCCESS,
        payload: data
    }
};

export const postSingleTransactionFailure = (error) => {
    return {
        type: POST_SINGLE_TRANSACTION_FAILURE,
        payload: error
    }
}

export const fetchSingleTransactionRequest = () => {
    return {
        type: FETCH_SINGLE_TRANSACTION_REQUEST
    }
};

export const fetchSingleTransactionSuccess = (transaction) => {
    return {
        type: FETCH_SINGLE_TRANSACTION_SUCCESS,
        payload: transaction
    }
};

export const fetchSingleTransactionFailure = (error) => {
    return {
        type: FETCH_SINGLE_TRANSACTION_FAILURE,
        payload: error
    }
};

export const updateSingleTransactionRequest = () => {
    return {
        type: UPDATE_SINGLE_TRANSACTION_REQUEST
    }
};

export const updateSingleTransactionSuccess = () => {
    return {
        type: UPDATE_SINGLE_TRANSACTION_SUCCESS
    }
};

export const updateSingleTransactionFailure = () => {
    return {
        type: UPDATE_SINGLE_TRANSACTION_FAILURE
    }
};