import { 
    DELETE_OCCUPATION_FAILURE, DELETE_OCCUPATION_REQUEST, DELETE_OCCUPATION_SUCCESS, 
    FETCH_OCCUPATION_FAILURE, FETCH_OCCUPATION_REQUEST, FETCH_OCCUPATION_SUCCESS, 
    FETCH_SINGLE_OCCUPATION_FAILURE, FETCH_SINGLE_OCCUPATION_REQUEST, FETCH_SINGLE_OCCUPATION_SUCCESS, 
    POST_OCCUPATION_FAILURE, POST_OCCUPATION_REQUEST, POST_OCCUPATION_SUCCESS, 
    UPDATE_OCCUPATION_FAILURE, UPDATE_OCCUPATION_REQUEST, UPDATE_OCCUPATION_SUCCESS 
} from "../actionTypes/occupationActionTypes"

export const fetchOccupationRequest = () => {
    return { 
        type: FETCH_OCCUPATION_REQUEST
    }
};

export const fetchOccupationSuccess = (occupation) => {
    return {
        type: FETCH_OCCUPATION_SUCCESS,
        payload: occupation
    }
};

export const fetchOccupationFailure = (error) => {
    return {
        type: FETCH_OCCUPATION_FAILURE,
        payload: error
    }
};

export const fetchSingleOccupationRequest = () => {
    return {
        type: FETCH_SINGLE_OCCUPATION_REQUEST
    }
};

export const fetchSingleOccupationSuccess = (occupation) => {
    return {
        type: FETCH_SINGLE_OCCUPATION_SUCCESS,
        payload: occupation
    }
};

export const fetchSingleOccupationFailure = (error) => {
    return {
        type: FETCH_SINGLE_OCCUPATION_FAILURE,
        payload: error
    }
};

export const postOccupationRequest = () => {
    return {
        type: POST_OCCUPATION_REQUEST
    }
};

export const postOccupationSuccess = () => {
    return {
        type: POST_OCCUPATION_SUCCESS
    }
};

export const postOccupationFailure = (error) => {
    return {
        type: POST_OCCUPATION_FAILURE,
        payload: error
    }
};

export const updateOccupationRequest = () => {
    return {
        type: UPDATE_OCCUPATION_REQUEST
    }
};

export const updateOccupationSuccess = () => {
    return {
        type: UPDATE_OCCUPATION_SUCCESS
    }
};

export const updateOccupationFailure = (error) => {
    return {
        type: UPDATE_OCCUPATION_FAILURE,
        payload: error
    }
};

export const deleteOccupationRequest = () => {
    return {
        type: DELETE_OCCUPATION_REQUEST
    }
};

export const deleteOccupationSuccess = () => {
    return {
        type: DELETE_OCCUPATION_SUCCESS
    }
};

export const deleteOccupationFailure = (error) => {
    return {
        type: DELETE_OCCUPATION_FAILURE,
        payload: error
    }
};