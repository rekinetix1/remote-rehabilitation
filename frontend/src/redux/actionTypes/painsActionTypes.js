export const FETCH_PAINS_REQUEST = "FETCH_PAINS_REQUEST";
export const FETCH_PAINS_SUCCESS = "FETCH_PAINS_SUCCESS";
export const FETCH_PAINS_FAILURE = "FETCH_PAINS_FAILURE";

export const FETCH_SINGLE_PAIN_REQUEST = "FETCH_SINGLE_PAIN_REQUEST";
export const FETCH_SINGLE_PAIN_SUCCESS = "FETCH_SINGLE_PAIN_SUCCESS";
export const FETCH_SINGLE_PAIN_FAILURE = "FETCH_SINGLE_PAIN_FAILURE";

export const POST_PAIN_REQUEST = "POST_PAIN_REQUEST";
export const POST_PAIN_SUCCESS = "POST_PAIN_SUCCESS";
export const POST_PAIN_FAILURE = "POST_PAIN_FAILURE";
 
export const UPDATE_PAIN_REQUEST = "UPDATE_PAIN_REQUEST";
export const UPDATE_PAIN_SUCCESS = "UPDATE_PAIN_SUCCESS";
export const UPDATE_PAIN_FAILURE = "UPDATE_PAIN_FAILURE";

export const DELETE_PAIN_REQUEST = "DELETE_PAIN_REQUEST";
export const DELETE_PAIN_SUCCESS = "DELETE_PAIN_SUCCESS";
export const DELETE_PAIN_FAILURE = "DELETE_PAIN_FAILURE";
