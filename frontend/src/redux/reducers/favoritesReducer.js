import {
  START_FAVORITES_REQUEST,
  DELETE_FAVORITES_SUCCESS,
  DELETE_FAVORITES_FAILURE,
  CREATE_FAVORITES_SUCCESS,
  CREATE_FAVORITES_FAILURE,
  FETCH_FAVORITES_SUCCESS,
  FETCH_FAVORITES_FAILURE
} from "../actionTypes/favoritesActionTypes";

const initialState = {
    favorites: [],
    loading: false,
    error: null
};

const favoritesReducer = (state = initialState, action) => {
  switch(action.type){
  case START_FAVORITES_REQUEST:
    return {
      ...state,
      loading: true,
      error: null
    };
  case FETCH_FAVORITES_SUCCESS:
    return {
      ...state,
      loading: false,
      favorites: action.payload
    };
  case FETCH_FAVORITES_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload
      };
  case DELETE_FAVORITES_SUCCESS:
    return {
      ...state,
      loading: false,
    };
  case DELETE_FAVORITES_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload
      };
  case CREATE_FAVORITES_SUCCESS:
      return {
        ...state,
        loading: false
      };
  case CREATE_FAVORITES_FAILURE:
    return {
      ...state,
      loading: false,
      error: action.payload
    };
  default:
    return state;
  }
};

export default favoritesReducer;