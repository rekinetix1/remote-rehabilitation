const {
    START_PROGRAMS_REQUEST,
    // FETCH_USER_PROGRAMS_SUCCESS,
    DELETE_PROGRAM_SUCCESS,
    DELETE_PROGRAM_FAILURE,
    CREATE_PROGRAM_SUCCESS,
    CREATE_PROGRAM_FAILURE,
    UPDATE_PROGRAM_SUCCESS,
    UPDATE_PROGRAM_FAILURE,
    FETCH_SINGLE_PROGRAM_SUCCESS,
    FETCH_SINGLE_PROGRAM_FAILURE,
    FETCH_PROGRAMS_REQUEST, FETCH_PROGRAMS_SUCCESS, FETCH_PROGRAMS_FAILURE
} = require("../actionTypes/programsActionsTypes");


const initialState = {
    programs: [],
    loading: false,
    error: null,
    currentProgram: {},
};


const programsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PROGRAMS_REQUEST:
            return {
                ...state,
                loading: true
            };
        case FETCH_PROGRAMS_SUCCESS:
            return {
                ...state,
                programs: action.programs,
                loading: false
            };
        case FETCH_PROGRAMS_FAILURE:
            return {
                ...state,
                error: action.error,
                loading: false
            };
        case START_PROGRAMS_REQUEST:
            return {
                ...state,
                loading: true
            };
        case DELETE_PROGRAM_SUCCESS:
            return {
                ...state,
                loading: false
            };
        case DELETE_PROGRAM_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        case CREATE_PROGRAM_SUCCESS:
            return {
                ...state,
                loading: false
            };
        case CREATE_PROGRAM_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            }
        case UPDATE_PROGRAM_SUCCESS:
            return {
                ...state,
                loading: false
            };
        case UPDATE_PROGRAM_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            }
        case FETCH_SINGLE_PROGRAM_SUCCESS:
            return {
                ...state,
                loading: false,
                currentProgram: action.payload,
            };
        case FETCH_SINGLE_PROGRAM_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload,
            };
        default:
            return {...state};
    }
}


export default programsReducer