import {
  FETCH_EXERCISE_CATEGORIES_FAILURE,
  FETCH_EXERCISE_CATEGORIES_REQUEST,
  FETCH_EXERCISE_CATEGORIES_SUCCESS, FETCH_EXERCISE_CATEGORY
} from "../actionTypes/exerciseCategoriesActionsTypes";

const initialState = {
  exerciseCategories: [],
  currentCategory: {},
  loading: false,
  error: null
};

const exerciseCategoriesReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_EXERCISE_CATEGORIES_REQUEST:
      return {
        ...state,
        loading: true,
        exerciseCategories: [],
        error: null
      };

    case FETCH_EXERCISE_CATEGORIES_SUCCESS:
      return {
        ...state,
        exerciseCategories: action.payload,
        currentCategory: {},
        loading: false,
        error: null
      };

    case FETCH_EXERCISE_CATEGORIES_FAILURE:
      return {
        ...state,
        exerciseCategories: [],
        currentCategory: {},
        loading: false,
        error: action.payload
      };

    case FETCH_EXERCISE_CATEGORY:
      return {
        ...state,
        loading: false,
        currentCategory: action.payload
      };

    default:
      return state;
  }
};

export default exerciseCategoriesReducer;