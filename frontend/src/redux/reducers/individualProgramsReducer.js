const { 
    FETCH_INDIVIDUAL_PROGRAMS_REQUEST, FETCH_INDIVIDUAL_PROGRAMS_SUCCESS, 
    FETCH_INDIVIDUAL_PROGRAMS_FAILURE, CREATE_INDIVIDUAL_PROGRAM_REQUEST,
    FETCH_INDIVIDUAL_PROGRAM_REQUEST, FETCH_INDIVIDUAL_PROGRAM_SUCCESS, 
    FETCH_INDIVIDUAL_PROGRAM_FAILURE, CREATE_INDIVIDUAL_PROGRAM_SUCCESS, 
    CREATE_INDIVIDUAL_PROGRAM_FAILURE, UPDATE_INDIVIDUAL_PROGRAM_REQUEST, 
    UPDATE_INDIVIDUAL_PROGRAM_SUCCESS, UPDATE_INDIVIDUAL_PROGRAM_FAILURE, 
    DELETE_INDIVIDUAL_PROGRAM_FAILURE, DELETE_INDIVIDUAL_PROGRAM_SUCCESS,
    DELETE_INDIVIDUAL_PROGRAM_REQUEST 
} = require("../actionTypes/individualProgramsTypes");

const initialState = {
    individualPrograms: [],
    currentIndividualProgram: {},
    loading: false,
    error: null
};

const IndividualProgramsReducer = (state = initialState, action) => {
    switch(action.type){
        case FETCH_INDIVIDUAL_PROGRAMS_REQUEST:
            return {
                ...state,
                loading: true,
                individualPrograms: [],
                currentIndividualProgram: {}
            };
        case FETCH_INDIVIDUAL_PROGRAMS_SUCCESS:
            return {
                ...state,
                loading: false,
                individualPrograms: action.payload
            };
        case FETCH_INDIVIDUAL_PROGRAMS_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        case FETCH_INDIVIDUAL_PROGRAM_REQUEST:
            return {
                ...state,
                loading: true
            };
        case FETCH_INDIVIDUAL_PROGRAM_SUCCESS:
            return {
                ...state,
                loading: false,
                currentIndividualProgram: action.payload
            };
        case FETCH_INDIVIDUAL_PROGRAM_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        case CREATE_INDIVIDUAL_PROGRAM_REQUEST:
            return {
                ...state,
                loading: true
            };
        case CREATE_INDIVIDUAL_PROGRAM_SUCCESS:
            return {
                ...state,
                loading: false
            };
        case CREATE_INDIVIDUAL_PROGRAM_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        case UPDATE_INDIVIDUAL_PROGRAM_REQUEST:
            return {
                ...state,
                loading: true
            };
        case UPDATE_INDIVIDUAL_PROGRAM_SUCCESS:
            return {
                ...state,
                loading: false
            };
        case UPDATE_INDIVIDUAL_PROGRAM_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        case DELETE_INDIVIDUAL_PROGRAM_REQUEST:
            return {
                ...state,
                loading: true
            };
        case DELETE_INDIVIDUAL_PROGRAM_SUCCESS:
            return {
                ...state,
                loading: false
            };
        case DELETE_INDIVIDUAL_PROGRAM_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        default:
            return {...state}
    }
};

export default IndividualProgramsReducer;
