import {
    FETCH_MUSCLE_REQUEST,
    FETCH_MUSCLES_SUCCESS, FETCH_MUSCLES_FAILURE,
    FETCH_MUSCLE_SUCCESS, FETCH_MUSCLE_FAILURE
} from "../actionTypes/musclesActionTypes";

const initialState = {
    muscles: [],
    muscle: {},
    loading: false,
    error: null
};

const musclesReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_MUSCLE_REQUEST:
            return { ...state, loading: true };
        case FETCH_MUSCLES_SUCCESS:
            return { ...state, loading: false, error: null, muscles: action.payload };
        case FETCH_MUSCLES_FAILURE:
            return { ...state, loading: false, error: action.payload };
        case FETCH_MUSCLE_SUCCESS:
            return { ...state, loading: false, error: null, muscle: action.payload };
        case FETCH_MUSCLE_FAILURE:
            return { ...state, loading: false, error: action.payload, }
        default:
            return state;
    }
};

export default musclesReducer;