const { FETCH_PROGRAM_CATEGORIES_REQUEST, FETCH_PROGRAM_CATEGORIES_SUCCESS, FETCH_PROGRAM_CATEGORIES_FAILURE, FETCH_PROGRAM_CATEGORY } = require("../actionTypes/programCategoriesActionsTypes");

const initialState = {
    programCategories: [],
    currentCategory: {},
    loading: false,
    error: null
};

const programCategoriesReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PROGRAM_CATEGORIES_REQUEST:
            return {
                ...state,
                loading: true,
                programCategories: [],
                error: null
            };
        case FETCH_PROGRAM_CATEGORIES_SUCCESS:
            return {
                ...state,
                programCategories: action.payload,
                currentCategory: {},
                loading: false,
                error: null
            };
        case FETCH_PROGRAM_CATEGORIES_FAILURE:
            return {
                ...state,
                programCategories: [],
                currentCategory: {},
                loading: false,
                error: action.payload
            };
        case FETCH_PROGRAM_CATEGORY:
            return {
                ...state,
                loading: false,
                currentCategory: action.payload
            }
        default:
            return state;
    }
};

export default programCategoriesReducer;