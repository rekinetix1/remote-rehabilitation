const { 
    FETCH_PAINS_REQUEST, FETCH_PAINS_SUCCESS, FETCH_PAINS_FAILURE, 
    FETCH_SINGLE_PAIN_REQUEST, FETCH_SINGLE_PAIN_SUCCESS, FETCH_SINGLE_PAIN_FAILURE, 
    POST_PAIN_REQUEST, POST_PAIN_SUCCESS, POST_PAIN_FAILURE, 
    UPDATE_PAIN_REQUEST, UPDATE_PAIN_SUCCESS, UPDATE_PAIN_FAILURE, 
    DELETE_PAIN_REQUEST, DELETE_PAIN_SUCCESS, DELETE_PAIN_FAILURE 
} = require("../actionTypes/painsActionTypes");

const initialState = {
    pains: [],
    currentPain: {},
    loading: false,
    error: null
};

const painsReducer = (state = initialState, action) => {
    switch(action.type){
        case FETCH_PAINS_REQUEST:
            return {
                ...state,
                pains: [],
                currentPain: {},
                loading: true,
                error: null
            };
        case FETCH_PAINS_SUCCESS:
            return {
                ...state,
                pains: action.payload,
                loading: false
            };
        case FETCH_PAINS_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        case FETCH_SINGLE_PAIN_REQUEST:
            return {
                ...state,
                loading: true
            };
        case FETCH_SINGLE_PAIN_SUCCESS:
            return {
                ...state,
                currentPain: action.payload,
                loading: false
            };
        case FETCH_SINGLE_PAIN_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        case POST_PAIN_REQUEST:
            return {
                ...state,
                loading: true
            };
        case POST_PAIN_SUCCESS:
            return {
                ...state,
                loading: false
            };
        case POST_PAIN_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        case UPDATE_PAIN_REQUEST:
            return {
                ...state,
                loading: true
            };
        case UPDATE_PAIN_SUCCESS:
            return {
                ...state,
                loading: false
            };
        case UPDATE_PAIN_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        case DELETE_PAIN_REQUEST:
            return {
                ...state,
                loading: true
            };
        case DELETE_PAIN_SUCCESS:
            return {
                ...state,
                loading: false
            };
        case DELETE_PAIN_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        default:
            return state;
    }
};

export default painsReducer;