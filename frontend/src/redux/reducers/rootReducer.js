import {combineReducers} from "redux";
import {connectRouter} from "connected-react-router";
import {createBrowserHistory} from "history";
import exerciseCategoriesReducer from "./exerciseCategoriesReducer";
import usersReducer from "./usersReducer";
import programsReducer from "./programsReducer";
import equipmentReducer from "./equipmentReducer";
import programCategoriesReducer from "./programCategoriesReducer";
import optionsReducer from "./optionsReducer";
import userProgramsReducer from "./userProgramsReducer";
import questionnairesReducer from "./questionnairesReducer";
import exercisesReducer from "./exercisesReducer";
import occupationReducer from "./occupationReducer";
import physicalActivitiesReducer from "./physicalActivitiesReducer";
import painsReducer from "./painsReducer";
import transactionsReducer from "./transactionsReducer";
import browsingHistoryReducer from "./browsingHistoryReducer";
import individualProgramsReducer from "./individualProgramsReducer";
import favoritesReducer from "./favoritesReducer";
import musclesReducer from "./musclesReducer"
import commentsReducer from "./commentsReducer"


export const history = createBrowserHistory();
 
const rootReducer = combineReducers({
  exerciseCategories: exerciseCategoriesReducer,
  users: usersReducer,
  programs: programsReducer,
  programCategories: programCategoriesReducer,
  options: optionsReducer,
  userPrograms: userProgramsReducer,
  questionnaires: questionnairesReducer,
  equipment: equipmentReducer,
  exercises: exercisesReducer,
  occupation: occupationReducer,
  physicalActivities: physicalActivitiesReducer,
  pains: painsReducer,
  transactions: transactionsReducer,
  browsingHistory: browsingHistoryReducer,
  individualPrograms: individualProgramsReducer,
  favorites: favoritesReducer,
  muscles: musclesReducer,
  comments: commentsReducer,
  router: connectRouter(history)
});

export default rootReducer;