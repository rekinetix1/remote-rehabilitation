import {
    FETCH_QUESTIONNAIRES_SUCCESS, FETCH_QUESTIONNAIRES_FAILURE, FETCH_QUESTIONNAIRES_REQUEST,
    FETCH_USER_QUESTIONNAIRE_SUCCESS, UPDATE_QUESTIONNAIRE_SUCCESS, FETCH_USER_QUESTIONNAIRE_FAILURE, 
    UPDATE_QUESTIONNAIRE_FAILURE, CREATE_QUESTIONNAIRE_SUCCESS, CREATE_QUESTIONNAIRE_FAILURE
} from "../actionTypes/questionnairesActionsTypes";

const initialState = {
    questionnaires: [],
    loading: false,
    error: null,
    userFilling: false,
    currentUserQuestionnaire:  {}
};

const questionnairesReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_QUESTIONNAIRES_REQUEST:
            return {
                ...state,
                loading: true,
                questionnaires: [],
                error: null
            };

        case FETCH_QUESTIONNAIRES_SUCCESS:
            return {
                ...state,
                questionnaires: action.payload,
                loading: false,
                error: null
            };

        case FETCH_QUESTIONNAIRES_FAILURE:
            return {
                ...state,
                questionnaires: [],
                loading: false,
                error: action.payload
            };
        case FETCH_USER_QUESTIONNAIRE_SUCCESS:
            return {
                ...state,
                userFilling: true,
                loading: false,
                currentUserQuestionnaire: action.payload
            }
        case FETCH_USER_QUESTIONNAIRE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            }
        case UPDATE_QUESTIONNAIRE_SUCCESS:
            return {
                ...state,
                loading: false
            };
        case UPDATE_QUESTIONNAIRE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        case CREATE_QUESTIONNAIRE_SUCCESS:
            return {
                ...state,
                loading: false
            };
        case CREATE_QUESTIONNAIRE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        default:
            return state;
    }
};

export default questionnairesReducer;