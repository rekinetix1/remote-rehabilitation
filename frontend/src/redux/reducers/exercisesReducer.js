import { 
    FETCH_EXERCISES_REQUEST, FETCH_EXERCISES_SUCCESS, FETCH_EXERCISES_FAILURE, 
    FETCH_EXERCISE_REQUEST, FETCH_EXERCISE_SUCCESS, FETCH_EXERCISE_FAILURE, 
    POST_EXERCISE_REQUEST, POST_EXERCISE_SUCCESS, POST_EXERCISE_FAILURE, 
    UPDATE_EXERCISE_REQUEST, UPDATE_EXERCISE_SUCCESS, UPDATE_EXERCISE_FAILURE, 
    DELETE_EXERCISE_REQUEST, DELETE_EXERCISE_SUCCESS, DELETE_EXERCISE_FAILURE 
} from "../actionTypes/exercisesActionTypes";

const initialState = {
    exercises: [],
    currentExercise: {},
    loading: false,
    error: null
};

const exercisesReducer = (state = initialState, action) => {
    switch(action.type){
        case FETCH_EXERCISES_REQUEST:
            return {
                ...state,
                exercises: [],
                loading: true,
                error: null
            };
        case FETCH_EXERCISES_SUCCESS:
            return {
                ...state,
                loading: false,
                currentExercise: {},
                exercises: action.payload
            };
        case FETCH_EXERCISES_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        case FETCH_EXERCISE_REQUEST: 
            return {
                ...state, 
                loading: true
            };
        case FETCH_EXERCISE_SUCCESS:
            return {
                ...state,
                currentExercise: action.payload,
                loading: false
            };
        case FETCH_EXERCISE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        case POST_EXERCISE_REQUEST: 
            return {
                ...state,
                loading: true
            };
        case POST_EXERCISE_SUCCESS:
            return {
                ...state, 
                loading: false
            };
        case POST_EXERCISE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            };
        case UPDATE_EXERCISE_REQUEST:
            return {
                ...state,
                loading: true
            };
        case UPDATE_EXERCISE_SUCCESS:
            return {
                ...state, 
                loading: false
            };
        case UPDATE_EXERCISE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        case DELETE_EXERCISE_REQUEST: 
            return {
                ...state,
                loading: true
            };
        case DELETE_EXERCISE_SUCCESS:
            return {
                ...state,
                loading: false
            };
        case DELETE_EXERCISE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            }
        default:
            return state;
    }
};

export default exercisesReducer;