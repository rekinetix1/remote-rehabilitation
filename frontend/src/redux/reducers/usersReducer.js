import {
    LOGIN_USER_REQUEST,
    LOGIN_USER_FAILURE,
    LOGIN_USER_SUCCESS,
    REGISTER_USER_FAILURE,
    REGISTER_USER_REQUEST,
    REGISTER_USER_SUCCESS,
    LOGOUT_USER_REQUEST,
    LOGOUT_USER_SUCCESS,
    LOGOUT_USER_FAILURE,
    FETCH_USERS_REQUEST,
    FETCH_USERS_SUCCESS,
    FETCH_USERS_FAILURE,
    FETCH_USER_REQUEST,
    FETCH_USER_SUCCESS,
    FETCH_USER_FAILURE,
    UPDATE_USER_REQUEST,
    UPDATE_USER_SUCCESS,
    UPDATE_USER_FAILURE,
    DELETE_USER_REQUEST,
    DELETE_USER_SUCCESS,
    DELETE_USER_FAILURE,
    UPDATE_USER_PROFILE_SUCCESS,
    VERIFY_USER_FAILURE,
    VERIFY_USER_SUCCESS,
    VERIFY_USER_REQUEST,
    CHECK_USER_IN_BASE_REQUEST,
    CHECK_USER_IN_BASE_SUCCESS,
    CHECK_USER_IN_BASE_FAILURE,
    RESET_USER_PASSWORD_REQUEST, RESET_USER_PASSWORD_FAILURE, RESET_USER_PASSWORD_SUCCESS
} from "../actionTypes/usersActionsTypes";

const initialState = {
    registerError: null,
    user: null,
    loginError: null,
    loading: null,
    logoutError: null,
    users: [],
    currentUser: {},
    checkUserInBase: null,
    error: null
};

const usersReducer = (state = initialState, action) => {
    switch (action.type) {
        case LOGIN_USER_REQUEST:
            return {
                ...state,
                loginError: null,
                registerError: null,
                logoutError: null,
                loading: true
            };
        case LOGIN_USER_SUCCESS:
            return {
                ...state,
                user: action.user,
                loginError: null,
                registerError: null,
                logoutError: null,
                loading: false
            };
        case LOGIN_USER_FAILURE:
            return {
                ...state,
                loginError: action.error,
                loading: false
            };
        case REGISTER_USER_REQUEST:
            return {
                ...state,
                loginError: null,
                registerError: null,
                logoutError: null,
                loading: true
            };
        case REGISTER_USER_SUCCESS:
            return {
                ...state,
                loading: false,
                loginError: null,
                registerError: null,
                logoutError: null,
            };
        case REGISTER_USER_FAILURE:
            return {
                ...state,
                registerError: action.error,
                loading: false
            };
        case LOGOUT_USER_REQUEST:
            return {
                ...state,
                loginError: null,
                registerError: null,
                logoutError: null,
                loading: true
            };
        case LOGOUT_USER_SUCCESS:
            return {
                ...state,
                loading: false,
                user: null,
                loginError: null,
                registerError: null,
                logoutError: null,
            };
        case LOGOUT_USER_FAILURE:
            return {
                ...state,
                loading: false,
                logoutError: action.error
            };
        case FETCH_USERS_REQUEST:
            return {
                ...state,
                loginError: null,
                registerError: null,
                logoutError: null,
                loading: true,
                users: []
            };
        case FETCH_USERS_SUCCESS:
            return {
                ...state,
                loading: false,
                loginError: null,
                registerError: null,
                logoutError: null,
                users: action.users
            };
        case FETCH_USERS_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            };
        case FETCH_USER_REQUEST:
            return {
                ...state,
                loginError: null,
                registerError: null,
                logoutError: null,
                loading: true
            };
        case FETCH_USER_SUCCESS:
            return {
                ...state,
                loading: false,
                loginError: null,
                registerError: null,
                logoutError: null,
                currentUser: action.currentUser
            };
        case FETCH_USER_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            };
        case UPDATE_USER_REQUEST:
            return {
                ...state,
                loginError: null,
                registerError: null,
                logoutError: null,
                loading: true
            };
        case UPDATE_USER_SUCCESS:
            return {
                ...state,
                loginError: null,
                registerError: null,
                logoutError: null,
                loading: false
            };
        case UPDATE_USER_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            };
        case UPDATE_USER_PROFILE_SUCCESS:
            return {
                ...state,
                loading: false,
                loginError: null,
                registerError: null,
                logoutError: null,
                user: {...state.user, ...action.user}
            };
        case DELETE_USER_REQUEST:
            return {
                ...state,
                loginError: null,
                registerError: null,
                logoutError: null,
                loading: true
            };
        case DELETE_USER_SUCCESS:
            return {
                ...state,
                loginError: null,
                registerError: null,
                logoutError: null,
                loading: false
            };
        case DELETE_USER_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            };
        case VERIFY_USER_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            };
        case VERIFY_USER_SUCCESS:
            return {
                ...state,
                loginError: null,
                registerError: null,
                logoutError: null,
                loading: false,
                user: {...state.user, verify: true}
            };
        case VERIFY_USER_REQUEST:
            return {
                ...state,
                loginError: null,
                registerError: null,
                logoutError: null,
                loading: true
            };
        case CHECK_USER_IN_BASE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            };
        case CHECK_USER_IN_BASE_SUCCESS:
            return {
                ...state,
                loading: false,
                loginError: null,
                registerError: null,
                logoutError: null,
                checkUserInBase: action.data
            };
        case CHECK_USER_IN_BASE_REQUEST:
            return {
                ...state,
                loading: true,
                loginError: null,
                registerError: null,
                logoutError: null,
                checkUserInBase: null
            };
        case RESET_USER_PASSWORD_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            };
        case RESET_USER_PASSWORD_SUCCESS:
            return {
                ...state,
                loginError: null,
                registerError: null,
                logoutError: null,
                loading: false,
            };
        case RESET_USER_PASSWORD_REQUEST:
            return {
                ...state,
                loginError: null,
                registerError: null,
                logoutError: null,
                loading: true,
            };
        default:
            return state;
    }
};

export default usersReducer;
