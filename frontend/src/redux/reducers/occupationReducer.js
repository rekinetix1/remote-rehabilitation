const { FETCH_OCCUPATION_REQUEST, FETCH_OCCUPATION_SUCCESS, FETCH_OCCUPATION_FAILURE, FETCH_SINGLE_OCCUPATION_REQUEST, FETCH_SINGLE_OCCUPATION_SUCCESS, FETCH_SINGLE_OCCUPATION_FAILURE, POST_OCCUPATION_REQUEST, POST_OCCUPATION_SUCCESS, POST_OCCUPATION_FAILURE, UPDATE_OCCUPATION_REQUEST, UPDATE_OCCUPATION_SUCCESS, UPDATE_OCCUPATION_FAILURE, DELETE_OCCUPATION_REQUEST, DELETE_OCCUPATION_SUCCESS, DELETE_OCCUPATION_FAILURE } = require("../actionTypes/occupationActionTypes");

const initialState = {
    occupation: [],
    currentOccupation: {},
    loading: false,
    error: null
};

const occupationReducer = (state = initialState, action) => {
    switch(action.type){
        case FETCH_OCCUPATION_REQUEST:
            return {
                ...state,
                occupation: [],
                currentOccupation: {},
                loading: true,
                error: null
            };
        case FETCH_OCCUPATION_SUCCESS:
            return {
                ...state,
                occupation: action.payload,
                loading: false
            };
        case FETCH_OCCUPATION_FAILURE:
            return {
                ...state, 
                loading: false,
                error: action.payload
            };
        case FETCH_SINGLE_OCCUPATION_REQUEST:
            return {
                ...state,
                loading: true
            };
        case FETCH_SINGLE_OCCUPATION_SUCCESS:
            return {
                ...state,
                currentOccupation: action.payload,
                loading: false
            };
        case FETCH_SINGLE_OCCUPATION_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        case POST_OCCUPATION_REQUEST:
            return {
                ...state,
                loading: true
            };
        case POST_OCCUPATION_SUCCESS:
            return {
                ...state,
                loading: false
            };
        case POST_OCCUPATION_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        case UPDATE_OCCUPATION_REQUEST:
            return {
                ...state,
                loading: true
            };
        case UPDATE_OCCUPATION_SUCCESS:
            return {
                ...state,
                loading: false
            };
        case UPDATE_OCCUPATION_FAILURE:
            return {
                ...state,
               loading: false,
                error: action.payload
            };
        case DELETE_OCCUPATION_REQUEST:
            return {
                ...state,
                loading: true
            };
        case DELETE_OCCUPATION_SUCCESS:
            return {
                ...state,
                loading: false
            };
        case DELETE_OCCUPATION_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        default:
            return state;
    }
};

export default occupationReducer;