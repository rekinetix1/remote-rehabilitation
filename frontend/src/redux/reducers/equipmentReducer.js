import {
  FETCH_EQUIPMENT_FAILURE,
  FETCH_EQUIPMENT_REQUEST,
  FETCH_EQUIPMENT_SUCCESS, FETCH_SINGLE_EQUIPMENT_FAILURE, FETCH_SINGLE_EQUIPMENT_SUCCESS
} from "../actionTypes/equipmentActionTypes";

const initialState = {
  equipment: [],
  currentEquipment: {},
  loading: [],
  error: null
};

const equipmentReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_EQUIPMENT_REQUEST:
      return {
        ...state,
        loading: true,
        equipment: [],
        currentEquipment: {},
        error: null
      };

    case FETCH_EQUIPMENT_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
        equipment: action.payload,
        currentEquipment: {}
      };

    case FETCH_EQUIPMENT_FAILURE:
      return {
        ...state,
        loading: false,
        equipment: [],
        error: action.payload,
        currentEquipment: {}
      };

    case FETCH_SINGLE_EQUIPMENT_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
        equipment: [],
        currentEquipment: action.payload
      };

    case FETCH_SINGLE_EQUIPMENT_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
        equipment: [],
        currentEquipment: {}
      }

    default:
      return state;
  }
};

export default equipmentReducer;