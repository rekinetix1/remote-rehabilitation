import {
    FETCH_HISTORY_FAILURE,
    FETCH_HISTORY_REQUEST,
    FETCH_HISTORY_SUCCESS
} from "../actionTypes/browsingHistoryActionTypes";

const initialState = {
    browsingHistory: [],
    currentTransaction: {},
    loading: false,
    error: null
};

const browsingHistoryReducer = (state = initialState, action) => {
    switch(action.type){
        case FETCH_HISTORY_REQUEST:
            return {
                ...state,
                loading: true
            };
        case FETCH_HISTORY_SUCCESS:
            return {
                ...state,
                loading: false,
                browsingHistory: action.payload
            };
        case FETCH_HISTORY_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            };

        default:
            return state;
    }
}

export default browsingHistoryReducer;