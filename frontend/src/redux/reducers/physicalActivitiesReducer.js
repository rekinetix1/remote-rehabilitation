const { Flag } = require("@material-ui/icons");
const { FETCH_ACTIVITIES_REQUEST, FETCH_ACTIVITIES_SUCCESS, FETCH_ACTIVITIES_FAILURE, FETCH_ACTIVITY_REQUEST, FETCH_ACTIVITY_SUCCESS, FETCH_ACTIVITY_FAILURE, POST_ACTIVITY_REQUEST, POST_ACTIVITY_SUCCESS, POST_ACTIVITY_FAILURE, UPDATE_ACTIVITY_REQUEST, UPDATE_ACTIVITY_SUCCESS, UPDATE_ACTIVITY_FAILURE, DELETE_ACTIVITY_REQUEST, DELETE_ACTIVITY_SUCCESS, DELETE_ACTIVITY_FAILURE } = require("../actionTypes/physicalActivitiesActionTypes");

const initialState = {
    activities: [],
    currentActivity: {},
    loading: false,
    error: null
};

const physicalActivitiesReducer = (state = initialState, action) => {
    switch(action.type){
        case FETCH_ACTIVITIES_REQUEST:
            return {
                ...state,
                activities: [],
                currentActivity: {},
                loading: true,
                error: action.payload
            };
        case FETCH_ACTIVITIES_SUCCESS:
            return {
                ...state,
                activities: action.payload,
                loading: false
            };
        case FETCH_ACTIVITIES_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        case FETCH_ACTIVITY_REQUEST:
            return {
                ...state,
                loading: true
            };
        case FETCH_ACTIVITY_SUCCESS:
            return {
                ...state,
                loading: false,
                currentActivity: action.payload
            };
        case FETCH_ACTIVITY_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        case POST_ACTIVITY_REQUEST:
            return {
                ...state,
                loading: true
            };
        case POST_ACTIVITY_SUCCESS:
            return {
                ...state,
                loading: false
            };
        case POST_ACTIVITY_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        case UPDATE_ACTIVITY_REQUEST:
            return {
                ...state,
                loading: true
            };
        case UPDATE_ACTIVITY_SUCCESS:
            return {
                ...state,
                loading: false
            };
        case UPDATE_ACTIVITY_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        case DELETE_ACTIVITY_REQUEST:
            return {
                ...state,
                loading: true
            };
        case DELETE_ACTIVITY_SUCCESS:
            return {
                ...state,
                loading: false
            };
        case DELETE_ACTIVITY_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            }
        default:
            return state;
    }
};

export default physicalActivitiesReducer;
