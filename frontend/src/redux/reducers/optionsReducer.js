const { 
    FETCH_OPTIONS_REQUEST, FETCH_OPTIONS_SUCCESS, FETCH_OPTIONS_FAILURE, 
    FETCH_OPTION_REQUEST, FETCH_OPTION_SUCCESS, FETCH_OPTION_FAILURE, 
    POST_OPTION_REQUEST, POST_OPTION_SUCCESS, UPDATE_OPTION_REQUEST, 
    UPDATE_OPTION_SUCCESS, UPDATE_OPTION_FAILURE, DELETE_OPTION_REQUEST, 
    DELETE_OPTION_SUCCESS, POST_OPTION_FAILURE, DELETE_OPTION_FAILURE
} = require("../actionTypes/optionsActionsTypes");

const initialState = {
    options: [],
    currentOption: {},
    loading: false,
    error: null
};

const optionsReducer = (state = initialState, action) => {
    switch (action.type){
        case FETCH_OPTIONS_REQUEST:
            return {
                ...state,
                options: [],
                loading: true,
                error: null
            };
        case FETCH_OPTIONS_SUCCESS:
            return {
                ...state,
                options: action.payload,
                loading: false
            };
        case FETCH_OPTIONS_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        case FETCH_OPTION_REQUEST:
            return {
                ...state,
                loading: true,
            };
        case FETCH_OPTION_SUCCESS:
            return {
                ...state,
                currentOption: action.payload,
                loading: false
            };
        case FETCH_OPTION_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        case POST_OPTION_REQUEST:
            return {
                ...state,
                loading: true
            };
        case POST_OPTION_SUCCESS:
            return {
                ...state,
                loading: false
            };
        case POST_OPTION_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        case UPDATE_OPTION_REQUEST:
            return {
                ...state,
                loading: true
            };
        case UPDATE_OPTION_SUCCESS:
            return {
                ...state,
                loading: false
            };
        case UPDATE_OPTION_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        case DELETE_OPTION_REQUEST:
            return {
                ...state,
                loading: true
            };
        case DELETE_OPTION_SUCCESS:
            return {
                ...state,
                loading: false
            };
        case DELETE_OPTION_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        default: 
            return state;
    }
};

export default optionsReducer;