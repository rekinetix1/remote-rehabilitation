import {
    FETCH_SINGLE_TRANSACTION_FAILURE, FETCH_SINGLE_TRANSACTION_REQUEST,
    FETCH_SINGLE_TRANSACTION_SUCCESS, FETCH_TRANSACTIONS_FAILURE,
    FETCH_TRANSACTIONS_REQUEST, FETCH_TRANSACTIONS_SUCCESS,POST_SINGLE_TRANSACTION_REQUEST,
    POST_SINGLE_TRANSACTION_SUCCESS, POST_SINGLE_TRANSACTION_FAILURE,
    UPDATE_SINGLE_TRANSACTION_FAILURE, UPDATE_SINGLE_TRANSACTION_REQUEST,
    UPDATE_SINGLE_TRANSACTION_SUCCESS
} from "../actionTypes/transactionsActionTypes";

const initialState = {
    transactions: [],
    currentTransaction: {},
    loading: false,
    error: null
};

const transactionsReducer = (state = initialState, action) => {
    switch(action.type){
        case FETCH_TRANSACTIONS_REQUEST:
            return {
                ...state,
                loading: true
            };
        case FETCH_TRANSACTIONS_SUCCESS:
            return {
                ...state,
                loading: false,
                transactions: action.payload
            };
        case FETCH_TRANSACTIONS_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        case FETCH_SINGLE_TRANSACTION_REQUEST:
            return {
                ...state,
                loading: true
            };
        case FETCH_SINGLE_TRANSACTION_SUCCESS:
            return {
                ...state,
                loading: false,
                currentTransaction: action.payload
            };
        case FETCH_SINGLE_TRANSACTION_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        case POST_SINGLE_TRANSACTION_REQUEST:
            return {
                ...state,
                transactions: [],
                currentTransaction: {},
                loading: true,
                error: null
            };
        case POST_SINGLE_TRANSACTION_SUCCESS:
            return {
                ...state,
                loading: false,
                currentTransaction: action.payload
            };
        case POST_SINGLE_TRANSACTION_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            };
        case UPDATE_SINGLE_TRANSACTION_REQUEST:
            return {
                ...state,
                loading: true
            };
        case UPDATE_SINGLE_TRANSACTION_SUCCESS:
            return {
                ...state,
                loading: false
            };
        case UPDATE_SINGLE_TRANSACTION_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        default:
            return state;
    }
}

export default transactionsReducer;