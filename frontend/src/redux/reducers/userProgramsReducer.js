import { 
    POST_USER_PROGRAM_REQUEST, POST_USER_PROGRAM_SUCCESS, POST_USER_PROGRAM_FAILURE, 
    UPDATE_USER_PROGRAM_FAILURE, DELETE_USER_PROGRAM_SUCCESS, FETCH_USER_PROGRAMS_SUCCESS,
    FETCH_USER_PROGRAMS_REQUEST, FETCH_USER_PROGRAMS_FAILURE, UPDATE_USER_PROGRAM_REQUEST,
    UPDATE_USER_PROGRAM_SUCCESS, DELETE_USER_PROGRAM_REQUEST, DELETE_USER_PROGRAM_FAILURE, 
    FETCH_CURRENT_USER_PROGRAM_REQUEST, FETCH_CURRENT_USER_PROGRAM_SUCCESS, 
    FETCH_CURRENT_USER_PROGRAM_FAILURE 
} from "../actionTypes/userProgramsActionsTpes";

const initialState = {
    userPrograms: [],
    currentUserProgram: {},
    loading: false,
    error: null
};

const userProgramsReducer = (state = initialState, action) => {
    switch (action.type){
        case POST_USER_PROGRAM_REQUEST:
            return {
                ...state,
                programs: [],
                loading: true,
                error: null
            };
        case POST_USER_PROGRAM_SUCCESS:
            return {
                ...state,
                loading: false
            };
        case POST_USER_PROGRAM_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.error
            };
        case FETCH_USER_PROGRAMS_REQUEST:
            return {
                ...state,
                loading: true
            };
        case FETCH_USER_PROGRAMS_SUCCESS:
            return {
                ...state,
                userPrograms: action.payload,
                loading: false
            };
        case FETCH_USER_PROGRAMS_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        case FETCH_CURRENT_USER_PROGRAM_REQUEST:
            return {
                ...state,
                loading: true
            };
        case FETCH_CURRENT_USER_PROGRAM_SUCCESS:
            return {
                ...state,
                loading: false,
                currentUserProgram: action.payload
            };
        case FETCH_CURRENT_USER_PROGRAM_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        case UPDATE_USER_PROGRAM_REQUEST:
            return {
                ...state,
                loading: true
            };
        case UPDATE_USER_PROGRAM_SUCCESS:
            return {
                ...state,
                loading: false
            };
        case UPDATE_USER_PROGRAM_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        case DELETE_USER_PROGRAM_REQUEST:
            return {
                ...state,
                loading: true
            };
        case DELETE_USER_PROGRAM_SUCCESS:
            return {
                ...state,
                loading: false
            };
        case DELETE_USER_PROGRAM_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        default:
            return state;
    }
};

export default userProgramsReducer;