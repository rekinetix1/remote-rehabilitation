import YouTube from "react-youtube";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import FavoriteIcon from "@material-ui/icons/Favorite";
import ModalWindow from "../../../components/UI/ModalWindow/ModalWindow";
import CheckCircleOutlineIcon from "@material-ui/icons/CheckCircleOutline";
import {Link as RouterLink} from "react-router-dom";
import OptionalMenu from "../../../components/OptionsMenu/OptionsMenu";
import Pay from "../../../components/ Widgets/CloudPayments/CloudPayments";
import React, {useEffect, useState} from "react";
import {useHistory} from "react-router-dom";
import "./UnboughtProgram.css";

const UnboughtProgram = (props) => {
    const [totalCost, setTotalCost] = useState(0);
    const [optionItems, setOptions] = useState([]);
    const [payment, setPayment] = useState(false);
    const [favorite, setFavorite] = useState(null);
    const [showModal, setShowModal] = useState(false);

    useEffect(() => {
        document.title = `${props.program.title} | Rekinetix`
        setTotalCost(props.program.cost)
    }, [props.program]);

    useEffect(() => {
        let favorite = props.favorites.find(favorite => favorite.program._id === props.programId)
        if (favorite) {
            setFavorite(favorite)
        }
    }, [props.favorites]);

    const history = useHistory()


    const showModalHandler = () => {
        setShowModal(true)
    }


    const buyHandler = () => {
        props.user ?
            showModalHandler() :
            history.push('/login')
    }

    const closeModalHandler = () => {
        setShowModal(false)
    }

    const changeFavorites = () => {
        if (favorite) {
            props.onDeleteFavorite(favorite._id)
            setFavorite(null)
        } else {
            let favorite_data = {
                user: props.user._id,
                program: props.programId
            }
            props.onCreateFavorite(favorite_data)
        }
    }

    const checkboxHandler = (e) => {
        const arr = [...optionItems];
        if (e.target.checked) {
            setTotalCost(parseInt(totalCost) + parseInt(e.target.value));
            arr.push(e.target.id);
            setOptions(arr);
        } else {
            setTotalCost(parseInt(totalCost) - parseInt(e.target.value));
            for (let i = arr.length - 1; i >= 0; i--) {
                if (arr[i] === e.target.id) {
                    arr.splice(i, 1);
                }
            }
            setOptions(arr);
        }
    }

    const startPaymant = () => {
        props.onPostTransaction(
            {
                user: props.user._id,
                program: props.program._id,
                cost: totalCost
            }
        ).then(data => {
            Pay({
                program: props.program,
                totalCost,
                user: props.user,
                programId: props.programId,
                currentTransaction: data,
                makePayment: () => setPayment(true),
                closeModal: closeModalHandler,
                onPostUserProgram: props.onPostUserProgram,
                onUpdateTransaction: props.onUpdateTransaction,
                optionItems
            });
        });

    }

    return (
        <div style={{textAlign: 'left'}} className="container">
            <h1 className="text-center mb-5">{props.program.title}</h1>
            <div className="mb-2"><b>Базовая цена:</b> {props.program.cost ? props.program.cost.toLocaleString() : props.program.cost} ₸</div>
            <div className="mb-4"><b>Сложность данной программы:</b> {props.program.complexity}</div>
            <p>{props.program.full_description}</p>
            <div className="text-center mb-3 m-auto" style={{maxWidth:600}}>
                <YouTube videoId={String(props.program.video).split('/',)[4]} className="w-100"/>
            </div>
            {
                !props.user || (props.user && props.user.verify && props.user.role == 'Пациент') ?
                    <Button onClick={buyHandler} color='primary' size='large'
                            variant="contained"> Купить! </Button> : <p>Верифицируйте аккаунт, чтобы купить программу</p>
            }
            {
                props.user && props.user.role == 'Пациент' ?
                    <IconButton aria-label="add to favorites" onClick={changeFavorites}>
                        <FavoriteIcon color={favorite ? "secondary" : 'inherit'}/>
                    </IconButton> : null
            }
            <div className='mt-3'>
                <div className='mt-3' id="div" style={{minHeight: "500px"}}>
                    <ModalWindow
                        show={showModal}
                        closed={payment ? closeModalHandler : null}
                    >
                        {payment ?
                            <div className="text-center mt-5"><CheckCircleOutlineIcon style={{color: 'green', fontSize: '150px'}}/>
                                <h4><b>Программа куплена! </b></h4>
                                <Button component={RouterLink} size="large"
                                        variant="outlined"
                                        color="primary"
                                        to="/personalArea/programs"> Перейти к купленным
                                    программам </Button>
                            </div>
                            : <div>
                                <h5 className='mb-3 modalTitle'>Вы покупаете программу <b>
                                    "{props.program.title}"</b></h5>
                                <div className='modalText'>Количество упражнений в программе: {props.exQty}</div>
                                <div className='modalText'>Длительность программы {props.program.duration} дней</div>
                                <div className='modalText'>Цена программы: {props.program.cost} ₸</div>
                                <OptionalMenu
                                    options={props.options}
                                    check={checkboxHandler}
                                />
                                <h4 className="mt-3 modalTitle"><b><strong>Итоговая
                                    цена: {totalCost ? totalCost.toLocaleString() : totalCost} ₸</strong></b></h4>
                                <Button className='m-2 modalButton' onClick={startPaymant}
                                        color='primary'
                                        // size='large'
                                        variant="contained"> Оплатить! </Button>
                                <Button className='modalButton' onClick={closeModalHandler} color='secondary' 
                                        variant="contained"> Закрыть окно </Button>
                            </div>}
                    </ModalWindow>
                </div>
            </div>
        </div>
    )
}

export default UnboughtProgram