import React, {useEffect, useState} from 'react';
import ProgramsApiService from "../../services/api/ProgramsApiService";
import UserProgramsApiService from "../../services/api/UserProgramsApiService";
import {connect} from "react-redux";
import OptionsApiService from '../../services/api/OptionsApiService';
import FavoritesApiService from "../../services/api/FavoritesApiService";
import TransactionsApiService from "../../services/api/TransactionsApiService";
import BrowsingHistoryApiService from "../../services/api/BrowsingHistoryApiService";
import UnboughtProgram from "./UnboughtProgram/UnboughtProgram";
import BoughtProgram from "./BoughtProgram/BoughtProgram";
import IndividualProgramsApiService from "../../services/api/IndividualProgramsApiService";
import withLoader from "../../hoc/withLoader";
import axios from "../../axios-api";
import QuestionnariesUser from '../../services/api/QuestionarryApiServices';
import {withRouter} from "react-router";

const FullProgram = (props) => {
    const [program, setProgram] = useState({});
    const [exQty, setExQty] = useState(0);

    let userProgramsId = Object.keys(props.userPrograms).map(program => {
        return props.userPrograms[program].program._id
    })

    useEffect(() => {

        props.onFetchPrograms();
        props.onFetchOptions();
        props.onFetchIndividualProgram(props.match.params.id)
        if (props.user) {
            props.onFetchUserPrograms(props.user._id)
            props.onFetchUserHistory(props.user._id)
            // props.getUserQuestionnaire(props.user._id,props.location.pathname)
            props.onFetchFavorites()
        }
        props.onFetchPrograms();
        props.onFetchOptions();

    }, [props.user]);


    useEffect(() => {
            if (!props.match.path.includes('individual')) {
                let program = props.programs.find(program => String(program._id) === String(props.match.params.id))
                setProgram(program)
                if (program) {
                    setExQty(program.days.length)
                }
            } else {
                if (props.individualProgram) {
                    setProgram(props.individualProgram)
                    console.log('efe')
                }
            }
        }
        ,
        [props.programs, props.individualProgram]
    )

    return (
        <div>
            {
                ((!props.user && program) || (program && !userProgramsId.includes(props.match.params.id)) && !props.individualProgram && program) ?
                    <UnboughtProgram program={program}
                                     programId={props.match.params.id}
                                     user={props.user}
                                     onPostUserProgram={props.onPostUserProgram}
                                     onPostTransaction={props.onPostTransaction}
                                     currentTransaction={props.currentTransaction}
                                     onUpdateTransaction={props.onUpdateTransaction}
                                     exQty={exQty}
                                     options={props.options}
                                     onDeleteFavorite={props.onDeleteFavorite}
                                     onCreateFavorite={props.onCreateFavorite}
                                     favorites={props.favorites}
                                     questionary={props.userQuestionnaires}
                    /> : null
            }

            {
                (props.user && program && (userProgramsId.includes(props.match.params.id) || props.individualProgram)) ?
                    <BoughtProgram
                        program={program}
                        programId={props.match.params.id}
                        questionary={props.userQuestionnaires}
                        getquestionary={props.getUserQuestionnaire}
                        user={props.user}
                        path={props.location.pathname}
                    /> : null
            }
        </div>
    );

}

const mapStateToProps = state => {
    return {
        user: state.users.user,
        programs: state.programs.programs,
        loading: state.programs.loading,
        error: state.programs.error,
        options: state.options.options,
        userPrograms: state.userPrograms.userPrograms,
        favorites: state.favorites.favorites,
        history: state.browsingHistory.browsingHistory,
        currentTransaction: state.transactions.currentTransaction,
        individualProgram: state.individualPrograms.currentIndividualProgram,
        userQuestionnaires: state.questionnaires.currentUserQuestionnaire,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchOptions: () => dispatch(OptionsApiService.fetchOptions()),
        onFetchPrograms: () => dispatch(ProgramsApiService.fetchPrograms()),
        onPostUserProgram: (data) => dispatch(UserProgramsApiService.postUserProgram(data)),
        onFetchUserPrograms: (id) => dispatch(UserProgramsApiService.fetchUserPrograms(id)),
        onPostTransaction: (data) => dispatch(TransactionsApiService.postTransaction(data)),
        onUpdateTransaction: (id, data) => dispatch(TransactionsApiService.updateTransaction(id, data)),
        onFetchFavorites: () => dispatch(FavoritesApiService.fetchFavorites()),
        getUserQuestionnaire: (data) => dispatch(QuestionnariesUser.getUserQuestionnaire(data)),
        onDeleteFavorite: (id) => dispatch(FavoritesApiService.deleteFavorite(id)),
        onCreateFavorite: (favorite) => dispatch(FavoritesApiService.createFavorite(favorite)),
        onFetchUserHistory: (user) => dispatch(BrowsingHistoryApiService.fetchBrowsingHistory(user)),
        onPrintInHistory: (data, user) => dispatch(BrowsingHistoryApiService.printInBrowsingHistory(data, user)),
        onFetchIndividualProgram: (data) => dispatch(IndividualProgramsApiService.fetchCurrentIndividualProgram(data))

    };
};



export default connect(mapStateToProps, mapDispatchToProps)(withRouter((withLoader(FullProgram, axios))))

