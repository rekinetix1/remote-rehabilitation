import YouTube from "react-youtube";
import Button from "@material-ui/core/Button";
import React, {useEffect, useState} from "react";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import makeStyles from "@material-ui/core/styles/makeStyles";
import './BoughtProgram.css'
import BrowsingHistoryApiService from "../../../services/api/BrowsingHistoryApiService";
import {connect} from "react-redux";
import LockIcon from '@material-ui/icons/Lock';
import Comments from "../../Comments/Comments";
import CommentsApiService from "../../../services/api/CommentsApiService";
import {withRouter} from "react-router-dom";
import UserProgramsApiService from "../../../services/api/UserProgramsApiService";
import {NotificationManager} from "react-notifications";

const useStyles = makeStyles({

    media: {
        height: 140,
    },
});

const BoughtProgram = (props) => {
    const classes = useStyles();
    const [state, setState] = React.useState({
        checked: true
    });

    const handleChangeCheckbox = (event) => {
        setState({...state, [event.target.name]: event.target.checked});
    };
    const [currentDay, setCurrentDay] = useState(0)
    const [currentExercise, setCurrentExercise] = useState(0)
    const [showDescription, setShowDescription] = useState(false)

    const printCards = () => {
        return props.program && props.program.days ? props.program.days.map((d, i) => {
            return (
                <div className='BoughtProgram_card' key={i}>
                    <Card key={d._id} className='BoughtProgram_card' onClick={() => setCurrentDay(i)}>
                        <CardActionArea className='BoughtProgram_card'>
                            <CardContent className='BoughtProgram_card_content'>
                                <Typography gutterBottom variant="h5" component="h2">
                                    День {i + 1}
                                </Typography>
                                <div className={'progress_day'}>
                                    {currentDay === i ? <Typography variant="body2" color="textSecondary" component="p">
                                        Выполнено упражнений {currentExercise} из {d.length}
                                    </Typography> : null}
                                    {currentDay > i ? <Typography variant="body2" color="textSecondary" component="p">
                                        День окончен
                                    </Typography> : null}
                                    {currentDay < i ? <Typography variant="body2" color="textSecondary" component="p">
                                        День закрыт
                                    </Typography> : null}
                                </div>
                            </CardContent>
                        </CardActionArea>
                    </Card>
                    {i > currentDay ? <div className='BoughtProgram_overlay'><LockIcon/></div> : null}
                </div>
            )
        }) : <div/>
    }

    const getVideoId = (link) => {
        let parts = link.split('/')
        let part = parts[parts.length - 1].split('=')
        return part[part.length - 1]
    }

    const printProgramExercises = () => {
        return props.program && props.program.days ? props.program.days[currentDay].map((day, i) => {
            return (<div className='BoughtProgram_exercise' key={i}>
                <Button fullWidth
                        onClick={() => setCurrentExercise(i)}
                        variant='contained'
                        key={day._id}>
                    {i <= currentExercise ? <div> {day.exercise.title}</div> : <div> Упражнение {i + 1} </div>}
                </Button>
                {i > currentExercise ? <div className='BoughtProgram_overlay'><LockIcon/></div> : null}
            </div>)
        }) : <div/>

    }

    const nextExerciseHandler = () => {
        if (props.program && props.path.includes('individual')) {
            setCurrentExercise(currentExercise + 1)
            return
        }

        if (props.program.days[currentDay].length - 1 > currentExercise) {
            setCurrentExercise(currentExercise + 1)
            props.onPrintInHistory({
                day: currentDay + 1,
                exercise: currentExercise + 1,
                program: props.programId,
                type: 'exercise',
                individualProgram: props.programId
            })
        } else {
            if (currentDay + 1 < props.program.days.length) {
                alert('День окончен')
                props.onPrintInHistory({
                    day: currentDay + 1,
                    program: props.programId,
                    type: 'day'
                })
                setCurrentDay(currentDay + 1)
                setCurrentExercise(0)
            } else {
                alert('Программа окончена =)')
            }
        }
    }

    useEffect(() => {
        props.onFetchUserHistory(props.user._id)
        props.getquestionary(props.user._id).then(() => {
            if (!props.userQuestionnaire) {
                NotificationManager.success("Заполните Анкету")
                props.history.push('/personalArea/questionnaire');
            }
        })
        props.onFetchUserProgram(props.programId, props.user._id);
    }, [])


    useEffect(() => {
        document.title = `${props.program.title} | Rekinetix`
    }, [props.program])


    useEffect(() => {
        let days = props.browserHistory.filter(h => h.program._id === props.programId)
            .filter(h => h.type === 'day')
            .sort((a, b) => a.day < b.day ? 1 : -1)

        if (days[0] && days[0].day) {
            setCurrentDay(days[0].day)
        }

        let exercises = props.browserHistory.filter(h => h.program._id === props.programId)
            .filter(h => h.day === currentDay + 1)
            .filter(h => h.type === 'exercise')
            .sort((a, b) => a.day < b.day ? 1 : -1)

        if (exercises[0] && exercises[0].exercise) {
            setCurrentExercise(exercises[0].exercise)
        }

    }, [props.browserHistory])

    const printIndividualEx = () => {
        return props.program.exercises.map((ex, i) => {
            return (<div className='BoughtProgram_exercise'>
                <Button fullWidth
                        onClick={() => setCurrentExercise(i)}
                        variant='contained'
                        key={ex._id}>
                    {i <= currentExercise ? <div> {ex.exercise.title}</div> : <div> Упражнение {i + 1} </div>}
                </Button>
                {i > currentExercise ? <div className='BoughtProgram_overlay'><LockIcon/></div> : null}
            </div>)
        })
    }

    return (
        <div>
            <div className={`flex ${props.path.includes('individual') ? 'Individual_program_flex' : null}`}>
                {!props.path.includes('individual') ?
                    <div className={'BroughtProgram_cards'}>
                        {printCards()}
                    </div> : null}
                <div className='BroughtProgram_content'>
                    <h3>{props.program.title}</h3>
                    <p>{props.program.full_description}</p>
                    {props.program.days && props.program.days[currentDay]
                    && props.program.days[currentDay][currentExercise]
                    && props.program.days[currentDay][currentExercise].exercise ?
                        <div>
                            <h5 className='BoughtProgram_day_title'>{props.program.days[currentDay][currentExercise].exercise.title}</h5>
                            <Button className='BroughtProgram_description_btn'
                                    onClick={() => setShowDescription(!showDescription)} color="primary">
                                {!showDescription ?
                                    <span> Показать описание ˅ </span> : <span>Скрыть описание ˄ </span>} </Button>
                            {showDescription ?
                                <p>{props.program.days[currentDay][currentExercise].exercise.description}</p>
                                : null
                            }
                            <p>Количество повторений данного
                                упражнения: {props.program.days[currentDay][currentExercise].repetitions}</p>
                            <p>Количество подходов данного
                                упражнения: {props.program.days[currentDay][currentExercise].workouts}</p>
                            <YouTube
                                onEnd={state.checked ? (e) => {
                                    nextExerciseHandler()
                                } : null}
                                opts={{
                                    height: '325',
                                    width: '100%',
                                }}
                                videoId={getVideoId(props.program.days[currentDay][currentExercise].exercise.video_link)}/>
                        </div> : null}

                    {props.program && props.path.includes('individual') && props.program.exercises ?
                        <div>
                            <h5 className='BoughtProgram_day_title'>{props.program.exercises[currentExercise].exercise.title}</h5>
                            <Button className='BroughtProgram_description_btn'
                                    onClick={() => setShowDescription(!showDescription)} color="primary">
                                {!showDescription ?

                                    <span> Показать описание ˅ </span> : <span>Скрыть описание ˄ </span>} </Button>
                            {showDescription ?
                                <p>{props.program.exercises[currentExercise].exercise.description}</p>
                                : null
                            }
                            <p>Количество повторений данного
                                упражнения: {props.program.exercises[currentExercise].repetitions}</p>
                            <p>Количество подходов данного
                                упражнения: {props.program.exercises[currentExercise].workouts}</p>
                            <YouTube
                                opts={{
                                    height: '325',
                                    width: '100%',
                                }}
                                videoId={getVideoId(props.program.exercises[currentExercise].exercise.video_link)}/>
                        </div>
                        : null}
                    <Button onClick={nextExerciseHandler} color='primary'>Я выполнил данное упражнение, перейти к
                        следующему</Button>
                </div>
                <div className='BroughtProgram_exercise_list'>
                    <FormControlLabel className='BoughtProgram_checkbox'
                                      control={
                                          <Checkbox
                                              checked={state.checked}
                                              onChange={handleChangeCheckbox}
                                              name="checked"
                                              color="primary"
                                          />
                                      }
                                      label="Выполнять повторение видео автоматически"
                    />
                    {printProgramExercises()}
                    {props.path.includes('individual') && props.program && props.program.exercises ?
                        printIndividualEx() : null}
                </div>
            </div>
            <div
                id='#comments'
                className={`BroughtProgram_comments ${props.path.includes('individual') ? 'Individual_program_comments' : null}`}>
                {(props.userProgram[0] && props.userProgram[0].options.filter(o => o.title === "Обратная связь").length > 0 || props.path.includes('individual')) ?
                    <>
                        <h4>Комментарии к текущей программе</h4>
                        <Comments
                            fetchComments={props.onFetchProgramComments}
                            path={props.path}
                            user={props.user}
                            programId={props.program._id}
                            comments={props.comments}
                            addComment={props.onAddReplyComment}
                            deleteComment={props.onDeleteComment}
                            editComment={props.onEditComment}
                        />
                    </>
                    : null}
            </div>
        </div>
    )

}

const mapStateToProps = state => {
    return {
        user: state.users.user,
        programs: state.programs.programs,
        loading: state.programs.loading,
        error: state.programs.error,
        options: state.options.options,
        userPrograms: state.userPrograms.userPrograms,
        favorites: state.favorites.favorites,
        comments: state.comments.comments,
        userQuestionnaire: state.questionnaires.currentUserQuestionnaire,
        browserHistory: state.browsingHistory.browsingHistory,
        userProgram: state.userPrograms.currentUserProgram
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onPrintInHistory: (data, user) => dispatch(BrowsingHistoryApiService.printInBrowsingHistory(data, user)),
        onFetchUserHistory: (user) => dispatch(BrowsingHistoryApiService.fetchBrowsingHistory(user)),
        onFetchProgramComments: (programId, type) => dispatch(CommentsApiService.fetchProgramComments(programId, type)),
        onAddReplyComment: (comm) => dispatch(CommentsApiService.addReplyComment(comm)),
        onDeleteComment: (id) => dispatch(CommentsApiService.deleteEComment(id)),
        onFetchUserProgram: (programId, userId) => dispatch(UserProgramsApiService.fetchCurrentProgram(programId, userId)),
        onEditComment: (id, data) => dispatch(CommentsApiService.updateComment(id, data))
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(BoughtProgram))
