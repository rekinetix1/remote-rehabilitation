import React, {Component} from "react";
import {connect} from "react-redux";
import TextField from '@material-ui/core/TextField';
import {Container} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import {Link as RouterLink} from "react-router-dom";
import UsersApiService from "../../services/api/UsersApiService";
import ModalWindow from "../../components/UI/ModalWindow/ModalWindow";
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';

class PasswordRecovery extends Component {
    state = {
        email: '',
        showModal: false
    }

    inputChangeHandler = e => {
        this.setState({[e.target.name]: e.target.value});
    };

    submitFormHandler = e => {
        e.preventDefault();

        this.showModal()
    };

    showModal = () => {
        setTimeout(() => {
            if (this.props.checkUser !== null) {
                this.setState({showModal: true})
            }
        }, 3000)
    };

    closeModal = () => {
        this.setState({showModal: false})
    };

    componentDidMount() {
        document.title = `Восстановление пароля | Rekinetix`
    }

    render() {
        return (
            <div className='Login'>
                <h2>
                    Восстановление пароля
                </h2>
                <form onSubmit={this.submitFormHandler}>
                    <Container maxWidth={'xs'}>

                        <div>
                            <label htmlFor='email'>Введите почту, на которую был зарегистрирован ваш аккаунт</label>
                            <TextField onChange={this.inputChangeHandler}
                                       margin="normal"
                                       name='email'
                                       fullWidth
                                       required
                                       id="email"
                                       type='email'
                                       label="Почта"/>
                        </div>
                        <div className='Login_submit'>
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                onClick={() => this.props.checkUserInBase(this.state.email)}
                            >
                                восстановить пароль
                            </Button>
                        </div>
                    </Container>
                </form>

                <ModalWindow show={this.state.showModal} closed={this.closeModal}>
                    <CheckCircleOutlineIcon style={{color: 'green', fontSize: '150px'}}/>
                    <span><h4>Письмо с восстановлением пароля отправлено вам на почту!
                        </h4>
                        <Button component={RouterLink} size="large"
                                variant="outlined"
                                color="primary"
                                to="/login"> Вернуться к входу в аккаунт </Button></span>
                </ModalWindow>


            </div>
        );
    }
}


const mapStateToProps = state => {
    return {
        checkUser: state.users.checkUserInBase,

    }
};
const mapDispatchToProps = dispatch => {
    return {
        checkUserInBase: (email) => dispatch(UsersApiService.checkUserInBase(email))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(PasswordRecovery);
