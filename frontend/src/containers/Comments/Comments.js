import React, {useEffect, useState} from "react";
import axios from '../../axios-api'
import {Button} from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import './Comments.css'
import withLoader from "../../hoc/withLoader";
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import IconButton from "@material-ui/core/IconButton";
import ReplyIcon from '@material-ui/icons/Reply';
import EditIcon from '@material-ui/icons/Edit';

const Comments = (props) => {
    const [commentField, setCommentField] = useState('')
    const [replyComment, setReplyComment] = useState('')
    const [editComment, setEditComment] = useState('')

    const changeCommentHandler = (e) => {
        setCommentField(e.target.value)
    };


    const [parentMessage, setParentMessage] = useState('')

    const addComment = () => {
        props.addComment(
            {
                message: commentField,
                program: props.path.includes('individual') ? null : props.programId,
                individualProgram: props.path.includes('individual') ? props.programId : null
            }
        ).then(() => {
            setCommentField('')
            props.fetchComments(props.programId, props.path.includes('individual') ? 'individual' : 'base')
        })

    }

    const addReplyComment = () => {
        props.addComment(
            {
                message: commentField,
                program: props.path.includes('individual') ? null : props.programId,
                parent: replyComment,
                individualProgram: props.programId
            }
        ).then(() => {
            setReplyComment('')
            setCommentField('')
            setParentMessage("")
            props.fetchComments(props.programId, props.path.includes('individual') ? 'individual' : 'base')
        })
    }

    const updateComment = () => {
        props.editComment(editComment, {
            message: commentField
        }).then(() => {
                setCommentField('')
                setEditComment('')
                props.fetchComments(props.programId, props.path.includes('individual') ? 'individual' : 'base')
            }
        )
    }


    const defineTypeCommentAndAdd = () => {
        if (replyComment) {
            addReplyComment()
        } else if (editComment) {
            updateComment()
        } else {
            addComment()
        }
    }

    useEffect(() => {
        props.fetchComments(props.programId, props.path.includes('individual') ? 'individual' : 'base')
    }, [props.programId])


    const setParentComment = (id, parentMessage) => {
        setReplyComment(id)
        setParentMessage(parentMessage.slice(0, 60))
    }

    const setEditComm = (id, text) => {
        setEditComment(id)
        setCommentField(text)
    }

    const deleteComm = (id) => {
        props.deleteComment(id).then( () =>
            props.fetchComments(props.programId, props.path.includes('individual') ? 'individual' : 'base')
        )
    }

    const printComments = () => {
        return props.comments.map(comm => {
            return <div className='Comment' key={comm._id}>
                <div className='Comment_head Comment_line'>{comm.author.name} {comm.author.surname}
                    <div className={'Comment_head_icons'}>
                        {props.user._id === comm.author._id ?
                            <IconButton
                                style={{
                                    color: 'white'
                                }}
                                href={'#comment'}
                                onClick={() => setEditComm(comm._id, comm.message)}> <EditIcon/>
                            </IconButton> : null}
                        {props.user._id === comm.author._id && props.user.role === 'Пациент' ?
                            <IconButton
                                style={{
                                    color: '#F21212'
                                }}
                                onClick={() => deleteComm(comm._id)}> <HighlightOffIcon/></IconButton> : null}

                        <IconButton className={'Comment_btn'} onClick={() => setParentComment(comm._id, comm.message)}
                                    href='#comment'> <ReplyIcon/> </IconButton>
                    </div>
                </div>
                <div className={'Comment_body'}>
                    {comm.message}
                    <div>{comm.parent ? <span className={'Comment_parent'}>
                        В ответ на
                        "<b>{comm.parent.author.name} {comm.parent.author.surname}:</b>
                        {comm.parent.message}"</span> : null}
                    </div>

                </div>
                <div className='Comment_bottom Comment_line'>
                    <div>
                        {props.user._id === comm.author._id ?
                            <IconButton
                                style={{
                                    color: 'white'
                                }}
                                href={'#comment'}
                                onClick={() => setEditComm(comm._id, comm.message)}> <EditIcon/>
                            </IconButton> : null}
                        {props.user._id === comm.author._id && props.user.role === 'Пациент' ?
                            <IconButton
                                style={{
                                    color: '#F21212'
                                }}
                                onClick={() => deleteComm(comm._id)}> <HighlightOffIcon/></IconButton> : null}

                        <IconButton className={'Comment_btn'} onClick={() => setParentComment(comm._id, comm.message)}
                                    href='#comment'> <ReplyIcon/> </IconButton>
                    </div>
                </div>

            </div>
        })
    }

    return (
        <>
            <div className="comments">
            {props.comments ? printComments() : null}
            </div>
            <TextField
                required
                id="comment"
                label={parentMessage ? `В ответ на "${parentMessage}..."` : "Напишите ваш комментарий"}
                multiline
                rowsMax={4}
                fullWidth
                value={commentField}
                onChange={changeCommentHandler}
                variant="outlined"
                margin={"normal"}
            />
            <Button variant={"contained"} color={'primary'} onClick={defineTypeCommentAndAdd}>Отправить
                комментарий</Button>
        </>
    )

}


export default withLoader(Comments, axios);