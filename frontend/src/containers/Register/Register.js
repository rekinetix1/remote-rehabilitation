import React, {Component} from "react";
import UsersApiService from "../../services/api/UsersApiService";
import {connect} from "react-redux";
import {Container} from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import {Link as RouterLink} from "react-router-dom";
import './Register.css'

class Register extends Component {
    state = {
        name: "",
        surname: "",
        email: "",
        city: "",
        password: "",
        displayName: "",
        avatarImage: ""
    };

    componentDidMount() {
        document.title = `Регистрация | Rekinetix`
    }

    inputChangeHandler = e => {
        this.setState({[e.target.name]: e.target.value});
    };

    submitFormHandler = e => {
        e.preventDefault();
        this.props.onUserRegistered(this.state);
    };


    getFieldError = fieldName => {
        return this.props.error && this.props.error.errors && this.props.error.errors[fieldName] && this.props.error.errors[fieldName].message;
    };


    render() {
        return (
            <div>
                <h2>Регистрация</h2>
                <form onSubmit={this.submitFormHandler}>
                    <Container maxWidth={'xs'}>
                        <div>
                            <TextField onChange={this.inputChangeHandler}

                                       margin="normal"
                                       name='name'
                                       fullWidth
                                       id="name"
                                       type='text'
                                       label="Введите ваше имя"
                                       helperText={this.getFieldError("name")}
                                       error={!!(this.props.error
                                           && this.props.error.errors
                                           && this.props.error.errors['name']
                                           && this.props.error.errors['name'].message)}

                            />

                        </div>
                        <div>
                            <TextField onChange={this.inputChangeHandler}
                                       margin="normal"
                                       name='surname'
                                       helperText={this.getFieldError("surname")}
                                       error={!!(this.props.error
                                           && this.props.error.errors
                                           && this.props.error.errors['surname']
                                           && this.props.error.errors['surname'].message)}

                                       fullWidth
                                       id="surname"
                                       type='text'
                                       label="Введите вашу фамилию"/>
                        </div>
                        <div>
                            <TextField onChange={this.inputChangeHandler}
                                       margin="normal"
                                       name='country'
                                       fullWidth
                                       helperText={this.getFieldError("country")}
                                       error={!!(this.props.error
                                           && this.props.error.errors
                                           && this.props.error.errors['country']
                                           && this.props.error.errors['country'].message)}

                                       id="country"
                                       type='text'
                                       label="Введите страну, в которой вы проживаете"/>
                        </div>
                        <div>
                            <TextField onChange={this.inputChangeHandler}
                                       helperText={this.getFieldError("city")}
                                       error={!!(this.props.error
                                           && this.props.error.errors
                                           && this.props.error.errors['city']
                                           && this.props.error.errors['city'].message)}
                                       margin="normal"
                                       name='city'
                                       fullWidth
                                       id="city"
                                       type='text'
                                       label="Введите город, в котором вы проживаете"/>
                        </div>
                        <div>
                            <TextField onChange={this.inputChangeHandler}
                                       helperText={this.getFieldError("email")}
                                       error={!!(this.props.error
                                           && this.props.error.errors
                                           && this.props.error.errors['email']
                                           && this.props.error.errors['email'].message)}
                                       margin="normal"
                                       name='email'
                                       fullWidth
                                       id="email"
                                       type='email'
                                       label="Введите вашу почту"/>
                        </div>
                        <div>
                            <TextField onChange={this.inputChangeHandler}
                                       margin="normal"
                                       name='password'
                                       helperText={this.getFieldError("password")}
                                       error={!!(this.props.error
                                           && this.props.error.errors
                                           && this.props.error.errors['password']
                                           && this.props.error.errors['password'].message)}
                                       fullWidth
                                       id="password"
                                       type='password'
                                       label="Введите пароль"/>
                        </div>
                        <div className='Register_submit'>
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                            >
                                Зарегистрироваться
                            </Button>
                        </div>
                        <Button component={RouterLink} color='primary' to="/login"> У меня уже есть аккаунт </Button>
                    </Container>
                </form>
            </div>
        );
    }
}


const mapStateToProps = state => {
    return {
        error: state.users.registerError
    }
};
const mapDispatchToProps = dispatch => {
    return {
        onUserRegistered: (userData, page) => dispatch(UsersApiService.registerUser(userData, page))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Register);
