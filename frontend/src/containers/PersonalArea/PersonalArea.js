import React, {useEffect, useState} from 'react';
import ProgramsApiService from "../../services/api/ProgramsApiService";
import FavoritesApiService from "../../services/api/FavoritesApiService";
import UserProgramsApiService from "../../services/api/UserProgramsApiService";
import IndividualProgramsApiService from "../../services/api/IndividualProgramsApiService";
import TransactionsApiService from "../../services/api/TransactionsApiService";
import OccupationApiService from "../../services/api/OccupationApiService";

import {connect} from "react-redux";

import {makeStyles} from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Grid from "@material-ui/core/Grid";
import Menu from '@material-ui/core/Menu';
import IconButton from '@material-ui/core/IconButton';
import MoreVertIcon from '@material-ui/icons/MoreVert';

import PersonalAreaPrograms from "./PersonalAreaPrograms/PersonalAreaPrograms";
import PersonalAreaFavorites from "./PersonalAreaFavorites/PersonalAreaFavorites";
import PersonalAreaQuestionnaire from "./PersonalAreaQuestionnaire/PersonalAreaQuestionnaire";
import PersonalAreaProfile from "./PersonalAreaProfile/PersonalAreaProfile";
import QuestionnariesUser from '../../services/api/QuestionarryApiServices';
import PhysicalActivitiesApiService from "../../services/api/PhysicalActivitiesApiService";
import PainsApiService from "../../services/api/PainsApiService";
import PersonalAreaAnswers from "./PersonalAreaAnswers/PersonalAreaAnswers";


function a11yProps(index) {
    return {
        id: `vertical-tab-${index}`,
        'aria-controls': `vertical-tabpanel-${index}`,
    };
}

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
        display: 'flex',
    },
    tabs: {
        borderRight: `1px solid ${theme.palette.divider}`,
    },
}));


const PersonalArea = (props) => {
    const classes = useStyles();
    const {match, history} = props;
    const {params} = match;
    const {page} = params;

    const tabNameToIndex = {
        0: "programs",
        1: "favorites",
        2: 'questionnaire',
        3: 'profile',
        4: 'answers'
    };

    const indexToTabName = {
        programs: 0,
        favorites: 1,
        questionnaire: 2,
        profile: 3,
        answers: 4
    };

    const [selectedTab, setSelectedTab] = React.useState(indexToTabName[page] || 0);

    const handleChange = (_event, newValue) => {
        history.push(`/personalArea/${tabNameToIndex[newValue]}`);
        setSelectedTab(newValue);
    };

    const handleChangeSmMenu = (_event, newValue) => {
        handleClose();
        history.push(`/personalArea/${tabNameToIndex[newValue]}`);
        setSelectedTab(newValue);
    };

    useEffect(() => {
        document.title = `Личный кабинет | Rekinetix`
    }, [])

    useEffect(() => {
        props.onFetchPrograms()
        props.fetchPhysicalActivities()
        props.fetchPains()
        props.fetchOccupation()
        props.onFetchQuestionnaire(props.user._id)
        setSelectedTab(indexToTabName[page] || 0)
    }, [page])

    const [anchorEl, setAnchorEl] = useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <div className={classes.root}>
            <Grid container direction="row"
                  justify="space-between"
                  spacing={2}>
                <div className="d-block d-sm-none fixed-top" style={{'marginTop': 80, right: 'auto', left: 20}}>
                    <IconButton aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
                        <MoreVertIcon/>
                    </IconButton>
                    <Menu
                        id="simple-menu"
                        anchorEl={anchorEl}
                        keepMounted
                        open={Boolean(anchorEl)}
                        onClose={handleClose}
                    >
                        <Tabs
                            value={selectedTab}
                            onChange={handleChangeSmMenu}
                            orientation="vertical"
                            variant="scrollable"
                            aria-label="Vertical tabs example"
                            className={classes.tabs}
                        >
                            <Tab label="Программы" {...a11yProps(0)}/>
                            <Tab label="Избранное" {...a11yProps(1)}/>
                            <Tab label="Анкета" {...a11yProps(2)}/>
                            <Tab label="Профиль" {...a11yProps(3)}/>
                            <Tab label="Ответы" {...a11yProps(4)}/>
                            <Tab label="Уведомления" {...a11yProps(5)}/>
                        </Tabs>
                    </Menu>
                </div>
                <Grid item md={3} xs={12} sm={4} className="d-none d-sm-block">
                    <Tabs
                        value={selectedTab}
                        onChange={handleChange}
                        orientation="vertical"
                        variant="scrollable"
                        aria-label="Vertical tabs example"
                        className={classes.tabs}
                    >
                        <Tab label="Программы" {...a11yProps(0)} />
                        <Tab label="Избранное" {...a11yProps(1)} />
                        <Tab label="Анкета" {...a11yProps(2)} />
                        getUserQuestionnaire <Tab label="Профиль" {...a11yProps(3)} />
                        <Tab label="Ответы" {...a11yProps(4)} />
                    </Tabs>
                </Grid>
                <Grid item md={9} xs={12} sm={6}>
                    {selectedTab === 0 && <PersonalAreaPrograms
                        userPrograms={props.userPrograms}
                        user={props.user}
                        fetchUserPrograms={props.onFetchUserPrograms}
                        programs={props.programs}
                        fetchIndividualPrograms={props.onFetchIndividualPrograms}
                        individualPrograms={props.individualPrograms}
                        onPostTransaction={props.onPostTransaction}
                        onUpdateIndividualProgram={props.onUpdateIndividualProgram}
                        onUpdateTransaction={props.onUpdateTransaction}/>}
                    {selectedTab === 1 && <PersonalAreaFavorites
                        fetchFavorites={props.onFetchFavorites}
                        removeFavorite={props.onDeleteFavorite}
                        addFavorite={props.onCreateFavorite}
                        user={props.user}
                        favorites={props.favorites}/>}
                    {selectedTab === 2 && <PersonalAreaQuestionnaire
                        getUserQuestionnaire={props.onFetchQuestionnaire}
                        submit={props.onCreateQuestionnaire}
                        physicalActivities={props.physicalActivity}
                        pains={props.pains}
                        occupation={props.occupation}
                        user={props.user}
                        hasQuestionnaire={props.userQuestionnaire ? true : false}
                        update={props.onUpdateQuestionnaire}
                    />
                    }
                    {selectedTab === 3 && <PersonalAreaProfile user={props.user}/>}
                    {selectedTab === 4 && <PersonalAreaAnswers user={props.user}/>}
                </Grid>
            </Grid>
        </div>
    );
}

const mapStateToProps = state => {
    return {
        questionnaires: state.questionnaires.questionnaires,
        userQuestionnaire: state.questionnaires.currentUserQuestionnaire,
        programs: state.programs.programs,
        favorites: state.favorites.favorites,
        user: state.users.user,
        userPrograms: state.userPrograms.userPrograms,
        individualPrograms: state.individualPrograms.individualPrograms,
        physicalActivity: state.physicalActivities.activities,
        pains: state.pains.pains,
        occupation: state.occupation.occupation
    }
};
const mapDispatchToProps = dispatch => {
    return {
        onCreateQuestionnaire: (data, url) => dispatch(QuestionnariesUser.createQuestionnaire(data, url)),
        onFetchQuestionnaire: (id) => dispatch(QuestionnariesUser.getUserQuestionnaire(id)),
        onUpdateQuestionnaire: (id, data) => dispatch(QuestionnariesUser.updateQuestionnaire(id, data)),
        onFetchUserPrograms: (id) => dispatch(UserProgramsApiService.fetchUserPrograms(id)),
        onFetchPrograms: () => dispatch(ProgramsApiService.fetchPrograms()),
        onFetchFavorites: () => dispatch(FavoritesApiService.fetchFavorites()),
        onDeleteFavorite: (id) => dispatch(FavoritesApiService.deleteFavorite(id)),
        onCreateFavorite: (favorite) => dispatch(FavoritesApiService.createFavorite(favorite)),
        onFetchIndividualPrograms: (userId) => dispatch(IndividualProgramsApiService.fetchIndividualPrograms(userId)),
        onUpdateIndividualProgram: (id, data) => dispatch(IndividualProgramsApiService.updateIndividualProgram(id, data)),
        onPostTransaction: (data) => dispatch(TransactionsApiService.postTransaction(data)),
        onUpdateTransaction: (id, data) => dispatch(TransactionsApiService.updateTransaction(id, data)),
        fetchPains: () => dispatch(PainsApiService.fetchPains()),
        fetchPhysicalActivities: () => dispatch(PhysicalActivitiesApiService.fetchPhysicalActivities()),
        fetchOccupation: () => dispatch(OccupationApiService.fetchOccupation())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(PersonalArea);

