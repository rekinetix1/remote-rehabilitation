import React, {useEffect} from 'react';
import axios from "../../../axios-api";
import {connect} from "react-redux";
import withLoader from "../../../hoc/withLoader";
import CommentsApiService from "../../../services/api/CommentsApiService";
import Button from "@material-ui/core/Button";
import {Link as RouterLink} from "react-router-dom";


const PersonalAreaAnswers = (props) => {
    useEffect(() => {
        props.onFetchAnswers()
    }, [])


    const printAnswers = () => {
        return props.comments.map(comm => {
            return <div className='Comment'>
                <div className='Comment_head'>{comm.author.name} {comm.author.surname}
                    <Button component={RouterLink}
                            to={comm.program ? `/programs/${comm.program}#comment` : `/programs/individual/${comm.individualProgram}`}
                            variant={'contained'}> Перейти к программе</Button>
                </div>`
                <div className={'Comment_body text-left'}>
                    {comm.message}
                    <div className={'Comment_parent'}>В ответ на ваш комментарий "{comm.parent.message}"</div>
                </div>
            </div>
        })
    }


    return (
        <>
            {props.comments.length > 0 ? printAnswers() : <div> Здесь будут отображаться ответы на ваши комментарии </div>}
        </>
    )

}


const mapStateToProps = state => {
    return {
        comments: state.comments.comments,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchAnswers: () => dispatch(CommentsApiService.fetchUserAnswers()),
    };
};


export default connect(mapStateToProps, mapDispatchToProps)((withLoader(PersonalAreaAnswers, axios)))