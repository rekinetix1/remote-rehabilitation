import React, {useEffect, useState} from 'react';
import Grid from "@material-ui/core/Grid";
import Program from "../../../components/Program/Program";
import Typography from "@material-ui/core/Typography";


const PersonalAreaFavorites = (props) => {
    useEffect(() => {
        if (props.user) {
            props.fetchFavorites()
        }
    }, [props.user._id])

    let printPrograms = () => {
        return props.favorites.map((favorite) => {
            return <Program
                key={favorite.program._id}
                category={favorite.program.category && favorite.program.category.title}
                title={favorite.program.title}
                id={favorite.program._id}
                removeFavorite={props.removeFavorite}
                addFavorite={props.addFavorite}
                image={favorite.program.image}
                short_description={favorite.program.short_description}
                cost={favorite.program.cost}
                user={props.user}
                favorite={favorite}
                width={200}
            />
        });
    };

    return (
        <>
            <Typography variant="h4" gutterBottom className="mb-5 pb-3">
               <h1><b>Избранное</b></h1>
            </Typography>
            <Grid container spacing={4}>
                { 
                    props.favorites.length > 0 ? printPrograms() : 
                    <h4 style={{margin: 20}}>В избранном пока ничего нет, но вы можете в добавить в него <a href="/">программу</a>, которая вам подходит. </h4>
                }
            </Grid>
        </>
    )

}


export default (PersonalAreaFavorites);
