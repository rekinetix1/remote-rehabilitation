import React, {useEffect, useState} from 'react';
import Button from "@material-ui/core/Button";
import makeStyles from "@material-ui/core/styles/makeStyles";
import EditQuestionnaireForm from "../../../components/PersonalArea/EditQuestionnaireForm/EditQuestionnaireForm";
import {withRouter} from "react-router";

const useStyles = makeStyles((theme) => ({
    formControl: {
        minWidth: 120,
        textAlign: 'left'
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));


const PersonalAreaQuestionnaire = (props) => {
    const classes = useStyles();
    const [userQuestionnaire, setUserQuestionnaire] = useState({
            occupation: '',
            gender: '',
            physicalActivity: "",
            weight: '',
            age: '',
            height: '',
            exacerbationsCountPerYear: '',
            painIntensity: '',
            pains: [],
            diagnosis: '',
            id: ''
        }
    )

    const [edit, setEdit] = useState(false);

    const editBtnHandler = () => {
        setEdit(true)
    }

    useEffect(() => {
        if (props.hasQuestionnaire){
            props.getUserQuestionnaire(props.user._id)
                .then(data => {
                    setUserQuestionnaire(
                        {
                            ...userQuestionnaire,
                            occupation: data.occupation,
                            gender: data.gender,
                            physicalActivity: data.physicalActivity,
                            weight: data.weight,
                            age: data.age,
                            height: data.height,
                            exacerbationsCountPerYear: data.exacerbationsCountPerYear,
                            painIntensity: data.painIntensity,
                            pains: data.pains,
                            diagnosis: data.diagnosis,
                            id: data._id
                        }
                    );
                })
            }
    }, [])

    const inputChangeHandler = e => {
        setUserQuestionnaire({...userQuestionnaire, [e.target.name]: e.target.value});
    };

    const checkboxHandler = (e) => {
        const arr = userQuestionnaire.pains ? [...userQuestionnaire.pains] : [];
        console.log(arr);
        if (e.target.checked) {
            arr.push(e.target.id);
            setUserQuestionnaire({
                ...userQuestionnaire,
                pains: arr
            });
        } else {
            for (let i = arr.length - 1; i >= 0; i--) {
                if (arr[i] === e.target.id) {
                    arr.splice(i, 1);
                }
            }
            setUserQuestionnaire({
                ...userQuestionnaire,
                pains: arr
            });
        }
    }

    const submitHandler = (e, data) => {
        e.preventDefault()
        if (props.hasQuestionnaire) {
            props.update(userQuestionnaire.id, data)
        } else {
            props.submit(data)
                .then(data => {
                    console.log(props.history);
                    props.history.go(-2);
                })
        }
        setEdit(false)
    }

    console.log(userQuestionnaire);

    let physicalActivity;
    let pains = [];
    let occupation;
    if (props.hasQuestionnaire){
        physicalActivity = props.physicalActivities.find(activity => activity._id === userQuestionnaire.physicalActivity);
        occupation = props.occupation.find(occ => occ._id === userQuestionnaire.occupation);
    
        if (userQuestionnaire.pains){
            for (let i = 0; i < userQuestionnaire.pains.length; i++){
                props.pains.forEach(pain => {
                    if (userQuestionnaire.pains[i] === pain._id){
                        pains.push(pain)
                    }
                })
            }
        }
    }

    return (
        <div>
            <h1 className=""><b>Анкета</b></h1>
            {
                !props.hasQuestionnaire ? 
                <EditQuestionnaireForm
                    classes={classes}
                    submitHandler={submitHandler}
                    userQuestionnaire={userQuestionnaire}
                    occupation={props.occupation}
                    pains={props.pains}
                    physicalActivities={props.physicalActivities}
                    inputChangeHandler={inputChangeHandler}
                    checkboxHandler={checkboxHandler}
                /> :
                edit ?
                <EditQuestionnaireForm
                    classes={classes}
                    edit={true}
                    submitHandler={submitHandler}
                    userQuestionnaire={userQuestionnaire}
                    occupation={props.occupation}
                    pains={props.pains}
                    userPains={userQuestionnaire.pains}
                    physicalActivities={props.physicalActivities}
                    inputChangeHandler={inputChangeHandler}
                    checkboxHandler={checkboxHandler}
                />
                :
                <div className="container pl-0 pl-md-5 ml-0 ml-md-5 text-left">
                    <div className="mb-3">
                        <div className="font-weight-bold"><span
                            className="font-weight-normal"> Возраст:</span> {userQuestionnaire.age}</div>
                        <div className="font-weight-bold"><span
                            className="font-weight-normal"> Пол:</span> {userQuestionnaire.gender}</div>
                        <div className="font-weight-bold"><span
                            className="font-weight-normal"> Физическая активность:</span> {physicalActivity && physicalActivity.title}
                        </div>
                        <div className="font-weight-bold"><span
                            className="font-weight-normal"> Род деятельности:</span> {occupation && occupation.title}
                        </div>
                        <div className="font-weight-bold"><span
                            className="font-weight-normal"> Вес:</span> {userQuestionnaire.weight} кг
                        </div>
                        <div className="font-weight-bold"><span
                            className="font-weight-normal"> Рост:</span> {userQuestionnaire.height} см
                        </div>
                        <div className="font-weight-bold"><span
                            className="font-weight-normal"> Боли:</span> 
                            {
                                <ul>
                                    {
                                        pains.map(option => {
                                            return <li key={option._id}>
                                                {option.title}
                                            </li>
                                        })
                                    }
                                </ul>
                            } 
                        </div>
                        <div className="font-weight-bold"><span
                            className="font-weight-normal"> Медицинский диагноз:</span> {userQuestionnaire.diagnosis}
                        </div>
                    </div>
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={editBtnHandler}
                    >Редактировать анкету </Button>
                </div>
            }


        </div>
    )

}

export default withRouter(PersonalAreaQuestionnaire)
