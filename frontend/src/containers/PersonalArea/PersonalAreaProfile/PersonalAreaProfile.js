import React, {useState} from 'react';
import EditUserForm from "../../../components/PersonalArea/EditUserForm/EditUserForm";
import UsersApiService from "../../../services/api/UsersApiService";
import {connect} from "react-redux";
import {Link as RouterLink} from "react-router-dom";
import {Button} from "@material-ui/core";


const PersonalAreaProfile = (props) => {
    const [edit, setEdit] = useState(false)
    const user = props.user;


    const [fields, setFields] = useState({
        name: user.name,
        surname: user.surname,
        email: user.email,
        city: user.city
    });

    const changeInputHandler = (e) => {
        setFields({
            ...fields,
            [e.target.name]: e.target.value
        });
    };

    const submitHandler = (e) => {
        e.preventDefault()
        props.onUpdateUser(user._id, fields)
        setEdit(false)
    }

    const editBtnHandler = () => {
        setEdit(true)
    }

    return (
        <>
            <h1 className="mb-5 container pl-5 ml-5 text-left"><b>Профиль</b></h1>
            {edit ? <EditUserForm submitFormHandler={submitHandler} changeInputHandler={changeInputHandler}
                                  user={fields}/> :
                <div className="container pl-0 pl-md-5 ml-0 ml-md-5 text-left">

                    <div className="mb-4 text-left">
                        <h5 className="text-left"><b className="text-black-50 text-left"> Имя:</b> {user.name} </h5>
                        <h5 className="text-left"><b className="text-black-50 text-left"> Фамилия:</b> {user.surname} </h5>
                        <h5 className="text-left"><b className="text-black-50 text-left"> Почта: </b> {user.email}</h5>
                        <h5 className="text-left"><b className="text-black-50 text-left"> Город: </b> {user.city} </h5>
                    </div>
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={editBtnHandler}
                        className="mt-2"
                    >Редактировать профиль </Button>
                    <Button
                        variant="contained"
                        color="primary"
                        component={RouterLink}
                        className="ml-0 mt-2 ml-md-5"
                        to={`/changePassword/${props.userId.user._id}`}
                    >Изменить пароль </Button>
                </div>
            }
        </>
    )

}

const mapStateToProps = (state) => {
    return {
        loading: state.users.loading,
        userId: state.users
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        onUpdateUser: (id, data) => dispatch(UsersApiService.updateUserProfile(id, data))
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(PersonalAreaProfile);