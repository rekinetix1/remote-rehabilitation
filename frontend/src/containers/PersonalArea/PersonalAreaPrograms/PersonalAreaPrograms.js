import React, {useEffect, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import config from "../../../config";
import {Link as RouterLink} from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import Pay from "../../../components/ Widgets/CloudPayments/CloudPayments";
import ModalWindow from "../../../components/UI/ModalWindow/ModalWindow";
import CheckCircleOutlineIcon from "@material-ui/icons/CheckCircleOutline";

const useStyles = makeStyles({
    root: {
        maxWidth: 380,
        textAlign: 'left'
    },
    media: {
        height: 230,
    },
});

const PersonalAreaPrograms = (props) => {
    const classes = useStyles();

    const [userPrograms , setUserPrograms] = useState([])
    const [individualPrograms, setIndividualPrograms] = useState([]);
    const [payment, setPayment] = useState(false);
    const [showModal, setShowModal] = useState(false);
    const [paymentInfo, setPaymentInfo] = useState({});


    useEffect(() => {
       props.fetchUserPrograms(props.user._id);
        props.fetchIndividualPrograms(props.user._id)
    }, [props.user._id])

    useEffect(() => {
        setUserPrograms(props.userPrograms);
    }, [props.userPrograms]);

    useEffect(() => {
        setIndividualPrograms(props.individualPrograms)
    }, [props.individualPrograms]);

    const showModalHandler = () => {
        console.log("show modal");
        setShowModal(true)
    }

    const closeModalHandler = () => {
        setShowModal(false)
    }

    const buyHandler = (program) => {
        showModalHandler();
        setPaymentInfo(program);
    }

    const startPaymant = (program) => {
        props.onPostTransaction(
            {
                user: program.user._id,
                individualProgram: program._id,
                cost: program.cost
            }
        ).then(data => {
            Pay({
                program: program,
                totalCost: program.cost,
                user: program.user,
                currentTransaction: data,
                makePayment: () => setPayment(true),
                individual: true,
                onUpdateIndividualProgram: props.onUpdateIndividualProgram,
                onUpdateTransaction: props.onUpdateTransaction
            });
        });

    }

    const printIndividualPrograms = () => {
        
        return individualPrograms.map(program => {
            
            let image = "http://via.placeholder.com/300x200";
            if (program.image) {
                image = config.apiURL + "/uploads/" + program.image;
            }

            return (
                    <Grid key={program._id} item md={4} xs={12} sm={6}>
                        <Card className={classes.root}>
                            <CardActionArea component={RouterLink} to={`/programs/individual/${program._id}`}>
                                <CardMedia
                                    className={classes.media}
                                    image={image}
                                    title="Contemplative Reptile"
                                />
                                <CardContent>
                                    <Typography gutterBottom variant="body2" color="textSecondary" component="p">
                                        Индивидуальная программа
                                    </Typography>
                                    <Typography gutterBottom variant="h6" component="h5">
                                        {program.title}
                                    </Typography>
                                </CardContent>
                            </CardActionArea>
                            <CardActions>
                                {
                                    program.paymentStatus === "Оплачено" ?
                                    <Button fullWidth component={RouterLink} to={`/programs/individual/${program._id}`} color='default' variant="contained"
                                    > 
                                        Заниматься 
                                    </Button> :
                                    <Button fullWidth onClick={() => buyHandler(program)} color='secondary' variant="contained"
                                    > 
                                        Оплатить 
                                    </Button>
                                }
                                
                            </CardActions>
                        </Card>
                    </Grid>
                
            )
        })
    };

    const printBasePrograms = () => {
        return userPrograms.map(p => {
            console.log(p);
            let program = p.program
            let image = "http://via.placeholder.com/300x200";
            if (program.image) {
                image = config.apiURL + "/uploads/" + program.image;
            }
            return (<Grid key={program._id} item md={4} xs={12} sm={6}>
                <Card className={classes.root + ' h-100 d-flex flex-column justify-content-between'}>
                    <CardActionArea component={RouterLink} to={'/programs/' + program._id}>
                        <CardMedia
                            className={classes.media}
                            image={image}
                            title="Contemplative Reptile"
                        />
                        <CardContent>
                            <Typography gutterBottom variant="body2" color="textSecondary" component="p">
                                Базовая программа
                            </Typography>
                            <Typography gutterBottom variant="h6" component="h5">
                                {program.title}
                            </Typography>
                        </CardContent>
                    </CardActionArea>
                    <CardActions>
                            <Button fullWidth component={RouterLink} to={`/programs/${program._id}`} color='default' variant="contained"> 
                                Заниматься 
                            </Button> 
                    </CardActions>
                </Card>
            </Grid>)
        })
    }

    return (
        <>
            <Typography variant="h4" gutterBottom className="mb-5 pb-3">
               <b>Программы</b>
            </Typography>
            <Grid container spacing={3}>
                {printIndividualPrograms()}
            </Grid>
            <Grid container spacing={3}>
                {printBasePrograms()}
            </Grid>
            {
                showModal ? 
            
            <ModalWindow
                        show={showModal}
                        closed={closeModalHandler}
                    >
                        { 
                            payment ?
                            <div className="text-center mt-5"><CheckCircleOutlineIcon style={{color: 'green', fontSize: '150px'}}/>
                                <h4><b>Программа куплена! </b></h4>
                                <Button size="large"
                                        variant="outlined"
                                        color="primary"
                                        onClick={closeModalHandler}
                                > 
                                    Перейти к программам 
                                </Button>
                            </div>
                            : <div style={{
                                    display: 'flex', 
                                    flexDirection: 'column',
                                    justifyContent: 'space-between',
                                    textAlign: 'left',
                                    height: '340px'
                                }}>
                                <h5 className='mb-3'>Вы покупаете индивидуальную программу <b>
                                    "{paymentInfo.title}"</b></h5>
                                <div>Назначена на: {paymentInfo && paymentInfo.user.name} {paymentInfo && paymentInfo.user.surname}</div>
                                <div>Количество упражнений в программе: {paymentInfo.exercises_count}</div>
                                <div>Цена программы: {paymentInfo.cost} ₸</div>
                                <div>
                                    <Button className='mr-2' onClick={() => startPaymant(paymentInfo)}
                                            color='primary'
                                            size='large'
                                            variant="contained"> Оплатить! </Button>
                                    <Button onClick={closeModalHandler} color='secondary' size='large'
                                            variant="contained"> Закрыть окно </Button>
                                </div>
                        </div>
                        }
            </ModalWindow> : null
            }
        </>
    );
}


export default (PersonalAreaPrograms);

