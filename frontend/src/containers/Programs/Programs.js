import React, {useEffect, useState} from 'react';
import Program from "../../components/Program/Program";
import ProgramsApiService from "../../services/api/ProgramsApiService";
import FavoritesApiService from "../../services/api/FavoritesApiService";
import ProgramCategoriesApiService from "../../services/api/ProgramCategoriesApiService";
import UserProgramsApiService from "../../services/api/UserProgramsApiService";
import {connect} from "react-redux";
import Container from "@material-ui/core/Container";
import {TextField, FormControl, Grid, Select, MenuItem, InputLabel} from "@material-ui/core";
import withLoader from "../../hoc/withLoader";
import axios from "../../axios-api";
import QuestionnariesUser from "../../services/api/QuestionarryApiServices";

const Programs = (props) => {
    const [filter, setFilter] = useState({
        category: null,
        complexity: null,
        lowCost: null,
        upCost: null
    });

    useEffect(() => {
        document.title = `Rekinetix`
        props.onFetchPrograms();
        props.onFetchProgramCategories();
        if (props.user) {
            props.onFetchQuestionnaire(props.user._id)
            props.onFetchFavorites()
            props.onFetchUserPrograms(props.user._id)
        }
    }, []);

    const inputChangeHandler = (e) => {
        setFilter({
            ...filter,
            [e.target.name]: e.target.value
        });
    };
    
    let printPrograms = (programs) => {
        return programs.map((program) => {
            return  <Program
                        key={program._id}
                        category={program.category && program.category.title}
                        title={program.title}
                        id={program._id}
                        image={program.image}
                        short_description={program.short_description}
                        cost={program.cost}
                        user={props.user}
                        favorite={props.favorites.find(favorite => favorite.program._id === program._id)}
                        userProgram={props.userPrograms.find(userProgram => userProgram.program._id === program._id)}
                        removeFavorite={props.onDeleteFavorite}
                        addFavorite={props.onCreateFavorite}
            />
        });
    };

    let programs = props.programs;

    if (filter.category){
        programs = programs.filter(program => filter.category === program.category._id);
        if (filter.complexity){
            programs = programs.filter(program => filter.complexity === program.complexity);
        }
        if (filter.lowCost){
            programs = programs.filter(program => filter.lowCost <= program.cost);
        }
        if (filter.upCost){
            programs = programs.filter(program => filter.upCost >= program.cost);
        }
    } else if (filter.complexity){
        programs = programs.filter(program => filter.complexity === program.complexity);
        if (filter.category){
            programs = programs.filter(program => filter.category === program.category);
        }
        if (filter.lowCost){
            programs = programs.filter(program => filter.lowCost <= program.cost);
        }
        if (filter.upCost){
            programs = programs.filter(program => filter.upCost >= program.cost);
        }
    } else if (filter.lowCost){
        programs = programs.filter(program => filter.lowCost <= program.cost);
        if (filter.category){
            programs = programs.filter(program => filter.category === program.category);
        }
        if (filter.complexity){
            programs = programs.filter(program => filter.complexity === program.complexity);
        }
        if (filter.upCost){
            programs = programs.filter(program => filter.upCost >= program.cost);
        }
    } else if (filter.upCost){
        programs = programs.filter(program => filter.upCost >= program.cost);
        if (filter.category){
            programs = programs.filter(program => filter.category === program.category);
        }
        if (filter.complexity){
            programs = programs.filter(program => filter.complexity === program.complexity);
        }
        if (filter.lowCost){
            programs = programs.filter(program => filter.lowCost <= program.cost);
        }
    }


    return (
        <>
            <Container maxWidth='lg'>
                <div>
                    <Grid container spacing={6} className='p-1'>
                        <Grid item  md={4} xs={12} sm={6} className='m-0 p-0 p-md-2  p-lg-4'>
                            <FormControl fullWidth className=''>
                                <InputLabel id="side_select">Категория</InputLabel>
                                <Select
                                    onChange={inputChangeHandler}
                                    name='category'
                                    labelId="side_select"
                                    id="side"
                                    defaultValue=""
                                >
                                <MenuItem value={undefined}>Не выбрано</MenuItem>
                                {
                                    props.programCategories.map((category) => {
                                        return <MenuItem value={category._id} key={category._id}>{category.title} </MenuItem>
                                        })
                                }
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item md={4} xs={12} sm={6} className='m-0 p-0 p-md-2 p-lg-4'>
                            <FormControl fullWidth className=''>
                                <InputLabel id="complexity_select">Сложность</InputLabel>
                                <Select
                                    onChange={inputChangeHandler}
                                    name='complexity'
                                    labelId="complexity_select"
                                    id="complexity"
                                    defaultValue=""
                                >
                                    <MenuItem value={undefined}>Не выбрано</MenuItem>
                                    <MenuItem value={'Низкая'}>Низкая</MenuItem>
                                    <MenuItem value={'Средняя'}>Средняя</MenuItem>
                                    <MenuItem value={'Тяжелая'}>Тяжелая</MenuItem>
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item md={2} xs={12} sm={6} className='m-0 p-0 p-md-2 p-lg-4'>
                            <TextField
                                onChange={inputChangeHandler}
                                // margin="normal"
                                name='lowCost'
                                id="lowCost"
                                fullWidth
                                type='number'
                                defaultValue=""
                                label="Цена от"/>
                        </Grid>
                        <Grid item md={2} xs={12} sm={6} className='m-0 p-0 p-md-2 p-lg-4'>
                            <TextField
                                onChange={inputChangeHandler}
                                // margin="normal"
                                name='upCost'
                                id="upCost"
                                fullWidth
                                type='number'
                                defaultValue=""
                                label="Цена до"/>
                        </Grid>
                    </Grid>
                    <hr></hr>
                </div>
                <Grid container spacing={3} className='d-flex align-items-stretch'>
                    {printPrograms(programs)}
                </Grid>
            </Container>
        </>
    );
}

const mapStateToProps = state => {
    return {
        user: state.users.user,
        programs: state.programs.programs,
        loading: state.programs.loading,
        favorites: state.favorites.favorites,
        userPrograms: state.userPrograms.userPrograms,
        error: state.programs.error,
        programCategories: state.programCategories.programCategories
    }
  };
  const mapDispatchToProps = dispatch => {
    return {
      onFetchPrograms: () => dispatch(ProgramsApiService.fetchPrograms()),
      onFetchFavorites: () => dispatch(FavoritesApiService.fetchFavorites()),
      onFetchQuestionnaire: (id) => dispatch(QuestionnariesUser.getUserQuestionnaire(id)),
      onFetchUserPrograms: (id) => dispatch(UserProgramsApiService.fetchUserPrograms(id)),
      onDeleteFavorite: (id) => dispatch(FavoritesApiService.deleteFavorite(id)),
      onCreateFavorite: (favorite) => dispatch(FavoritesApiService.createFavorite(favorite)),
      onFetchProgramCategories: () => dispatch(ProgramCategoriesApiService.fetchProgramCategories())
    };
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(withLoader(Programs, axios))


