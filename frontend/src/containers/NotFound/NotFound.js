import React from "react";

const NotFound = () => {
    return (
        <div className="text-center m-5 p-5">
            <h1>Такой страницы не существует</h1>
        </div>
    );
};

export default NotFound;