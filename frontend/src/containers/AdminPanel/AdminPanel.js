import React, {useEffect, useState} from "react";
import {makeStyles} from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import {Link} from "react-router-dom";
import './AdminPanel.css'
import {Container} from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import {connect} from "react-redux";

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
}));

let crudLinks = [
    {
        title: "Пользователи",
        link: "/users"
    },
    {
        title: "Транзакции",
        link: "/transactions"
    },
    {
		title: "Индивидуальные программы",
		link: "/individual_programs"
    },
    {
        title: "Программы пациентов",
        link: "user_programs"
    },
    {
        title: "Программы",
        link: "/programs_panel"
    },
    {
        title: 'История просмотров',
        link: 'browsing_history'
    },
    {
        title: "Категории программ",
        link: "/program-categories"
    },
    {
        title: "Опции",
        link: "/options"
    },
    {
        title: "Упражнения",
        link: "/exercises"
    },
    {
        title: "Инвентарь",
        link: "/equipment"
    },
    {
        title: "Mышцы",
        link: "/muscles"
    },
    {
        title: "Области применения упражнений",
        link: "/exercise-categories"
    },
    {
        title: "Род деятельности",
        link: "/occupation"
    },
    {
        title: "Физическая активность",
        link: "/activities"
    },
    {
        title: "Жалобы",
        link: "/pains"
    },
    {
        title: "Комментарии",
        link: "/comments"
    },
];




const AdminPanel = props => {
    const classes = useStyles();

    if (props.user && props.user.role === "Автор БП"){
        crudLinks = [
            {
                title: "Пользователи",
                link: "/users"
            },
            {
                title: "Транзакции",
                link: "/transactions"
            },
            {
                title: "Программы пациентов",
                link: "user_programs"
            },
            {
                title: "Программы",
                link: "/programs_panel"
            },
            {
                title: 'История просмотров',
                link: 'browsing_history'
            },
            {
                title: "Упражнения",
                link: "/exercises"
            },
            {
                title: "Комментарии",
                link: "/comments"
            }
        ];
    } else if (props.user && props.user.role === "Терапевт"){
        crudLinks = [
            {
                title: "Пользователи",
                link: "/users"
            },
            {
                title: "Транзакции",
                link: "/transactions"
            },
            {
                title: "Индивидуальные программы",
                link: "/individual_programs"
            },
            {
                title: "Программы пациентов",
                link: "user_programs"
            },
            {
                title: "Программы",
                link: "/programs_panel"
            },
            {
                title: 'История просмотров',
                link: 'browsing_history'
            },
            {
                title: "Упражнения",
                link: "/exercises"
            },
            {
                title: "Комментарии",
                link: "/comments"
            },
        ];
    } else if (props.user && props.user.role === "Модератор"){
        crudLinks = [
            {
                title: "Пользователи",
                link: "/users"
            },
            {
                title: "Индивидуальные программы",
                link: "/individual_programs"
            },
            {
                title: "Программы",
                link: "/programs_panel"
            },
            {
                title: 'История просмотров',
                link: 'browsing_history'
            },
            {
                title: "Упражнения",
                link: "/exercises"
            },
            {
                title: "Комментарии",
                link: "/comments"
            },
        ]
    } else if (props.user && props.user.role === "Менеджер") {
        crudLinks = [
            {
                title: "Пользователи",
                link: "/users"
            },
            {
                title: "Транзакции",
                link: "/transactions"
            },
            {
                title: "Индивидуальные программы",
                link: "/individual_programs"
            },
            {
                title: "Базовые программы пациентов",
                link: "user_programs"
            },
            {
                title: "Программы",
                link: "/programs_panel"
            },
            {
                title: 'История просмотров',
                link: 'browsing_history'
            },
            {
                title: "Упражнения",
                link: "/exercises"
            },
            {
                title: "Комментарии",
                link: "/comments"
            },
        ];
    }

    const adminPanelLinks = crudLinks.map(item => {
        return (
            <div key={item.title}>
                <Link to={`${item.link}`} key={item.title}>
                    <ListItem className='AdminPanel_link' button>
                        <ListItemText primary={item.title}/>
                    </ListItem>
                </Link>
                <Divider/>
            </div>
        )
    })

    const [open, setOpen] = useState(true)

    const openBtnHandler = () => {
        setOpen(!open)
    }

    useEffect(() => {
        document.title = `Панель администратора | Rekinetix`
    }, [])

    return (
        <>
            <Container maxWidth='lg' className={props.admin_page ? "d-none d-sm-block" : ""}>
                {props.closable ?
                    <div className="AdminPanel_arrow_wrap">
                        <IconButton onClick={openBtnHandler} className='AdminPanel_arrow'>
                            {open ? <span> ᐸ   </span> : <span> ᐳ </span>}
                        </IconButton>
                    </div>
                    : null}
                <List component="nav"
                    style={{
                        transition: '0.3s',
                        transform: open ? 'translateX(0)' : 'translateX(-300px)',
                        width: open ? '100%' : '0',
                    }}
                    className={classes.root} aria-label="mailbox folders">
                    {adminPanelLinks}
                </List>
            </Container>
        </>
    )
};

const mapStateToProps = state => {
    return {
        user: state.users.user
    }
};

export default connect(mapStateToProps)(AdminPanel);