import React from 'react';

export default class ErrorBoundary extends React.Component {
	state = {
		hasError: false
	}

	componentDidCatch(error, errorInfo) {
		this.setState({hasError: true})
	}

	render() {
		if (this.state.hasError) return <div>Что-то пошло не так! Обновите страницу или попробуйте зайти позже</div>;

		return this.props.children;
	}
}
