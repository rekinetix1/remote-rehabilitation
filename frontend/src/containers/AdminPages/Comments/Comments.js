import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import '../AdminPages.scss';
import Breadcrumbs from "../../../components/UI/Breadcrumbs";
import AdminPanel from "../../AdminPanel/AdminPanel";
import CommentsApiService from "../../../services/api/CommentsApiService";
import CommentsTable from "../../../components/CommentsTable/CommentsTable";
import axios from "../../../axios-api";

const Comments = (props) => {
    useEffect(() => {
        document.title = `Комментарии | Rekinetix`
        props.onFetchComments()
    }, []);

    const rowsItem = [
        {title: 'datetime', width: '15%'},
        {title: 'program', width: '25%'},
        {title: 'message', width: '10%'},
    ]

    return (
        <div className="AdminPage">
            <Breadcrumbs current="Комментарии"/>
            <h2 className="m-auto align-left">Комментарии</h2>
            <div className="pt-3 pb-4"></div>
            <div className=' w-100 AdminPage_flexbox'>
                <div className='AdminPage_panel'>
                    <AdminPanel closable admin_page={true}/>
                </div>
                <div className='w-100'>
                    <CommentsTable
                        columns={CommentsApiService.columns}
                        rows={props.comments}
                        route={'browsing_history'}
                        hideIcons={true}
                        onFetchComments={props.onFetchComments}
                        addReplyComment={(comm) => props.onAddReplyComment(comm)}
                        rowsItem={rowsItem}
                    />

                </div>
            </div>

        </div>
    )
};

const mapStateToProps = (state) => {
    return {
        browsingHistory: state.browsingHistory.browsingHistory,
        comments: state.comments.comments,
        loading: state.browsingHistory.loading,
        error: state.browsingHistory.error
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        onFetchComments: () => dispatch(CommentsApiService.fetchComments()),
        onAddReplyComment: (comm) => dispatch(CommentsApiService.addReplyComment(comm))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Comments);