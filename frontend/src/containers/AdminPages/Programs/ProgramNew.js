import React, {Component} from "react";
import {connect} from "react-redux";
import ProgramForm from "../../../components/ProgramForm/ProgramForm"
import UsersApiService from "../../../services/api/UsersApiService";
import ProgramCategoriesApiService from "../../../services/api/ProgramCategoriesApiService";
import ProgramsApiService from "../../../services/api/ProgramsApiService";
import ExercisesApiService from "../../../services/api/ExercisesApiService"
import Breadcrumbs from "../../../components/UI/Breadcrumbs/Breadcrumbs";

class ProgramNew extends Component {
  componentDidMount() {
    document.title = `Создание программы | Rekinetix`
  }

  render() {
    let moderators = this.props.moderators ? this.props.moderators : []
    return (
      <>
        <Breadcrumbs
          current="Создание программы"
          prevLink="programs_panel"
          prevPage="Программы"
        />
        <ProgramForm
          edit={false}
          moderators={moderators}
          programCategories={this.props.programCategories}
          exercises={this.props.exercises}
          program={this.props.program}
          user={this.props.user}
          onCreateProgram={this.props.onCreateProgram}
          onFetchProgramCategory={this.props.onFetchProgramCategory}
          onfetchExercises={this.props.onfetchExercises}
          onfetchUsers={this.props.onfetchUsers}
          error={this.props.errorProgam}
        />
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    error: state.users.loginError,
    errorProgam: state.programs.error,
    programCategories: state.programCategories.programCategories,
    moderators: state.users.users,
    exercises: state.exercises.exercises,
    user: state.users.user,
  }
};
const mapDispatchToProps = dispatch => {
  return {
    onFetchProgramCategory: () => dispatch(ProgramCategoriesApiService.fetchProgramCategories()),
    onfetchUsers: (role) => dispatch(UsersApiService.fetchUsers(role)),
    onCreateProgram: (program) => dispatch(ProgramsApiService.createProgram(program)),
    onfetchExercises: () => dispatch(ExercisesApiService.fetchExercises()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProgramNew);
