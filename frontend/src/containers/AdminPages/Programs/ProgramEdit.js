import React, {Component} from "react";
import {connect} from "react-redux";
import ProgramForm from "../../../components/ProgramForm/ProgramForm"
import UsersApiService from "../../../services/api/UsersApiService";
import ProgramCategoriesApiService from "../../../services/api/ProgramCategoriesApiService";
import ProgramsApiService from "../../../services/api/ProgramsApiService";
import ExercisesApiService from "../../../services/api/ExercisesApiService"
import Breadcrumbs from "../../../components/UI/Breadcrumbs/Breadcrumbs";

class ProgramEdit extends Component {
  componentDidMount() {
    document.title = `Редактирование программы | Rekinetix`
  }

  render() {
    let moderators = this.props.moderators ? this.props.moderators : []
    return (
      <>
        <Breadcrumbs
          current="Изменение программы"
          prevLink="programs_panel"
          prevPage="Программы"
        />
        <ProgramForm
          edit={true}
          moderators={moderators}
          programCategories={this.props.programCategories}
          exercises={this.props.exercises}
          program={this.props.program}
          user={this.props.user}
          onUpdateProgram={this.props.onUpdateProgram}
          onFetchSingleProgram={this.props.onFetchSingleProgram}
          id={this.props.match.params.id}
          onFetchProgramCategory={this.props.onFetchProgramCategory}
          onfetchExercises={this.props.onfetchExercises}
          onfetchUsers={this.props.onfetchUsers}
          error={this.props.error}
        />
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    error: state.users.loginError,
    programCategories: state.programCategories.programCategories,
    moderators: state.users.users,
    exercises: state.exercises.exercises,
    user: state.users.user,
    program: state.programs.currentProgram
  }
};
const mapDispatchToProps = dispatch => {
  return {
    onFetchProgramCategory: () => dispatch(ProgramCategoriesApiService.fetchProgramCategories()),
    onfetchUsers: (role) => dispatch(UsersApiService.fetchUsers(role)),
    onUpdateProgram: (id, program) => dispatch(ProgramsApiService.updateProgram(id, program)),
    onfetchExercises: () => dispatch(ExercisesApiService.fetchExercises()),
    onFetchSingleProgram: (id) => dispatch(ProgramsApiService.fetchSingleProgram(id)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProgramEdit);