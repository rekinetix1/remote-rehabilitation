import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import ProgramsApiService from "../../../services/api/ProgramsApiService";
import AdminTable from "../../../components/AdminTable";
import {Link} from "react-router-dom";
import Button from "@material-ui/core/Button";
import '../AdminPages.scss';
import AdminPanel from "../../AdminPanel/AdminPanel";
import Breadcrumbs from "../../../components/UI/Breadcrumbs/Breadcrumbs";
import SearchForm from "../../../components/UI/SearchForm/SearchForm";

const Equipment = ({programs, onFetchPrograms, onDeleteProgram, history, location, onSearchPrograms, user}) => {
  const [search, setSearch] = useState(null);

  useEffect(() => {
    document.title = `Программы | Rekinetix`
    if (location.search) {
      const query = new URLSearchParams(location.search);
      for (let param of query.entries()) {
        setSearch(param[1])
        onSearchPrograms(param[1])
      }
    } else {
      setSearch(null)
      onFetchPrograms();
    }
  }, [location.search]);

  const rowsItem = [
    {title: 'title', width: '30%'},
    {title: 'description', width: '60%'},
  ];

  let hideEditIcon = false;
  if (user.role === "Терапевт" || user.role === "Модератор" || user.role === "Менеджер") hideEditIcon = true;

  return (
    <div className="AdminPage">
      <Breadcrumbs current="Программы" />
      <h2 className="m-auto pb-4">Программы</h2>
      <div className="justify-content-end add-search-block">
        <div className="search">
          <SearchForm
            route="programs_panel"
            history={history}
            placeholder="поиск по названию"
            search={search}/>
        </div>
        <div className="add_record">
          <Link
            to="/programs/new"
            style={{
              color: "inherit",
              textDecoration: "none"
            }}
          >
            {
              hideEditIcon ? null :
              <Button variant="contained" color="primary" className="ml-3">+ Добавить</Button> 
            }
          </Link>
        </div>
      </div>
      <div className='AdminPage_flexbox'>
          <div className='AdminPage_panel'>
              <AdminPanel closable admin_page={true}/>
          </div>
          <AdminTable
            columns={ProgramsApiService.columns}
            rows={programs.map((program) => (
                {
                    _id:         program._id,
                    title:       program.title,
                    description: program.short_description
                }
            ))}
            route={'programs'}
            deleteRow={onDeleteProgram}
            hideEditIcon={hideEditIcon ? true : false}
            hideDeleteIcon={user.role !== "Администратор" ? true: false}
            rowsItem={rowsItem}
          />
      </div>
    </div>
  )
};

const mapStateToProps = (state) => {
  return {
    programs: state.programs.programs,
    loading: state.programs.loading,
    error: state.programs.error,
    user: state.users.user
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    onFetchPrograms: () => dispatch(ProgramsApiService.fetchPrograms()),
    onDeleteProgram: (id) => dispatch(ProgramsApiService.deleteProgram(id)),
    onSearchPrograms: (data) => dispatch(ProgramsApiService.searchPrograms(data)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Equipment);