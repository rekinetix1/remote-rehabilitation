import React, {useEffect} from "react";
import {connect} from "react-redux";
import ExerciseApiService from "../../../services/api/ExercisesApiService";
import config from "../../../config";
import Breadcrumbs from "../../../components/UI/Breadcrumbs";

const ExerciseFull = (props) => {
    useEffect(() => {
        document.title = `Упражнения | Rekinetix`
        props.onFetchExercise(props.match.params.id);
    },[]);

    return (
        <div>
            <Breadcrumbs
              prevLink="exercises"
              prevPage="Упражнения"
            />
            <h2><b>{props.currentExercise && props.currentExercise.title}</b></h2>
            <p>{props.currentExercise && props.currentExercise.description}</p>
            <p>{props.currentExercise.category && props.currentExercise.category.title}</p>
            <p>{props.currentExercise && props.currentExercise.video_link}</p>
            <p>{props.currentExercise && props.currentExercise.equipment && props.currentExercise.equipment.title}</p>
            <img src={config.apiURL + "/uploads/" + props.currentExercise.image}/>
        </div>
    );
};

const mapStateToProps = (state) => {
	return {
        currentExercise: state.exercises.currentExercise
	}
};

const mapDispatchToProps = (dispatch) => {
	return {
		onFetchExercise: (id) => dispatch(ExerciseApiService.fetchSingleExercise(id))
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(ExerciseFull);