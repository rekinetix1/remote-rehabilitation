import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import AdminTable from "../../../components/AdminTable";
import Button from "@material-ui/core/Button";
import ExerciseApiService from "../../../services/api/ExercisesApiService";
import '../AdminPages.scss'
import Breadcrumbs from "../../../components/UI/Breadcrumbs";
import AdminPanel from "../../AdminPanel/AdminPanel";
import SearchForm from "../../../components/UI/SearchForm/SearchForm";

const Exercises = (props) => {
    const [search, setSearch] = useState(null);

    useEffect(() => {
      document.title = `Упражнения | Rekinetix`
        if (props.location.search) {
          const query = new URLSearchParams(props.location.search);
          for (let param of query.entries()) {
            setSearch(param[1])
            props.onSearchExercises(param[1])
          }
        } else {
          setSearch(null)
          props.onFetchExercises();
        }
      }, [props.location.search]);

    const rowsItem = [
        {title: 'title', width: '20%'},
        {title: 'description', width: '60%'},
        {title: 'complexity', width: '10%'},
    ]

    return(
        <div className="AdminPage">
            <Breadcrumbs current="Упражнения" />
            <h2 className="m-auto pb-4">Упражнения</h2>
            <div className="justify-content-end add-search-block">
                <div className="search">
                    <SearchForm
                        route="exercises"
                        history={props.history}
                        placeholder="поиск по названию"
                        search={search}/>
                </div>
                <div className="add_record">
                    <Link
                        to="/create-exercises"
                        style={{
                        color: "inherit",
                        textDecoration: "none"
                        }}
                    >
                      {
                        props.user.role !== "Администратор" ? null :
                        <Button variant="contained" color="primary" className="ml-3">+ Добавить</Button>
                      }
                    </Link>
                </div>
            </div>
            <div className='AdminPage_flexbox'>
                <div className='AdminPage_panel'>
                    <AdminPanel closable admin_page={true}/>
                </div>
                <AdminTable
                    columns={ExerciseApiService.columns}
                    rows={props.exercises}
                    route={'exercises'}
                    hideEditIcon={props.user.role !== "Администратор" ? true : false}
                    hideDeleteIcon={props.user.role !== "Администратор" ? true: false}
                    deleteRow={props.onDeleteExercise}
                    rowsItem={rowsItem}
                />
            </div>

        </div>
    );
};

const mapStateToProps = state => {
    return {
      exercises: state.exercises.exercises,
      loading: state.exercises.loading,
      error: state.exercises.error,
      user: state.users.user
    }
  };
  
  const mapDispatchToProps = dispatch => {
    return {
      onFetchExercises: () => dispatch(ExerciseApiService.fetchExercises()),
      onDeleteExercise: (id) => dispatch(ExerciseApiService.deleteExercise(id)),
      onSearchExercises: (data) => dispatch(ExerciseApiService.searchExercises(data)),
    }
  };
  
  export default connect(mapStateToProps ,mapDispatchToProps)(Exercises);