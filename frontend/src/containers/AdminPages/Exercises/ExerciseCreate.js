import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import TextField from "@material-ui/core/TextField";
import Select from '@material-ui/core/Select';
import {InputLabel, MenuItem} from "@material-ui/core";
import CancelIcon from '@material-ui/icons/Cancel';
import '../AdminPages.scss';
import Button from "@material-ui/core/Button";
import ExerciseApiService from "../../../services/api/ExercisesApiService";
import ExerciseCategoriesApiService from "../../../services/api/ExerciseCategoriesApiService";
import EquipmentApiService from "../../../services/api/EquipmentApiService";
import MusclesApiService from "../../../services/api/MusclesApiService";
import Breadcrumbs from "../../../components/UI/Breadcrumbs";
import FormControl from "@material-ui/core/FormControl";
import FormHelperText from "@material-ui/core/FormHelperText";


const ExerciseCreate = props => {
    useEffect(() => {
        document.title = `Добавление упражнений | Rekinetix`
        props.onFetchExerciseCategories();
        props.onFetchEquipment();
        props.onFetchMuscles();
    }, [])

    const [exercise, setExercise] = useState({
        title: "",
        description: "",
        video_link: "",
        image: "",
        kinematic_chain: "",
        complexity: "",
        category: "",
        equipment: [],
        muscle: ""
    });

    const changeInputHandler = (e) => {
        setExercise({
            ...exercise,
            [e.target.name]: e.target.value
        });
    };

    const changeInputEquipment = (e, i) => {
        let equipment = [...exercise.equipment]
        equipment[i] = e.target.value
        setExercise({
            ...exercise,
            equipment: equipment
        });
    };

    const addEquipment = () => {
        setExercise({
            ...exercise,
            equipment: [...exercise.equipment, ""]
        });
    };

    const changeFileHandler = e => {
        setExercise({
            ...exercise,
            image: e.target.files[0]
        });
    };

    const submitForm = (e) => {
        e.preventDefault();
        const formData = new FormData();
        Object.keys(exercise).forEach(key => {
            if (key === 'equipment') {
                formData.append(key, JSON.stringify(exercise[key]))
            } else {
                formData.append(key, exercise[key]);
            }
        });
        props.onCreateExercise(formData);
    };

    const deleteEquipment = (index) => {
        let equipment = [...exercise.equipment]
        equipment.splice(index, 1)
        setExercise({
            ...exercise,
            equipment: equipment
        });
    }

    const getFieldError = fieldName => {
        return props.error && props.error.errors && props.error.errors[fieldName] && props.error.errors[fieldName].message;
    };

    const formForEquipment = () => {
        let formEquipment = [];
        for (let i = 0; i < exercise.equipment.length; i++) {
            formEquipment.push(
                <div className="AdminPage_form_input_wrapper" key={i}>
                    <FormControl margin='normal' variant="outlined" className="AdminPage_form_input">
                        <InputLabel id="equipment">Инвентарь</InputLabel>
                        <Select
                            labelId="equipment"
                            id="outlined-basic"
                            value={exercise.equipment[i]}
                            onChange={(event) => changeInputEquipment(event, i)}
                            name="equipment"
                            label="Инвентарь"
                        >
                            {
                                props.equipment.map(item => {
                                    return <MenuItem
                                        value={item._id}
                                        key={item._id}
                                    >
                                        {item.title}
                                    </MenuItem>
                                })
                            }
                        </Select>
                        {!!(props.error && props.error.errors && props.error.errors['equipment']) ?
                            <FormHelperText>Выберите инвентарь</FormHelperText> : null}
                    </FormControl>
                    <CancelIcon
                        fontSize="small"
                        color="secondary"
                        onClick={() => deleteEquipment(i)}
                        />
                </div>
            );
        }

        return formEquipment
    };

    return (
        <div className="CreatePage">
            <Breadcrumbs
                current="Добавление упражнений"
                prevLink="exercises"
                prevPage="Упражнения"
            />
            <h2 className="AdminPage_title">Добавление упражнений</h2>
            <form
                className="AdminPage_form"
                onSubmit={submitForm}>
                <div className="AdminPage_form_input_wrapper">
                    <TextField
                        helperText={getFieldError("title")}
                        className="AdminPage_form_input"
                        error={!!(props.error && props.error.errors && props.error.errors['title'] && props.error.errors['title'].message)}
                        id="outlined-basic"
                        name="title"
                        label="Заголовок"
                        variant="outlined"
                        value={exercise.title}
                        onChange={changeInputHandler}
                        margin='normal'
                    />
                </div>
                <div className="AdminPage_form_input_wrapper">
                    <TextField
                        helperText={getFieldError("description")}
                        className="AdminPage_form_input"
                        error={!!(props.error && props.error.errors && props.error.errors['description'] && props.error.errors['description'].message)}
                        id="outlined-basic"
                        name="description"
                        label="Описание"
                        variant="outlined"
                        value={exercise.description}
                        onChange={changeInputHandler}
                        margin='normal'
                    />
                </div>
                <div className="AdminPage_form_input_wrapper">
                    <TextField
                        helperText={getFieldError("video_link")}
                        className="AdminPage_form_input"
                        error={!!(props.error && props.error.errors && props.error.errors['video_link'] && props.error.errors['video_link'].message)}
                        margin='normal'
                        id="outlined-basic"
                        name="video_link"
                        label="Ссылка на видео"
                        variant="outlined"
                        value={exercise.video_link}
                        onChange={changeInputHandler}
                    />
                </div>
                <div className="AdminPage_form_input_wrapper">
                    <FormControl error={!!(props.error && props.error.errors && props.error.errors['kinematic_chain'])}
                                margin='normal' variant="outlined" className="AdminPage_form_input">
                        <InputLabel id="demo-simple-select-outlined-label">Кинематическая цепь</InputLabel>
                        <Select
                            labelId="kinematic_chain"
                            value={exercise.kinematic_chain}
                            onChange={changeInputHandler}
                            variant="outlined"
                            name="kinematic_chain"
                            label='Кинематическая цепь'
                        >
                            <MenuItem value={'ОКЦ'}>ОКЦ</MenuItem>
                            <MenuItem value={'ЗКЦ'}>ЗКЦ</MenuItem>
                        </Select>
                        {!!(props.error && props.error.errors && props.error.errors['kinematic_chain']) ?
                            <FormHelperText>Выберите кинематическую цепь</FormHelperText> : null}
                    </FormControl>
                </div>
                <div className="AdminPage_form_input_wrapper">
                    <FormControl error={!!(props.error && props.error.errors && props.error.errors['complexity'])}
                                margin='normal' variant="outlined" className="AdminPage_form_input">
                        <InputLabel id="demo-simple-select-outlined-label">Сложность</InputLabel>
                        <Select
                            labelId="complexity"
                            value={exercise.complexity}
                            onChange={changeInputHandler}
                            variant="outlined"
                            name="complexity"
                            label='Сложность'
                        >
                            <MenuItem value={'Низкая'}>Низкая</MenuItem>
                            <MenuItem value={'Средняя'}>Средняя</MenuItem>
                            <MenuItem value={'Тяжелая'}>Тяжелая</MenuItem>
                        </Select>
                        {!!(props.error && props.error.errors && props.error.errors['complexity']) ?
                            <FormHelperText>Выберите сложность</FormHelperText> : null}
                    </FormControl>
                </div>
                <div className="AdminPage_form_input_wrapper">
                    <FormControl error={!!(props.error && props.error.errors && props.error.errors['category'])}
                                margin='normal' variant="outlined" className="AdminPage_form_input">
                        <InputLabel id="demo-simple-select-outlined-label">Область применения</InputLabel>
                        <Select
                            labelId="category"
                            value={exercise.category}
                            onChange={changeInputHandler}
                            variant="outlined"
                            name="category"
                            label='Область применения'
                        >
                            {
                                props.exerciseCategories.map(category => {
                                    return <MenuItem
                                        value={category._id}
                                        key={category._id}
                                    >
                                        {category.title}
                                    </MenuItem>
                                })
                            }
                        </Select>
                        {!!(props.error && props.error.errors && props.error.errors['category']) ?
                            <FormHelperText>Выберите область применения</FormHelperText> : null}
                    </FormControl>
                </div>
                <div className="AdminPage_form_input_wrapper">
                    <FormControl error={!!(props.error && props.error.errors && props.error.errors['muscle'])}
                                margin='normal' variant="outlined" className="AdminPage_form_input">
                        <InputLabel id="demo-simple-select-outlined-label">Мышца</InputLabel>
                        <Select
                            labelId="muscle"
                            value={exercise.muscle}
                            onChange={changeInputHandler}
                            variant="outlined"
                            name="muscle"
                            label='Мышца'
                        >
                            {
                                props.muscles.map(muscle => {
                                    return <MenuItem
                                        value={muscle._id}
                                        key={muscle._id}
                                    >
                                        {muscle.title}
                                    </MenuItem>
                                })
                            }
                        </Select>
                        {!!(props.error && props.error.errors && props.error.errors['muscle']) ?
                            <FormHelperText>Выберите мышцу</FormHelperText> : null}
                    </FormControl>
                </div>
                <div className="AdminPage_form_input_wrapper">
                    <InputLabel id="image">Картинка</InputLabel>
                    <TextField
                        margin='normal'
                        type="file"
                        id="outlined-basic"
                        name="image"
                        variant="outlined"
                        className="AdminPage_form_input"
                        onChange={changeFileHandler}
                    />
                </div>
                <div className="AdminPage_form_input_wrapper">
                    { formForEquipment() }
                </div>
                <div className='mb-2'>
                    <Button
                        color="primary"
                        onClick={addEquipment}>
                        Добавить инвентарь
                    </Button>
                </div>
                <Button
                    type="submit"
                    variant="contained"
                    color="primary"
                >
                    Создать
                </Button>
            </form>
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        exerciseCategories: state.exerciseCategories.exerciseCategories,
        equipment: state.equipment.equipment,
        muscles: state.muscles.muscles,
        error: state.exercises.error
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onCreateExercise: (data) => dispatch(ExerciseApiService.postExercise(data)),
        onFetchEquipment: () => dispatch(EquipmentApiService.fetchEquipment()),
        onFetchExerciseCategories: () => dispatch(ExerciseCategoriesApiService.fetchExerciseCategories()),
        onFetchMuscles: () => dispatch(MusclesApiService.fetchMuscles())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ExerciseCreate);