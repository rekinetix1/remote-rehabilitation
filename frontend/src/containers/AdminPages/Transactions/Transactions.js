import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import '../AdminPages.scss';
import TransactionsApiService from "../../../services/api/TransactionsApiService";
import AdminTable from "../../../components/AdminTable/AdminTable"
import Breadcrumbs from "../../../components/UI/Breadcrumbs";
import AdminPanel from "../../AdminPanel/AdminPanel";
import SearchForm from "../../../components/UI/SearchForm/SearchForm";

const Transactions = (props) => {
  const [search, setSearch] = useState(null);

  useEffect(() => {
    document.title = `Транзакции | Rekinetix`
    if (props.location.search) {
      const query = new URLSearchParams(props.location.search);
      for (let param of query.entries()) {
        setSearch(param[1])
        props.onSearchTransactions(param[1])
      }
    } else {
      setSearch(null)
      props.onFetchTransactions();
    }
  }, [props.location.search]);

  const transactions = props.transactions.map((transaction) => {
    if (transaction.program && transaction.program.title) {
      transaction.program = transaction.program.title;
      transaction.type = "Б"
    } else if (transaction.individualProgram && transaction.individualProgram.title) {
      transaction.program = transaction.individualProgram.title
      transaction.type = "И"
    }
    if (transaction.user) {
      transaction.email = transaction.user.email
    };
    return transaction
  });

  const rowsItem = [
    {title: 'invoiceId', width: '15%'},
    {title: 'email', width: '20%'},
    {title: 'type', width: '10%'},
    {title: 'program', width: '20%'},
    {title: 'cost', width: '10%'},
    {title: 'status', width: '10%'},
    {title: 'reason', width: '20%'},
    {title: 'datetime', width: '10%'},
  ]

  return (
    <div className="AdminPage">
      <Breadcrumbs current="Транзакции"/>
      <h2 className="m-auto pb-4">Транзакции</h2>
      <div className="justify-content-end add-search-block">
        <div className="search">
          <SearchForm
            route="transactions"
            history={props.history}
            placeholder="поиск по почте"
            search={search}/>
        </div>
      </div>
      <div className='AdminPage_flexbox'>
          <div className='AdminPage_panel'>
              <AdminPanel closable admin_page={true}/>
          </div>
          <AdminTable
            columns={TransactionsApiService.columns}
            rows={transactions}
            route={'transactions'}
            hideEditIcon={props.user.role !== "Администратор" ? true : false}
            hideDeleteIcon={true}
            rowsItem={rowsItem}
          />
      </div>
    </div>
  )
};

const mapStateToProps = (state) => {
  return {
    transactions: state.transactions.transactions,
    loading: state.userPrograms.loading,
    error: state.userPrograms.error,
    user: state.users.user
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    onFetchTransactions: () => dispatch(TransactionsApiService.fetchTransactions()),
    onSearchTransactions: (data) => dispatch(TransactionsApiService.searchTransactions(data)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Transactions);