import React, {useEffect} from "react";
import {connect} from "react-redux";
import TransactionsApiService from "../../../services/api/TransactionsApiService";
import Breadcrumbs from "../../../components/UI/Breadcrumbs";
import Moment from 'react-moment';

const TransactionsFull = (props) => {
    useEffect(() => {
		document.title = `Транзакции | Rekinetix`
        props.onFetchTransaction(props.match.params.id);
	}, []);
	console.log(props.currentTransaction);
	return (
		<div className="CreatePage">
			<Breadcrumbs
				current="Транзакция"
				prevLink="transactions"
				prevPage="Статус операции"
			/>
			<h2 className="AdminPage_title">Статус операции</h2>
				<div className="AdminPage_form_input_wrapper">
					<p><b>Пользователь:</b> {props.currentTransaction && props.currentTransaction.user && props.currentTransaction.user.email }</p>
					<p><b>Тип:</b> {props.currentTransaction.program ? "Базовая" : "Индивидуальная"}</p>
                    <p><b>Программа:</b> {props.currentTransaction && props.currentTransaction.program && props.currentTransaction.program.title || props.currentTransaction.individualProgram && props.currentTransaction.individualProgram.title}</p>
                    <p><b>Цена:</b> {props.currentTransaction.cost} тенге</p>
                    <p><b>Дата покупки:</b> <Moment format="D MMM YYYY HH:MM" withTitle>{props.currentTransaction.datetime}</Moment></p>
                    <p><b>Статус:</b> {props.currentTransaction.status}</p>
				</div>
		</div>
	)
};

const mapStateToProps = (state) => {
	return {
		currentTransaction: state.transactions.currentTransaction,
		loading: state.userPrograms.loading
	}
};

const mapDispatchToProps = (dispatch) => {
	return {
        onFetchTransaction: (id) => dispatch(TransactionsApiService.fetchSingleTransaction(id))
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(TransactionsFull);