import React, {useState, useEffect} from "react";
import {connect} from "react-redux";
import Select from '@material-ui/core/Select';
import {InputLabel, MenuItem} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import TransactionsApiService from "../../../services/api/TransactionsApiService";
import Breadcrumbs from "../../../components/UI/Breadcrumbs";

const TransactionsEdit = (props) => {
    useEffect(() => {
        document.title = `Редактирование транзакции | Rekinetix`
        props.onFetchTransaction(props.match.params.id)
            .then(data => {
                setTransaction({
                    user: data.user && data.user.email,
                    program: data.program && data.program.title,
                    program_rate: data.program_rate,
                    datetime: data.datetime,
                    status: data.status
                })
            })
    }, []);

	const [transaction, setTransaction] = useState({
        user: "",
        program: "",
        program_rate: "",
        datetime: "",
		status: ""
	});

	const changeInputHandler = (e) => {
		setTransaction({
			...transaction,
			[e.target.name]: e.target.value
		});
	};

	const submitForm = (e) => {
		e.preventDefault();
		props.onUpdateTransaction(props.match.params.id, {status: transaction.status});
		props.history.push("/transactions");
    };

	return (
		<div className="CreatePage">
			<Breadcrumbs
				current="Изменение статуса операции"
				prevLink="transactions"
				prevPage="Транзакции"
			/>
			<h2 className="AdminPage_title">Изменение статуса операции</h2>
			<form
				onSubmit={submitForm}
				className="AdminPage_form">
				<div className="AdminPage_form_input_wrapper">
					<p><b>Пользователь:</b> {transaction.user }</p>
                    <p><b>Программа:</b> {transaction.program }</p>
                    <p><b>Цена:</b> {transaction.program_rate} тенге</p>
                    <p><b>Дата покупки:</b> {transaction.datetime}</p>
				</div>
				<div className="AdminPage_form_input_wrapper">
                <InputLabel id="status">Статус</InputLabel>
                    <Select 
                        labelId="status" 
                        id="select" 
                        value={transaction.status} 
                        onChange={changeInputHandler}
                        variant="outlined"
                        name="status"
                    >
                        <MenuItem 
                            value="Успешно"
                        >
                            Успешно
                        </MenuItem> 
                        <MenuItem 
                            value="Не успешно"
                        >
                            Не успешно
                        </MenuItem> 
                        <MenuItem 
                            value="Возврат"
                        >
                            Возврат
                        </MenuItem> 
                    </Select>
				</div>
				<Button
					type="submit"
					variant="contained"
					color="primary"
				>Изменить</Button>
			</form>
		</div>
	)
};

const mapStateToProps = (state) => {
	return {
		currentProgram: state.userPrograms.currentProgram,
		loading: state.userPrograms.loading
	}
};

const mapDispatchToProps = (dispatch) => {
	return {
        onFetchTransaction: (id) => dispatch(TransactionsApiService.fetchSingleTransaction(id)),
		onUpdateTransaction: (id, data) => dispatch(TransactionsApiService.updateTransaction(id, data))
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(TransactionsEdit);