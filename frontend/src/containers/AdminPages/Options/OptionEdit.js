import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import OptionsApiService from "../../../services/api/OptionsApiService";
import Breadcrumbs from "../../../components/UI/Breadcrumbs";

const OptionCreate = (props) => {
    useEffect(() => {
		document.title = `Редактирование опции | Rekinetix`
        props.onFetchOption(props.match.params.id)
            .then(data => {
                setOption({
                    title: data.title,
                    cost: data.cost
                })
            })
    }, []);
    
	const [option, setOption] = useState({
		title: "",
		cost: ""
	});

	const changeInputHandler = (e) => {
		setOption({
			...option,
			[e.target.name]: e.target.value
		});
	};

	const submitForm = (e) => {
		e.preventDefault();
		props.onUpdateOption(props.match.params.id, option);
		props.history.push("/options");
	};

	return (
		<div className="CreatePage">
			<Breadcrumbs
				current="Изменение дополниельной услуги"
				prevLink="options"
				prevPage="Опции"
			/>
			<h2 className="AdminPage_title">Изменение дополниельной услуги</h2>
			<form
				onSubmit={submitForm}
				className="AdminPage_form">
				<div className="AdminPage_form_input_wrapper">
					<TextField
						id="outlined-basic"
						name="title"
						label="Заголовок"
						required
						variant="outlined"
						className="AdminPage_form_input"
						value={option.title}
						onChange={changeInputHandler}
					/>
				</div>
				<div className="AdminPage_form_input_wrapper">
					<TextField
						id="outlined-basic" 
						name="cost"
						required
						label="Цена"
						variant="outlined"
						className="AdminPage_form_input"
						value={option.cost}
						onChange={changeInputHandler}
					/>
				</div>
				<Button
					type="submit"
					variant="contained"
					color="primary"
				>Изменить</Button>
			</form>
		</div>
	)
};

const mapDispatchToProps = (dispatch) => {
	return {
        onFetchOption: (id) => dispatch(OptionsApiService.fetchSingleOption(id)),
		onUpdateOption: (id, data) => dispatch(OptionsApiService.updateOption(id, data))
	}
};

export default connect(null, mapDispatchToProps)(OptionCreate);