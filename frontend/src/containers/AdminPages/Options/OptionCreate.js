import React, {useState, useEffect} from "react";
import {connect} from "react-redux";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import OptionsApiService from "../../../services/api/OptionsApiService";
import Breadcrumbs from "../../../components/UI/Breadcrumbs";

const OptionCreate = (props) => {
    const [option, setOption] = useState({
        title: "",
        cost: ""
    });

    useEffect(() => {
		document.title = `Создание опции | Rekinetix`
    }, []);

    const changeInputHandler = (e) => {
        setOption({
            ...option,
            [e.target.name]: e.target.value
        });
    };

    const submitForm = (e) => {
        e.preventDefault();
        props.onCreateOption(option);
    };



    const getFieldError = fieldName => {
        return props.error && props.error.errors && props.error.errors[fieldName] && props.error.errors[fieldName].message;
    };

    return (
        <div className="CreatePage">
            <Breadcrumbs
                current="Добавление дополнительной опции"
                prevLink="options"
                prevPage="Опции"
            />
            <h2 className="AdminPage_title">Добавление дополнительной услуги</h2>
            <form
                onSubmit={submitForm}
                className="AdminPage_form">
                <div className="AdminPage_form_input_wrapper">
                    <TextField
                        helperText={getFieldError("title")}
                        error={!!(props.error && props.error.errors && props.error.errors['title'] && props.error.errors['title'].message)}
                        id="outlined-basic"
                        name="title"
                        label="Заголовок"
                        variant="outlined"
                        className="AdminPage_form_input"
                        value={option.title}
                        onChange={changeInputHandler}
                    />
                </div>
                <div className="AdminPage_form_input_wrapper">
                    <TextField
                        error={!!(props.error && props.error.errors && props.error.errors['cost'] && props.error.errors['cost'])}
                        helperText={(props.error && props.error.errors && props.error.errors['cost']
                            && props.error.errors['cost']) && !(props.error && props.error.errors && props.error.errors['cost'] && props.error.errors['cost'].message) ? 'Введите числовое значение' : getFieldError("cost")}
                        id="outlined-basic"
                        name="cost"
                        label="Цена"
                        variant="outlined"
                        className="AdminPage_form_input"
                        value={option.cost}
                        onChange={changeInputHandler}
                    />
                </div>
                <Button
                    type="submit"
                    variant="contained"
                    color="primary"
                >Создать</Button>
            </form>
        </div>
    )
};

const mapDispatchToProps = (dispatch) => {
    return {
        onCreateOption: (data) => dispatch(OptionsApiService.postOption(data))
    }
};
const mapStateToProps = state => {
    return {
        error: state.occupation.error
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(OptionCreate);