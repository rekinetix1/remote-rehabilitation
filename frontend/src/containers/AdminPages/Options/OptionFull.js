import React, {useEffect} from "react";
import {connect} from "react-redux";
import OptionsApiService from "../../../services/api/OptionsApiService";
import Breadcrumbs from "../../../components/UI/Breadcrumbs";

const OptionFull = (props) => {
  useEffect(() => {
    document.title = `Опции | Rekinetix`
    props.onFetchOption(props.match.params.id);
  }, []);

  return (
    <div>
      <Breadcrumbs
        prevLink="options"
        prevPage="Опции"
      />
      <h1>{props.option && props.option.title}</h1>
      <p>{props.option && props.option.cost} тенге</p>
    </div>
  )
};

const mapStateToProps = (state) => {
  return {
    option: state.options.currentOption
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    onFetchOption: (id) => dispatch(OptionsApiService.fetchSingleOption(id))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(OptionFull);