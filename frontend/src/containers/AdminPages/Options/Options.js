import React, {useEffect} from "react";
import {connect} from "react-redux";
import OptionsApiService from "../../../services/api/OptionsApiService";
import {Link} from "react-router-dom";
import Button from "@material-ui/core/Button";
import '../AdminPages.scss';
import AdminTable from "../../../components/AdminTable/AdminTable"
import Breadcrumbs from "../../../components/UI/Breadcrumbs/Breadcrumbs";
import AdminPanel from "../../AdminPanel/AdminPanel";

const Options = (props) => {
  useEffect(() => {
    document.title = `Опции | Rekinetix`
    props.onFetchOptions();
  }, []);

  const rowsItem = [
    {title: 'title', width: '50%'},
    {title: 'cost', width: '40%'},
  ]

  return (
    <div className="AdminPage">
      <Breadcrumbs current="Опции" />
      <h2 className="m-auto pb-4">Опции</h2>
      <div className="ml-auto">
        <Link
          to="/create-options"
          style={{
            color: "inherit",
            textDecoration: "none"
          }}
        >
          {
            props.user.role !== "Администратор" ? null :
            <Button variant="contained" color="primary" className="ml-3">+ Добавить</Button>
          }
        </Link>
      </div>
        <div className='AdminPage_flexbox'>
            <div className='AdminPage_panel'>
                <AdminPanel closable admin_page={true}/>
            </div>
            <AdminTable
              columns={OptionsApiService.columns}
              rows={props.options}
              route={'options'}
              deleteRow={props.onDeleteOptions}
              hideEditIcon={props.user.role !== "Администратор" ? true : false}
              hideDeleteIcon={props.user.role !== "Администратор" ? true: false}
              rowsItem={rowsItem}
            />
        </div>

    </div>
  )
};

const mapStateToProps = (state) => {
  return {
    options: state.options.options,
    loading: state.options.loading,
    error: state.options.error,
    user: state.users.user
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    onFetchOptions: () => dispatch(OptionsApiService.fetchOptions()),
    onDeleteOptions: (id) => dispatch(OptionsApiService.deleteOption(id)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Options);