import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import '../AdminPages.scss';
import Breadcrumbs from "../../../components/UI/Breadcrumbs";
import AdminPanel from "../../AdminPanel/AdminPanel";
import BrowsingHistoryApiService from "../../../services/api/BrowsingHistoryApiService";
import AdminTable from "../../../components/AdminTable/AdminTable";
import SearchForm from "../../../components/UI/SearchForm/SearchForm";

const BrowsingHistory = ({browsingHistory, onFetchHistory, historyPage, location, history, onSearchHistory}) => {
    const [search, setSearch] = useState(null);

    useEffect(() => {
        document.title = `История просмотров | Rekinetix`
        if (location.search) {
            const query = new URLSearchParams(location.search);
            for (let param of query.entries()) {
                setSearch(param[1])
                onSearchHistory(param[1])
            }
        } else {
            setSearch('')
            onFetchHistory();
        }
    }, [location.search]);

    const browseHistory = browsingHistory.map((history) => {
        if (history.program.title) {
            history.program = history.program.title
        }
        if (history.user) {
            history.email = history.user.email
            history.name = `${history.user.name} ${history.user.surname}`
        };
        return history
    });

    const rowsItem = [
        {title: 'datetime', width: '15%'},
        {title: 'email', width: '20%'},
        {title: 'name', width: '20%'},
        {title: 'program', width: '25%'},
        {title: 'day', width: '10%'},
        {title: 'exercise', width: '10%'},
    ]

    return (
        <div className="AdminPage">
            <Breadcrumbs current="История просмотров"/>
            <h2 className="m-auto pb-4">История просмотров</h2>
            <div className="d-flex ml-auto w-75 justify-content-end">
                <SearchForm
                    route="browsing_history"
                    history={history}
                    placeholder="поиск по почте"
                    search={search}/>
            </div>
            <div className='AdminPage_flexbox'>
                <div className='AdminPage_panel'>
                    <AdminPanel closable admin_page={true}/>
                </div>
                <AdminTable
                    columns={BrowsingHistoryApiService.columns}
                    rows={browseHistory}
                    route={'browsing_history'}
                    hideIcons={true}
                    rowsItem={rowsItem}
                />
            </div>
        </div>
    )
};

const mapStateToProps = (state) => {
    return {
        browsingHistory: state.browsingHistory.browsingHistory,
        loading: state.browsingHistory.loading,
        error: state.browsingHistory.error
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        onFetchHistory: () => dispatch(BrowsingHistoryApiService.fetchBrowsingHistory()),
        onSearchHistory: (data) => dispatch(BrowsingHistoryApiService.searchBrowsingHistory(data)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(BrowsingHistory);