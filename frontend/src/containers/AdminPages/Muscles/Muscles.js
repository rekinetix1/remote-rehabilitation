import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import {Link as RouterLink} from "react-router-dom";
import AdminTable from "../../../components/AdminTable";
import Button from "@material-ui/core/Button";
import MusclesApiService from "../../../services/api/MusclesApiService";
import '../AdminPages.scss'
import Breadcrumbs from "../../../components/UI/Breadcrumbs";
import AdminPanel from "../../AdminPanel/AdminPanel";
import SearchForm from "../../../components/UI/SearchForm/SearchForm";

const Muscles = (props) => {
  const [search, setSearch] = useState(null);

  useEffect(() => {
    document.title = `Мышцы | Rekinetix`
    if (props.location.search) {
      const query = new URLSearchParams(props.location.search);
      for (let param of query.entries()) {
        setSearch(param[1])
        props.onSearchMuscles(param[1])
      }
    } else {
      setSearch(null)
      props.onFetchMuscles();
    }
  }, [props.location.search]);

  const rowsItem = [
    {title: 'title', width: '90%'},
  ]

  return (
    <div className="AdminPage">
      <Breadcrumbs current="Мышцы" />
      <h2 className="m-auto pb-4">Мышцы</h2>
      <div className="justify-content-end add-search-block">
        <div className="search">
          <SearchForm
              route="muscles"
              history={props.history}
              placeholder="поиск по названию"
              search={search}/>
        </div>
        <div className="add_record">
          <RouterLink
            to="/muscles/new"
            style={{
              color: "inherit",
              textDecoration: "none"
            }}
          >
            {
              props.user.role !== "Администратор" ? null :
              <Button variant="contained" color="primary" className="ml-3">+ Добавить</Button>
            }
          </RouterLink>
        </div>
      </div>
      <div className='AdminPage_flexbox'>
          <div className='AdminPage_panel'>
              <AdminPanel closable admin_page={true}/>
          </div>
          <AdminTable
              columns={MusclesApiService.columns}
              rows={props.muscles ? props.muscles : []}
              route={'muscles'}
              deleteRow={props.onDeleteMuscles}
              hideEditIcon={props.user.role !== "Администратор" ? true : false}
              hideDeleteIcon={props.user.role !== "Администратор" ? true: false}
              rowsItem={rowsItem}
              hideShowIcon={true}
          />
      </div>
    </div>
  )
};

const mapStateToProps = state => {
  return {
    muscles: state.muscles.muscles,
    loading: state.muscles.loading,
    error: state.muscles.error,
    user: state.users.user
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchMuscles: () => dispatch(MusclesApiService.fetchMuscles()),
    onDeleteMuscles: (id) => dispatch(MusclesApiService.deleteMuscle(id)),
    onSearchMuscles: (data) => dispatch(MusclesApiService.searchMuscles(data)),
  }
};

export default connect(mapStateToProps ,mapDispatchToProps)(Muscles);