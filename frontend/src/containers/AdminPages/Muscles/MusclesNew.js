import React, {useState, useEffect} from "react";
import {connect} from "react-redux";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import MusclesApiService from "../../../services/api/MusclesApiService";
import '../AdminPages.scss';
import Breadcrumbs from "../../../components/UI/Breadcrumbs";

const MusclesNew = (props) => {
	const [muscle, setMuscle] = useState({
		title: ""
	});

	useEffect(() => {
		document.title = `Cоздание мышцы | Rekinetix`
    }, []);

	const changeInputHandler = (e) => {
		setMuscle({
			...muscle,
			[e.target.name]: e.target.value
		});
	};

	const getFieldError = fieldName => {
		return props.error && props.error.errors && props.error.errors[fieldName] && props.error.errors[fieldName].message;
	};


	const submitForm = (e) => {
		e.preventDefault();
		props.onCreateMuscle(muscle);
	};

	return (
		<div className="CreatePage">
			<Breadcrumbs
				current="Добавление мышцы"
				prevLink="muscles"
				prevPage="Мышцы"
			/>
			<h2 className="AdminPage_title">Добавление мышцы</h2>
			<form
				onSubmit={submitForm}
				className="AdminPage_form">
				<div className="AdminPage_form_input_wrapper">
					<TextField
						helperText={getFieldError("title")}
						id="outlined-basic"
						name="title"
						label="Заголовок"
						variant="outlined"
						className="AdminPage_form_input"
						value={muscle.title}
						onChange={changeInputHandler}
						error={!!(props.error && props.error.errors && props.error.errors['title'] && props.error.errors['title'].message)}
					/>
				</div>
				<Button
					type="submit"
					variant="contained"
					color="primary"
				>Создать</Button>
			</form>
		</div>
	)
};


const mapStateToProps = state => {
	return {
		error: state.muscles.error
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		onCreateMuscle: (data) => dispatch(MusclesApiService.createMuscle(data))
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(MusclesNew);