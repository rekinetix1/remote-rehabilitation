import React, {useState, useEffect} from "react";
import {connect} from "react-redux";
import TextField from "@material-ui/core/TextField";
import {Button} from "@material-ui/core";
import MusclesApiService from "../../../services/api/MusclesApiService";
import Breadcrumbs from "../../../components/UI/Breadcrumbs";

const MuscleEdit = (props) => {
	const [muscle, setMuscle] = useState({
		title: ""
	});

	useEffect(() => {
		document.title = `Редактирование мышцы | Rekinetix`
        props.onFetchMuscle(props.match.params.id)
    }, []);

	useEffect(() => {
        setMuscle({
            ...muscle,
            title: props.muscle.title
        })
    }, [props.muscle]);

	const changeInputHandler = (e) => {
		setMuscle({
			...muscle,
			[e.target.name]: e.target.value
		})
	};

	const submitForm = (e) => {
		e.preventDefault();
		props.onUpdateMuscle(props.match.params.id, muscle);
		props.history.push("/muscles");
	};

	return (
		<div className="AdminPage">
			<Breadcrumbs
				current="Изменение Мышцы"
				prevLink="muscles"
				prevPage="Мышцы"
			/>
			<h2 className="AdminPage_title">Изменение Мышцы</h2>
			<form
				onSubmit={submitForm}
				className="AdminPage_form"
			>
				<div className="AdminPage_form_input_wrapper">
					<TextField
						required
						id="outlined-basic"
						name="title"
						label="Заголовок"
						variant="outlined"
						className="AdminPage_form_input"
						value={muscle.title || ''}
                        onChange={changeInputHandler}
                        error={!!(props.error && props.error.errors && props.error.errors['title'] && props.error.errors['title'].message)}
					/>
				</div>
				<Button
					type="submit"
					variant="contained"
					color="primary"
				>
					Изменить
				</Button>
			</form>
		</div>
	)

};

const mapStateToProps = (state) => {
	return {
        muscle: state.muscles.muscle,
		loading: state.muscles.loading
	}
};

const mapDispatchToProps = (dispatch) => {
	return {
		onFetchMuscle: (id) => dispatch(MusclesApiService.fetchMuscle(id)),
		onUpdateMuscle: (id, data) => dispatch(MusclesApiService.updateMuscle(id, data))
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(MuscleEdit);