import React, {useEffect} from "react";
import {connect} from "react-redux";
import Breadcrumbs from "../../../components/UI/Breadcrumbs";
import IndividualProgramsApiService from "../../../services/api/IndividualProgramsApiService";

const IndividualProgramFull = (props) => {
    useEffect(() => {
        document.title = `Индивидуальные программы | Rekinetix`
        props.onFetchIndividualProgram(props.match.params.id);
    }, []);

    let src = typeof(props.program.image) === 'string' ? `http://localhost:8000/uploads/${props.program.image}` : ''
    return(
        <div style={{textAlign: "left"}}>
            <Breadcrumbs
				prevLink="individual_programs"
				prevPage="Индивидуальные программы"
			/>
            <img src={src}/>
            <h1>Название программы: {props.program.title}</h1>
            <p>Пациент: {props.program && props.program.user && props.program.user.email}</p>
            <p>Статус: {props.program.status}</p>
            <p>Статус оплаты: {props.program.paymentStatus}</p>
            <p>Количество упражнении: {props.program.exercises_count}</p>
            <p>Сложность: {props.program.complexity}</p>
            <p>Стоимость: {props.program.cost} тенге</p>
            <p>Модератор: {props.program && props.program.moderator && props.program.moderator.surname}</p>
        </div>
    );
};

const mapStateToProps = (state) => {
	return {
		program: state.individualPrograms.currentIndividualProgram
	}
};

const mapDispatchToProps = (dispatch) => {
	return {
		onFetchIndividualProgram: (id) => dispatch(IndividualProgramsApiService.fetchCurrentIndividualProgram(id))
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(IndividualProgramFull);