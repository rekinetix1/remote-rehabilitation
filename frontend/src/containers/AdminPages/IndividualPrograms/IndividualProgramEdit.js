import React, {Component} from "react";
import {connect} from "react-redux";
import IndividualProgramForm from "../../../components/IndividualProgramForm/IndividualProgramForm"
import UsersApiService from "../../../services/api/UsersApiService";
import ExerciseCategoriesApiService from "../../../services/api/ExerciseCategoriesApiService";
import IndividualProgramsApiService from "../../../services/api/IndividualProgramsApiService";
import ExercisesApiService from "../../../services/api/ExercisesApiService";
import EquipmentApiService from "../../../services/api/EquipmentApiService";
import MusclesApiService from "../../../services/api/MusclesApiService";
import Breadcrumbs from "../../../components/UI/Breadcrumbs/Breadcrumbs";
import '../AdminPages.scss';

class IndividualProgramNew extends Component {
  componentDidMount() {
    document.title = `Изменение индивидуальной программы | Rekinetix`
  }

  render() {
    let users = this.props.users ? this.props.users : []
    return (
      <>
        <Breadcrumbs
          current="Изменение индивидуальной программы"
          prevLink="individual_programs"
          prevPage="Индивидуальные программы"
        />
        <IndividualProgramForm
          edit={true}
          users={users}
          programCategories={this.props.programCategories}
          exercises={this.props.exercises}
          exerciseCategories={this.props.exerciseCategories}
          equipment={this.props.equipment}
          program={this.props.program}
          user={this.props.user}
          muscles={this.props.muscles}
          onCreateProgram={this.props.onCreateProgram}
          onFetchExerciseCategories={this.props.onFetchExerciseCategories}
          onfetchExercises={this.props.onfetchExercises}
          onfetchUsers={this.props.onfetchUsers}
          onFetchMuscles={this.props.onFetchMuscles}
          onFetchEquipment={this.props.onFetchEquipment}
          onFetchIndividualProgram={this.props.onFetchIndividualProgram}
          id={this.props.match.params.id}
          onUpdateProgram={this.props.onUpdateProgram}
        />
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    error: state.users.loginError,
    programCategories: state.programCategories.programCategories,
    moderators: state.users.users,
    exercises: state.exercises.exercises,
    users: state.users.users,
    user: state.users.user,
    equipment: state.equipment.equipment,
    exerciseCategories: state.exerciseCategories.exerciseCategories,
    program: state.individualPrograms.currentIndividualProgram,
    muscles: state.muscles.muscles
  }
};
const mapDispatchToProps = dispatch => {
  return {
    onfetchUsers: () => dispatch(UsersApiService.fetchUsers()),
    onFetchExerciseCategories: () => dispatch(ExerciseCategoriesApiService.fetchExerciseCategories()),
    onCreateProgram: (program) => dispatch(IndividualProgramsApiService.createIndividualProgram(program)),
    onFetchIndividualProgram: (id) => dispatch(IndividualProgramsApiService.fetchCurrentIndividualProgram(id)),
    onUpdateProgram: (id, program, link) => dispatch(IndividualProgramsApiService.updateIndividualProgram(id, program, link)),
    onfetchExercises: () => dispatch(ExercisesApiService.fetchExercises()),
    onFetchEquipment: () => dispatch(EquipmentApiService.fetchEquipment()),
    onFetchMuscles: () => dispatch(MusclesApiService.fetchMuscles())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(IndividualProgramNew);
