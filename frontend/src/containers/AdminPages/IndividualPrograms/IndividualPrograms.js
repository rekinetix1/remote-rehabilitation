import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import IndividualProgramsApiService from "../../../services/api/IndividualProgramsApiService";
import AdminTable from "../../../components/AdminTable/AdminTable"
import {Link as RouterLink} from "react-router-dom";
import Button from "@material-ui/core/Button";
import '../AdminPages.scss';
import Breadcrumbs from "../../../components/UI/Breadcrumbs/Breadcrumbs";
import AdminPanel from "../../AdminPanel/AdminPanel";
import SearchForm from "../../../components/UI/SearchForm/SearchForm";

const IndividualPrograms = ({individualPrograms, onFetchPrograms, onDeleteProgram, onSearchPrograms, history, location, user}) => {
  const [search, setSearch] = useState(null);

  useEffect(() => {
    document.title = `Индивидуальные программы | Rekinetix`
    if (location.search) {
      const query = new URLSearchParams(location.search);
      for (let param of query.entries()) {
        setSearch(param[1])
        onSearchPrograms(param[1])
      }
    } else {
      setSearch(null)
      onFetchPrograms();
    }
  }, [location.search]);

  const programs = individualPrograms.map((individualProgram) => {
    if (individualProgram.user) {
      individualProgram.email = individualProgram.user.email
      individualProgram.name = individualProgram.user.surname + " " + individualProgram.user.name
    };
    return individualProgram
  });

  const rowsItem = [
    {title: 'title', width: '30%'},
    {title: 'name', width: '20%'},
    {title: 'email', width: '20%'},
    {title: 'status', width: '15%%'},
    {title: 'paymentStatus', width: '15%'},
  ]

  return (
    <div className="AdminPage">
      <Breadcrumbs current="Индивидуальные программы" />
      <h2 className="m-auto pb-4 text-center">Индивидуальные программы</h2>
      <div className="justify-content-end add-search-block">
        <div className="search">
          <SearchForm
            route="individual_programs"
            history={history}
            placeholder="поиск по почте"
            search={search}/>
        </div>
        <div className="add_record">
          <RouterLink
            to="/individual_programs/new"
            style={{
              color: "inherit",
              textDecoration: "none"
            }}
          >
            {
              user.role === "Модератор" || user.role === "Менеджер" ? null :
              <Button variant="contained" color="primary" className="ml-3">+ Добавить</Button>
            }
          </RouterLink>
        </div>
      </div>
      <div className='AdminPage_flexbox'>
        <div className='AdminPage_panel'>
          <AdminPanel closable admin_page={true}/>
        </div>
        <AdminTable
          columns={IndividualProgramsApiService.columns}
          rows={programs}
          route={'individual_programs'}
          hideEditIcon={user.role === "Модератор" ? true : false}
          hideDeleteIcon={user.role !== "Администратор" ? true: false}
          deleteRow={onDeleteProgram}
          rowsItem={rowsItem}
        />
      </div>
    </div>
  )
};

const mapStateToProps = (state) => {
  return {
    individualPrograms: state.individualPrograms.individualPrograms,
    loading: state.individualPrograms.loading,
    error: state.individualPrograms.error,
    user: state.users.user
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    onFetchPrograms: () => dispatch(IndividualProgramsApiService.fetchIndividualPrograms()),
    onDeleteProgram: (id) => dispatch(IndividualProgramsApiService.deleteIndividualProgram(id)),
    onSearchPrograms: (data) => dispatch(IndividualProgramsApiService.searchIndividualPrograms(data)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(IndividualPrograms);