import React, {useState, useEffect} from "react";
import {connect} from "react-redux";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import PhysicalActivitiesApiService from "../../../services/api/PhysicalActivitiesApiService";
import Breadcrumbs from "../../../components/UI/Breadcrumbs";

const PhysicalActivitiesCreate = (props) => {
    useEffect(() => {
		document.title = `Редактирование физической активности | Rekinetix`
        props.onFetchActivity(props.match.params.id)
            .then(data => {
                setActivity({
                    title: data.title
                })
            })
    }, []);

	const [activity, setActivity] = useState({
		title: ""
	});

	const changeInputHandler = (e) => {
		setActivity({
			...activity,
			[e.target.name]: e.target.value
		});
	};

	const submitForm = (e) => {
		e.preventDefault();
		props.onUpdateActivity(props.match.params.id, activity);
		props.history.push("/activities");
	};

	return (
		<div className="CreatePage">
			<Breadcrumbs
				current="Изменение физической активности"
				prevLink="activities"
				prevPage="Физическая активность"
			/>
			<h2 className="AdminPage_title">Изменение физической активности</h2>
			<form
				onSubmit={submitForm}
				className="AdminPage_form">
				<div className="AdminPage_form_input_wrapper">
					<TextField
						id="outlined-basic"
						name="title"
						required
						label="Заголовок"
						variant="outlined"
						className="AdminPage_form_input"
						value={activity.title}
						onChange={changeInputHandler}
					/>
				</div>
				<Button
					type="submit"
					variant="contained"
					color="primary"
				>Изменить</Button>
			</form>
		</div>
	)
};

const mapStateToProps = (state) => {
	return {
		currentActivity: state.physicalActivities.currentActivity,
		loading: state.physicalActivities.loading
	}
};

const mapDispatchToProps = (dispatch) => {
	return {
        onFetchActivity: (id) => dispatch(PhysicalActivitiesApiService.fetchSingleActivity(id)),
		onUpdateActivity: (id, data) => dispatch(PhysicalActivitiesApiService.updateActivity(id, data))
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(PhysicalActivitiesCreate);