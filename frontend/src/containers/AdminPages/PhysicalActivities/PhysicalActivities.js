import React, {useEffect} from "react";
import {connect} from "react-redux";
import AdminTable from "../../../components/AdminTable";
import {Link} from "react-router-dom";
import Button from "@material-ui/core/Button";
import '../AdminPages.scss';
import PhysicalActivitiesApiService from "../../../services/api/PhysicalActivitiesApiService";
import Breadcrumbs from "../../../components/UI/Breadcrumbs";
import AdminPanel from "../../AdminPanel/AdminPanel";

const PhysicalActivities = (props) => {
  useEffect(() => {
    document.title = `Физическая активность | Rekinetix`
    props.onFetchActivities();
  }, []);

  const rowsItem = [
    {title: 'title', width: '90%'},
  ]

  return (
    <div className="AdminPage">
      <Breadcrumbs current="Физическая активность" />
      <h2 className="m-auto pb-4">Физическая активность</h2>
      <div className="ml-auto">
        <Link
          to="/create-activities"
          style={{
            color: "inherit",
            textDecoration: "none"
          }}
        >
          {
            props.user.role !== "Администратор" ? null :
            <Button variant="contained" color="primary" className="ml-3">+ Добавить</Button>
          }
        </Link>
      </div>
        <div className='AdminPage_flexbox'>
            <div className='AdminPage_panel'>
                <AdminPanel closable admin_page={true}/>
            </div>
            <AdminTable
                columns={PhysicalActivitiesApiService.columns}
                rows={props.activities}
                route={'activities'}
                deleteRow={props.onDeleteActivity}
                rowsItem={rowsItem}
                hideShowIcon={true}
                hideEditIcon={props.user.role !== "Администратор" ? true : false}
                hideDeleteIcon={props.user.role !== "Администратор" ? true: false}
            />
        </div>

    </div>
  )
};

const mapStateToProps = (state) => {
  return {
    activities: state.physicalActivities.activities,
    loading: state.physicalActivities.loading,
    error: state.physicalActivities.error,
    user: state.users.user
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    onFetchActivities: () => dispatch(PhysicalActivitiesApiService.fetchPhysicalActivities()),
    onDeleteActivity: (id) => dispatch(PhysicalActivitiesApiService.deleteActivity(id)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PhysicalActivities);