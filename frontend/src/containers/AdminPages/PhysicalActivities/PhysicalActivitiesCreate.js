import React, {useState, useEffect} from "react";
import {connect} from "react-redux";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import PhysicalActivitiesApiService from "../../../services/api/PhysicalActivitiesApiService";
import Breadcrumbs from "../../../components/UI/Breadcrumbs";

const PhysicalActivitiesCreate = (props) => {
	const [activity, setActivity] = useState({
		title: ""
	});

	useEffect(() => {
		document.title = `Добавление физической активности | Rekinetix`
	}, []);

	const changeInputHandler = (e) => {
		setActivity({
			...activity,
			[e.target.name]: e.target.value
		});
	};

	const submitForm = (e) => {
		e.preventDefault();
		props.onCreateActivity(activity);
	};

	const getFieldError = fieldName => {
		return props.error && props.error.errors && props.error.errors[fieldName] && props.error.errors[fieldName].message;
	};

	return (
		<div className="CreatePage">
			<Breadcrumbs
				current="Добавление физической активности"
				prevLink="activities"
				prevPage="Физическая активность"
			/>
			<h2 className="AdminPage_title">Добавление физической активности</h2>
			<form
				onSubmit={submitForm}
				className="AdminPage_form">
				<div className="AdminPage_form_input_wrapper">
					<TextField
						helperText={getFieldError("title")}
						error={!!(props.error && props.error.errors && props.error.errors['title'] && props.error.errors['title'].message)}
						id="outlined-basic"
						name="title"
						label="Заголовок"
						variant="outlined"
						className="AdminPage_form_input"
						value={activity.title}
						required
						onChange={changeInputHandler}
					/>
				</div>
				<Button
					type="submit"
					variant="contained"
					color="primary"
				>Создать</Button>
			</form>
		</div>
	)
};

const mapStateToProps = state => {
	return {
		error: state.physicalActivities.error
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		onCreateActivity: (data) => dispatch(PhysicalActivitiesApiService.createActivity(data))
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(PhysicalActivitiesCreate);