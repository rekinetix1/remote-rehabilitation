import React, { useEffect, useState } from "react";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import Button from "@material-ui/core/Button";
import AdminTable from "../../../components/AdminTable";
import UsersApiService from "../../../services/api/UsersApiService";
import SearchForm from "../../../components/UI/SearchForm/SearchForm";

import '../AdminPages.scss';
import Breadcrumbs from "../../../components/UI/Breadcrumbs";
import AdminPanel from "../../AdminPanel/AdminPanel";


const Users = (props) => {
  const [search, setSearch] = useState(null);

    useEffect(() => {
        if (props.location.search) {
          const query = new URLSearchParams(props.location.search);
          for (let param of query.entries()) {
            setSearch(param[1])
            props.onSearchUsers(param[1])
          }
        } else {
          setSearch(null)
          props.onFetchUsers();
        }
        document.title = `Пользователи | Rekinetix`
    },[props.location.search]);

    const rowsItem = [
      {title: 'email', width: '25%'},
      {title: 'fullname', width: '35%'},
      {title: 'role', width: '30%'},
    ]

    return (
        <div className="AdminPage">
          <Breadcrumbs current="Пользователи"/>
          <h2 className="m-auto pb-4">Пользователи</h2>
          <div className="justify-content-end add-search-block">
            <div className="search">
              <SearchForm
                route="users"
                history={props.history}
                placeholder="поиск по логину, фамилии, имени и роли"
                search={search}/>
            </div>
            <div className="add_record">
              <Link
                  to="/create-user"
                  style={{
                      color: "inherit",
                      textDecoration: "none"
                  }}
              >
                  {
                    props.user.role === "Автор БП" || props.user.role === "Модератор" ? null :
                    <Button variant="contained" color="primary" className="ml-3">+ Добавить</Button>
                  }
              </Link>
            </div>
          </div>
            <div className='AdminPage_flexbox'>
                <div className='AdminPage_panel'>
                    <AdminPanel closable admin_page={true}/>
                </div>
                {
                    props.users ?
                        <AdminTable
                          columns={UsersApiService.columns}
                          rows={ props.users.map((user) => {
                            if (user.name && !user.fullname) {
                              user.fullname = `${user.name} ${user.surname}`
                            };
                            return user
                          })}
                          route={'users'}
                          deleteRow={props.onDeleteUser}
                          hideDeleteIcon={props.user.role !== "Администратор" ? true: false}
                          rowsItem={rowsItem}
                        /> : null
                }
            </div>

        </div>
    ) 
};

const mapStateToProps = state => {
    return {
      users: state.users.users,
      loading: state.users.loading,
      error: state.users.error,
      user: state.users.user
    }
  };
  
  const mapDispatchToProps = dispatch => {
    return {
      onFetchUsers: () => dispatch(UsersApiService.fetchUsers()),
      onSearchUsers: (data_search) => dispatch(UsersApiService.searchUsers(data_search)),
      onDeleteUser: (id) => dispatch(UsersApiService.deleteUser(id))
    }
  };
  
  export default connect(mapStateToProps ,mapDispatchToProps)(Users);