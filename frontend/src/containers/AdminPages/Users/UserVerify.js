import React, {useEffect} from 'react';
import {connect} from "react-redux";
import UsersApiService from "../../../services/api/UsersApiService";
import Button from "@material-ui/core/Button";

const UserVerify = (props) => {

    useEffect(
        () => {
            props.onVerify(props.match.params.id)
        }
        , [])


    const sendLink = () => {
        props.onSendVerify(props.user._id)
    };

    return (
        <>
            {props.user.verify ? <h3>Вы успешно верифицированы!</h3> :
                <div>
                    <h3>Верифицируйте свой аккаунт. письмо с ссылкой на верификацию отправлено вам на почту {props.user.email}</h3>
                    <Button className={'mb-2'} variant='contained'
                            onClick={sendLink}
                            color={'primary'}>Отправить ссылку еще раз
                    </Button>
                </div>
            }
        </>
    )

}

const mapStateToProps = (state) => {
    return {
        user: state.users.user
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        onVerify: (id) => dispatch(UsersApiService.verifyUser(id)),
        onSendVerify: (id) => dispatch(UsersApiService.sendVerifyUser(id))
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(UserVerify);