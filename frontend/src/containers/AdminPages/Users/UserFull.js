import React, {useEffect} from "react";
import {connect} from "react-redux";
import UsersApiService from "../../../services/api/UsersApiService";
import Breadcrumbs from "../../../components/UI/Breadcrumbs";

const SingleUser = (props) => {
	useEffect(() => {
		props.onFetchUser(props.match.params.id);
		document.title = `Пользователи | Rekinetix`
	}, []);

	return (
		<div className="SingleUser">
			<Breadcrumbs
				prevLink="users"
				prevPage="Пользователи"
			/>
			<h3>Пользователь: {props.currentUser && props.currentUser.email}</h3>
			<p>Имя: {props.currentUser && props.currentUser.name}</p>
			<p>Фамилия: {props.currentUser && props.currentUser.surname}</p>
			<p>Город: {props.currentUser && props.currentUser.city}</p>
			<p>Роль: {props.currentUser && props.currentUser.role}</p>
		</div>
	)
};

const mapStateToProps = (state) => {
	return {
		currentUser: state.users.currentUser
	}
};

const mapDispatchToProps = (dispatch) => {
	return {
		onFetchUser: (id) => dispatch(UsersApiService.fetchUser(id))
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(SingleUser);