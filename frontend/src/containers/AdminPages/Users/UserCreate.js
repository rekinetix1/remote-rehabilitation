import React, {useState, useEffect} from "react";
import {connect} from "react-redux";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import UsersApiService from "../../../services/api/UsersApiService";
import '../AdminPages.scss';
import Breadcrumbs from "../../../components/UI/Breadcrumbs";
import {FormControl, Select, MenuItem, InputLabel} from "@material-ui/core";

const AdminPage = (props) => {
    const [user, setUser] = useState({
        name: "",
        surname: "",
        email: "",
        country: "",
        city: "", 
        password: "",
        role: ""
    });

    useEffect(() => {
      document.title = `Добавление пользователя | Rekinetix`
    }, []);

    const changeInputHandler = (e) => {
        setUser({
            ...user,
            [e.target.name]: e.target.value
        });
    };
      
    const submitFormHandler = e => {
        e.preventDefault();
        props.onUserRegistered(user, "/users");
     };

    return (
        <div className="CreatePage">
            <Breadcrumbs
              current="Добавление пользователей"
              prevLink="users"
              prevPage="Пользователи"
            />
            <h2 className="AdminPage_title">Добавление пользователей</h2>
            <form
              onSubmit={submitFormHandler}
              className="AdminPage_form">
                <div className="AdminPage_form_input_wrapper">
                    <TextField
                      id="outlined-basic"
                      name="name"
                      label="Имя"
                      variant="outlined"
                      className="AdminPage_form_input"
                      value={user.name}
                      onChange={changeInputHandler}
                    />
                </div>
                <div className="AdminPage_form_input_wrapper">
                    <TextField
                      id="outlined-basic"
                      name="surname"
                      label="Фамилия"
                      variant="outlined"
                      className="AdminPage_form_input"
                      value={user.surname}
                      onChange={changeInputHandler}
                    />
                </div>
                <div className="AdminPage_form_input_wrapper">
                    <TextField
                      id="outlined-basic"
                      name="country"
                      label="Страна"
                      variant="outlined"
                      className="AdminPage_form_input"
                      value={user.country}
                      onChange={changeInputHandler}
                    />
                </div>
                <div className="AdminPage_form_input_wrapper">
                    <TextField
                      id="outlined-basic"
                      name="city"
                      label="Город"
                      variant="outlined"
                      className="AdminPage_form_input"
                      value={user.city}
                      onChange={changeInputHandler}
                    />
                </div>
                <div className="AdminPage_form_input_wrapper">
                    <TextField
                      id="outlined-basic"
                      name="email"
                      label="Почта"
                      variant="outlined"
                      className="AdminPage_form_input"
                      value={user.email}
                      onChange={changeInputHandler}
                    />
                </div>
                <div className="AdminPage_form_input_wrapper">
                    <TextField
                      id="outlined-basic"
                      name="password"
                      label="Пароль"
                      variant="outlined"
                      className="AdminPage_form_input"
                      value={user.password}
                      onChange={changeInputHandler}
                    />
                </div>
                  <FormControl variant="outlined" className="AdminPage_form_input">
                    <InputLabel id="role_select">Роль</InputLabel>
                    <Select
                      onChange={changeInputHandler}
                      name='role'
                      label="Тип сокращения"
                      required
                      labelId="role_select"
                      id="outlined-basic"
                      variant="outlined"
                      value={user.role}>
                      <MenuItem value={'Администратор'}>Администратор</MenuItem>
                      <MenuItem value={'Терапевт'}>Терапевт</MenuItem>
                      <MenuItem value={'Модератор'}>Модератор</MenuItem>
                      <MenuItem value={'Автор БП'}>Автор БП</MenuItem>
                      <MenuItem value={'Менеджер'}>Менеджер</MenuItem>
                      <MenuItem value={'Пациент'}>Пациент</MenuItem>
                    </Select>
                  </FormControl>
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  className="mt-4"
                >Создать</Button>
            </form>
        </div>
    );
}

const mapDispatchToProps = (dispatch) => {
    return {
        onUserRegistered: (userData, page) => dispatch(UsersApiService.registerUser(userData, page))
    }
};

export default connect(null, mapDispatchToProps)(AdminPage);