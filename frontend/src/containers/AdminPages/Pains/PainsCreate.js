import React, {useState, useEffect} from "react";
import {connect} from "react-redux";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import PainsApiService from "../../../services/api/PainsApiService";
import Breadcrumbs from "../../../components/UI/Breadcrumbs";

const PainsCreate = (props) => {
	const [pain, setPain] = useState({
		title: ""
	});

	useEffect(() => {
		document.title = `Создание жалобы | Rekinetix`
	}, []);

	const changeInputHandler = (e) => {
		setPain({
			...pain,
			[e.target.name]: e.target.value
		});
	};

	const submitForm = (e) => {
		e.preventDefault();
		props.onCreatePain(pain);
		props.history.push("/pains");
	};

	return (
		<div className="CreatePage">
			<Breadcrumbs
				current="Добавление категории жалоб"
				prevLink="pains"
				prevPage="Жалобы"
			/>
			<h2 className="AdminPage_title">Добавление категории жалоб</h2>
			<form
				onSubmit={submitForm}
				className="AdminPage_form">
				<div className="AdminPage_form_input_wrapper">
					<TextField
						id="outlined-basic"
						name="title"
						label="Заголовок"
						variant="outlined"
						className="AdminPage_form_input"
						value={pain.title}
						onChange={changeInputHandler}
						required
					/>
				</div>
				<Button
					type="submit"
					variant="contained"
					color="primary"
				>Создать</Button>
			</form>
		</div>
	)
};

const mapDispatchToProps = (dispatch) => {
	return {
		onCreatePain: (data) => dispatch(PainsApiService.createPain(data))
	}
};

export default connect(null, mapDispatchToProps)(PainsCreate);