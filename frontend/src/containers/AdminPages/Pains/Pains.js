import React, {useEffect} from "react";
import {connect} from "react-redux";
import PainsApiService from "../../../services/api/PainsApiService";
import AdminTable from "../../../components/AdminTable";
import {Link} from "react-router-dom";
import Button from "@material-ui/core/Button";
import '../AdminPages.scss';
import Breadcrumbs from "../../../components/UI/Breadcrumbs";
import AdminPanel from "../../AdminPanel/AdminPanel";

const Pains = (props) => {
  useEffect(() => {
    document.title = `Жалобы | Rekinetix`
    props.onFetchPains();
  }, []);

  const rowsItem = [
    {title: 'title', width: '90%'},
  ]

  return (
    <div className="AdminPage">
      <Breadcrumbs current="Жалобы" />
      <h2 className="m-auto pb-4">Жалобы</h2>
      <div className="ml-auto">
        <Link
          to="/create-pains"
          style={{
            color: "inherit",
            textDecoration: "none"
          }}
        >
          {
            props.user.role !== "Администратор" ? null :
            <Button variant="contained" color="primary" className="ml-3">+ Добавить</Button>
          }
        </Link>
      </div>
        <div className='AdminPage_flexbox'>
            <div className='AdminPage_panel'>
                <AdminPanel closable admin_page={true}/>
            </div>
            <AdminTable
              columns={PainsApiService.columns}
              rows={props.pains}
              route={'pains'}
              deleteRow={props.onDeletePain}
              hideEditIcon={props.user.role !== "Администратор" ? true : false}
              hideDeleteIcon={props.user.role !== "Администратор" ? true: false}
              rowsItem={rowsItem}
              hideShowIcon={true}
            />
        </div>

    </div>
  )
};

const mapStateToProps = (state) => {
  return {
    pains: state.pains.pains,
    loading: state.pains.loading,
    error: state.pains.error,
    user: state.users.user
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    onFetchPains: () => dispatch(PainsApiService.fetchPains()),
    onDeletePain: (id) => dispatch(PainsApiService.deletePain(id)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Pains);