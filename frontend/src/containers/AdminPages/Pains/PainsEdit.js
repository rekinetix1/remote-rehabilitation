import React, {useState, useEffect} from "react";
import {connect} from "react-redux";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import PainsApiService from "../../../services/api/PainsApiService";
import Breadcrumbs from "../../../components/UI/Breadcrumbs";

const PainsEdit = (props) => {
    useEffect(() => {
		document.title = `Редактирование жалобы | Rekinetix`
        props.onFetchSinglePain(props.match.params.id)
            .then(data => {
                setPain({
                    title: data.title
                });
            })
    }, []);

	const [pain, setPain] = useState({
		title: ""
	});

	const changeInputHandler = (e) => {
		setPain({
			...pain,
			[e.target.name]: e.target.value
		});
	};

	const submitForm = (e) => {
		e.preventDefault();
		props.onUpdatePain(props.match.params.id, pain);
		props.history.push("/pains");
	};

	return (
		<div className="CreatePage">
			<Breadcrumbs
				current="Изменение категории жалоб"
				prevLink="pains"
				prevPage="Жалобы"
			/>
			<h2 className="AdminPage_title">Изменение категории жалоб</h2>
			<form
				onSubmit={submitForm}
				className="AdminPage_form">
				<div className="AdminPage_form_input_wrapper">
					<TextField
						id="outlined-basic"
						name="title"
						label="Заголовок"
						variant="outlined"
						className="AdminPage_form_input"
						value={pain.title}
						onChange={changeInputHandler}
						required
					/>
				</div>
				<Button
					type="submit"
					variant="contained"
					color="primary"
				>Изменить</Button>
			</form>
		</div>
	)
};

const mapStateToProps = (state) => {
	return {
		currentPain: state.pains.currentPain,
		loading: state.pains.loading
	}
};

const mapDispatchToProps = (dispatch) => {
	return {
        onFetchSinglePain: (id) => dispatch(PainsApiService.fetchSinglePain(id)),
		onUpdatePain: (id, data) => dispatch(PainsApiService.updatePain(id, data))
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(PainsEdit);