import React, {useState, useEffect} from "react";
import {connect} from "react-redux";
import UserProgramsApiService from "../../../services/api/UserProgramsApiService";
import OptionsApiService from "../../../services/api/OptionsApiService";
import Breadcrumbs from "../../../components/UI/Breadcrumbs/Breadcrumbs";
import {Container, TextField} from "@material-ui/core";
import {FormGroup, Input, Label } from "reactstrap";
import Button from "@material-ui/core/Button";

const UserBaseProgramEdit = props => {
    const [userProgram, setUserProgram] = useState({
        user: {},
        program: "",
        status: "Оплачено",
        options: [],
        program_rate: 0
    });

    const [totalCost, setTotalCost] = useState(0);

    useEffect(() => {
        props.onFetchOptions();
        props.onFetchCurrentUserProgram(props.match.params.id)
            .then(data => {
                setUserProgram({
                    ...userProgram,
                    user: data.user,
                    program: data.program._id,
                    options: data.options,
                    program_rate: data.program_rate
                })
                setTotalCost(data.program_rate);
            })
    }, []);

    const changeInputHandler = (e) => {
        setUserProgram({
            ...userProgram,
            [e.target.name]: e.target.value
        });
    };
      
    const submitFormHandler = e => {
        e.preventDefault();
        props.onUpdateUserProgram(props.match.params.id, {
            user: userProgram.user,
            program: userProgram.program,
            options: userProgram.options,
            status: userProgram.status,
            program_rate: totalCost
        });
    };

    const checkboxHandler = (e) => {
        const arr = [...userProgram.options];
        if (e.target.checked) {
            setTotalCost(parseInt(totalCost) + parseInt(e.target.value));
            arr.push(e.target.id);
            setUserProgram({
                ...userProgram,
                options: arr
            });
        } else {
            setTotalCost(parseInt(totalCost) - parseInt(e.target.value));
            for (let i = arr.length - 1; i >= 0; i--) {
                if (arr[i] === e.target.id) {
                    arr.splice(i, 1);
                }
            }
            setUserProgram({
                ...userProgram,
                options: arr
            });
        }
    }

    const printOptions = () => {
        return props.options.map(option => {
            let checked = false;
            let opt = userProgram.options.find(userOption => userOption === option._id);
            if (opt) checked = true;

            return(
                <FormGroup key={option._id} check variant="outlined" className="AdminPage_form_input">
                    <Label check >
                        {
                            checked ?
                            <Input 
                                type="checkbox" 
                                id={option._id} 
                                value={option.cost} 
                                defaultChecked
                                onClick={checkboxHandler}
                            /> :
                            <Input 
                                type="checkbox" 
                                id={option._id} 
                                value={option.cost} 
                                onClick={checkboxHandler}
                            />
                        }
                        
                            <i>{option.title}</i>
                    </Label>
                    <Label check for={`${option._id}`} className="float-right">
                        <i>{option.cost} тенге</i>
                    </Label>
                </FormGroup>
            );
        })
    }

    return (
            <>
                <Breadcrumbs
                    current="Добавление базовой программы пациенту"
                    prevLink="user_programs"
                    prevPage="Базовые программы пациентов"
                />
                <div className = "EditUserProgram">
                    <h2>Добавление базовой программы пациента</h2>
                    <form onSubmit={submitFormHandler} className="text-left mt-5">
                        <Container>
                            <div className="AdminPage_form_input_wrapper">
                                <TextField
                                    onChange={props.inputChangeHandler}
                                    margin="normal"
                                    name='user'
                                    className="AdminPage_form_input"
                                    variant="outlined"
                                    required
                                    disabled
                                    id="user"
                                    type='text'
                                    value={props.currentUserProgram && props.currentUserProgram.user && props.currentUserProgram.user.surname + " " + props.currentUserProgram.user.name + " - " + props.currentUserProgram.user.email}
                                    label=""/>
                            </div> 
                            <div className="AdminPage_form_input_wrapper">
                                <TextField
                                    onChange={props.inputChangeHandler}
                                    margin="normal"
                                    name='user'
                                    className="AdminPage_form_input"
                                    variant="outlined"
                                    required
                                    disabled
                                    id="user"
                                    type='text'
                                    value={props.currentUserProgram && props.currentUserProgram.program && props.currentUserProgram.program.title}
                                    label=""/>
                            </div>
                            <div className="AdminPage_form_input_wrapper">
                                <h6 className="m-3"><b>Дополнительные возможности:</b></h6>
                                {printOptions()}
                            </div>
                            <div className="AdminPage_form_input_wrapper">
                                <TextField
                                    onChange={changeInputHandler}
                                    margin="normal"
                                    name='program_rate'
                                    disabled
                                    variant="outlined"
                                    className="AdminPage_form_input"
                                    required
                                    id="program_rate"
                                    type='number'
                                    value={totalCost}
                                    label="Стоимость"
                                />
                            </div>
                            <div className="AdminPage_form_input_wrapper">
                                <Button
                                    type="submit"
                                    variant="contained"
                                    color="primary"
                                    >
                                    Назначить
                                </Button>
                            </div>
                        </Container>
                    </form>
                </div>
            </>
        );
}


const mapStateToProps = state => {
    return {
      error: state.users.loginError,
      user: state.users.user,
      options: state.options.options,
      currentUserProgram: state.userPrograms.currentUserProgram
    }
  };

  const mapDispatchToProps = dispatch => {
    return {
      onFetchCurrentUserProgram: (id) => dispatch(UserProgramsApiService.fetchCurrentUserProgram(id)),
      onUpdateUserProgram: (id, program) => dispatch(UserProgramsApiService.updateUserProgram(id, program)),
      onFetchOptions: () => dispatch(OptionsApiService.fetchOptions()),
    };
  };

export default connect(mapStateToProps, mapDispatchToProps)(UserBaseProgramEdit);