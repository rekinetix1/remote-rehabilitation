import React, {useState, useEffect} from "react";
import {connect} from "react-redux";
import UsersApiService from "../../../services/api/UsersApiService";
import ProgramsApiService from "../../../services/api/ProgramsApiService";
import UserProgramsApiService from "../../../services/api/UserProgramsApiService";
import OptionsApiService from "../../../services/api/OptionsApiService";
import Breadcrumbs from "../../../components/UI/Breadcrumbs/Breadcrumbs";
import {Container, FormControl, TextField} from "@material-ui/core";
import {FormGroup, Input, Label } from "reactstrap";
import Button from "@material-ui/core/Button";
import { Autocomplete } from '@material-ui/lab';
import '../AdminPages.scss';

const UserBaseProgramNew = props => {
    const [userProgram, setUserProgram] = useState({
        user: "",
        program: "",
        status: "Оплачено",
        options: [],
        program_rate: 0
    });

    const [totalCost, setTotalCost] = useState(0);

    useEffect(() => {
        props.onFetchUsers('Пациент');
        props.onFetchOptions();
        props.onFetchPrograms();
    }, []);

    useEffect(() => {
        if (userProgram.user){
            props.onFetchUserPrograms(userProgram.user);
        }
    }, [userProgram.user]);

    useEffect(() => {
        if (userProgram.program){
            props.onFetchCurrentProgram(userProgram.program)
            .then(data => {
                console.log(data);
                setUserProgram({
                    ...userProgram,
                    program_rate: data.cost
                });
                setTotalCost(data.cost);
            })
        }
    }, [userProgram.program]);

    const changeInputHandler = (e) => {
        setUserProgram({
            ...userProgram,
            [e.target.name]: e.target.value
        });
    };
      
    const submitFormHandler = e => {
        e.preventDefault();
        props.onCreateUserProgram({
            user: userProgram.user,
            program: userProgram.program,
            options: userProgram.options,
            status: userProgram.status,
            program_rate: totalCost
        }, "user_programs");
    };

    const autocompleteHandler = (_e, values) => {
        if (values) {
            if (values.role){
                setUserProgram({
                    ...userProgram,
                    user: values._id
                })
            } else {
                setUserProgram({
                    ...userProgram,
                    program: values._id
                })
            }
        }
    };

    const checkboxHandler = (e) => {
        const arr = [...userProgram.options];
        if (e.target.checked) {
            setTotalCost(parseInt(totalCost) + parseInt(e.target.value));
            arr.push(e.target.id);
            setUserProgram({
                ...userProgram,
                options: arr
            });
        } else {
            if (parseInt(totalCost) - parseInt(e.target.value) >= userProgram.program_rate) {
                setTotalCost(parseInt(totalCost) - parseInt(e.target.value));
                for (let i = arr.length - 1; i >= 0; i--) {
                    if (arr[i] === e.target.id) {
                        arr.splice(i, 1);
                    }
                }
                setUserProgram({
                    ...userProgram,
                    options: arr
                });
            }
        }
    }
    
    let users = props.users ? props.users : [];
    let programs = props.programs ? [...props.programs] : [];
    
    let newPrograms = [];
    if (props.userPrograms){
        for (let i = 0; i < programs.length; i++){
            let newp = props.userPrograms.find(program => program.program._id === programs[i]._id);
            if (!newp) {
                newPrograms.push(programs[i]);
            }
        }
    }

    return (
            <>
                <Breadcrumbs
                    current="Добавление базовой программы пациенту"
                    prevLink="user_programs"
                    prevPage="Базовые программы пациентов"
                />
                <div className = "NewUserProgram">
                    <h2>Добавление базовой программы пациента</h2>
                    <form onSubmit={submitFormHandler} className="text-left mt-5">
                        <Container>
                            <div className="AdminPage_form_input_wrapper">
                                <FormControl variant="outlined" className="AdminPage_form_input">
                                    <Autocomplete
                                        id="combo-box-demo"
                                        required
                                        label="Пациент"
                                        options={users}
                                        getOptionLabel={(user) => user.surname + " " + user.name + " - " + user.email}
                                        onChange={autocompleteHandler}
                                        value={users.find(user => user._id === props.users.users)}
                                        renderInput={(params) => <TextField {...params} label="Пациент" variant="outlined"/>}
                                    />
                                </FormControl>
                            </div>
                            <div className="AdminPage_form_input_wrapper">
                                <FormControl variant="outlined" className="AdminPage_form_input">
                                    <Autocomplete
                                        id="combo-box-demo"
                                        required
                                        label="Программа"
                                        disabled={userProgram.user ? false: true}
                                        options={newPrograms}
                                        getOptionLabel={(program) => program.title}
                                        onChange={autocompleteHandler}
                                        value={newPrograms.find(program => program._id === newPrograms.programs)}
                                        renderInput={(params) => <TextField {...params} label="Программа" variant="outlined"/>}
                                    />
                                </FormControl>
                            </div>
                            <div className="AdminPage_form_input_wrapper">
                                <h6 className="m-3"><b>Дополнительные возможности:</b></h6>
                                {
                                    props.options.map(option => {
                                        return(
                                            <FormGroup key={option._id} check variant="outlined" className="AdminPage_form_input">
                                                <Label check >
                                                    <Input 
                                                        type="checkbox" 
                                                        id={option._id} 
                                                        disabled={userProgram.program ? false: true}
                                                        value={option.cost} 
                                                        onClick={checkboxHandler}
                                                    />
                                                    <i>{option.title}</i>
                                                </Label>
                                                <Label check for={`${option._id}`} className="float-right">
                                                    <i>{option.cost} тенге</i>
                                                </Label>
                                            </FormGroup>
                                        );
                                    })
                                }           
                            </div>
                            <div className="AdminPage_form_input_wrapper">
                                <TextField
                                    onChange={changeInputHandler}
                                    margin="normal"
                                    name='program_rate'
                                    disabled
                                    variant="outlined"
                                    className="AdminPage_form_input"
                                    required
                                    id="program_rate"
                                    type='number'
                                    value={totalCost}
                                    label="Стоимость"
                                />
                            </div>
                            <div className="AdminPage_form_input_wrapper">
                                <Button
                                    type="submit"
                                    variant="contained"
                                    color="primary"
                                    >
                                    Назначить
                                </Button>
                            </div>
                        </Container>
                    </form>
                </div>
            </>
        );
}


const mapStateToProps = state => {
    return {
      error: state.users.loginError,
      users: state.users.users,
      user: state.users.user,
      programs: state.programs.programs,
      currentProgram: state.programs.currentProgram,
      options: state.options.options,
      userPrograms: state.userPrograms.userPrograms
    }
  };

  const mapDispatchToProps = dispatch => {
    return {
      onFetchPrograms: () => dispatch(ProgramsApiService.fetchPrograms()),
      onFetchCurrentProgram: (id) => dispatch(ProgramsApiService.fetchSingleProgram(id)),
      onFetchUsers: (role) => dispatch(UsersApiService.fetchUsers(role)),
      onCreateUserProgram: (program, link) => dispatch(UserProgramsApiService.postUserProgram(program, link)),
      onFetchOptions: () => dispatch(OptionsApiService.fetchOptions()),
      onFetchUserPrograms: (id) => dispatch(UserProgramsApiService.fetchUserPrograms(id))
    };
  };

export default connect(mapStateToProps, mapDispatchToProps)(UserBaseProgramNew);