import React, {useState, useEffect} from 'react';
import {connect} from "react-redux";
import UserProgramsApiService from "../../../services/api/UserProgramsApiService";
import OptionsApiService from "../../../services/api/OptionsApiService";
import Breadcrumbs from "../../../components/UI/Breadcrumbs/Breadcrumbs";

const UserBaseProgramFull = props => {
    const [userProgram, setUserProgram] = useState({
        user: {},
        program: {},
        status: "Оплачено",
        options: [],
        program_rate: 0
    });

    useEffect(() => {
        props.onFetchOptions();
        props.onFetchCurrentUserProgram(props.match.params.id)
            .then(data => {
                setUserProgram({
                    ...userProgram,
                    user: data.user,
                    program: data.program,
                    options: data.options,
                    program_rate: data.program_rate
                })
            });
    }, []);

    let options = [];
    if (props.options){
        for (let i = 0; i < userProgram.options.length; i++){
            props.options.forEach(option => {
                if (userProgram.options[i] === option._id){
                    options.push(option)
                }
            })
        }
    }

    return (
        <>
            <Breadcrumbs
                current="Базовая программа пациента"
                prevLink="user_programs"
                prevPage="Базовые программы пациентов"
            />
            <div className="FullUserProgram" style={{textAlign: 'left'}}>
                <p><b>Программа: {userProgram.program.title}</b></p>
                <p>Пациент: {userProgram.user.surname} {userProgram.user.name}</p>
                <p>Дополнительные опции: {userProgram.options.length > 0 ? null: "Не куплены"}</p>
                <ul>
                    {
                        options.map(option => {
                            return <li key={option._id}>
                                {option.title} - {option.cost} тенге
                            </li>
                        })
                    }
                </ul>
                <p>Итоговая цена: {userProgram.program_rate} тенге</p>
            </div>
        </>
    );
}

const mapStateToProps = state => {
    return {
      error: state.users.loginError,
      options: state.options.options,
      currentUserProgram: state.userPrograms.currentUserProgram
    }
};

const mapDispatchToProps = dispatch => {
    return {
      onFetchCurrentUserProgram: (id) => dispatch(UserProgramsApiService.fetchCurrentUserProgram(id)),
      onFetchOptions: () => dispatch(OptionsApiService.fetchOptions()),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserBaseProgramFull);