import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import UserProgramsApiService from "../../../services/api/UserProgramsApiService";
import AdminTable from "../../../components/AdminTable";
import {Link} from "react-router-dom";
import Button from "@material-ui/core/Button";
import '../AdminPages.scss';
import AdminPanel from "../../AdminPanel/AdminPanel";
import Breadcrumbs from "../../../components/UI/Breadcrumbs/Breadcrumbs";

const UserBasePrograms = ({programs, onFetchUserPrograms, onDeleteUserProgram, onCreateUserProgram, user}) => {

  useEffect(() => {
      onFetchUserPrograms();
  }, []);

  const rowsItem = [
    {title: 'name', width: '25%'},
    {title: 'email', width: '25%'},
    {title: 'title', width: '35%'},
    {title: 'program_rate', width: '15%'}
  ];

  let hideEditIcon = false;
  if (user.role === "Терапевт" || user.role === "Модератор" || user.role === "Автор БП") hideEditIcon = true;

  const userPrograms = programs.map((program) => {
    if (program.user) {
      program.email = program.user.email
      program.name = program.user.surname + " " + program.user.name
    };
    if (program.program) {
        program.title = program.program.title
    };
    return program;
  });
  return (
    <div className="AdminPage">
      <Breadcrumbs current="Базовые программы пациентов" />
      <h2 className="m-auto pb-4">Базовые программы пациентов</h2>
      <div className="d-flex ml-auto w-75 justify-content-end">
        <div>
          <Link
            to="/user_programs/new"
            style={{
              color: "inherit",
              textDecoration: "none"
            }}
          >
            {
              user.role === "Модератор" ? null :
              <Button variant="contained" color="primary" className="ml-3">+ Назначить</Button> 
            }
          </Link>
        </div>
      </div>
      <div className='AdminPage_flexbox'>
          <div className='AdminPage_panel'>
              <AdminPanel closable/>
          </div>
          <AdminTable
            columns={UserProgramsApiService.columns}
            rows={userPrograms}
            route={'user_programs'}
            deleteRow={onDeleteUserProgram}
            hideEditIcon={hideEditIcon ? true : false}
            hideDeleteIcon={user.role !== "Администратор" ? true: false}
            rowsItem={rowsItem}
          />
      </div>
    </div>
  )
};

const mapStateToProps = (state) => {
  return {
    programs: state.userPrograms.userPrograms,
    loading: state.userPrograms.loading,
    error: state.programs.error,
    user: state.users.user
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    onFetchUserPrograms: () => dispatch(UserProgramsApiService.fetchUserPrograms()),
    onCreateUserProgram: (program) => dispatch(UserProgramsApiService.postUserProgram(program)),
    onDeleteUserProgram: (id) => dispatch(UserProgramsApiService.deleteUserProgram(id))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserBasePrograms);