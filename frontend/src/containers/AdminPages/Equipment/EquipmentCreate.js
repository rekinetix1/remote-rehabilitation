import React, {useState, useEffect} from "react";
import {connect} from "react-redux";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import EquipmentApiService from "../../../services/api/EquipmentApiService";
import '../AdminPages.scss';
import Breadcrumbs from "../../../components/UI/Breadcrumbs";

const EquipmentCreate = (props) => {
	const [equipment, setEquipment] = useState({
		title: ""
	});

	useEffect(() => {
		document.title = `Добавление инвентаря | Rekinetix`
	  }, []);

	const changeInputHandler = (e) => {
		setEquipment({
			...equipment,
			[e.target.name]: e.target.value
		});
	};

	const getFieldError = fieldName => {
		return props.error && props.error.errors && props.error.errors[fieldName] && props.error.errors[fieldName].message;
	};


	const submitForm = (e) => {
		e.preventDefault();
		props.onCreateEquipment(equipment);
	};

	return (
		<div className="CreatePage">
			<Breadcrumbs
				current="Добавление инвентаря"
				prevLink="equipment"
				prevPage="Инвентарь"
			/>
			<h2 className="AdminPage_title">Добавление инвентаря</h2>
			<form
				onSubmit={submitForm}
				className="AdminPage_form">
				<div className="AdminPage_form_input_wrapper">
					<TextField
						helperText={getFieldError("title")}
						id="outlined-basic"
						name="title"
						label="Заголовок"
						variant="outlined"
						className="AdminPage_form_input"
						value={equipment.title}
						onChange={changeInputHandler}
						error={!!(props.error && props.error.errors && props.error.errors['title'] && props.error.errors['title'].message)}
					/>
				</div>
				<Button
					type="submit"
					variant="contained"
					color="primary"
				>Создать</Button>
			</form>
		</div>
	)
};


const mapStateToProps = state => {
	return {
		error: state.equipment.error
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		onCreateEquipment: (data) => dispatch(EquipmentApiService.createEquipment(data))
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(EquipmentCreate);