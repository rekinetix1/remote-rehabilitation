import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import EquipmentApiService from "../../../services/api/EquipmentApiService";
import AdminTable from "../../../components/AdminTable";
import {Link as RouterLink} from "react-router-dom";
import Button from "@material-ui/core/Button";
import '../AdminPages.scss';
import Breadcrumbs from "../../../components/UI/Breadcrumbs";
import AdminPanel from "../../AdminPanel/AdminPanel";
import SearchForm from "../../../components/UI/SearchForm/SearchForm";

const Equipment = ({equipment, onFetchEquipment, onDeleteEquipment, history, onSearchEquipments, location, user}) => {
    const [search, setSearch] = useState(null);

    useEffect(() => {
        document.title = `Инвентарь | Rekinetix`
        if (location.search) {
          const query = new URLSearchParams(location.search);
          for (let param of query.entries()) {
            setSearch(param[1])
            onSearchEquipments(param[1])
          }
        } else {
          setSearch(null)
          onFetchEquipment();
        }
    }, [location.search]);

    const rowsItem = [
        {title: 'title', width: '90%'},
    ]

    return (
        <div className="AdminPage">
            <Breadcrumbs current="Инвентарь"/>
            <h2 className="m-auto pb-4">Инвентарь</h2>
            <div className="justify-content-end add-search-block">
                <div className="search">
                    <SearchForm
                        route="equipment"
                        history={history}
                        placeholder="поиск по названию"
                        search={search}/>
                </div>
                <div className="add_record">
                    <RouterLink
                        to="/create-equipment"
                        style={{
                            color: "inherit",
                            textDecoration: "none"
                        }}
                    >
                      {
                        user.role !== "Администратор" ? null :
                        <Button variant="contained" color="primary" className="ml-3">+ Добавить</Button>
                      }
                    </RouterLink>
                </div>
            </div>
            <div className='AdminPage_flexbox'>
                <div className='AdminPage_panel'>
                <AdminPanel closable admin_page={true}/>
                </div>
                <AdminTable
                    columns={EquipmentApiService.columns}
                    rows={equipment}
                    route={'equipment'}
                    deleteRow={onDeleteEquipment}
                    rowsItem={rowsItem}
                    hideShowIcon={true}
                    hideEditIcon={user.role !== "Администратор" ? true : false}
                    hideDeleteIcon={user.role !== "Администратор" ? true: false}
                />
            </div>
        </div>
    )
};

const mapStateToProps = (state) => {
    return {
        equipment: state.equipment.equipment,
        loading: state.equipment.loading,
        error: state.equipment.error,
        user: state.users.user
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        onFetchEquipment: () => dispatch(EquipmentApiService.fetchEquipment()),
        onDeleteEquipment: (id) => dispatch(EquipmentApiService.deleteEquipment(id)),
        onSearchEquipments: (data) => dispatch(EquipmentApiService.searchEquipments(data)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Equipment);