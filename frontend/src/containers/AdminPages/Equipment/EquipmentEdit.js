import React, {useState, useEffect} from "react";
import {connect} from "react-redux";
import TextField from "@material-ui/core/TextField";
import {Button} from "@material-ui/core";
import EquipmentApiService from "../../../services/api/EquipmentApiService";
import Breadcrumbs from "../../../components/UI/Breadcrumbs";

const EquipmentEdit = (props) => {
	const [equipment, setEquipment] = useState({
		title: ""
	});

	useEffect(() => {
		document.title = `Редактирование инвентаря | Rekinetix`
		props.onFetchSingleEquipment(props.match.params.id)
			.then((data) => {
				setEquipment({
					...equipment,
					title: data.title
				})
			})
	}, []);

	const changeInputHandler = (e) => {
		setEquipment({
			...equipment,
			[e.target.name]: e.target.value
		})
	};

	const submitForm = (e) => {
		e.preventDefault();
		props.onUpdateEquipment(props.match.params.id, equipment);
		props.history.push("/equipment");
	};

	return (
		<div className="AdminPage">
			<Breadcrumbs
				current="Изменение инвентаря"
				prevLink="equipment"
				prevPage="Инвентарь"
			/>
			<h2 className="AdminPage_title">Изменение инвентаря</h2>
			<form
				onSubmit={submitForm}
				className="AdminPage_form"
			>
				<div className="AdminPage_form_input_wrapper">
					<TextField
						required
						id="outlined-basic"
						name="title"
						label="Заголовок"
						variant="outlined"
						className="AdminPage_form_input"
						value={equipment.title}
						onChange={changeInputHandler}
					/>
				</div>
				<Button
					type="submit"
					variant="contained"
					color="primary"
				>
					Изменить
				</Button>
			</form>
		</div>
	)

};

const mapStateToProps = (state) => {
	return {
		currentEquipment: state.equipment.currentEquipment,
		loading: state.equipment.loading
	}
};

const mapDispatchToProps = (dispatch) => {
	return {
		onFetchSingleEquipment: (id) => dispatch(EquipmentApiService.fetchSingleEquipment(id)),
		onUpdateEquipment: (id, data) => dispatch(EquipmentApiService.updateEquipment(id, data))
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(EquipmentEdit);