import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import OccupationApiService from "../../../services/api/OccupationApiService";
import Breadcrumbs from "../../../components/UI/Breadcrumbs";

const OccupationCreate = (props) => {
	const [occupation, setOccupation] = useState({
		title: ""
	});

	useEffect(() => {
		document.title = `Создание рода деятельности | Rekinetix`
	  }, []);

	const changeInputHandler = (e) => {
		setOccupation({
			...occupation,
			[e.target.name]: e.target.value
		});
	};

	const submitForm = (e) => {
		e.preventDefault();
		props.onCreateOccupation(occupation);
	};

	const getFieldError = fieldName => {
		return props.error && props.error.errors && props.error.errors[fieldName] && props.error.errors[fieldName].message;
	};

	return (
		<div className="CreatePage">
			<Breadcrumbs
				current="Добавление рода деятельности"
				prevLink="occupation"
				prevPage="Род деятельности"
			/>
			<h2 className="AdminPage_title">Добавление рода деятельности</h2>
			<form
				onSubmit={submitForm}
				className="AdminPage_form">
				<div className="AdminPage_form_input_wrapper">
					<TextField
						helperText={getFieldError("title")}
						error={!!(props.error && props.error.errors && props.error.errors['title'] && props.error.errors['title'].message)}
						id="outlined-basic"
						name="title"
						label="Заголовок"
						variant="outlined"
						className="AdminPage_form_input"
						value={occupation.title}
						onChange={changeInputHandler}
					/>
				</div>
				<Button
					type="submit"
					variant="contained"
					color="primary"
				>Создать</Button>
			</form>
		</div>
	)
};


const mapStateToProps = state => {
	return {
		error: state.occupation.error
	};
};
const mapDispatchToProps = (dispatch) => {
	return {
		onCreateOccupation: (data) => dispatch(OccupationApiService.createOccupation(data))
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(OccupationCreate);