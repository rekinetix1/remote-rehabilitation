import React, {useEffect} from "react";
import {connect} from "react-redux";
import OccupationApiService from "../../../services/api/OccupationApiService";
import AdminTable from "../../../components/AdminTable";
import {Link} from "react-router-dom";
import Button from "@material-ui/core/Button";
import '../AdminPages.scss';
import Breadcrumbs from "../../../components/UI/Breadcrumbs/Breadcrumbs";
import AdminPanel from "../../AdminPanel/AdminPanel";

const Occupation = (props) => {
  useEffect(() => {
    document.title = `Род деятельности | Rekinetix`
    props.onFetchOccupation();
  }, []);

  const rowsItem = [
    {title: 'title', width: '90%'},
  ]

  return (
    <div className="AdminPage">
      <Breadcrumbs current="Род деятельности" />
      <h2 className="m-auto pb-4">Род деятельности</h2>
      <div className="ml-auto">
        <Link
          to="/create-occupation"
          style={{
            color: "inherit",
            textDecoration: "none"
          }}
        >
          {
            props.user.role !== "Администратор" ? null :
            <Button variant="contained" color="primary" className="ml-3">+ Добавить</Button>
          }
        </Link>
      </div>
        <div className='AdminPage_flexbox'>
            <div className='AdminPage_panel'>
                <AdminPanel closable admin_page={true}/>
            </div>
            <AdminTable
              columns={OccupationApiService.columns}
              rows={props.occupation}
              route={'occupation'}
              deleteRow={props.onDeleteOccupation}
              hideEditIcon={props.user.role !== "Администратор" ? true : false}
              hideDeleteIcon={props.user.role !== "Администратор" ? true: false}
              rowsItem={rowsItem}
              hideShowIcon={true}
            />
        </div>
    </div>
  )
};

const mapStateToProps = (state) => {
  return {
    occupation: state.occupation.occupation,
    loading: state.occupation.loading,
    error: state.occupation.error,
    user: state.users.user
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    onFetchOccupation: () => dispatch(OccupationApiService.fetchOccupation()),
    onDeleteOccupation: (id) => dispatch(OccupationApiService.deleteOccupation(id)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Occupation);