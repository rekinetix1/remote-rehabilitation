import React, {useState, useEffect} from "react";
import {connect} from "react-redux";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import OccupationApiService from "../../../services/api/OccupationApiService";
import Breadcrumbs from "../../../components/UI/Breadcrumbs";

const OccupationCreate = (props) => {
    useEffect(() => {
		document.title = `Редактирование рода деятельности | Rekinetix`
        props.onFetchSingleOccupation(props.match.params.id)
            .then(data => {
                setOccupation({
                    title: data.title
                });
            })
    }, []);

	const [occupation, setOccupation] = useState({
		title: ""
	});

	const changeInputHandler = (e) => {
		setOccupation({
			...occupation,
			[e.target.name]: e.target.value
		});
	};

	const submitForm = (e) => {
		e.preventDefault();
		props.onUpdateOccupation(props.match.params.id, occupation);
		props.history.push("/occupation");
	};

	return (
		<div className="CreatePage">
			<Breadcrumbs
				current="Изменение рода деятельности"
				prevLink="occupation"
				prevPage="Род деятельности"
			/>
			<h2 className="AdminPage_title">Изменение рода деятельности</h2>
			<form
				onSubmit={submitForm}
				className="AdminPage_form">
				<div className="AdminPage_form_input_wrapper">
					<TextField
						id="outlined-basic"
						name="title"
						required
						label="Заголовок"
						variant="outlined"
						className="AdminPage_form_input"
						value={occupation.title}
						onChange={changeInputHandler}
					/>
				</div>
				<Button
					type="submit"
					variant="contained"
					color="primary"
				>Изменить</Button>
			</form>
		</div>
	)
};

const mapStateToProps = (state) => {
	return {
		currentOccupation: state.occupation.currentOccupation,
		loading: state.occupation.loading
	}
};

const mapDispatchToProps = (dispatch) => {
	return {
        onFetchSingleOccupation: (data) => dispatch(OccupationApiService.fetchSingleOccupation(data)),
        onUpdateOccupation: (id, data) => dispatch(OccupationApiService.updateOccupation(id, data))
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(OccupationCreate);