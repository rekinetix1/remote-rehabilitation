import React, {useState, useEffect} from "react";
import {connect} from "react-redux";
import TextField from "@material-ui/core/TextField";
import {Button} from "@material-ui/core";
import ProgramCategoriesApiService from "../../../services/api/ProgramCategoriesApiService";
import Breadcrumbs from "../../../components/UI/Breadcrumbs";

const ProgramCategoryEdit = (props) => {
	const [category, setCategory] = useState({
		title: ""
	});

	useEffect(() => {
		document.title = `Редактирование категории программ | Rekinetix`
		props.onFetchProgramCategory(props.match.params.id)
			.then((data) => {
				setCategory({
					...category,
					title: data.title
				})
			})
	}, []);

	const changeInputHandler = (e) => {
		setCategory({
			...category,
			[e.target.name]: e.target.value
		})
	};

	const submitForm = (e) => {
		e.preventDefault();
		props.onUpdateProgramCategory(props.match.params.id, category);
		props.history.push("/program-categories");
	};

	return (
		<div className="CreatePage">
			<Breadcrumbs
				current="Изменение категории программ"
				prevLink="program-categories"
				prevPage="Категории программ"
			/>
			<h2 className="AdminPage_title">Изменение категории программ</h2>
			<form
				onSubmit={submitForm}
				className="AdminPage_form"
			>
				<div className="AdminPage_form_input_wrapper">
					<TextField
						id="outlined-basic"
						name="title"
						label="Заголовок"
						variant="outlined"
						className="AdminPage_form_input"
						value={category.title}
						onChange={changeInputHandler}
					/>
				</div>
				<Button
					type="submit"
					variant="contained"
					color="primary"
				>
					Изменить
				</Button>
			</form>
		</div>
	)

};

const mapStateToProps = (state) => {
	return {
		currentCategory: state.programCategories.currentCategory,
		loading: state.exerciseCategories.loading
	}
};

const mapDispatchToProps = (dispatch) => {
	return {
		onFetchProgramCategory: (id) => dispatch(ProgramCategoriesApiService.fetchProgramCategory(id)),
		onUpdateProgramCategory: (id, data) => dispatch(ProgramCategoriesApiService.updateProgramCategory(id, data))
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(ProgramCategoryEdit);