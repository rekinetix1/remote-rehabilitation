import React, {useState, useEffect} from "react";
import {connect} from "react-redux";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import ProgramCategoriesApiService from "../../../services/api/ProgramCategoriesApiService";
import Breadcrumbs from "../../../components/UI/Breadcrumbs";

const ProgramCategoryCreate = (props) => {
    const [category, setCategory] = useState({
        title: ""
    });

    const changeInputHandler = (e) => {
        setCategory({
            ...category,
            [e.target.name]: e.target.value
        });
    };

    useEffect(() => {
        document.title = `Создание категории программ | Rekinetix`
    }, []);

    const submitForm = (e) => {
        e.preventDefault();
        props.onCreateProgramCategory(category);
    };
    const getFieldError = fieldName => {
        return props.error && props.error.errors && props.error.errors[fieldName] && props.error.errors[fieldName].message;
    };


    return (
        <div className="CreatePage">
            <Breadcrumbs
              current="Добавление категории программ"
              prevLink="program-categories"
              prevPage="Категории программ"
            />
            <h2 className="AdminPage_title">Добавление категории программ</h2>
            <form
              onSubmit={submitForm}
              className="AdminPage_form">
                <div className="AdminPage_form_input_wrapper">
                    <TextField
                      id="outlined-basic"
                      name="title"
                      label="Заголовок"
                      variant="outlined"
                      className="AdminPage_form_input"
                      value={category.title}
                      onChange={changeInputHandler}
                      helperText={getFieldError("title")}
                      error={!!(props.error && props.error.errors && props.error.errors['title'] && props.error.errors['title'].message)}
                    />
                </div>
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                >Создать</Button>
            </form>
        </div>
    )
};
const mapStateToProps = state => {
    return {
        error: state.programCategories.error
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onCreateProgramCategory: (data) => dispatch(ProgramCategoriesApiService.createProgramCategory(data))
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(ProgramCategoryCreate);

