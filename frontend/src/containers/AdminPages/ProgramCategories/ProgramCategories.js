import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import '../AdminPages.scss';
import ProgramCategoriesApiService from "../../../services/api/ProgramCategoriesApiService";
import Button from "@material-ui/core/Button";
import AdminTable from "../../../components/AdminTable";
import Breadcrumbs from "../../../components/UI/Breadcrumbs";
import AdminPanel from "../../AdminPanel/AdminPanel";
import SearchForm from "../../../components/UI/SearchForm/SearchForm";

const ProgramCategories = (props) => {
  const [search, setSearch] = useState(null);

  useEffect(() => {
    document.title = `Категории программ | Rekinetix`
    if (props.location.search) {
      const query = new URLSearchParams(props.location.search);
      for (let param of query.entries()) {
        setSearch(param[1])
        props.onSearchProgramsCategories(param[1])
      }
    } else {
      setSearch(null)
      props.onFetchProgramCategories();
    }
  }, [props.location.search]);

  const rowsItem = [
    {title: 'title', width: '90%'},
  ]

  return (
    <div className="AdminPage">
      <Breadcrumbs current="Категории программ" />
      <h2 className="m-auto pb-4">Категории программ</h2>
      <div className="justify-content-end add-search-block">
        <div className="search">
          <SearchForm
            route="program-categories"
            history={props.history}
            placeholder="поиск по названию"
            search={search}/>
        </div>
        <div className="add_record">
          <Link
            to="/create-program_category"
            style={{
              color: "inherit",
              textDecoration: "none"
            }}
          >
            {
              props.user.role !== "Администратор" ? null :
              <Button variant="contained" color="primary" className="ml-3">+ Добавить</Button>
            }
          </Link>
        </div>
      </div>
      <div className='AdminPage_flexbox'>
          <div className='AdminPage_panel'>
              <AdminPanel closable admin_page={true}/>
          </div>
          <AdminTable
              columns={ProgramCategoriesApiService.columns}
              rows={props.programCategories}
              route={'program-categories'}
              deleteRow={props.onDeleteProgramCategory}
              rowsItem={rowsItem}
              hideShowIcon={true}
              hideEditIcon={props.user.role !== "Администратор" ? true : false}
              hideDeleteIcon={props.user.role !== "Администратор" ? true: false}
          />
      </div>
    </div>
  )
};

const mapStateToProps = state => {
  return {
    programCategories: state.programCategories.programCategories,
    loading: state.programCategories.loading,
    error: state.programCategories.error,
    user: state.users.user
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchProgramCategories: () => dispatch(ProgramCategoriesApiService.fetchProgramCategories()),
    onDeleteProgramCategory: (id) => dispatch(ProgramCategoriesApiService.deleteProgramCategory(id)),
    onSearchProgramsCategories: (data) => dispatch(ProgramCategoriesApiService.searchProgramCategories(data)),
  }
};

export default connect(mapStateToProps ,mapDispatchToProps)(ProgramCategories);