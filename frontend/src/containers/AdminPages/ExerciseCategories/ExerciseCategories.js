import React, {useEffect} from "react";
import {connect} from "react-redux";
import {Link as RouterLink} from "react-router-dom";
import AdminTable from "../../../components/AdminTable";
import Button from "@material-ui/core/Button";
import ExerciseCategoriesApiService from "../../../services/api/ExerciseCategoriesApiService";
import '../AdminPages.scss'
import Breadcrumbs from "../../../components/UI/Breadcrumbs";
import AdminPanel from "../../AdminPanel/AdminPanel";

const ExerciseCategories = (props) => {
  useEffect(() => {
    document.title = `Области применения упражнений | Rekinetix`
    props.onFetchExerciseCategories();
  }, []);

  const rowsItem = [
    {title: 'title', width: '90%'},
  ]

  return (
    <div className="AdminPage">
      <Breadcrumbs current="Области применения упражнений" />
      <h2 className="m-auto pb-4">Области применения упражнений</h2>
      <div className="d-flex ml-auto w-75 justify-content-end">
        <div>
          <RouterLink
            to="/create-exercise-category"
            style={{
              color: "inherit",
              textDecoration: "none"
            }}
          >
            {
              props.user.role !== "Администратор" ? null :
              <Button variant="contained" color="primary" className="ml-3">+ Добавить</Button>
            }
        </RouterLink>
        </div>
      </div>
      <div className='AdminPage_flexbox'>
          <div className='AdminPage_panel'>
              <AdminPanel closable admin_page={true}/>
          </div>
          <AdminTable
              columns={ExerciseCategoriesApiService.columns}
              rows={props.exerciseCategories}
              route={'exercise-categories'}
              deleteRow={props.onDeleteExerciseCategory}
              rowsItem={rowsItem}
              hideShowIcon={true}
              hideEditIcon={props.user.role !== "Администратор" ? true : false}
              hideDeleteIcon={props.user.role !== "Администратор" ? true: false}
          />
      </div>
    </div>
  )
};

const mapStateToProps = state => {
  return {
    exerciseCategories: state.exerciseCategories.exerciseCategories,
    loading: state.exerciseCategories.loading,
    error: state.exerciseCategories.error,
    user: state.users.user
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchExerciseCategories: () => dispatch(ExerciseCategoriesApiService.fetchExerciseCategories()),
    onDeleteExerciseCategory: (id) => dispatch(ExerciseCategoriesApiService.deleteExerciseCategory(id)),
  }
};

export default connect(mapStateToProps ,mapDispatchToProps)(ExerciseCategories);