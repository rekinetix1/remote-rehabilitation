import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import TextField from "@material-ui/core/TextField";
import '../AdminPages.scss';
import Button from "@material-ui/core/Button";
import ExerciseCategoriesApiService from "../../../services/api/ExerciseCategoriesApiService";
import Breadcrumbs from "../../../components/UI/Breadcrumbs";

const ExerciseCategoryCreate = (props) => {
    const [category, setCategory] = useState({
        title: ""
    });

    useEffect(() => {
		document.title = `Создание области применения упражнений | Rekinetix`
	}, []);

    const changeInputHandler = (e) => {
        setCategory({
            ...category,
            [e.target.name]: e.target.value
        });
    };

    const submitForm = (e) => {
        e.preventDefault();
        props.onCreateExerciseCategory(category);
    };

    const getFieldError = fieldName => {
        return props.error && props.error.errors && props.error.errors[fieldName] && props.error.errors[fieldName].message;
    };

    useEffect(() => {
        console.log(props.error)
    }, [props.error])

    return (
        <div className="CreatePage">
            <Breadcrumbs
              current="Добавление области применения упражнений"
              prevLink="exercise-categories"
              prevPage="Области применения упражнений"
            />
            <h2 className="AdminPage_title">Добавление области применения упражнений</h2>
            <form
                onSubmit={submitForm}
                className=" AdminPage_form">
                <div className="AdminPage_form_input_wrapper">
                    <TextField
                        id="outlined-basic"
                        name="title"
                        label="Заголовок"
                        variant="outlined"
                        className="AdminPage_form_input"
                        value={category.title}
                        onChange={changeInputHandler}
                        helperText={getFieldError("title")}
                        error={!!(props.error && props.error.errors && props.error.errors['title'] && props.error.errors['title'].message)}
                    />
                </div>
                <Button
                    type="submit"
                    variant="contained"
                    color="primary"
                >Создать</Button>
            </form>
        </div>
    )
};
const mapStateToProps = state => {
    return {
        error: state.exerciseCategories.error
    };
};


const mapDispatchToProps = (dispatch) => {
    return {
        onCreateExerciseCategory: (data) => dispatch(ExerciseCategoriesApiService.createExerciseCategory(data))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ExerciseCategoryCreate);