import React, {useState, useEffect} from "react";
import {connect} from "react-redux";
import TextField from "@material-ui/core/TextField";
import {Button} from "@material-ui/core";
import ExerciseCategoriesApiService from "../../../services/api/ExerciseCategoriesApiService";
import Breadcrumbs from "../../../components/UI/Breadcrumbs";

const ExerciseCategoryEdit = (props) => {
	const [category, setCategory] = useState({
		title: ""
	});


	const getFieldError = fieldName => {
		return props.error && props.error.errors && props.error.errors[fieldName] && props.error.errors[fieldName].message;
	};

	useEffect(() => {
		document.title = `Редактирование области применения упражнений | Rekinetix`
		props.onFetchExerciseCategory(props.match.params.id)
			.then((data) => {
				setCategory({
					...category,
					title: data.title
				})
			})
	}, []);

	const changeInputHandler = (e) => {
		setCategory({
			...category,
			[e.target.name]: e.target.value
		})
	};

	const submitForm = (e) => {
		e.preventDefault();
		props.onUpdateExerciseCategory(props.match.params.id, category)
	};

	return (
		<div className="AdminPage">
			<Breadcrumbs
				current="Изменение области применения упражнений"
				prevLink="exercise-categories"
				prevPage="Области применения упражнений"
			/>
			<h2 className="AdminPage_title">Изменение области применения упражнений</h2>
			<form
				onSubmit={submitForm}
				className="AdminPage_form"
			>
				<div className="AdminPage_form_input_wrapper">
					<TextField
						helperText={getFieldError("title")}
						id="outlined-basic"
						name="title"
						label="Заголовок"
						variant="outlined"
						className="AdminPage_form_input"
						value={category.title}
						onChange={changeInputHandler}
						error={!!(props.error && props.error.errors && props.error.errors['title'] && props.error.errors['title'].message)}
					/>
				</div>
					<Button
						type="submit"
						variant="contained"
						color="primary"
					>
						Изменить
					</Button>
			</form>
		</div>
	)

};

const mapStateToProps = (state) => {
	return {
		currentCategory: state.exerciseCategories.currentCategory,
		loading: state.exerciseCategories.loading,
		error: state.exerciseCategories.error
	}
};


const mapDispatchToProps = (dispatch) => {
	return {
		onFetchExerciseCategory: (id) => dispatch(ExerciseCategoriesApiService.fetchExerciseCategory(id)),
		onUpdateExerciseCategory: (id, data) => dispatch(ExerciseCategoriesApiService.updateExerciseCategory(id, data))
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(ExerciseCategoryEdit);