import React, {Component} from "react";
import {connect} from "react-redux";
import TextField from '@material-ui/core/TextField';
import {Container} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import {Link as RouterLink} from "react-router-dom";
import UsersApiService from "../../services/api/UsersApiService";
import ModalWindow from "../../components/UI/ModalWindow/ModalWindow";
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';

class ChangePassword extends Component {
    state = {
        newPassword:[],
        showModal: false
    }

    componentDidMount() {
        document.title = `Изменение пароля | Rekinetix`
    }

    inputChangeHandler = e => {
        this.setState({...this.state.newPassword,newPassword:{...this.state.newPassword,[e.target.name]: e.target.value}});
    };

    submitFormHandler = e => {
        e.preventDefault();

        this.showModal()
    };

    showModal = () => {
        setTimeout(() => {
            if (this.props.checkUser === 12) {
                this.setState({showModal: true})
            }
        }, 3000)
    };

    closeModal = () => {
        this.setState({showModal: false})
    };

    render() {
        let idUser=this.props.match.params.id
        return (
            <div className='Login'>
                <h2>
                    Изменение пароля
                </h2>
                <form onSubmit={this.submitFormHandler}>
                    <Container maxWidth={'xs'}>
                        {(this.props.user)?
                            <div>
                                <label htmlFor='oldPassword'>Введите старый пароль</label>
                                <TextField onChange={this.inputChangeHandler}
                                           margin="normal"
                                           name='oldPassword'
                                           fullWidth
                                           required
                                           id="oldPassword"
                                           type='password'
                                           label="Старый пароль"/>
                            </div>
                        :null}
                        <div>
                            <label htmlFor='password'>Введите новый пароль</label>
                            <TextField onChange={this.inputChangeHandler}
                                       margin="normal"
                                       name='password'
                                       fullWidth
                                       required
                                       id="password"
                                       type='password'
                                       label="Пароль"/>
                        </div>
                        <div>
                            <label htmlFor='confirmPassword'>подтвердите пароль пароль</label>
                            <TextField onChange={this.inputChangeHandler}
                                       margin="normal"
                                       name='confirmPassword'
                                       fullWidth
                                       required
                                       id="confirmPassword"
                                       type='confirmPassword'
                                       label="Подтвердите пароль"/>
                        </div>
                        <div className='Login_submit'>
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                onClick={() => this.props.resetPasswordUserInBase(this.state.newPassword, idUser)}
                            >
                               Изменить пароль
                            </Button>
                        </div>
                    </Container>
                </form>

                <ModalWindow show={this.state.showModal} closed={this.closeModal}>
                    <CheckCircleOutlineIcon style={{color: 'green', fontSize: '150px'}}/>
                    <span><h4>Письмо с восстановлением пароля отправлено вам на почту!
                        </h4>
                        <Button component={RouterLink} size="large"
                                variant="outlined"
                                color="primary"
                                to="/login"> Вернуться к входу в аккаунт </Button></span>
                </ModalWindow>


            </div>
        );
    }
}


const mapStateToProps = state => {
    return {
        checkUser: state.users.checkUserInBase,
        user: state.users.user
    }
};
const mapDispatchToProps = dispatch => {
    return {
        resetPasswordUserInBase: (passwords, idUser) => dispatch(UsersApiService.resetPasswordInBase(passwords, idUser))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ChangePassword);
