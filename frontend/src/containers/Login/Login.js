import React, {Component} from "react";
import UsersApiService from "../../services/api/UsersApiService";
import {connect} from "react-redux";
import './Login.css'
import TextField from '@material-ui/core/TextField';
import {Container} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import {Link as RouterLink} from "react-router-dom";

class Login extends Component {
    inputChangeHandler = e => {
        this.setState({[e.target.name]: e.target.value});
    };

    submitFormHandler = e => {
        e.preventDefault();
        this.props.loginUser(this.state)
        .then(data => {
            if (data) this.props.history.push('/')
        })
    };

    componentDidMount() {
        document.title = `Авторизация | Rekinetix`
    }

    getFieldError = fieldName => {
        return this.props.error && this.props.error.errors && this.props.error.errors[fieldName]
    };

    render() {
        return (
            <div className='Login'>
                <h2>
                    Вход
                </h2>
                <form onSubmit={this.submitFormHandler}>
                    <Container maxWidth={'xs'}>
                        <div>
                            <TextField onChange={this.inputChangeHandler}
                                           helperText={this.getFieldError("email")}
                                           error={!!(this.props.error
                                               && this.props.error.errors
                                               && this.props.error.errors['email'])}
                                       margin="normal"
                                       name='email'
                                       fullWidth
                                       required
                                       id="email"
                                       type='email'
                                       label="Введите ваш логин"/>
                        </div>
                        <div>
                            <TextField onChange={this.inputChangeHandler}
                                       helperText={this.getFieldError("password")}
                                       error={!!(this.props.error
                                           && this.props.error.errors
                                           && this.props.error.errors['password']
                                         )}
                                       margin="normal"
                                       name='password'
                                       fullWidth
                                       required
                                       id="password"
                                       type='password'
                                       label="Введите ваш пароль"/>
                        </div>
                        <div className='Login_submit'>
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                            >
                                ВОЙТИ
                            </Button>
                        </div>
                        <Button component={RouterLink} color='primary' to="/register"> Зарегистрироваться </Button>
                        <Button component={RouterLink} color='primary' to="/passwordRecovery"> Восстановить пароль</Button>
                    </Container>
                </form>
            </div>
        );
    }
}


const mapStateToProps = state => {
    return {
        error: state.users.loginError
    }
};
const mapDispatchToProps = dispatch => {
    return {
        loginUser: (userData) => dispatch(UsersApiService.loginUser(userData))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);



