import React, {Component} from 'react';
import {NotificationContainer} from "react-notifications";
import 'react-notifications/lib/notifications.css';
import './App.css';
import Toolbar from "../components/UI/Toolbar/Toolbar";
import {Route, Switch} from "react-router-dom";

import Login from "./Login/Login";
import Register from "./Register/Register";
import PersonalArea from "./PersonalArea/PersonalArea";
import {connect} from 'react-redux';
import {withRouter} from 'react-router';
import UsersApiService from "../services/api/UsersApiService";
import PasswordRecovery from "./PasswordRecovery/PasswordRecovery";

import Users from "./AdminPages/Users/Users";
import UserCreate from './AdminPages/Users/UserCreate';
import UserEdit from './AdminPages/Users/UserEdit';
import UserFull from './AdminPages/Users/UserFull';
import UserVerify from './AdminPages/Users/UserVerify';

import ProgramCategories from './AdminPages/ProgramCategories';
import ProgramCategoryCreate from './AdminPages/ProgramCategories/ProgramCategoryCreate';
import ProgramCategoryEdit from './AdminPages/ProgramCategories/ProgramCategoryEdit';

import ExerciseCategories from "./AdminPages/ExerciseCategories";
import ExerciseCategoryCreate from "./AdminPages/ExerciseCategories/ExerciseCategoryCreate";
import ExerciseCategoryEdit from "./AdminPages/ExerciseCategories/ExerciseCategoryEdit";

import Equipment from "./AdminPages/Equipment";
import EquipmentEdit from "./AdminPages/Equipment/EquipmentEdit";
import EquipmentCreate from "./AdminPages/Equipment/EquipmentCreate";

import Exercises from "./AdminPages/Exercises"
import ExerciseCreate from './AdminPages/Exercises/ExerciseCreate';
import ExerciseEdit from './AdminPages/Exercises/ExerciseEdit';
import ExerciseFull from "./AdminPages/Exercises/ExerciseFull";

import Occupation from './AdminPages/Occupation/Occupation';
import OccupationCreate from './AdminPages/Occupation/OccupationCreate';
import OccupationEdit from './AdminPages/Occupation/OccupationEdit';

import Options from "./AdminPages/Options";
import OptionCreate from './AdminPages/Options/OptionCreate';
import OptionEdit from "./AdminPages/Options/OptionEdit";
import OptionFull from './AdminPages/Options/OptionFull';

import ProgramIndex from './AdminPages/Programs/ProgramIndex';
import Programs from "./Programs/Programs";
import FullProgram from "./FullProgram/FullProgram";
import ProgramNew from "./AdminPages/Programs/ProgramNew";
import ProgramEdit from "./AdminPages/Programs/ProgramEdit";

import PhysicalActivities from './AdminPages/PhysicalActivities/PhysicalActivities';
import PhysicalActivitiesCreate from './AdminPages/PhysicalActivities/PhysicalActivitiesCreate';
import PhysicalActivitiesEdit from './AdminPages/PhysicalActivities/PhysicalActivitiesEdit';

import Pains from './AdminPages/Pains/Pains';
import PainsCreate from './AdminPages/Pains/PainsCreate';
import PainsEdit from './AdminPages/Pains/PainsEdit';

import Transactions from './AdminPages/Transactions/Transactions';
import TransactionsEdit from './AdminPages/Transactions/TransactionsEdit';
import TransactionsFull from './AdminPages/Transactions/TransactionsFull';

import IndividualPrograms from './AdminPages/IndividualPrograms/IndividualPrograms';
import IndividualProgramNew from './AdminPages/IndividualPrograms/IndividualProgramNew';
import IndividualProgramEdit from './AdminPages/IndividualPrograms/IndividualProgramEdit';
import IndividualProgramFull from './AdminPages/IndividualPrograms/IndividualProgramFull';

import Muscles from "./AdminPages/Muscles/Muscles";
import MusclesNew from "./AdminPages/Muscles/MusclesNew";
import MusclesEdit from "./AdminPages/Muscles/MusclesEdit";

import Comments from "./AdminPages/Comments/Comments";

import AdminPanel from "./AdminPanel/AdminPanel";
import ChangePassword from "./ChangePassword/ChangePassword";
import BrowsingHistory from "./AdminPages/BrowsingHistory/BrowsingHistory";
import Container from "@material-ui/core/Container";
import {Redirect} from "react-router";
import UserBasePrograms from './AdminPages/UserBasePrograms/UserBasePrograms';
import UserBaseProgramNew from './AdminPages/UserBasePrograms/UserBaseProgramNew';
import UserBaseProgramEdit from './AdminPages/UserBasePrograms/UserBaseProgramEdit';
import UserBaseProgramFull from './AdminPages/UserBasePrograms/UserBaseProgramFull';
import NotFound from './NotFound/NotFound'
import Footer from "../components/Footer/Footer";
import axios from '../axios-api'
import withLoader from "../hoc/withLoader";

const ProtectedRoute = props => {
    return props.isAllowed ?
        <Route {...props} /> : <Redirect to={props.redirectTo}/>
};

class App extends Component {
    render() {
        const {user} = this.props;
        console.log(process.env);
        return (
            <div className="App">
                <div className="App_content">
                    <NotificationContainer/>
                    <Toolbar
                        user={user}
                        logout={this.props.logout}
                    />
                    <Container maxWidth={false} className='App-routes px-5'>
                        {user && user.verify ?

                            <Switch>
                                <Route path="/" exact component={Programs}/>
                                <Route path="/programs" exact component={Programs}/>
                                <Route path="/programs/new" exact component={ProgramNew}/>
                                <Route path="/programs/:id" exact component={FullProgram}/>
                                <Route path="/programs/individual/:id" exact component={FullProgram}/>
                                <Route path="/changePassword/:id" exact component={ChangePassword}/>
                                <Route path="/passwordRecovery" exact component={PasswordRecovery}/>

                                <ProtectedRoute
                                    path="/admin-panel"
                                    component={AdminPanel}
                                    exact
                                    isAllowed={user && user.role !== "Пациент"}
                                    redirectTo="/"
                                />
                                <Route path="/personalArea/:page?" render={props => <PersonalArea{...props}/>}/>

                                <Route path="/create-user" exact component={UserCreate}/>
                                <Route path="/users/:id" exact component={UserFull}/>
                                <Route path="/users/:id/edit" exact component={UserEdit}/>
                                <Route path="/users/verify/:id" exact component={UserVerify}/>
                                <Route path="/users" component={Users}/>

                                <Route path="/program-categories" exact component={ProgramCategories}/>
                                <Route path="/create-program_category" exact component={ProgramCategoryCreate}/>
                                <Route path="/program-categories/:id/edit" exact component={ProgramCategoryEdit}/>

                                <Route path="/exercise-categories" exact component={ExerciseCategories}/>
                                <Route path="/create-exercise-category" exact component={ExerciseCategoryCreate}/>
                                <Route path="/exercise-categories/:id/edit" exact component={ExerciseCategoryEdit}/>

                                <Route path="/equipment" exact component={Equipment}/>
                                <Route path="/create-equipment" exact component={EquipmentCreate}/>
                                <Route path="/equipment/:id/edit" exact component={EquipmentEdit}/>

                                <Route path="/exercises" exact component={Exercises}/>
                                <Route path="/create-exercises" exact component={ExerciseCreate}/>
                                <Route path="/exercises/:id/edit" exact component={ExerciseEdit}/>
                                <Route path="/exercises/:id" exact component={ExerciseFull}/>

                                <Route path="/occupation" exact component={Occupation}/>
                                <Route path="/create-occupation" exact component={OccupationCreate}/>
                                <Route path="/occupation/:id/edit" exact component={OccupationEdit}/>

                                <Route path="/options" exact component={Options}/>
                                <Route path="/create-options" exact component={OptionCreate}/>
                                <Route path="/options/:id/edit" exact component={OptionEdit}/>
                                <Route path="/options/:id" exact component={OptionFull}/>

                                <Route path="/activities" exact component={PhysicalActivities}/>
                                <Route path="/create-activities" exact component={PhysicalActivitiesCreate}/>
                                <Route path="/activities/:id/edit" exact component={PhysicalActivitiesEdit}/>

                                <Route path="/pains" exact component={Pains}/>
                                <Route path="/create-pains" exact component={PainsCreate}/>
                                <Route path="/pains/:id/edit" exact component={PainsEdit}/>

                                <Route path="/transactions" exact component={Transactions}/>
                                <Route path="/transactions/:id/edit" exact component={TransactionsEdit}/>
                                <Route path="/transactions/:id" exact component={TransactionsFull}/>

                                <Route path="/programs_panel" exact component={ProgramIndex}/>
                                <Route path="/programs/:id/edit" exact component={ProgramEdit}/>

                                <Route path="/individual_programs" exact component={IndividualPrograms}/>

                                <Route path="/individual_programs/new" exact component={IndividualProgramNew}/>
                                <Route path="/individual_programs/:id" exact component={IndividualProgramFull}/>
                                <Route path="/individual_programs/:id/edit" exact component={IndividualProgramEdit}/>

                                <Route path="/user_programs" exact component={UserBasePrograms}/>
                                <Route path="/user_programs/new" exact component={UserBaseProgramNew}/>
                                <Route path="/user_programs/:id" exact component={UserBaseProgramFull}/>
                                <Route path="/user_programs/:id/edit" exact component={UserBaseProgramEdit}/>

                                <Route path="/muscles" exact component={Muscles}/>
                                <Route path="/muscles/new" exact component={MusclesNew}/>
                                <Route path="/muscles/:id/edit" exact component={MusclesEdit}/>

                                <Route path="/browsing_history" exact component={BrowsingHistory}/>
                                <Route path="/comments" exact component={Comments}/>
                                <Route path="/users/verify/:id" exact component={UserVerify}/>
                                <Route component={NotFound}/>
                            </Switch> :
                            <Switch>
                                <Route path="/programs/:id" exact component={FullProgram}/>
                                <Route path="/register" exact component={Register}/>
                                <Route path="/login" exact component={Login}/>
                                <Route path="/" exact component={Programs}/>
                                <Route path="/programs/:id" exact component={FullProgram}/>
                                <Route path="/programs" exact component={Programs}/>
                                <Route path="/users/verify/:id" exact component={UserVerify}/>
                                <Route path="/changePassword/:id" exact component={ChangePassword}/>
                                <Route path="/passwordRecovery" exact component={PasswordRecovery}/>
                                {user ? <Route exact component={UserVerify}/> : <Route component={NotFound}/>}
                            </Switch>
                        }
                    </Container>
                </div>
                <Footer/>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        user: state.users.user
    };
};

const mapDispatchToProps = disptach => {
    return {
        logout: () => disptach(UsersApiService.logout())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(withLoader(App, axios)));
