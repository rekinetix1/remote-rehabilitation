import React, {Component} from "react";
import Loader from "../components/UI/Loader/Loader";

const withLoader = (WrappedComponent, axios) => {
    return class extends Component {
        interceptorRes;
        interceptorReq;
        state = {loading: false};

        componentDidMount() {
            this.interceptorReq = axios.interceptors.request.use(req => {
                this.setState({loading: true});
                return req;
            });

            this.interceptorRes = axios.interceptors.response.use(res => {
                this.setState({loading: false});
                return res;
            }, error => {
                this.setState({loading: false});
                return Promise.reject(error);
            });
        }

        componentWillUnmount() {
            axios.interceptors.response.eject(this.interceptorRes);
            axios.interceptors.request.eject(this.interceptorReq);
        }

        render() {
            return (
                <>
                    <WrappedComponent {...this.props} />
                    {this.state.loading ? <Loader/> : null}
                </>
            );
        }
    }
};

export default withLoader;


