import axios from "../../axios-api";
import {
    createQuestionnaireSuccess, fetchQuestionnairesFailure, fetchQuestionnairesRequest, 
    fetchQuestionnairesSuccess, fetchUserQuestionnaireSuccess, updateUserQuestionnaireSuccess,
    updateUserQuestionnaireFailure, createQuestionnaireFailure, fetchUserQuestionnaireFailure
} from "../../redux/actions/questionnairesActions";
import UsersApiService from "./UsersApiService";

class QuestionnariesUser {
    getUserQuestionnaire = (id) => {
        return dispatch => {
            dispatch(fetchQuestionnairesRequest());
            return axios.get(`/questionnaires/${id}`)
                .then((response) => {
                    dispatch(fetchUserQuestionnaireSuccess(response.data));
                    return response.data;
                    
                }).catch(error => {
                    dispatch(fetchUserQuestionnaireFailure(error));
            })
        }
    };

    updateQuestionnaire = (id, userData) => {
        return dispatch => {
            dispatch(fetchQuestionnairesRequest());
            axios.put(`/questionnaires/${id}`, userData)
                .then(response => {
                    dispatch(updateUserQuestionnaireSuccess(response.data));
                })
                .catch(error => {
                    dispatch(updateUserQuestionnaireFailure(error));
            })
        }
    };

    createQuestionnaire = (data, url) => {
        return dispatch => {
            dispatch(fetchQuestionnairesRequest());
            return axios.post("/questionnaires", data)
                .then(res => {
                    console.log(res)
                    dispatch(createQuestionnaireSuccess());
                    dispatch(UsersApiService.updateUser(res.data.user, {questionnaire: res.data._id}));
                    return res.data;
                })
                .catch(error => {
                    dispatch(createQuestionnaireFailure(error));
                })
        }
    };
    fetchQuestionnaires = () => {
        return dispatch => {
            dispatch(fetchQuestionnairesRequest());
            axios.get("/questionnaires")
                .then(response => {
                    dispatch(fetchQuestionnairesSuccess(response.data));
                })
                .catch(error => {
                    console.log(error);
                    dispatch(fetchQuestionnairesFailure(error));
                })
        }
    };
}

export default new QuestionnariesUser;
