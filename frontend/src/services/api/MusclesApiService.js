import axios from "../../axios-api";
import {NotificationManager} from "react-notifications";
import {
    fetchMusclesRequest,
    fetchMusclesSuccess,
    fetchMusclesFailure,
    fetchMuscleSuccess,
    fetchMuscleFailure
} from "../../redux/actions/musclesActions";
import {push} from "connected-react-router";

class MusclesApiService {
	columns = [
		{ id: 'title', numeric: false, disablePadding: true, label: 'Заголовок' },
		{ id: 'actions', numeric: false, disablePadding: false, label: 'Действия' },
	];

    fetchMuscles() {
        return dispatch => {
            dispatch(fetchMusclesRequest());
            axios.get("/muscles")
                .then(response => {
                dispatch(fetchMusclesSuccess(response.data))
                })
                .catch(error => {
                dispatch(fetchMusclesFailure(error))
                })
        }
    };

    searchMuscles = (data) => {
        let url = `/muscles?search=${data}`;
        return dispatch => {
          dispatch(fetchMusclesRequest());
          axios.get(url).then(response => {
            dispatch(fetchMusclesSuccess(response.data));
          }).catch(error => {
            dispatch(fetchMusclesFailure(error));
          });
        };
    };

    fetchMuscle(id) {
        return dispatch => {
            dispatch(fetchMusclesRequest());
            axios.get(`/muscles/${id}`)
                .then(response => {
                    dispatch(fetchMuscleSuccess(response.data));
                    return response.data;
                })
                .catch(error => {
                    dispatch(fetchMuscleFailure(error))
                })
        }
    };

    createMuscle = data => {
        return dispatch => {
            dispatch(fetchMusclesRequest());
            axios.post("/muscles", data)
                .then(() => {
                    NotificationManager.success("Успешно создано");
                    dispatch(push("/muscles"));
                })
                .catch(error => {
                    if (error.response && error.response.data) {
                        dispatch(fetchMuscleFailure(error.response.data));
                    }
                })
        }
    };

    deleteMuscle = id => {
        return dispatch => {
        axios.delete(`/muscles/${id}`)
            .then(() => {
                dispatch(this.fetchMuscles());
                NotificationManager.info("Успешно удалено");
            })
            .catch(error => {
                if (error.response && error.response.data) {
                    dispatch(fetchMuscleFailure(error.response.data));
                }
            })
        }
    };

    updateMuscle = (id, data) => {
        return dispatch => {
        axios.put(`/muscles/${id}`, data)
            .then(() => {
                dispatch(this.fetchMuscles());
                NotificationManager.success("Успешно обновлено");
            })
            .catch(error => {
                if (error.response && error.response.data) {
                    dispatch(fetchMuscleFailure(error.response.data));
                }
            })
        }
    };
};

export default new MusclesApiService();