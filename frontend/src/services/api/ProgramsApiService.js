import axios from "../../axios-api";
import {push} from "connected-react-router"
import {
    startProgramsRequest, fetchSingleProgramSuccess,
    fetchProgramsSuccess, fetchProgramsFailure,
    deleteProgramSuccess, deleteProgramFailure,
    createProgramSuccess, createProgramFailure,
    updateProgramSuccess
} from "../../redux/actions/programsActions";
import {NotificationManager} from "react-notifications";

class ProgramsApiService {
	columns = [
		{ id: 'title', numeric: false, disablePadding: true, label: 'Заголовок' },
		{ id: 'description', numeric: false, disablePadding: false, label: 'Описание' },
		{ id: 'actions', numeric: false, disablePadding: false, label: 'Действия' },
	];

    fetchPrograms = () => {
        let url = "/programs";
        return dispatch => {
            dispatch(startProgramsRequest());
            axios.get(url)
                .then(response => {
                    dispatch(fetchProgramsSuccess(response.data));
                }).catch(error => {
                dispatch(fetchProgramsFailure(error));
            });
        };
    };

    searchPrograms = (data) => {
        let url = `/programs?search=${data}`;
        return dispatch => {
          dispatch(startProgramsRequest());
          axios.get(url).then(response => {
            dispatch(fetchProgramsSuccess(response.data));
          }).catch(error => {
            dispatch(fetchProgramsFailure(error));
          });
        };
    };

    fetchSingleProgram = (id) => {
        return dispatch => {
            dispatch(startProgramsRequest());
            return axios.get(`/programs/${id}`)
                .then(response => {
                    console.log(response.data);
                    dispatch(fetchSingleProgramSuccess(response.data));
                    return response.data;
                })
        }
    }

    deleteProgram = (id) => {
        return dispatch => {
            dispatch(startProgramsRequest());
            axios.delete(`/programs/${id}`)
                .then(() => {
                    dispatch(deleteProgramSuccess());
                    dispatch(this.fetchPrograms());
                    NotificationManager.info("Успешно удалено");
                })
                .catch(error => {
                    dispatch(deleteProgramFailure(error));
                });
        }
    };

    createProgram = (program) => {
        return dispatch => {
            dispatch(startProgramsRequest());
            axios.post("/programs", program)
                .then(response => {
                    dispatch(createProgramSuccess(response.data));
                    dispatch(push(`/programs/${response.data._id}`));
                    NotificationManager.success("Успешно создано");
                }).catch(error => {
                    if (error.response && error.response.data) {
                        dispatch(createProgramFailure(error.response.data));
                    }
            });
        };
    };


    updateProgram = (id, data) => {
        return dispatch => {
            dispatch(startProgramsRequest());
            axios.put(`/programs/${id}`, data)
                .then((response) => {
                    dispatch(updateProgramSuccess(response.data));
                    dispatch(push(`/programs/${id}`));
                    NotificationManager.success("Успешно обновлено");
                })
                .catch(error => {
                    if (error.response && error.response.data) {
                        dispatch(createProgramFailure(error.response.data));
                    }
                });
        }
    };
};

export default new ProgramsApiService();