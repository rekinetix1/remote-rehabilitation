import axios from "../../axios-api";
import {
  startFavoritesRequest, deleteFavoritesSuccess, deleteFavoritesFailure,
  createFavoritesFailure, createFavoritesSuccess,
  fetchFavoritesSuccess, fetchFavoritesFailure
} from "../../redux/actions/favoritesAction";
import {NotificationManager} from "react-notifications";

class ExercisesApiService {

  fetchFavorites = () => {
    return (dispatch, getState) => {
      const user_id = getState().users.user._id;

      dispatch(startFavoritesRequest());
      axios.get(`/favorites?user=${user_id}`)
        .then(response => {
          dispatch(fetchFavoritesSuccess(response.data));
        })
        .catch(error => {
          if (error.response && error.response.data) {
            dispatch(fetchFavoritesFailure(error.response.data));
          }
        })
    }
  };

  createFavorite = (data) => {
    return (dispatch, getState) => {
      const user_id = getState().users.user._id;

      dispatch(startFavoritesRequest());
      axios.post("/favorites", data)
        .then(() => {
          dispatch(createFavoritesSuccess());
          dispatch(this.fetchFavorites(user_id));
          NotificationManager.success("Успешно добавлено в избранное");
        })
        .catch(error => {
          if (error.response && error.response.data) {
            dispatch(createFavoritesFailure(error.response.data));
          }
        })
    }
  };

  deleteFavorite = (id) => {
    return (dispatch, getState) => {
      const user_id = getState().users.user._id;

      dispatch(startFavoritesRequest());
      axios.delete(`/favorites/${id}`)
        .then(() => {
          dispatch(deleteFavoritesSuccess());
          dispatch(this.fetchFavorites(user_id));
          NotificationManager.info("Успешно удалено из избранного");
        })
        .catch(error => {
          dispatch(deleteFavoritesFailure(error));
        })
    }
  };
};

export default new ExercisesApiService();