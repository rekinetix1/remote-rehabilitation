import axios from "../../axios-api";
import { deletePainFailure, deletePainRequest,
         deletePainSuccess, fetchPainsFailure,
         fetchPainsRequest, fetchPainsSuccess,
         fetchSinglePainFailure, fetchSinglePainRequest,
         fetchSinglePainSuccess, postPainFailure, postPainRequest,
         postPainSuccess, updatePainFailure, updatePainRequest,
         updatePainSuccess } from "../../redux/actions/painsActions";
import {NotificationManager} from "react-notifications";

class PainsApiService {
    columns = [
		{ id: 'title', numeric: false, disablePadding: true, label: 'Заголовок' },
		{ id: 'actions', numeric: false, disablePadding: false, label: 'Действия' },
	];

    fetchPains = () => {
        return dispatch => {
            dispatch(fetchPainsRequest());
            axios.get("/pains")
                .then(response => {
                    dispatch(fetchPainsSuccess(response.data));
                })
                .catch(error => {
                    dispatch(fetchPainsFailure(error));
                })
        }
    };

    fetchSinglePain = (id) => {
        return dispatch => {
            dispatch(fetchSinglePainRequest());
            return axios.get(`/pains/${id}`)
                .then(response => {
                    dispatch(fetchSinglePainSuccess(response.data));
                    return response.data;
                })
                .catch(error => {
                    dispatch(fetchSinglePainFailure(error));
                });
        }
    };

    createPain = (data) => {
        return dispatch => {
            dispatch(postPainRequest());
            axios.post("/pains", data)
                .then(() => {
                    dispatch(postPainSuccess());
                    dispatch(this.fetchPains());
                    NotificationManager.success("Успешно создано");
                })
                .catch(error => {
                    dispatch(postPainFailure(error));
                });
        }
    };

    updatePain = (id, data) => {
        return dispatch => {
            dispatch(updatePainRequest());
            axios.put(`/pains/${id}`, data)
                .then(() => {
                    dispatch(updatePainSuccess());
                    dispatch(this.fetchPains());
                    NotificationManager.success("Успешно обновлено");
                })
                .catch(error => {
                    dispatch(updatePainFailure(error));
                });
        }
    };

    deletePain = (id) => {
        return dispatch => {
            dispatch(deletePainRequest());
            axios.delete(`/pains/${id}`)
                .then(() => {
                    dispatch(deletePainSuccess());
                    dispatch(this.fetchPains());
                    NotificationManager.info("Успешно удалено");
                })
                .catch(error => {
                    dispatch(deletePainFailure(error));
                });
        }
    };
};

export default new PainsApiService();