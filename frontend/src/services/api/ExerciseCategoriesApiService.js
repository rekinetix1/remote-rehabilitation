import axios from "../../axios-api";
import {
	fetchExerciseCategoriesFailure,
	fetchExerciseCategoriesRequest,
	fetchExerciseCategoriesSuccess, fetchExerciseCategorySuccess
} from "../../redux/actions/exerciseCategoriesActions";
import {NotificationManager} from "react-notifications";
import {push} from "connected-react-router";

class ExerciseCategoriesApiService {
	columns = [
		{ id: 'title', numeric: false, disablePadding: true, label: 'Заголовок' },
		{ id: 'actions', numeric: false, disablePadding: false, label: 'Действия' },
	];
	fetchExerciseCategories() {
		return dispatch => {
			dispatch(fetchExerciseCategoriesRequest());
			axios.get("/exercise_categories")
				.then(response => {
					dispatch(fetchExerciseCategoriesSuccess(response.data));
				})
				.catch(error => {
					dispatch(fetchExerciseCategoriesFailure(error));
				})
		}
	};

	fetchExerciseCategory = (id) => {
		return dispatch => {
			dispatch(fetchExerciseCategoriesRequest());
			return axios.get(`/exercise_categories/${id}`)
				.then(response => {
					dispatch(fetchExerciseCategorySuccess(response.data));
					return response.data;
				})
				.catch(error => {
					console.log(error);
				})
		}
	};


	createExerciseCategory = data => {
		return dispatch => {
			axios.post("/exercise_categories", data)
				.then(() => {
					dispatch(this.fetchExerciseCategories());
					dispatch(push("/exercise-categories"));
					NotificationManager.success("Успешно создано");
				})
				.catch(error => {
					if (error.response && error.response.data) {
						dispatch(fetchExerciseCategoriesFailure(error.response.data));
				}})
		}
	};

	deleteExerciseCategory = id => {
		return dispatch => {
			axios.delete(`/exercise_categories/${id}`)
				.then(() => {
					dispatch(this.fetchExerciseCategories());
					NotificationManager.info("Успешно удалено");
				})
				.catch(error => {
					console.log(error);
				})
		}
	};

	updateExerciseCategory = (id, data) => {
		return dispatch => {
			axios.put(`/exercise_categories/${id}`, data)
				.then(() => {
					dispatch(this.fetchExerciseCategories());
					NotificationManager.success("Успешно обновлено");
					dispatch(push("/exercise-categories"));
				})
				.catch(error => {
					if (error.response && error.response.data) {
						dispatch(fetchExerciseCategoriesFailure(error.response.data));
					}
				})
		}
	};

};

export default new ExerciseCategoriesApiService();