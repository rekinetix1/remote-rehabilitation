import axios from "../../axios-api";
import { 
    createIndividualProgramFailure, createIndividualProgramRequest, createIndividualProgramSuccess, 
    deleteIndividualProgramFailure, deleteIndividualProgramRequest, fetchIndividualProgramRequest, 
    fetchIndividualProgramsRequest, updateIndividualProgramFailure, updateIndividualProgramSuccess, 
    fetchIndividualProgramsSuccess, fetchIndividualProgramsFailure, fetchIndividualProgramSuccess,
    fetchIndividualProgramFailure, updateIndividualProgramRequest, deleteIndividualProgramSuccess
} from "../../redux/actions/individualProgramsActions";
import {NotificationManager} from "react-notifications";
import {push} from "connected-react-router";

class IndividualProgramsApiService {
    columns = [
        { id: 'title', numeric: false, disablePadding: true, label: 'Заголовок' },
        { id: 'name', numeric: false, disablePadding: false, label: 'ФИО' },
        { id: 'email', numeric: false, disablePadding: false, label: 'Email' },
        { id: 'status', numeric: false, disablePadding: false, label: 'Статус' },
        { id: 'paymentStatus', numeric: false, disablePadding: false, label: 'Статус оплаты' },
        { id: 'actions', numeric: false, disablePadding: false, label: 'Действия' },
        
    ];

    fetchIndividualPrograms = (user) => {
        return dispatch => {
            dispatch(fetchIndividualProgramsRequest());
            if (!user){
                axios.get("/individual_programs")
                    .then(response => {
                        dispatch(fetchIndividualProgramsSuccess(response.data));
                    })
                    .catch(error => {
                        dispatch(fetchIndividualProgramsFailure(error));
                    });
            } else {
                axios.get(`/individual_programs?user=${user}`)
                    .then(response => {
                        dispatch(fetchIndividualProgramsSuccess(response.data));
                    })
                    .catch(error => {
                        dispatch(fetchIndividualProgramsFailure(error));
                    });
            }

        }
    };

    searchIndividualPrograms = (data) => {
        let url = `/individual_programs?search=${data}`;
        return dispatch => {
          dispatch(fetchIndividualProgramsRequest());
          axios.get(url).then(response => {
              console.log(response.data)
            dispatch(fetchIndividualProgramsSuccess(response.data));
          }).catch(error => {
            dispatch(fetchIndividualProgramsFailure(error));
          });
        };
      };

    fetchCurrentIndividualProgram = (id) => {
        return dispatch => {
            dispatch(fetchIndividualProgramRequest());
            return axios.get("/individual_programs/" + id)
                .then(response => {
                    dispatch(fetchIndividualProgramSuccess(response.data));
                    return response.data;
                })
                .catch(error => {
                    dispatch(fetchIndividualProgramFailure(error));
                });
        }
    };

    createIndividualProgram = (program) => {
        return dispatch => {
            dispatch(createIndividualProgramRequest());
            axios.post("/individual_programs", program)
                .then(() => {
                    dispatch(createIndividualProgramSuccess());
                    dispatch(this.fetchIndividualPrograms());
                    dispatch(push("/individual_programs"));
                    NotificationManager.success("Успешно создано");
                })
                .catch(error => {
                    dispatch(createIndividualProgramFailure(error));
                });
        }
    };

    updateIndividualProgram = (id, program, link) => {
        return dispatch => {
            dispatch(updateIndividualProgramRequest());
            axios.put(`/individual_programs/${id}`, program)
                .then(() => {
                    dispatch(updateIndividualProgramSuccess());
                    dispatch(this.fetchIndividualPrograms());
                    NotificationManager.success("Успешно обновлено");
                    if (link) dispatch(push(`/${link}`));
                })
                .catch(error => {
                    dispatch(updateIndividualProgramFailure(error));
                })
        }
    };

    deleteIndividualProgram = (id) => {
        return dispatch => {
            dispatch(deleteIndividualProgramRequest());
            axios.delete(`/individual_programs/${id}`)
                .then(() => {
                    dispatch(deleteIndividualProgramSuccess());
                    dispatch(this.fetchIndividualPrograms());
                    NotificationManager.info("Успешно удалено");
                })
                .catch(error => {
                    dispatch(deleteIndividualProgramFailure(error));
                })
        }
    };
};

export default new IndividualProgramsApiService();
