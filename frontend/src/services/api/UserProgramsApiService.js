import axios from "../../axios-api";
import { 
    deleteUserProgramSuccess, fetchUserProgramsRequest, fetchUserProgramsSuccess, 
    postUserProgramFailure, postUserProgramSuccess, updateUserProgramFailure, 
    updateUserProgramRequest, updateUserProgramSuccess, fetchUserProgramsFailure,
    postUserProgramRequest, deleteUserProgramRequest, deleteUserProgramFailure, fetchCurrentUserProgramRequest, fetchCurrentUserProgramSuccess, fetchCurrentUserProgramFailure 
} from "../../redux/actions/userProgramsActions";
import {push} from "connected-react-router";
import {NotificationManager} from "react-notifications";

class UserProgramsApiService {
	columns = [
        { id: 'name', numeric: false, disablePadding: false, label: 'ФИО' },
        { id: 'email', numeric: false, disablePadding: false, label: 'Email' },
        { id: 'title', numeric: false, disablePadding: true, label: 'Программа' },
		{ id: 'program_rate', numeric: false, disablePadding: true, label: 'Цена' },
        { id: 'actions', numeric: false, disablePadding: false, label: 'Действия' },
    ];

    fetchUserPrograms = (id) => {
        let url = "/user_programs";
        if (id) url = `/user_programs?user=${id}`;
        return dispatch => {
            dispatch(fetchUserProgramsRequest());
            axios.get(url)
                .then(response => {
                    dispatch(fetchUserProgramsSuccess(response.data));
                })
                .catch(error => {
                    dispatch(fetchUserProgramsFailure(error));
                });
        }
    };

    fetchCurrentUserProgram = (id) => {
        return dispatch => {
            dispatch(fetchCurrentUserProgramRequest());
            return axios.get(`/user_programs/${id}`)
                .then(response => {
                    dispatch(fetchCurrentUserProgramSuccess(response.data));
                    return response.data;
                })
                .catch(error => {
                    dispatch(fetchCurrentUserProgramFailure(error));
                });
        }
    };

    fetchCurrentProgram = (programId, userId) => {
        return dispatch => {
            dispatch(fetchCurrentUserProgramRequest());
            return axios.get(`/user_programs?program=${programId}&user=${userId}`)
                .then(response => {
                    dispatch(fetchCurrentUserProgramSuccess(response.data));
                    return response.data;
                })
                .catch(error => {
                    dispatch(fetchCurrentUserProgramFailure(error));
                });
        }
    };

    postUserProgram = (program, link) => {
        return dispatch => {
            dispatch(postUserProgramRequest());
            axios.post("/user_programs", program)
                .then(() => {
                    dispatch(postUserProgramSuccess());
                    if (link){
                        dispatch(push(`/${link}`));
                        NotificationManager.info("Успешно назначено");
                    }
                })
                .catch(error => {
                    dispatch(postUserProgramFailure(error));
                });
        }
    };

    updateUserProgram = (id, program) => {
        return dispatch => {
            dispatch(updateUserProgramRequest());
            axios.put(`/user_programs/${id}`, program)
                .then(() => {
                    dispatch(updateUserProgramSuccess());
                    dispatch(push(`/user_programs`));
                    NotificationManager.success("Успешно обновлено");
                })
                .catch(error => {
                    dispatch(updateUserProgramFailure(error));
                });
        }
    };

    deleteUserProgram = (id) => {
        return dispatch => {
            dispatch(deleteUserProgramRequest());
            axios.delete(`/user_programs/${id}`)
                .then(() => {
                    dispatch(deleteUserProgramSuccess());
                    dispatch(this.fetchUserPrograms());
                })
                .catch(error => {
                    dispatch(deleteUserProgramFailure(error))
                });
        }
    };
};

export default new UserProgramsApiService();
