import axios from "../../axios-api";
import {fetchCommentsFailure, fetchCommentsRequest, fetchCommentsSuccess} from "../../redux/actions/commentsActions";
import {NotificationManager} from "react-notifications";

class CommentsApiService {
    columns = [
        {id: 'name', numeric: false, disablePadding: false, label: 'Имя пользователя'},
        {id: 'datetime', numeric: false, disablePadding: true, label: 'Дата'},
        {id: 'program', numeric: false, disablePadding: false, label: 'Программа'},
        {id: 'comment', numeric: false, disablePadding: false, label: 'Комментарий'},
    ];

    fetchComments() {
        return dispatch => {
            dispatch(fetchCommentsRequest());
            axios.get(`/comments`)
                .then(response => {
                    dispatch(fetchCommentsSuccess(response.data))
                    return response
                })
                .catch(error => {
                    dispatch(fetchCommentsFailure(error))
                })
        }
    };

    fetchUserAnswers() {
        return dispatch => {
            dispatch(fetchCommentsRequest());
            axios.get(`/comments/user_answers`)
                .then(response => {
                    console.log(response.data)
                    dispatch(fetchCommentsSuccess(response.data))
                    return response
                })
                .catch(error => {
                    dispatch(fetchCommentsFailure(error))
                })
        }
    };

    addReplyComment(comment) {
        return dispatch => {
            return axios.post(`/comments`, {...comment})
                .then(res => {
                    NotificationManager.success("Вы успешно ответили на комментарий");
                })
                .catch(err => {
                    dispatch(fetchCommentsFailure(err))
                    NotificationManager.failure('Что-то пошло не так, комментарий не отправлен :(')
                })
        }
    }


    fetchProgramComments(programId, type) {
        return dispatch => {
            dispatch(fetchCommentsRequest());
            axios.get(type === 'individual' ? `/comments?individualProgram=${programId}` : `/comments?program=${programId}`)
                .then(response => {
                    dispatch(fetchCommentsSuccess(response.data))
                    return response
                })
                .catch(error => {
                    dispatch(fetchCommentsFailure(error))
                })
        }
    }

    deleteEComment = id => {
        return dispatch => {
           return axios.delete(`/comments/${id}`)
                .then(() => {
                    NotificationManager.info("Комментарий успешно удален");
                })
                .catch(error => {
                    console.log(error);
                })
        }
    };

    updateComment = (id, data) => {
        return dispatch => {
            return axios.put(`/comments/${id}`, data)
                .then(() => {
                    dispatch(this.fetchComments());
                    NotificationManager.success("Вы успешно обновили комментарий");
                })
                .catch(error => {
                    console.log(error);
                })
        }
    };
}

export default new CommentsApiService();