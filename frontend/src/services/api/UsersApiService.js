import axios from "../../axios-api";
import {push} from "connected-react-router";
import {NotificationManager} from "react-notifications";
import {
    deleteUserFailure,
    deleteUserRequest,
    fetchUserFailure,
    fetchUserRequest,
    fetchUsersRequest,
    fetchUsersSuccess,
    fetchVerifySuccess,
    registerUserSuccess,
    loginUserFailure,
    loginUserRequest,
    loginUserSuccess,
    logoutUserError,
    logoutUserSuccess,
    registerUserFailure,
    registerUserRequest,
    updateUserFailure,
    updateUserProfileSuccess,
    updateUserRequest,
    updateUserSuccess,
    fetchUsersFailure,
    fetchUserSuccess,
    deleteUserSuccess,
    fetchVerifyRequest,
    fetchVerifyFailure,
    fetchCheckUserRequest,
    fetchCheckUserSuccess,
    fetchCheckUserFailure,
    fetchResetPasswordRequest,
    fetchResetPasswordSuccess,
    fetchResetPasswordFailure
} from "../../redux/actions/usersActions";

class UsersApiService {
    columns = [
      { id: 'email', numeric: false, disablePadding: true, label: 'Логин' },
      { id: 'fullname', numeric: false, disablePadding: false, label: 'Имя Фамилия' },
      { id: 'role', numeric: false, disablePadding: false, label: 'Роль' },
      { id: 'actions', numeric: false, disablePadding: false, label: 'Действия' },
    ];

    loginUser = (userData) => {
        return dispatch => {
          dispatch(loginUserRequest());
          return axios.post("/users/sessions", userData).then(response => {
            dispatch(loginUserSuccess(response.data));
            return response.data;
          }, error => {
              if (error.response && error.response.data) {
                  dispatch(loginUserFailure(error.response.data))
              }
          });
        };
    };

    logout = () => {
        return (dispatch, getState) => {
          const token = getState().users.user && getState().users.user.token;
          const headers = {
            Token: token
          };
          dispatch(logoutUserSuccess());
          axios.delete("/users/sessions", {headers}).then(() => {
            dispatch(logoutUserSuccess());
            dispatch(push("/"));
          }, error => dispatch(logoutUserError(error.response.data)));
        };
    };

    registerUser = (userData, page) => {
        return dispatch => {
          dispatch(registerUserRequest());
          return axios.post("/users", userData).then(response => {
            dispatch(registerUserSuccess());
            dispatch(push(!page ? "/" : page));
            dispatch(this.fetchUsers());
            if (!page) dispatch(this.loginUser(userData))
          }, error => {
            if (error.response && error.response.data) {
              dispatch(registerUserFailure(error.response.data));
            } else {
              dispatch(registerUserFailure({global: "No internet"}));
            }
          });
        };
    };

    fetchUsers = (role) => {
        let url = "/users";
        if (role) {
          url = `${url}?role=${role}`
        }
        console.log(url);
        return dispatch => {
          dispatch(fetchUsersRequest());
          axios.get(url).then(response => {
            console.log(response.data);
            dispatch(fetchUsersSuccess(response.data));
          }).catch(error => {
            dispatch(fetchUsersFailure(error));
          });
        };
    };

    searchUsers = (data) => {
      let url = `/users?search=${data}`;
      return dispatch => {
        dispatch(fetchUsersRequest());
        axios.get(url).then(response => {
          dispatch(fetchUsersSuccess(response.data));
        }).catch(error => {
          dispatch(fetchUsersFailure(error));
        });
      };
  };
    
    fetchUser = (id) => {
        return dispatch => {
          dispatch(fetchUserRequest());
          return axios.get(`/users/${id}`).then(response => {
            dispatch(fetchUserSuccess(response.data));
            return response.data;
          }).catch(error => {
            dispatch(fetchUserFailure(error));
          });
        };
    };

    updateUser = (id, userData) => {
        return dispatch => {
          dispatch(updateUserRequest());
          axios.put(`/users/${id}`, userData).then(() => {
            dispatch(updateUserSuccess());
            dispatch(this.fetchUser(id));
            dispatch(this.fetchUsers());
          }).catch(error => {
            dispatch(updateUserFailure(error));
          })
        }
    };

    deleteUser = (id) => {
        return dispatch => {
          dispatch(deleteUserRequest());
          axios.delete(`/users/${id}`).then(() => {
            dispatch(deleteUserSuccess());
            dispatch(this.fetchUsers());
            NotificationManager.info("Успешно удалено");
          }).catch(error => {
            dispatch(deleteUserFailure(error));
          });
        }
    };

    updateUserProfile = (id, userData) => {
        return dispatch => {
          dispatch(updateUserRequest());
          axios.put(`/users/${id}`, userData).then(() => {
            dispatch(updateUserProfileSuccess(userData));
            NotificationManager.success("Успешно обновлено");
          }).catch(error => {
            dispatch(updateUserFailure(error));
          })
        }
    };

    verifyUser = (id) => {
      return dispatch => {
        dispatch(fetchVerifyRequest());
        axios.get(`/users/verify/${id}`).then(() => {
          dispatch(fetchVerifySuccess());
        }).catch(error => {
          dispatch(fetchVerifyFailure(error));
        })
      }
    };

    sendVerifyUser = (id) => {
      return dispatch => {
        dispatch(fetchVerifyRequest());
        axios.get(`/users/send_verify/${id}`).then((response) => {
          dispatch(fetchUserSuccess(response.data));
        }).catch(error => {
          dispatch(fetchVerifyFailure(error));
        })
      }
    };

    checkUserInBase = (email) => {
        return dispatch => {
            dispatch(fetchCheckUserRequest());
            axios.get(`/users/check_user/${email}`).then((res) => {
                dispatch(fetchCheckUserSuccess(res.data));
            }).catch(error => {
                NotificationManager.error(error.response.data.message)
                dispatch(fetchCheckUserFailure(error));
            })
        }
    };


    resetPasswordInBase = (passwords, idUser) => {
        if(passwords.password === passwords.confirmPassword){
            return dispatch => {
                dispatch(fetchResetPasswordRequest());
                axios.put(`/users/reset_password/${idUser}`,passwords).then((res) => {
                    let loginUser= {
                        password: passwords.password,
                        email: res.data.email
                    }
                    NotificationManager.success("Пароль успешно обновлен");
                    dispatch(fetchResetPasswordSuccess(res.data));
                    dispatch (this.loginUser(loginUser))
                }).catch(error => {
                    NotificationManager.error(error.response.data.message)
                    dispatch(fetchResetPasswordFailure(error));
                })
            }
        }else{
            NotificationManager.error("Пароли не совпадают")
        }
    };
}

export default new UsersApiService();
