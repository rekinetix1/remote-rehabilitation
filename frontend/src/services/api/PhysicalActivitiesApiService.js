import axios from "../../axios-api";
import { deleteActivityFailure, deleteActivityRequest,
         deleteActivitySuccess, fetchActivitiesFailure,
         fetchActivitiesRequest, fetchActivitiesSuccess,
         fetchActivityFailure, fetchActivityRequest, fetchActivitySuccess,
         postActivityFailure, postActivityRequest, postActivitySuccess,
         updateActivityFailure, updateActivityRequest, updateActivitySuccess } from "../../redux/actions/physicalActivitiesActions";
import {NotificationManager} from "react-notifications";
import {push} from "connected-react-router";

class PhysicalActivitiesApiService {
	columns = [
		{ id: 'title', numeric: false, disablePadding: true, label: 'Заголовок' },
		{ id: 'actions', numeric: false, disablePadding: false, label: 'Действия' },
	];

    fetchPhysicalActivities = () => {
        return dispatch => {
            dispatch(fetchActivitiesRequest());
            axios.get("/physical_activities")
                .then(response => {
                    dispatch(fetchActivitiesSuccess(response.data));
                })
                .catch(error => {
                    dispatch(fetchActivitiesFailure(error));
                });
        }
    };

    fetchSingleActivity = (id) => {
        return dispatch => {
            dispatch(fetchActivityRequest());
            return axios.get(`/physical_activities/${id}`)
                .then(response => {
                    dispatch(fetchActivitySuccess(response.data));
                    return response.data;
                })
                .catch(error => {
                    dispatch(fetchActivityFailure(error));
                });
        }
    };

    createActivity = (data) => {
        return dispatch => {
            dispatch(postActivityRequest());
            axios.post("/physical_activities", data)
                .then(() => {
                    dispatch(postActivitySuccess());
                    dispatch(this.fetchPhysicalActivities());
                    dispatch(push("/activities"));
                    NotificationManager.success("Успешно создано");
                })
                .catch(error => {
                    if (error.response && error.response.data) {
                        dispatch(postActivityFailure(error.response.data));
                    }
                });
        }
    };

    updateActivity = (id, data) => {
        return dispatch => {
            dispatch(updateActivityRequest());
            axios.put(`/physical_activities/${id}`, data)
                .then(() => {
                    dispatch(updateActivitySuccess());
                    dispatch(this.fetchPhysicalActivities());
                    NotificationManager.success("Успешно обновлено");
                })
                .catch(error => {
                    dispatch(updateActivityFailure(error));
                });
        }
    };

    deleteActivity = (id) => {
        return dispatch => {
            dispatch(deleteActivityRequest());
            axios.delete(`/physical_activities/${id}`)
                .then(() => {
                    dispatch(deleteActivitySuccess());
                    dispatch(this.fetchPhysicalActivities());
                    NotificationManager.info("Успешно удалено");
                })
                .catch(error => {
                    dispatch(deleteActivityFailure(error));
                });
        }
    };
};

export default new PhysicalActivitiesApiService();