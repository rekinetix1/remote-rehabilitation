import axios from "../../axios-api";
import {NotificationManager} from "react-notifications";
import {push} from "connected-react-router";
import {
    fetchProgramCategoriesFailure, fetchProgramCategoriesRequest, 
    fetchProgramCategoriesSuccess, fetchProgramCategorySuccess 
} from "../../redux/actions/programCategoriesActions";

class ProgramCategoriesApiService {
	columns = [
		{ id: 'title', numeric: false, disablePadding: true, label: 'Заголовок' },
		{ id: 'actions', numeric: false, disablePadding: false, label: 'Действия' },
	];

  fetchProgramCategories = () => {
    return dispatch => {
      dispatch(fetchProgramCategoriesRequest());
      axios.get("/program_categories")
        .then(response => {
          dispatch(fetchProgramCategoriesSuccess(response.data));
        })
        .catch(error => {
          dispatch(fetchProgramCategoriesFailure(error));
        })
    }
  };

  searchProgramCategories = (data) => {
    let url = `/program_categories?search=${data}`;
    return dispatch => {
      dispatch(fetchProgramCategoriesRequest());
      axios.get(url).then(response => {
        dispatch(fetchProgramCategoriesSuccess(response.data));
      }).catch(error => {
        dispatch(fetchProgramCategoriesFailure(error));
      });
    };
  };

  fetchProgramCategory = (id) => {
    return dispatch => {
      return axios.get(`/program_categories/${id}`)
        .then(response => {
          dispatch(fetchProgramCategorySuccess(response.data));
          return response.data;
        })
        .catch(error => {
          console.log(error);
        })
    }
  };

  createProgramCategory = data => {
    return dispatch => {
      axios.post("/program_categories", data)
        .then(() => {
          dispatch(this.fetchProgramCategories());
          dispatch(push("/program-categories"));
          NotificationManager.success("Успешно создано");
        })
        .catch(error => {
          console.log(error);
        })
    }
  };

  deleteProgramCategory = id => {
    return dispatch => {
      axios.delete(`/program_categories/${id}`)
        .then(() => {
          dispatch(this.fetchProgramCategories());
          NotificationManager.info("Успешно удалено");
        })
        .catch(error => {
          console.log(error);
        })
    }
  };

  updateProgramCategory = (id, data) => {
    return dispatch => {
      axios.put(`/program_categories/${id}`, data)
        .then(() => {
          dispatch(this.fetchProgramCategories());
          NotificationManager.success("Успешно обновлено");
        })
        .catch(error => {
          console.log(error);
        })
    }
  };
};

export default new ProgramCategoriesApiService();