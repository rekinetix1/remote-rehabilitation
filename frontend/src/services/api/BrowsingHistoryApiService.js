import axios from "../../axios-api";
import {
    fetchHistoryFailure,
    fetchHistoryRequest,
    fetchHistorySuccess
} from "../../redux/actions/browsingHistoryActions";
import {postPainFailure} from "../../redux/actions/painsActions";

class BrowsingHistoryApiService {
    columns = [
		{ id: 'datetime', numeric: false, disablePadding: true, label: 'Дата' },
		{ id: 'email', numeric: false, disablePadding: false, label: 'Почта пользователя' },
        { id: 'name', numeric: false, disablePadding: false, label: 'Имя пользователя' },
        { id: 'program', numeric: false, disablePadding: false, label: 'Программа' },
		{ id: 'day', numeric: true, disablePadding: false, label: 'День' },
		{ id: 'exercise', numeric: false, disablePadding: false, label: 'Упражнение' },
	];

    fetchBrowsingHistory(userID) {
        return dispatch => {
            dispatch(fetchHistoryRequest());
            axios.get(userID ? `/userBrowsingHistories?user=${userID}` : "/userBrowsingHistories")
                .then(response => {
                    dispatch(fetchHistorySuccess(response.data))
                })
                .catch(error => {
                    dispatch(fetchHistoryFailure(error))
                })
        }
    };

    searchBrowsingHistory = (data) => {
        let url = `/userBrowsingHistories?search=${data}`;
        return dispatch => {
          dispatch(fetchHistoryRequest());
          axios.get(url).then(response => {
            dispatch(fetchHistorySuccess(response.data));
          }).catch(error => {
            dispatch(fetchHistoryFailure(error));
          });
        };
      };

    printInBrowsingHistory(data, user){
        return dispatch => {
            dispatch(fetchHistoryRequest());
            axios.post('/userBrowsingHistories', data)
                .then(() => {
                    dispatch(this.fetchBrowsingHistory(user));
                })
                .catch(error => {
                    dispatch(postPainFailure(error));
                });
        }

    }
};

export default new BrowsingHistoryApiService();