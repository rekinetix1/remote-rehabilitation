import axios from "../../axios-api";
import { 
    deleteOptionFailure, deleteOptionRequest, deleteOptionSuccess, 
    fetchOptionsFailure, fetchOptionsRequest, fetchOptionsSuccess, 
    fetchSingleOptionFailure, fetchSingleOptionRequest, fetchSingleOptionSuccess, 
    postOptionRequest, postOptionSuccess, updateOptionFailure, 
    updateOptionRequest, updateOptionSuccess 
} from "../../redux/actions/optionsActions";
import {NotificationManager} from "react-notifications";
import {postOccupationFailure} from "../../redux/actions/occupationActions";
import {push} from "connected-react-router";

class OptionsApiService {
    columns = [
		{ id: 'title', numeric: false, disablePadding: true, label: 'Заголовок' },
		{ id: 'cost', numeric: true, disablePadding: false, label: 'Цена' },
		{ id: 'actions', numeric: false, disablePadding: false, label: 'Действия' },
	];

    fetchOptions = () => {
        return dispatch => {
            dispatch(fetchOptionsRequest());
            axios.get("/options_menus")
            .then(response => {
                dispatch(fetchOptionsSuccess(response.data));
            })
            .catch(error => {
                dispatch(fetchOptionsFailure(error));
            });
        }
    };

    fetchSingleOption = (id) => {
        return dispatch => {
            dispatch(fetchSingleOptionRequest());
            return axios.get(`/options_menus/${id}`)
                .then(response => {
                    dispatch(fetchSingleOptionSuccess(response.data));
                    return response.data;
                })
                .catch(error => {
                    dispatch(fetchSingleOptionFailure(error));
                })
        }
    };

    postOption = (data) => {
        return dispatch => {
            dispatch(postOptionRequest());
            axios.post("/options_menus", data)
                .then(() => {
                    dispatch(postOptionSuccess());
                    dispatch(this.fetchOptions());
                    dispatch(push("/options"));
                    NotificationManager.success("Успешно создано");
                })
                .catch(error => {
                    if (error.response && error.response.data) {
                        dispatch(postOccupationFailure(error.response.data));
                    }
                })
        }
    };

    updateOption = (id, data) => {
        return dispatch => {
            dispatch(updateOptionRequest());
            axios.put(`/options_menus/${id}`, data)
                .then(() => {
                    dispatch(updateOptionSuccess());
                    dispatch(this.fetchOptions());
                    NotificationManager.success("Успешно обновлено");
                })
                .catch(error => {
                    dispatch(updateOptionFailure(error));
                })
        }
    };

    deleteOption = (id) => {
        return dispatch => {
            dispatch(deleteOptionRequest());
            axios.delete(`/options_menus/${id}`)
                .then(() => {
                    dispatch(deleteOptionSuccess());
                    dispatch(this.fetchOptions());
                    NotificationManager.info("Успешно удалено");
                })
                .catch(error => {
                    dispatch(deleteOptionFailure());
                })
        }
    };
};

export default new OptionsApiService();