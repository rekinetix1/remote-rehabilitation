import axios from "../../axios-api";
import { 
    fetchSingleTransactionRequest, fetchSingleTransactionSuccess, fetchTransactionsFailure, 
    fetchTransactionsRequest, fetchTransactionsSuccess, updateSingleTransactionFailure, 
    updateSingleTransactionRequest, updateSingleTransactionSuccess, fetchSingleTransactionFailure, 
    postSingleTransactionRequest, postSingleTransactionSuccess, postSingleTransactionFailure 
} from "../../redux/actions/transactionsActions";

class TransactionsApiService {
    columns = [
        { id: 'invoiceId', numeric: false, disablePadding: true, label: 'Номер заказа'},
        { id: 'email', numeric: false, disablePadding: true, label: 'Пользователь' },
		{ id: 'type', numeric: false, disablePadding: true, label: 'Тип' },
        { id: 'program', numeric: false, disablePadding: false, label: 'Программа' },
        { id: 'cost', numeric: true, disablePadding: false, label: 'Общая цена' },
        { id: 'status', numeric: false, disablePadding: false, label: 'Статус' },
        { id: 'reason', numeric: false, disablePadding: false, label: 'Причина' },
        { id: 'datetime', numeric: false, disablePadding: false, label: 'Дата' },
		{ id: 'actions', numeric: false, disablePadding: false, label: 'Действия' },
	];

    fetchTransactions = () => {
        return dispatch => {
            dispatch(fetchTransactionsRequest());
            axios.get("/transactions")
                .then(response => {
                    dispatch(fetchTransactionsSuccess(response.data));
                })
                .catch(error => {
                    dispatch(fetchTransactionsFailure(error));
                });
        }
    };

    searchTransactions = (data) => {
        let url = `/transactions?search=${data}`;
        return dispatch => {
          dispatch(fetchTransactionsRequest());
          axios.get(url).then(response => {
            dispatch(fetchTransactionsSuccess(response.data));
          }).catch(error => {
            dispatch(fetchTransactionsFailure(error));
          });
        };
    };

    fetchSingleTransaction = (id) => {
        return dispatch => {
            dispatch(fetchSingleTransactionRequest());
            return axios.get(`/transactions/${id}`)
                .then(response => {
                    dispatch(fetchSingleTransactionSuccess(response.data));
                    return response.data;
                })
                .catch(error => {
                    dispatch(fetchSingleTransactionFailure(error));
                });
        }
    };

    postTransaction = (data) => {
        return dispatch=> {
            dispatch(postSingleTransactionRequest());
            return axios.post("/transactions", data)
                .then(response => {
                    dispatch(postSingleTransactionSuccess());
                    return response.data;
                })
                .catch(error => {
                    dispatch(postSingleTransactionFailure(error))
                })
        };
    };

    updateTransaction = (id, data) => {
        return dispatch => {
            dispatch(updateSingleTransactionRequest());
            axios.put(`/transactions/${id}`, data)
                .then(() => {
                    dispatch(updateSingleTransactionSuccess());
                    dispatch(this.fetchTransactions());
                })
                .catch(error => {
                    dispatch(updateSingleTransactionFailure(error));
                })
        }
    };
};

export default new TransactionsApiService();