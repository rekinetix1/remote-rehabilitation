import axios from "../../axios-api";
import {
    deleteOccupationFailure,
    deleteOccupationRequest,
    deleteOccupationSuccess,
    fetchOccupationFailure,
    fetchOccupationRequest,
    fetchOccupationSuccess,
    fetchSingleOccupationFailure,
    fetchSingleOccupationRequest,
    fetchSingleOccupationSuccess,
    postOccupationFailure,
    postOccupationRequest,
    postOccupationSuccess,
    updateOccupationFailure,
    updateOccupationRequest,
    updateOccupationSuccess
} from "../../redux/actions/occupationActions";
import {NotificationManager} from "react-notifications";
import {push} from "connected-react-router";

class OccupationApiService {
	columns = [
		{ id: 'title', numeric: false, disablePadding: true, label: 'Заголовок' },
		{ id: 'actions', numeric: false, disablePadding: false, label: 'Действия' },
	];

    fetchOccupation = () => {
        return dispatch => {
            dispatch(fetchOccupationRequest());
            axios.get("/occupation")
                .then(response => {
                    dispatch(fetchOccupationSuccess(response.data));
                })
                .catch(error => {
                    dispatch(fetchOccupationFailure(error));
                });
        }
    };

    fetchSingleOccupation = (id) => {
        return dispatch => {
            dispatch(fetchSingleOccupationRequest());
            return axios.get(`/occupation/${id}`)
                .then(response => {
                    dispatch(fetchSingleOccupationSuccess(response.data));
                    return response.data;
                })
                .catch(error => {
                    dispatch(fetchSingleOccupationFailure(error));
                });
        }
    };

    createOccupation = (data) => {
        return dispatch => {
            dispatch(postOccupationRequest());
            axios.post("/occupation", data)
                .then(() => {
                    dispatch(postOccupationSuccess());
                    dispatch(this.fetchOccupation());
                    dispatch(push("/occupation"));
                    NotificationManager.success("Успешно создано");
                })
                .catch(error => {
                    if (error.response && error.response.data) {
                        dispatch(postOccupationFailure(error.response.data));
                    }
                });
        }
    };

    updateOccupation = (id, data) => {
        return dispatch => {
            dispatch(updateOccupationRequest());
            axios.put(`/occupation/${id}`, data)
                .then(() => {
                    dispatch(updateOccupationSuccess());
                    dispatch(this.fetchOccupation());
                    NotificationManager.success("Успешно обновлено");
                })
                .catch(error => {
                    dispatch(updateOccupationFailure(error));
                });
        }
    };

    deleteOccupation = (id) => {
        return dispatch => {
            dispatch(deleteOccupationRequest());
            axios.delete(`/occupation/${id}`)
                .then(() => {
                    dispatch(deleteOccupationSuccess());
                    dispatch(this.fetchOccupation());
                    NotificationManager.info("Успешно удалено");
                })
                .catch(error => {
                    dispatch(deleteOccupationFailure(error));
                });
        }
    };
}

export default new OccupationApiService();