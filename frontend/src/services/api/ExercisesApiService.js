import axios from "../../axios-api";
import { 
    deleteExerciseFailure, deleteExerciseRequest, deleteExerciseSuccess, 
    fetchExerciseFailure, fetchExerciseRequest, fetchExercisesFailure, 
    fetchExercisesRequest, fetchExercisesSuccess, fetchExerciseSuccess, 
    postExerciseFailure, postExerciseRequest, postExerciseSuccess, 
    updateExerciseFailure, updateExerciseRequest, updateExerciseSuccess 
} from "../../redux/actions/exersicesActions";
import {NotificationManager} from "react-notifications";
import {push} from "connected-react-router";

class ExercisesApiService {
	columns = [
		{ id: 'title', numeric: false, disablePadding: true, label: 'Заголовок' },
        { id: 'description', numeric: false, disablePadding: false, label: 'Описание' },
        { id: 'complexity', numeric: false, disablePadding: false, label: 'Сложность' },
		{ id: 'actions', numeric: false, disablePadding: false, label: 'Действия' },
    ];

    fetchExercises = () => {
        return dispatch => {
            dispatch(fetchExercisesRequest());
            axios.get("/exercises")
            .then(response => {
                dispatch(fetchExercisesSuccess(response.data));
            })
            .catch(error => {
                dispatch(fetchExercisesFailure(error));
            })
        }
    };

    searchExercises = (data) => {
        let url = `/exercises?search=${data}`;
        return dispatch => {
          dispatch(fetchExercisesRequest());
          axios.get(url).then(response => {
            dispatch(fetchExercisesSuccess(response.data));
          }).catch(error => {
            dispatch(fetchExercisesFailure(error));
          });
        };
      };

    fetchSingleExercise = (id) => {
        return dispatch => {
            dispatch(fetchExerciseRequest());
            return axios.get(`/exercises/${id}`)
                .then(response => {
                    dispatch(fetchExerciseSuccess(response.data));
                    return response.data;
                })
                .catch(error => {
                    dispatch(fetchExerciseFailure(error));
                })
        }
    };

    postExercise = (data) => {
        return dispatch => {
            dispatch(postExerciseRequest());
            axios.post("/exercises", data)
                .then(() => {
                    dispatch(postExerciseSuccess());
                    dispatch(this.fetchExercises());
                    dispatch(push("/exercises"));
                    NotificationManager.success("Успешно создано");
                })
                .catch(error => {
                    if (error.response && error.response.data) {
                        dispatch(postExerciseFailure(error.response.data));
                    }
                })
        }
    };

    updateExercise = (id, data) => {
        return dispatch => {
            dispatch(updateExerciseRequest());
            axios.put(`/exercises/${id}`, data)
                .then(() => {
                    dispatch(updateExerciseSuccess());
                    dispatch(this.fetchExercises());
                    dispatch(push("/exercises"));
                    NotificationManager.success("Успешно обновлено");
                })
                .catch(error => {
                    dispatch(updateExerciseFailure(error));
                })
        }
    };

    deleteExercise = (id) => {
        return dispatch => {
            dispatch(deleteExerciseRequest());
            axios.delete(`/exercises/${id}`)
                .then(() => {
                    dispatch(deleteExerciseSuccess());
                    dispatch(this.fetchExercises());
                    NotificationManager.info("Успешно удалено");
                })
                .catch(error => {
                    dispatch(deleteExerciseFailure(error));
                })
        }
    };
};

export default new ExercisesApiService();