import axios from "../../axios-api";
import {NotificationManager} from "react-notifications";
import {
  fetchEquipmentFailure,
  fetchEquipmentRequest,
  fetchEquipmentSuccess,
  fetchSingleEquipmentSuccess
} from "../../redux/actions/equipmentActions";
import {push} from "connected-react-router";

class EquipmentApiService {
	columns = [
		{ id: 'title', numeric: false, disablePadding: true, label: 'Заголовок' },
		{ id: 'actions', numeric: false, disablePadding: false, label: 'Действия' },
	];

  fetchEquipment() {
    return dispatch => {
      dispatch(fetchEquipmentRequest());
      axios.get("/equipment")
        .then(response => {
          dispatch(fetchEquipmentSuccess(response.data))
        })
        .catch(error => {
          dispatch(fetchEquipmentFailure(error))
        })
    }
  };

  searchEquipments = (data) => {
    let url = `/equipment?search=${data}`;
    return dispatch => {
      dispatch(fetchEquipmentRequest());
      axios.get(url).then(response => {
        dispatch(fetchEquipmentSuccess(response.data));
      }).catch(error => {
        dispatch(fetchEquipmentFailure(error));
      });
    };
  };

  fetchSingleEquipment(id) {
    return dispatch => {
    return axios.get(`/equipment/${id}`)
      .then(response => {
        dispatch(fetchSingleEquipmentSuccess(response.data));
        return response.data;
      })
      .catch(error => {
        console.log(error);
      })
    }
  };

  createEquipment = data => {
    return dispatch => {
      axios.post("/equipment", data)
        .then(() => {
          dispatch(this.fetchEquipment());
          NotificationManager.success("Успешно создано");
          dispatch(push("/equipment"));
        })
        .catch(error => {
          if (error.response && error.response.data) {
            dispatch(fetchEquipmentFailure(error.response.data));
          }
        })
    }
  };

  deleteEquipment = id => {
    return dispatch => {
      axios.delete(`/equipment/${id}`)
        .then(() => {
          dispatch(this.fetchEquipment());
          NotificationManager.info("Успешно удалено");
        })
        .catch(error => {
          console.log(error);
        })
    }
  };

  updateEquipment = (id, data) => {
    return dispatch => {
      axios.put(`/equipment/${id}`, data)
        .then(() => {
          dispatch(this.fetchEquipment());
          NotificationManager.success("Успешно обновлено");
        })
        .catch(error => {
          console.log(error);
        })
    }
  };
};

export default new EquipmentApiService();