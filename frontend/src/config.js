let port = 8000;
let apiUrl = "http://localhost";

if (process.env.REACT_APP_NODE_ENV === "test") {
  port = 8010;
}

if (process.env.REACT_APP_NODE_ENV === "production") {
  apiUrl = "https://rekinetix.sytes.net";
  port = 81;
}

export default {
  apiURL: apiUrl + ":" + port
};
