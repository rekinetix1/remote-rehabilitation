import {NavItem, NavLink} from "reactstrap";
import classnames from "classnames";
import React from "react";
import './Tab.css'


const Tab = (props) => {
    return (
        <NavItem>
            <NavLink
                className={classnames({ active: props.activeTab === props.tabId })}
                onClick={() => {props.toggle(props.tabId)}}
            >
                {props.children}
            </NavLink>
        </NavItem>
    )
}
export default Tab