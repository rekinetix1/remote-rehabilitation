import {Col, Row, TabPane} from "reactstrap";
import React from "react";


const TabFill = (props) => {
    return (
        <TabPane tabId={props.tabId}>
            <Row>
                <Col sm="12">
                    <h4>{props.children}</h4>
                </Col>
            </Row>
        </TabPane>
    )
}
export default TabFill