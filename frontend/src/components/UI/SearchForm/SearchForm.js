import React, {useState, useEffect} from "react";
import Button from '@material-ui/core/Button';
import TextField from "@material-ui/core/TextField";

const SearchForm = props => {
  const [search, setSearch] = useState(props.search);
 
  useEffect(() => {
    setSearch(props.search);
  }, [props.search]);

  const changeInputHandler = (e) => {
		setSearch(e.target.value);
  };

  const submitForm = (e) => {
    e.preventDefault();
    if (search) {
      props.history.push(`/${props.route}?search=${search}`);
    } else {
      props.history.push(`/${props.route}`);
    }
  };

  return (
    <>
      <TextField
        placeholder={props.placeholder}
        id="outlined-basic"
        name="title"
        variant="outlined"
        className="mr-3 search-input"
        size="small"
        value={search ? search : ''}
        onChange={changeInputHandler}
      />
      <Button variant="contained" color="primary" onClick={submitForm}>Найти</Button>
    </>
  );
};

export default SearchForm;
