import React from "react";
import {makeStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import AnonymousMenu from "./Menus/AnonymousMenu";
import {Link as RouterLink} from 'react-router-dom';
import UserMenu from "./Menus/UserMenu";
import {Container} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    nav: {
        background: 'rgba(255,255,255)',
        color: 'rgba(0,0,0,0.8)',
        fontSize: '20',
        fontWeight: 'bold'
    },
    grow: {
        flexGrow: 1,
    },
}));


const NavToolbar = props => {
    const classes = useStyles()
    return (
        <div className={classes.grow}>
            <AppBar className={classes.nav}>
                <Container maxWidth={'lg'}>
                    <Toolbar>
                        <Typography component={RouterLink} to="/" className={classes.title} variant="h6" noWrap>
                            <img
                                alt='rekinetix'
                                src='https://static.tildacdn.com/tild3263-3138-4235-a566-666430323339/Logo_Rekinetix_12.png'
                                width='150'/>
                        </Typography>
                        <div className={classes.grow}/>
                        {
                            props.user ?
                                <UserMenu
                                    user={props.user}
                                    logout={props.logout}
                                /> : <AnonymousMenu/>
                        }
                    </Toolbar>
                </Container>
            </AppBar>


        </div>
    );


};

export default NavToolbar;



