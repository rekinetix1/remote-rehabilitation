import React from "react";
import {Link as RouterLink} from "react-router-dom";
import Button from "@material-ui/core/Button";
import {makeStyles} from "@material-ui/core/styles";
import ExitToAppIcon from '@material-ui/icons/ExitToApp';

const useStyles = makeStyles((theme) => ({
    links: {
        fontSize: '16px',
        padding: '15px'
    }
}));

const AnonymousMenu = () => {
    const classes = useStyles()
    return (
        <>
            <div className="d-block d-sm-none">
                <Button component={RouterLink} to="/programs"> Программы </Button>
                <Button component={RouterLink} className={classes.links} to="/login"><ExitToAppIcon style={{color: 'red'}}/></Button>
            </div>
            <div className="d-none d-sm-block">
                <Button component={RouterLink} className={classes.links} to="/programs"> Программы </Button>
                <Button component={RouterLink} className={classes.links} to="/register"> Зарегистрироваться </Button>
                <Button component={RouterLink} className={classes.links} to="/login"> Войти </Button>
            </div>
        </>
    );
};

export default AnonymousMenu;