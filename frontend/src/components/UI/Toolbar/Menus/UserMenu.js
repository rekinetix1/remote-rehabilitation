import React from "react";
import {Link as RouterLink} from "react-router-dom";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import AccountCircle from "@material-ui/icons/AccountCircle";
import Menu from "@material-ui/core/Menu";
import {makeStyles} from '@material-ui/core/styles';

import MenuItem from '@material-ui/core/MenuItem';

const useStyles = makeStyles((theme) => ({
    links: {
        fontSize: '16px',
        padding: '15px'
    }
}));

const UserMenu = props => {
    const classes = useStyles()

    const [anchorEl, setAnchorEl] = React.useState(null);

    const isMenuOpen = Boolean(anchorEl);

    const handleMenuClose = () => {
        setAnchorEl(null);
    };


    const logoutHandler = () => {
        handleMenuClose()
        props.logout()
    }


    const menuId = 'primary-search-account-menu';
    const renderMenu = (
        <Menu
            anchorEl={anchorEl}
            anchorOrigin={{vertical: 'top', horizontal: 'right'}}
            id={menuId}
            keepMounted
            transformOrigin={{vertical: 'top', horizontal: 'right'}}
            open={isMenuOpen}
            onClose={handleMenuClose}
        >
            <MenuItem component={RouterLink}
                      className={classes.links}
                      onClick={handleMenuClose}
                      to="/personalArea">Личный кабинет
            </MenuItem>
            <MenuItem className={classes.links} onClick={logoutHandler} component={RouterLink} to="/login"> Выйти
            </MenuItem>
        </Menu>
    );

    const handleProfileMenuOpen = (event) => {
        setAnchorEl(event.currentTarget);
    };


    return (
        <>
            {
                props.user.role !== "Пациент" ?
                <Button component={RouterLink} className={classes.links} to="/admin-panel">
                    Админ панель
                </Button> : null
            }
            <IconButton
                className={classes.links}
                edge="end"
                aria-label="account of current user"
                aria-controls={menuId}
                aria-haspopup="true"
                onClick={handleProfileMenuOpen}
                color="inherit"
            >
                <AccountCircle/>
            </IconButton>
            {renderMenu}
        </>

    );
};

export default UserMenu;