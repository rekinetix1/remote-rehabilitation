import React, {useState} from "react";
import VisibilityIcon from '@material-ui/icons/Visibility';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Button from '@material-ui/core/Button';
import {Link} from "react-router-dom";
import ModalWindow from "../ModalWindow/ModalWindow"

const IconsTable = props => {
  const [open, setOpen] = useState(false);

  const deleteAndClose = (row_id) => {
    props.deleteRow(row_id)
    setOpen(false)
  };

  return (
    <>
      <Link
        to={`/${props.route}/${props.row_id}`}
      >
        <VisibilityIcon
          className={props.hideShowIcon ? "d-none" : "AdminTable_icon"}
          fontSize="small"
          color="primary"
        />
      </Link>
      <Link
        to={`/${props.route}/${props.row_id}/edit`}
      >
        <EditIcon
          className={props.hideEditIcon ? "d-none" : "AdminTable_icon"} 
          fontSize="small"
          color="primary"
        />
      </Link>
      <DeleteIcon
        className={props.hideDeleteIcon ? "d-none" : "AdminTable_icon"}
        fontSize="small"
        color="primary"
        onClick={() => setOpen(true)}
      />
      <ModalWindow 
        show={open}
        closed={() => setOpen(false)}
        children={
          <div>
            <h2 id="simple-modal-title">Вы уверены что хотите удалить запись</h2>
            <Button
              variant="contained"
              color="primary"
              className="m-3"
              onClick={() => deleteAndClose(props.row_id)}>
              Да
            </Button>
            <Button 
              variant="contained"
              color="secondary"
              onClick={() => setOpen(false)}>
              Нет
            </Button>
          </div>
        }
      />
    </>
  );
};

export default IconsTable;