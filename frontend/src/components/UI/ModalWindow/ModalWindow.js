import React from "react";
import "./ModalWindow.css";
import Backdrop from "../Backdrop/Backdrop";


const ModalWindow = props => {
    return (
        <>
            <Backdrop
                show={props.show}
                clicked={props.closed}
            />
            <div
                className="Modal m-auto m-sm-1"
                style={{
                    transform: props.show ? "translateY(0)" : "translateY(-100vh)",
                    opacity: props.show ? 1 : 0
                }}
            >
                <div className='Modal_children'>
                {props.children}
                </div>
            </div>
        </>
    );
};

export default ModalWindow;
