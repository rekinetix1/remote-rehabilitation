import React from "react";
import BreadcrumbsComponent from "@material-ui/core/Breadcrumbs";
import {Link} from "react-router-dom";
import Typography from "@material-ui/core/Typography";

const Breadcrumbs = ({prevLink, prevPage, current}) => {
  return (
    <BreadcrumbsComponent aria-label="breadcrumb" style={{marginBottom: "20px"}}>
      <Link color="inherit" to="/">
        Главная
      </Link>
      <Link color="inherit" to="/admin-panel">
        Админ панель 
      </Link>
      {
        prevLink && prevPage ? (
          <Link color="inherit" to={`/${prevLink}`}>
            {prevPage}
          </Link>
        ) : null
      }
      {
        current ? (
          <Typography color="textPrimary" className="text-left">{current}</Typography>
        ) : null
      }
    </BreadcrumbsComponent>
  )
};

export default Breadcrumbs;