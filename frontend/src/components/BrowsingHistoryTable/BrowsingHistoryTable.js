import React from "react";
import {makeStyles} from '@material-ui/core/styles';
import Table from "@material-ui/core/Table";
import TableContainer from "@material-ui/core/TableContainer";
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import StyledTableCell from "../StyledTableCell";
import StyledTableRow from "../StyledTableRow";
import Moment from 'react-moment';
import '../AdminTable/AdminTable.scss'

const useStyles = makeStyles({
    table: {
        minWidth: 700,
    },
});


const BrowsingHistoryTable = ({columns, rows, route}) => {
    const classes = useStyles();

    return (
        <TableContainer component={Paper} style={{marginTop: "20px"}}>
            <Table className={classes.table} aria-label="customized table">
                <TableHead>
                    <TableRow>
                        {
                            columns.map(column => {
                                return <StyledTableCell key={column} align="center">{column}</StyledTableCell>
                            })
                        }
                    </TableRow>
                </TableHead>
                <TableBody>
                    {
                        rows.reverse().map((row) => (
                            <StyledTableRow  key={row._id}>
                                <StyledTableCell align="center">
                                    <Moment format="D/MM/YYYY HH:mm" withTitle>{row.datetime}</Moment>{}
                                </StyledTableCell>
                                <StyledTableCell align="center">{row.user && row.user.email}</StyledTableCell>
                                <StyledTableCell align="center">{row.user && row.user.name} {row.user && row.user.surname}</StyledTableCell>
                                <StyledTableCell align="center">{row.program.title}</StyledTableCell>
                                <StyledTableCell align="center">{row.day}</StyledTableCell>
                                <StyledTableCell align="center">{row.exercise ? row.exercise : <span>День окончен</span>}</StyledTableCell>
                            </StyledTableRow>
                        ))}
                </TableBody>
            </Table>
        </TableContainer>
    )
};

export default BrowsingHistoryTable;
