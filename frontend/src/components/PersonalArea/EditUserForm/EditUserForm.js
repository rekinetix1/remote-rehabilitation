import React from 'react'
import TextField from "@material-ui/core/TextField";
import {Button, Container} from "@material-ui/core";

const EditUserForm = props  => {


    return (
        <Container maxWidth={"sm"} className="EditUserForm">
            <h2 className="EditUserForm_title">Редактирование данных</h2>
            <form
                onSubmit={props.submitFormHandler}
                className="EditUserForm_form">
                <div className="EditUserForm_form_input_wrapper">
                    <TextField
                        fullWidth
                        margin='normal'
                        id="outlined-basic"
                        name="name"
                        label="Имя"
                        variant="outlined"
                        className="EditUserForm_input"
                        value={props.user.name}
                        onChange={props.changeInputHandler}
                    />
                </div>
                <div className="EditUserForm_input_wrapper">
                    <TextField
                        fullWidth
                        margin='normal'
                        id="outlined-basic"
                        name="surname"
                        label="Фамилия"
                        variant="outlined"
                        className="EditUserForm_input"
                        value={props.user.surname}
                        onChange={props.changeInputHandler}
                    />
                </div>
                <div className="EditUserForm_input_wrapper">
                    <TextField
                        fullWidth
                        margin='normal'
                        id="outlined-basic"
                        name="city"
                        label="Город"
                        variant="outlined"
                        className="EditUserForm_input"
                        value={props.user.city}
                        onChange={props.changeInputHandler}
                    />
                </div>
                <Button
                    type="submit"
                    variant="contained"
                    color="primary"
                >Изменить</Button>
            </form>
        </Container>
    )

}

export default EditUserForm