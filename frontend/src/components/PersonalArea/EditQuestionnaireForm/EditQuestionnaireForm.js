import React from "react";
import {Container} from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import FormLabel from "@material-ui/core/FormLabel";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Button from "@material-ui/core/Button";

const EditQuestionnaireForm = (props) => {

    const pains = props.pains.map((pain) => { 
        if (props.edit){
            let checked = false;
            let opt = props.userPains.find(userPain => userPain === pain._id);
            if (opt) checked = true;
            return (
                <FormControlLabel key={pain._id}
                    control={<Checkbox checked={checked}
                                onChange={props.checkboxHandler}
                                name="pains"
                                id={pain._id}
                            />}
                    label={pain.title}
                />
            )
        } else {
            return (
                <FormControlLabel key={pain._id}
                    control={<Checkbox 
                                onChange={props.checkboxHandler}
                                name="pains"
                                id={pain._id}
                            />}
                    label={pain.title}
                />
            )
        }
        
    })
    return (
        <form onSubmit={(e) => props.submitHandler(e, props.userQuestionnaire)}>
            <Container maxWidth={'xs'}>
                <div>
                    <TextField 
                        onChange={props.inputChangeHandler}
                        margin="normal"
                        name='age'
                        value={props.userQuestionnaire.age}
                        fullWidth
                        required
                        id="age"
                        type='number'
                        label="Введите ваш возраст"/>
                </div>
                <div>
                    <TextField 
                        onChange={props.inputChangeHandler}
                        margin="normal"
                        name='weight'
                        fullWidth
                        value={props.userQuestionnaire.weight}
                        required
                        id="weight"
                        type='number'
                        label="Введите ваш вес"/>
                </div>
                <div>
                    <TextField 
                        onChange={props.inputChangeHandler}
                        margin="normal"
                        name='height'
                        fullWidth
                        required
                        value={props.userQuestionnaire.height}
                        id="height"
                        type='number'
                        label="Введите ваш рост"/>
                </div>
                <FormControl fullWidth margin='normal' className={props.classes.formControl}>
                    <InputLabel id="gender">Выберите ваш пол</InputLabel>
                    <Select
                        labelId="gender"
                        id="gender"
                        name='gender'
                        value={props.userQuestionnaire.gender}
                        onChange={props.inputChangeHandler}
                    >
                        <MenuItem value="Женщина">Женщина</MenuItem>
                        <MenuItem value="Мужчина">Мужчина</MenuItem>
                    </Select>
                </FormControl>
                <FormControl fullWidth margin='normal' className={props.classes.formControl}>
                    <InputLabel id="occupation">Выберите ваш род деятельности</InputLabel>
                    <Select
                        labelId="occupation"
                        id="occupation"
                        name='occupation'
                        value={props.userQuestionnaire.occupation}
                        onChange={props.inputChangeHandler}
                    >
                        {props.occupation.map(occ => {
                            return <MenuItem key={occ._id} value={occ._id}>{occ.title}</MenuItem>
                        })}
                    </Select>
                </FormControl>

                <FormControl fullWidth margin='normal' className={props.classes.formControl}>
                    <InputLabel id="activity">Выберите степень вашей физической активности</InputLabel>
                    <Select
                        labelId="activity"
                        id="activity"
                        name='physicalActivity'
                        value={props.userQuestionnaire.physicalActivity}
                        onChange={props.inputChangeHandler}
                    >
                        {props.physicalActivities.map(activity => {
                            return <MenuItem key={activity._id} value={activity._id}>{activity.title}</MenuItem>
                        })}
                    </Select>
                </FormControl>
                <FormControl margin='normal' component="fieldset" className={props.classes.formControl}>
                    <FormLabel component="legend">Жалобы на боли</FormLabel>
                    <FormGroup>
                        {pains}

                    </FormGroup>
                </FormControl>

                <div>
                    <TextField onChange={props.inputChangeHandler}
                               margin="normal"
                               name='exacerbationsCountPerYear'
                               fullWidth
                               value={props.userQuestionnaire.exacerbationsCountPerYear}
                               required
                               id="exacerbationsCountPerYear"
                               type='number'
                               label="Количество обострений в год"/>
                </div>
                <div>
                    <TextField onChange={props.inputChangeHandler}
                               margin="normal"
                               name='painIntensity'
                               fullWidth
                               required
                               id="painIntensity"
                               type='number'
                               value={props.userQuestionnaire.painIntensity}
                               label="Интенсивность боли от 1 до 10"/>
                </div>

                <div>
                    <TextField onChange={props.inputChangeHandler}
                               margin="normal"
                               name="diagnosis"
                               fullWidth
                               value={props.userQuestionnaire.diagnosis}
                               id="diagnosis"
                               type='text'
                               label="Медицинский диагноз, если он есть"/>
                </div>

                <div className='Register_submit'>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                    >
                        сохранить анкету
                    </Button>
                </div>
            </Container>
        </form>
    )
}

export default EditQuestionnaireForm