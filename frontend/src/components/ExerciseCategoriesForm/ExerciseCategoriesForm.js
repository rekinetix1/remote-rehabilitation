import React, {useState} from "react";
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';

const ExerciseCategoriesForm = (props) => {
  const [form, setForm] = useState({
    title: "",
    description: ""
  });

  const inputHandler = e => {
    setForm({
      ...form,
      [e.target.name]: e.target.value
    })
  };

  const submitForm = (e) => {
    e.preventDefault();
    props.create(form);
    setForm({
      title: "",
      description: ""
    });
  };

  return (
    <Form
      onSubmit={submitForm}
      className="mb-5"
    >
      <FormGroup>
        <Label for="title">Заголовок</Label>
        <Input
          type="text"
          name="title"
          id="title"
          placeholder="Заголовок области применения упражнений"
          onChange={inputHandler}
          value={form.title}
        />
      </FormGroup>
      <FormGroup>
        <Label for="description">Описание</Label>
        <Input
          type="textarea"
          name="description"
          id="description"
          placeholder="Описание области применения упражнений"
          onChange={inputHandler}
          value={form.description}
        />
      </FormGroup>
      <Button type="submit">Создать</Button>
    </Form>
  )
};

export default ExerciseCategoriesForm;