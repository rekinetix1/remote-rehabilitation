import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import config from "../../config";
import IconButton from "@material-ui/core/IconButton";
import FavoriteIcon from '@material-ui/icons/Favorite';
import {Link as RouterLink} from "react-router-dom";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles({
    root: {
        maxWidth: 380,
        textAlign: 'left'
    },
    media: {
        height: 230,
    },
});

const Program = (props) => {
    const classes = useStyles();
    let image = "http://via.placeholder.com/300x200";
    if (props.image) {
        image = config.apiURL + "/uploads/" + props.image;
    }

    const changeFavorites = () => {
        let favorite = {
            user: props.user._id,
            program: props.id
        }
        props.favorite ? props.removeFavorite(props.favorite._id) : props.addFavorite(favorite)
    };

    return (
        <Grid item md={4} xs={12} sm={6}>
            <Card className={classes.root + ' h-100 d-flex flex-column justify-content-between'}>
                <CardActionArea component={RouterLink} to={'./programs/' + props.id}>
                    <CardMedia
                        className={classes.media}
                        image={image}
                        title="Contemplative Reptile"
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                            {props.title}
                        </Typography>
                        <Typography gutterBottom variant="subtitle1" component="h6">
                        Категория: {props.category}
                        </Typography>
                        <Typography gutterBottom variant="subtitle1" component="h6">
                            Базовая цена: {props.cost.toLocaleString()} ₸
                        </Typography>
                        <Typography variant="body2" color="textSecondary" component="p">
                            {props.short_description}
                        </Typography>
                    </CardContent>
                </CardActionArea>
                <CardActions >
                    <Button  fullWidth component={RouterLink} color={props.userProgram ? 'secondary' : 'default'} variant="contained" to={'/programs/' + props.id}>{props.userProgram ? "Заниматься" : "Полное описание"}</Button>
                    {
                        props.user ?
                        <IconButton  aria-label="add to favorites" onClick={() => changeFavorites()}>
                            <FavoriteIcon color={props.favorite ? "secondary" : 'inherit'}/>
                        </IconButton> : null
                    }
                </CardActions>
            </Card>
        </Grid>
    );
}
export default Program