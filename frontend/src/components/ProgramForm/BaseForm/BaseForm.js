import React from "react";
import {InputLabel, Select, MenuItem, FormControl, Grid} from "@material-ui/core";
import FormHelperText from "@material-ui/core/FormHelperText";
import TextField from '@material-ui/core/TextField';

const BaseForm = (props) => {
  const getFieldError = fieldName => {
    return props.error && props.error.errors && props.error.errors[fieldName] && props.error.errors[fieldName].message;
  };

  return (
    <div>
      <div>
        <InputLabel id="image">Основное фото</InputLabel>
        <TextField
            fullWidth
            type="file"
            id="outlined-basic"
            name="image"
            variant="outlined"
            onChange={props.onChangeFile}
        />
      </div>
      <div>
        <TextField
          onChange={props.inputChangeHandler}
          margin="normal"
          name='title'
          fullWidth
          required
          variant="outlined"
          id="title"
          type='text'
          value={props.program.title}
          helperText={getFieldError("title")}
          error={!!(props.error && props.error.errors && props.error.errors['title'] && props.error.errors['title'].message)}
          label="Название программы"/>
      </div>
      <div>
        <TextField 
          onChange={props.inputChangeHandler}
          margin="normal"
          name='short_description'
          fullWidth
          variant="outlined"
          required
          id="short_description"
          type='text'
          value={props.program.short_description}
          helperText={getFieldError("short_description")}
          error={!!(props.error && props.error.errors && props.error.errors['short_description'] && props.error.errors['short_description'].message)}
          label="Короткое описание программы"/>
      </div>
      <div>
        <TextField
          onChange={props.inputChangeHandler}
          margin="normal"
          name='full_description'
          fullWidth
          required
          variant="outlined"
          id="full_description"
          type='text'
          helperText={getFieldError("full_description")}
          error={!!(props.error && props.error.errors && props.error.errors['full_description'] && props.error.errors['full_description'].message)}
          value={props.program.full_description}
          label="Полное описание программы"/>
      </div>
      <div>
        <TextField
          onChange={props.inputChangeHandler}
          margin="normal"
          name='video'
          fullWidth
          required
          variant="outlined"
          id="video"
          type='text'
          value={props.program.video}
          label="Ссылка на видео описания программы"/>
      </div>
      <Grid container spacing={3}>
        <Grid item xs>
          <FormControl error={!!(props.error && props.error.errors && props.error.errors['complexity'] && props.error.errors['complexity'].message)}
                       fullWidth className='mt-3' variant="outlined">
            <InputLabel id="demo-simple-select-label">Сложность</InputLabel>
            <Select
              onChange={props.inputChangeHandler}
              name='complexity'
              fullWidth
              required
              label='Сложность'
              labelId="demo-simple-select-label"
              id="complexity"
              value={props.program.complexity}>
              <MenuItem value={'Низкая'}>Низкая</MenuItem>
              <MenuItem value={'Средняя'}>Средняя</MenuItem>
              <MenuItem value={'Тяжелая'}>Тяжелая</MenuItem>
            </Select>
            {!!(props.error && props.error.errors && props.error.errors['complexity']) ?
                            <FormHelperText>Выберите cложность</FormHelperText> : null}
          </FormControl>
        </Grid>
        <Grid item xs>
          <TextField
            onChange={props.inputChangeHandler}
            margin="normal"
            name='cost'
            fullWidth
            variant="outlined"
            required
            id="cost"
            type='number'
            helperText={getFieldError("cost")}
            error={!!(props.error && props.error.errors && props.error.errors['cost'] && props.error.errors['cost'].message)}
            value={props.program.cost}
            label="Стоимость"/>
        </Grid>
        <Grid item xs>
          <TextField
            margin="normal"
            disabled
            value={props.program.duration}
            name='duration'
            fullWidth
            variant="outlined"
            required
            id="duration"
            type='number'
            label="Длительность (в днях)"/>
        </Grid>
      </Grid>
      <Grid container spacing={2}>
        <Grid item xs>
          <FormControl error={!!(props.error && props.error.errors && props.error.errors['cost'] && props.error.errors['cost'].message)}
                       fullWidth className='mt-3' variant="outlined">
            <InputLabel id="demo-simple-select-label">Модератор</InputLabel>
            <Select
              onChange={props.inputChangeHandler}
              name='moderator'
              fullWidth
              required
              label='Модератор'
              labelId="demo-simple-select-label"
              value={props.program.moderator}
              id="moderator">
              {
                props.moderators.map((moderator) => {
                  return <MenuItem value={moderator._id} key={moderator._id}>{moderator.name} {moderator.surname}</MenuItem>
                })
              }
            </Select>
            {!!(props.error && props.error.errors && props.error.errors['moderator']) ?
                            <FormHelperText>Выберите модератора</FormHelperText> : null}
          </FormControl>
        </Grid>
        <Grid item xs>
          <FormControl fullWidth className='mt-3 AdminPage_form_input' variant="outlined">
          <InputLabel id="demo-simple-select-outlined-label">Категория</InputLabel>
            <Select
              onChange={props.inputChangeHandler}
              name='category'
              fullWidth
              required
              labelId="category"
              id="category"
              value={props.program.category}
              label='Категория'
            >
              {
                props.programCategories.map((category) => {
                  return <MenuItem value={category._id} key={category._id}>{category.title}</MenuItem>
                })
              }
            </Select>
          </FormControl>
        </Grid>
      </Grid>
    </div>
  )
};

export default BaseForm;