import React, {useState} from "react";
import Button from "@material-ui/core/Button";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import CancelIcon from '@material-ui/icons/Cancel';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import FormExerciseProgram from "./FormExerciseProgram/FormExerciseProgram"

const DayForm = (props) => {
  const formForExercises = () => {
    let formExercises = [];
    props.day.map((exercise, i) => {
      formExercises.push(
        <FormExerciseProgram
          key={i}
          exercise={exercise}
          exercises={props.exercises}
          exercise_number={i}
          day_number={props.day_number}
          onChangeExercise={props.onChangeExercise}
          onChangeRepetition={props.onChangeRepetition}
          deleteExersise={props.deleteExersise}/>
      );
    })
    return formExercises
  };


  return (
    <div>
      <div className="border my-2 px-1 pb-3 bg-light">
        <div className="d-flex justify-content-between m-2">
          <p className="font-weight-bold">{props.day_number + 1} день</p>
          <div>
            <FileCopyIcon
              className="mr-2"
              fontSize="small"
              color="primary"
              onClick={() => props.copyDay(props.day_number)}/>
            <CancelIcon
              fontSize="small"
              color="secondary"
              onClick={() => props.deleteDay(props.day_number)}/>
          </div>
        </div>
        <div className="Exersises">
          { formForExercises() }
        </div>
        <div className="text-right mt-3">
          <Button
            color="primary"
            onClick={() => props.addExersise(props.day_number)}
            size="small">
            Добавить упражнение
          </Button>
        </div>
      </div>
      {
        !props.last ?
        <div
          className="text-center"
          aria-label="delete"
          onClick={() => props.swapDays(props.day_number)}>
          <ExpandLessIcon fontSize="small" color="primary"/>
          <ExpandMoreIcon fontSize="small" color="primary"/>
        </div> : null
      }
    </div>
  )
};

export default DayForm;