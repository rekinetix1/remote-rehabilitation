import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import {TextField, FormControl, Grid} from "@material-ui/core";
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import { Autocomplete } from '@material-ui/lab';

const FormExerciseProgram = (props) => {
  const useStyles = makeStyles((theme) => ({
    margin: {
      margin: theme.spacing(1),
    },
  }));
  const classes = useStyles();

  const autocompleteHandler = (_e, values) => {
    if (values) {
      props.onChangeExercise(values._id, props.day_number, props.exercise_number)
    }
  };

  return (
    <div>
      <Grid container spacing={4} className='p-2'>
        <Grid item xs={1} className='pt-4'>
          {props.exercise_number + 1}
        </Grid>
        <Grid item xs={5} className='p-0 m-0'>
          <FormControl fullWidth className='px-3'>
            <Autocomplete
              id="combo-box-demo"
              required
              options={props.exercises}
              getOptionLabel={(option) => option.title}
              onChange={autocompleteHandler}
              value={props.exercises.find(exercise => exercise._id === props.exercise.exercise)}
              renderInput={(params) => <TextField {...params} label="Упражнение" variant="outlined"/>}
            />
          </FormControl>
        </Grid>
        <Grid item xs={3} className='p-0 m-0 pr-2'>
          <TextField
            fullWidth
            required
            variant="outlined"
            id="repetitions"
            name="repetitions"
            type='number'
            label="Повторений"
            value={props.exercise.repetitions}
            onChange={(e) => props.onChangeRepetition(e, props.day_number, props.exercise_number)}/>
        </Grid>
        <Grid item xs={2} className='p-0 m-0'>
          <TextField
            fullWidth
            required
            variant="outlined"
            id="approaches"
            name="approaches"
            type='number'
            label="Подходы"
            value={props.exercise.approaches}
            onChange={(e) => props.onChangeRepetition(e, props.day_number, props.exercise_number)}/>
        </Grid>
        <Grid item xs={1} className='p-0 m-0'>
          <IconButton 
            aria-label="delete"
            onClick={() => props.deleteExersise(props.day_number, props.exercise_number)}
            className={classes.margin}>
            <DeleteIcon fontSize="small" />
          </IconButton>
        </Grid>
      </Grid>
    </div>
  )
};

export default FormExerciseProgram;