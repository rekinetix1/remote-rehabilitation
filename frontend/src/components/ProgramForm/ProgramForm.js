import React, {Component} from "react";
import BaseForm from "./BaseForm/BaseForm"
import {Container} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import DayForm from "./DayForm/DayForm"
import arrayMove from 'array-move';

class ProgramForm extends Component {
  state = {
    title: "",
    short_description: "",
    full_description: "",
    complexity: "",
    duration: 0,
    cost: "",
    moderator: "",
    category: "",
    video: "",
    image: "",
    author: "",
    days: [],
  };

  componentDidMount() {
    this.props.onFetchProgramCategory();
    this.props.onfetchExercises();
    this.props.onfetchUsers('Модератор');
    if (this.props.edit) {
      this.props.onFetchSingleProgram(this.props.id)
        .then((data) => {
          this.setState({
            title: data.title,
            short_description: data.short_description,
            full_description: data.full_description,
            complexity: data.complexity,
            duration: data.duration,
            cost: data.cost,
            moderator: data.moderator._id,
            category: data.category,
            video: data.video,
            image: data.image,
            author: data.author,
            days: data.days,
          });
        })
    } else {
      this.setState({author: this.props.user._id})
    }
  }

  inputChangeHandler = e => {
    this.setState({[e.target.name]: e.target.value});
  };

  onChangeFile = e => {
    this.setState({[e.target.name]: e.target.files[0]})
  };

  onChangeExercise = (id, day, exercise_number) => {
    let days = [...this.state.days];
    days[day][exercise_number]['exercise'] = id
    this.setState({days})
  };

  onChangeRepetition = (e, day, exercise_number) => {
    let days = [...this.state.days];
    days[day][exercise_number][e.target.name] = e.target.value
    this.setState({days})
  };

  addDay = e => {
    let exersise = [{
      exercise: '',
      repetitions: '',
      approaches: '',
    }]
    this.setState({
      duration: this.state.duration + 1,
      days: [...this.state.days, exersise],
    })
  };

  deleteDay = day => {
    let days = [...this.state.days];
    days.splice(day, 1)
    this.setState({
      duration: this.state.duration - 1,
      days: days,
    })
  };

  swapDays = day_number => {
    let days = [...this.state.days];
    const changeDays = arrayMove(days, day_number, day_number+1)
    this.setState({days: changeDays})
  };

  copyDay = day_number => {
    let days = [...this.state.days];
    let duplicate_day = []
    days[day_number].map((exercise) => {
      duplicate_day.push({
        exercise:       exercise.exercise,
        repetitions:    exercise.repetitions,
        approaches:     exercise.approaches,
      })
    })
    days.push(duplicate_day)
    this.setState({
      duration: this.state.duration + 1,
      days: days,
    })
  };

  addExersise = (day) => {
    let exersise = {
      exercise: '',
      repetitions: '',
      approaches: '',
    }
    let days = [...this.state.days];
    days[day].push(exersise)
    this.setState({days})
  };

  deleteExersise = (day, exersise) => {
    let days = [...this.state.days];
    days[day].splice(exersise, 1)
    this.setState({days})
  };

  submitFormHandler = e => {
    e.preventDefault();
    const formData = new FormData();
    Object.keys(this.state).forEach(key => {
      if (key === 'days') {
        formData.append(key, JSON.stringify(this.state[key]))
      } else {
        formData.append(key, this.state[key]);
      }
    });
    if (this.props.edit) {
      this.props.onUpdateProgram(this.props.id, formData)
    } else {
      this.props.onCreateProgram(formData)
    }
  };

  formForDays = () => {
    let formDays = [];
    for (let i = 0; i < this.state.duration; i++) {
      formDays.push(
        <DayForm
          exercises={this.props.exercises}
          key={i}
          day_number={i}
          day={this.state.days[i]}
          addExersise={this.addExersise}
          onChangeExercise={this.onChangeExercise}
          onChangeRepetition={this.onChangeRepetition}
          deleteExersise={this.deleteExersise}
          deleteDay={this.deleteDay}
          last={this.state.duration === i+1}
          swapDays={this.swapDays}
          copyDay={this.copyDay}/>
      );
    }
    return formDays
  };

  render() {
    return (
      <div className="NewProgram">
        <h2>{ this.props.edit ? 'Редактирование' : 'Cоздание' } программы</h2>
        <form onSubmit={this.submitFormHandler} className="text-left">
          <Container>
            <BaseForm
              onChangeFile={this.onChangeFile}
              inputChangeHandler={this.inputChangeHandler}
              program={this.state}
              moderators={this.props.moderators}
              programCategories={this.props.programCategories}
              error={this.props.error}
            />
            <div className="Days" id="days">
              { this.formForDays() }
            </div>
            <div className='mt-4'>
              <Button
                color="primary"
                variant="outlined"
                onClick={this.addDay}>
                {this.state.duration === 0 ? 'Добавить первый день' : 'Добавить день'}
              </Button>
            </div>
            <div className='Register_submit'>
              <Button
                type="submit"
                variant="contained"
                color="primary">
                { this.props.edit ? 'Изменить' : 'Cоздать' }
              </Button>
            </div>
          </Container>
        </form>
      </div>
    );
  }
};

export default ProgramForm;