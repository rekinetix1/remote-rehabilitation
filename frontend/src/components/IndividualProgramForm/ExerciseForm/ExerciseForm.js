import React from "react";
import Button from "@material-ui/core/Button";
import FormExerciseProgram from "./FormExerciseProgram/FormExerciseProgram"

const DayForm = (props) => {
  const formForExercises = () => {
    let formExercises = [];
    props.exercises.map((exercise, i) => {
      return formExercises.push(
        <FormExerciseProgram
          key={i}
          exercise={exercise}
          allExercises={props.allExercises}
          category={props.category}
          equipment={props.equipment}
          kinematic_chain={props.kinematic_chain}
          complexity={props.complexity}
          muscle={props.muscle}
          exercises={props.exercises}
          exercise_number={i}
          onChangeExercise={props.onChangeExercise}
          onChangeRepetition={props.onChangeRepetition}
          deleteExersise={props.deleteExersise}/>
      );
    })
    return formExercises
  };

  return (
    <div>
      <div className="border my-4 px-1 pb-3 py-bg-light">
        <div className="Exersises">
          { formForExercises() }
        </div>
        <div className="text-right mt-3">
          <Button
            color="primary"
            onClick={() => props.addExersise(props.exersiceNum)}
            size="small">
            Добавить упражнение
          </Button>
        </div>
      </div>
      
    </div>
  )
};

export default DayForm;