import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import {TextField, FormControl, Grid, Select, MenuItem, InputLabel} from "@material-ui/core";
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import { Autocomplete } from '@material-ui/lab';

const FormExerciseProgram = (props) => {
  const useStyles = makeStyles((theme) => ({
    margin: {
      margin: theme.spacing(1),
    },
  }));
  const classes = useStyles();

  const autocompleteHandler = (_e, values) => {
    if (values) {
      props.onChangeExercise(values._id, props.exercise_number)
    }
  };

  let options = props.allExercises;
  let opt = [];

  if (props.kinematic_chain) {
    options = options.filter(exercise => props.kinematic_chain === exercise.kinematic_chain);
    if (props.complexity){
      options = options.filter(exercise => props.complexity === exercise.complexity)
    }
    if (props.category){
      options = options.filter(exercise => props.category === exercise.category._id);
    }
    if (props.equipment){
      for (let i = 0; i < options.length; i++){
        for (let g = 0; g < options[i].equipment.length; g++){
          if (options[i].equipment[g]._id === props.equipment){
            opt.push(options[i]);
          }
        }
      }
      options = [...opt];
    }
    if (props.muscle){
      options = options.filter(exercise => exercise.muscle)
        .filter(exercise => props.muscle === exercise.muscle);
    }
  } else if (props.complexity){
      options = options.filter(exercise => props.complexity === exercise.complexity)
    
    if (props.kinematic_chain){
      options = options.filter(exercise => props.kinematic_chain === exercise.kinematic_chain);
    }
    if (props.category){
      options = options.filter(exercise => props.category === exercise.category._id);
    }
    if (props.equipment){
      for (let i = 0; i < options.length; i++){
        for (let g = 0; g < options[i].equipment.length; g++){
          if (options[i].equipment[g]._id === props.equipment){
            opt.push(options[i]);
          }
        }
      }
      options = [...opt];
    }
    if (props.muscle){
      options = options.filter(exercise => exercise.muscle)
        .filter(exercise => props.muscle === exercise.muscle);
    }
  } else if (props.category){
      options = options.filter(exercise => props.category === exercise.category._id);
    
    if (props.kinematic_chain){
      options = options.filter(exercise => props.kinematic_chain === exercise.kinematic_chain);
    }
    if (props.complexity){
      options = options.filter(exercise => props.complexity === exercise.complexity)
    }
    if (props.equipment){
      for (let i = 0; i < options.length; i++){
        for (let g = 0; g < options[i].equipment.length; g++){
          if (options[i].equipment[g]._id === props.equipment){
            opt.push(options[i]);
          }
        }
      }
      options = [...opt];
    }
    if (props.muscle){
      options = options.filter(exercise => exercise.muscle)
        .filter(exercise => props.muscle === exercise.muscle);
    }
  } else if (props.muscle){
      options = options.filter(exercise => exercise.muscle)
        .filter(exercise => props.muscle === exercise.muscle);
    if (props.kinematic_chain){
      options = options.filter(exercise => props.kinematic_chain === exercise.kinematic_chain);
    }
    if (props.complexity){
      options = options.filter(exercise => props.complexity === exercise.complexity)
    }
    if (props.equipment){
      for (let i = 0; i < options.length; i++){
        for (let g = 0; g < options[i].equipment.length; g++){
          if (options[i].equipment[g]._id === props.equipment){
            opt.push(options[i]);
          }
        }
      }
      options = [...opt];
    }
    if (props.category){
      options = options.filter(exercise => props.category === exercise.category._id);
    }
  } else if (props.equipment){
    for (let i = 0; i < options.length; i++){
      for (let g = 0; g < options[i].equipment.length; g++){
        if (options[i].equipment[g]._id === props.equipment){
          opt.push(options[i]);
        }
      }
    }
    options = [...opt];
  if (props.kinematic_chain){
    options = options.filter(exercise => props.kinematic_chain === exercise.kinematic_chain);
  }
  if (props.complexity){
    options = options.filter(exercise => props.complexity === exercise.complexity)
  }
  if (props.muscle){
    options = options.filter(exercise => exercise.muscle)
      .filter(exercise => props.muscle === exercise.muscle);
  }
  if (props.category){
    options = options.filter(exercise => props.category === exercise.category._id);
  }
}

  return (
    <div>
      <Grid container spacing={4} className='p-2 mt-4'>
        <Grid item xs={1} sm={1} className='pt-0 px-2 px-md-2 px-lg-4'>
          {props.exercise_number + 1}
        </Grid>
        <Grid item xs={11} sm={11} md={5} className='p-0 m-0'>
          <FormControl fullWidth className='px-2 px-md-3 mx-lg-0 '>
            <Autocomplete
              id="combo-box-demo"
              required
              label="Упражнение"
              options={options}
              getOptionLabel={(option) => option.title}
              onChange={autocompleteHandler}
              value={options.find(exercise => exercise._id === props.exercise.exercise)}
              renderInput={(params) => <TextField {...params} label="Упражнение" variant="outlined"/>}
            />
          </FormControl>
        </Grid>
        <Grid item xs={12} sm={6} md={2} className='p-0 m-0'>
          <FormControl fullWidth className='m-3 pr-4 px-md-0 m-lg-0 pr-md-2' variant="outlined">
            <InputLabel id="side_select">Сторона</InputLabel>
            <Select
              onChange={(e) => props.onChangeRepetition(e, props.exercise_number)}
              name='side'
              label="Сторона"
              required
              labelId="side_select"
              id="side"
              value={props.exercise.side}>
              <MenuItem value={'Правая'}>Правая</MenuItem>
              <MenuItem value={'Левая'}>Левая</MenuItem>
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={12} sm={5} md={3} className='p-0 m-0'>
          <FormControl fullWidth className='mx-3 mb-3 mt-0 mt-sm-3 pr-4 pr-md-0 m-lg-0' variant="outlined">
            <InputLabel id="reduction_type_select">Тип сокращения</InputLabel>
            <Select
              onChange={(e) => props.onChangeRepetition(e, props.exercise_number)}
              name='reduction_type'
              label="Тип сокращения"
              required
              labelId="reduction_type_select"
              id="reduction_type"
              value={props.exercise.reduction_type}>
              <MenuItem value={'Изометрическое'}>Изометрическое</MenuItem>
              <MenuItem value={'Концентрическое'}>Концентрическое</MenuItem>
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={1} className='p-0 m-0 d-none d-lg-block'>
          <IconButton 
            aria-label="delete"
            onClick={() => props.deleteExersise(props.exercise_number)}
            className={classes.margin}>
            <DeleteIcon fontSize="small" />
          </IconButton>
        </Grid>
      </Grid>
      <Grid container spacing={6} className='p-2'>
        <Grid item xs={1} className='m-0 d-none d-lg-block'>
        </Grid>
        <Grid item xs={4} sm={3} className='m-0 pt-4 pl-3 pr-0 pr-md-4 pl-md-4 text-center'>
          <TextField
            fullWidth
            required
            id="repetitions"
            name="repetitions"
            type='number'
            label="Повторений"
            variant="outlined"
            value={props.exercise.repetitions}
            onChange={(e) => props.onChangeRepetition(e, props.exercise_number)}/>
        </Grid>
        <Grid item xs={5} sm={4} className='m-0 pt-2 pl-1 pr-0 text-center'>
          <TextField
            onChange={(e) => props.onChangeRepetition(e, props.exercise_number)}
            margin="normal"
            name='workouts'
            required
            variant="outlined"
            id="workouts"
            type='number'
            value={props.exercise.workouts}
            label="Тренировок в день"/>
        </Grid>
        <Grid item xs={3} sm={3} className='m-0 pt-2 pl-1 pr-3 text-center'>
          <TextField
            onChange={(e) => props.onChangeRepetition(e, props.exercise_number)}
            margin="normal"
            name='approaches'
            required
            variant="outlined"
            id="approaches"
            type='number'
            value={props.exercise.approaches}
            label="Подходы"/>
        </Grid>
        <Grid item xs={1} className='d-none d-lg-block'>
        </Grid>
        <Grid item xs={12} className='p-0 mr-3 d-block d-lg-none text-right'>
          <IconButton 
            aria-label="delete"
            onClick={() => props.deleteExersise(props.exercise_number)}
            className={classes.margin + " p-0 text-right"}>
            <DeleteIcon fontSize="small" />
          </IconButton>
        </Grid>
      </Grid>
      <hr></hr>
    </div>
  )
};

export default FormExerciseProgram;