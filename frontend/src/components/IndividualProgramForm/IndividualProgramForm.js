import React, {Component} from "react";
import {Container, Grid} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import IndividualBaseForm from "./IndividualBaseForm/IndividualBaseForm";
import ExerciseForm from "./ExerciseForm/ExerciseForm";
import Filter from "./Filter/Filter";

class IndividualProgramForm extends Component {
    state = {
        user: "",
        title: "",
        status: "Подготовлена",
        lifeTime: "",
        complexity: "",
        cost: "",
        moderator: "",
        goal: "",
        result: "",
        exercises: [],
        exercises_count: 0,
        kinematic_chain: null,
        filterComplexity: null,
        category: null,
        equipment: null,
        muscle: null,
        paymentStatus: "",
        edit: true
    };

    componentDidMount(){
        this.props.onFetchExerciseCategories();
        this.props.onfetchUsers();
        this.props.onFetchEquipment();
        this.props.onfetchExercises();
        this.props.onFetchMuscles();
        if (this.props.edit){
            this.props.onFetchIndividualProgram(this.props.id)
                .then(data => {
                    this.setState({
                        user: data.user._id,
                        title: data.title,
                        status: data.status,
                        lifeTime: data.lifeTime,
                        complexity: data.complexity,
                        cost: data.cost,
                        moderator: data.moderator._id,
                        goal: data.goal,
                        result: data.result,
                        exercises: data.exercises,
                        exercises_count: data.exercises_count,
                        paymentStatus: data.paymentStatus
                    });
                })
        }
    }

    inputChangeHandler = e => {
        this.setState({[e.target.name]: e.target.value});
    };

    onChangeExercise = (id, exercise_number) => {
        let exercises = [...this.state.exercises];
        exercises[exercise_number]['exercise'] = id
        this.setState({exercises})
    };
    
    onChangeRepetition = (e, exercise_number) => {
        let exercises = [...this.state.exercises];
        exercises[exercise_number][e.target.name] = e.target.value
        this.setState({exercises})
    };

    autocompleteHandler = (_e, values) => {
        if (values) {
            this.setState({user: values._id})
        } 
    };

    addExersise = () => {
        let exersise = {
          exercise: '',
          repetitions: '',
          side: '',
          reduction_type: '',
          workouts: '',
          approaches: '',
        }
        let exercises = [...this.state.exercises];
        exercises.push(exersise)
        this.setState({
            exercises_count: this.state.exercises_count + 1,
            exercises: exercises
        })
    };

    deleteExersise = (exersise) => {
        let exercises = [...this.state.exercises];
        exercises.splice(exersise, 1)
        this.setState({exercises})
    };

    submitFormHandler = e => {
        e.preventDefault();

        if (this.props.edit) {
          this.props.onUpdateProgram(this.props.id, this.state, "individual_programs");
        } else {
          this.props.onCreateProgram(this.state);
        }
    };

    render(){
        return (
            <div className = "NewProgram">
                <h2>{ this.props.edit ? 'Редактирование' : 'Cоздание' } индивидуальной программы</h2>
                <form onSubmit={this.submitFormHandler} className="text-left">
                    <Container>
                        <IndividualBaseForm
                            inputChangeHandler={this.inputChangeHandler}
                            autocompleteHandler={this.autocompleteHandler}
                            program={this.state}
                            users={this.props.users}
                            programCategories={this.props.programCategories}
                            edit={this.props.edit}
                        />
                        <Grid container spacing={3}>
                            <Grid item xs={12} sm={3} className="d-none d-sm-block">
                                <Filter
                                    inputChangeHandler={this.inputChangeHandler}
                                    categories={this.props.exerciseCategories}
                                    equipment={this.props.equipment}
                                    muscles={this.props.muscles ? this.props.muscles : []}
                                />
                            </Grid>
                            <Grid item xs={12} sm={9}>
                                <ExerciseForm
                                    allExercises={this.props.exercises}
                                    exercises={this.state.exercises}
                                    addExersise={this.addExersise}
                                    kinematic_chain={this.state.kinematic_chain}
                                    complexity={this.state.filterComplexity}
                                    muscle={this.state.muscle}
                                    category={this.state.category}
                                    equipment={this.state.equipment}
                                    onChangeExercise={this.onChangeExercise}
                                    onChangeRepetition={this.onChangeRepetition}
                                    deleteExersise={this.deleteExersise}
                                    last={this.state.exercises_count +1}
                                />
                            </Grid>
                            <Grid item xs={12} sm={3} className='d-block d-sm-none'>
                                <Filter
                                    inputChangeHandler={this.inputChangeHandler}
                                    categories={this.props.exerciseCategories}
                                    equipment={this.props.equipment}
                                    muscles={this.props.muscles ? this.props.muscles : []}
                                />
                            </Grid>
                        </Grid>
                        <div className='Register_submit'>
                            <Button
                                type="submit"
                                variant="contained"
                                color="primary">
                                { this.props.edit ? 'Изменить' : 'Cоздать' }
                            </Button>
                        </div>
                    </Container>
                </form>
            </div>
        );
    }
}

export default IndividualProgramForm;