import React from "react";
import {InputLabel, Select, MenuItem, FormControl, Grid} from "@material-ui/core";
import TextField from '@material-ui/core/TextField';
import { Autocomplete } from '@material-ui/lab';

const IndividualBaseForm = (props) => {
  let users = props.users.filter(user => user.role === "Пациент");
  let user = users.find(user => user._id === props.program.user);

  console.log(users);
  console.log(user);
  return (
    <div>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={6} className="py-0 py-md-3">
          {
            !props.edit ?
            <FormControl fullWidth className='mt-3' variant="outlined">
              <Autocomplete
                id="combo-box-demo"
                required
                label="Пациент"
                options={users}
                getOptionLabel={(user) => user.surname + " " + user.name + " - " + user.email}
                onChange={props.autocompleteHandler}
                value={users.find(user => user._id === props.program.user)}
                renderInput={(params) => <TextField {...params} label="Пациент" variant="outlined"/>}
              />
            </FormControl> :
            <TextField
              onChange={props.inputChangeHandler}
              margin="normal"
              name='user'
              fullWidth
              variant="outlined"
              required
              disabled
              id="user"
              type='text'
              value={user && user.surname + " " + user.name + " - " + user.email}
              label=""/>
          }
        </Grid>
        <Grid item xs={12} sm={6} className="py-0 py-md-3">
          <FormControl fullWidth className='mt-3' variant="outlined">
            <InputLabel id="demo-simple-select-label">Статус оплаты</InputLabel>
            <Select
              onChange={props.inputChangeHandler}
              name='paymentStatus'
              fullWidth
              required
              labelId="demo-simple-select-label"
              id="paymentStatus"
              value={props.program.paymentStatus}
              label="Статус оплаты"
            >
                <MenuItem value={'Оплачено'}>Оплачено</MenuItem>
                <MenuItem value={'Не оплачено'}>Не оплачено</MenuItem>
            </Select>
          </FormControl>
          </Grid>
        </Grid>
      <div className="py-2 py-md-0">
        <TextField
          onChange={props.inputChangeHandler}
          margin="normal"
          name='title'
          fullWidth
          variant="outlined"
          required
          id="title"
          type='text'
          value={props.program.title}
          label="Название программы"/>
      </div>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={6} className="py-0 py-md-3">
          <FormControl fullWidth className='mt-3' variant="outlined">
            <InputLabel id="demo-simple-select-label">Статус программы</InputLabel>
            <Select
              onChange={props.inputChangeHandler}
              name='status'
              fullWidth
              required
              labelId="demo-simple-select-label"
              label="Статус программы"
              id="status"
              disabled={props.edit ? false : true}
              value={props.program.status}>   
                <MenuItem value={'Подготовлена'}>Подготовлена</MenuItem>
                <MenuItem value={'Действующая'}>Действующая</MenuItem>
                <MenuItem value={'На корректировке'}>На корректировке</MenuItem>
                <MenuItem value={'Завершена'}>Завершена</MenuItem>
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={12} sm={6} className="py-0 py-md-3">
          <TextField
          onChange={props.inputChangeHandler}
          margin="normal"
          name='lifeTime'
          fullWidth
          required
          variant="outlined"
          id="lifeTime"
          type='number'
          value={props.program.lifeTime}
          label="Срок жизни программы в днях"/>
          </Grid>
        </Grid>
      <Grid container spacing={3}>
        <Grid item item xs={12} sm={4} className="py-0 py-md-3">
          <FormControl fullWidth className='mt-3 mt-md-0' variant="outlined">
            <InputLabel id="demo-simple-select-label">Сложность</InputLabel>
            <Select
              onChange={props.inputChangeHandler}
              name='complexity'
              fullWidth
              required
              labelId="demo-simple-select-label"
              label="Сложность"
              id="complexity"
              value={props.program.complexity}>
              <MenuItem value={'Низкая'}>Низкая</MenuItem>
              <MenuItem value={'Средняя'}>Средняя</MenuItem>
              <MenuItem value={'Тяжелая'}>Тяжелая</MenuItem>
            </Select>
          </FormControl>
        </Grid>
        <Grid item item xs={12} sm={4} className="py-0 py-md-3">
          <TextField
            onChange={props.inputChangeHandler}
            className='mb-3 mt-md-0'
            margin="normal"
            name='cost'
            fullWidth
            variant="outlined"
            required
            id="cost"
            type='number'
            value={props.program.cost}
            label="Стоимость"/>
        </Grid>
        <Grid item item xs={12} sm={4} className="py-0 py-md-3">
          <FormControl fullWidth className='mb-3 mb-md-0' variant="outlined">
            <InputLabel id="demo-simple-select-label">Модератор</InputLabel>
            <Select
              onChange={props.inputChangeHandler}
              name='moderator'
              fullWidth
              required
              label="Модератор"
              labelId="demo-simple-select-label"
              value={props.program.moderator}
              id="moderator">
              {
                props.users.map((moderator) => {
                    if (moderator.role === "Модератор"){
                        return <MenuItem value={moderator._id} key={moderator._id}>{moderator.name} {moderator.surname}</MenuItem>
                    }
                })
              }
            </Select>
          </FormControl>
        </Grid>
      </Grid>
      <div>
        <TextField
          onChange={props.inputChangeHandler}
          margin="normal"
          name='goal'
          fullWidth
          required
          variant="outlined"
          id="goal"
          type='text'
          value={props.program.goal}
          label="Цель программы"/>
      </div>
      <div>
        <TextField
          onChange={props.inputChangeHandler}
          margin="normal"
          name='result'
          fullWidth
          variant="outlined"
          id="result"
          type='text'
          value={props.program.result}
          label="Результат"/>
      </div>
    </div>
  )
};

export default IndividualBaseForm;