import React from "react";
import {InputLabel, Select, MenuItem, FormControl } from "@material-ui/core";

const Filter = (props) => {
    return(
        <div className="border my-4 px-1 pb-3 py-3 bg-light">
            <h2>Фильтр</h2>
            <FormControl fullWidth className='mt-3'>
                <InputLabel id="demo-simple-select-label">Кинематическая цепь</InputLabel>
                    <Select
                        onChange={props.inputChangeHandler}
                        name='kinematic_chain'
                        fullWidth
                        labelId="demo-simple-select-label"
                        id="kinematic_chain"
                        defaultValue="">
                            <MenuItem value={undefined}>Не выбрано </MenuItem>    
                            <MenuItem value="ОКЦ">ОКЦ </MenuItem>
                            <MenuItem value="ЗКЦ">ЗКЦ </MenuItem>    
                    </Select>
            </FormControl>
            <FormControl fullWidth className='mt-3'>
                <InputLabel id="demo-simple-select-label">Сложность</InputLabel>
                    <Select
                        onChange={props.inputChangeHandler}
                        name='filterComplexity'
                        fullWidth
                        labelId="demo-simple-select-label"
                        id="filterComplexity"
                        defaultValue="">
                            <MenuItem value={undefined}>Не выбрано </MenuItem>    
                            <MenuItem value="Низкая">Низкая </MenuItem>
                            <MenuItem value="Средняя">Средняя </MenuItem>    
                            <MenuItem value="Тяжелая">Тяжелая</MenuItem>    
                    </Select>
            </FormControl>
            <FormControl fullWidth className='mt-3'>
                <InputLabel id="demo-simple-select-label">Область применения</InputLabel>
                    <Select
                        onChange={props.inputChangeHandler}
                        name='category'
                        fullWidth
                        labelId="demo-simple-select-label"
                        id="category"
                        defaultValue="">
                        <MenuItem value={undefined}>Не выбрано </MenuItem>    
                        {
                            props.categories.map((category) => {
                            return <MenuItem value={category._id} key={category._id}>{category.title} </MenuItem>
                            })
                        }
                    </Select>
            </FormControl>
            <FormControl fullWidth className='mt-3'>
                <InputLabel id="demo-simple-select-label">Мышца</InputLabel>
                    <Select
                        onChange={props.inputChangeHandler}
                        name='muscle'
                        fullWidth
                        labelId="demo-simple-select-label"
                        id="muscle"
                        defaultValue="">
                        <MenuItem value={undefined}>Не выбрано </MenuItem>    
                        {
                            props.muscles.map((item) => {
                            return <MenuItem value={item._id} key={item._id}>{item.title} </MenuItem>
                            })
                        }
                    </Select>
            </FormControl>
            <FormControl fullWidth className='mt-3'>
                <InputLabel id="demo-simple-select-label">Инвентарь</InputLabel>
                    <Select
                        onChange={props.inputChangeHandler}
                        name='equipment'
                        fullWidth
                        labelId="demo-simple-select-label"
                        id="equipment"
                        defaultValue="">
                        <MenuItem value={undefined}>Не выбрано </MenuItem>    

                        {
                            props.equipment.map((item) => {
                            return <MenuItem value={item._id} key={item._id}>{item.title} </MenuItem>
                            })
                        }
                    </Select>
            </FormControl>
            
        </div>
    )
};

export default Filter;