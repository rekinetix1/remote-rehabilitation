const Pay = (props) => {
    const widget = new window.cp.CloudPayments();
    widget.pay('auth', // или 'charge'
        { //options
            publicId: 'test_api_00000000000000000000001', //id из личного кабинета
            description: 'Покупка программы: ' + props.program.title, //назначение
            amount: props.totalCost, //сумма
            currency: 'KZT', //валюта
            invoiceId: props.currentTransaction.invoiceId, //номер заказа  (необязательно)
            accountId: props.user.email, //идентификатор плательщика (необязательно)
            skin: "mini", //дизайн виджета (необязательно)
            email: props.user.email,
            data: {
                myProp: 'myProp value'
            }
        },
        {
            onSuccess: function (options) { // success
                //действие при успешной оплате
                props.onUpdateTransaction(props.currentTransaction._id, {
                    status: "Успешная"
                });
                props.makePayment();

                if (props.individual){
                    props.onUpdateIndividualProgram(props.program._id, {
                        ...props.program,
                        paymentStatus: "Оплачено"
                    });
                } else {
                    props.onPostUserProgram({
                        user: props.user._id,
                        program: props.programId,
                        options: props.optionItems,
                        program_rate: props.totalCost,
                    });
                }
                
            },
            onFail: function (reason, options) { // fail
                //действие при неуспешной оплате
                    props.onUpdateTransaction(props.currentTransaction._id, {
                        status: "Не успешная",
                        reason: reason,
                    });
                    // props.closeModal();
                
                
            },
            onComplete: function (paymentResult, options) { //Вызывается как только виджет получает от api.cloudpayments ответ с результатом транзакции.
                //например вызов вашей аналитики Facebook Pixel
            }
        }
    )
}


export default Pay