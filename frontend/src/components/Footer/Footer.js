import React, {useState} from "react";
import './Footer.css'
import {Container} from "@material-ui/core";
import FacebookIcon from '@material-ui/icons/Facebook';
import IconButton from "@material-ui/core/IconButton";
import InstagramIcon from '@material-ui/icons/Instagram';


const Footer = (props) => {
    return (
        <footer className='Footer'>
            <Container>
                <div className='Footer_info_box'>
                    <div className='Footer_company_info Footer_info_box_item'>
                        <img className={'Footer_logo'} width={'150'} src="https://static.tildacdn.com/tild6433-3335-4739-b062-663232653431/photo.png" alt="logo"/>
                        <div>Центр лечения боли и восстановления движения</div>
                        <div>
                            <div className={'Footer_info_item'}> <b> Адрес: </b> 050026 Республика Казахстан,
                                г. Алматы, Алмалинский район, ул.Жарокова 20
                            </div>
                            <div className={'Footer_info_item'} > <b> Режим работы: </b> пн-сб: 08:00 — 20:00</div>
                            <div>ТОО "Новейшие технологии реабилитации"</div>
                        </div>
                        <div>БИН 160 440 016 264</div>
                    </div>
                    <div className='Footer_company_contacts Footer_info_box_item'>
                        <div className='Footer_contacts_title'><b> Контакты:</b></div>
                        <a href="tel:+77273449107">+7 727 344 91 07</a>
                        <a href="tel:+77071494752">+7 707 149 47 52</a>
                        <a href="tel:+77077494752">+7 707 749 47 52</a>
                        <a href="tel:+77470523583">+7 747 052 35 83</a>
                        <a href="mailto:rekinetix.kz@gmail.com">
                            rekinetix.kz@gmail.com</a>
                    </div>
                    <div className='Footer_company_social Footer_info_box_item'>
                        <b> Мы в социальных сетях: </b>
                        <div className='Footer_social_icons'>
                            <IconButton href={'https://www.facebook.com/rekinetix/'} target="_blank"><FacebookIcon
                                className='Footer_social_icon'/></IconButton>
                            <IconButton href={'https://www.instagram.com/rekinetix/'} target="_blank"><InstagramIcon
                                className='Footer_social_icon'/></IconButton>

                        </div>
                    </div>
                </div>
            </Container>
            <div className='Footer_copyright'>made by developers attractor school</div>
        </footer>
    )
};

export default Footer;