import React, {useState} from "react";
import {makeStyles} from '@material-ui/core/styles';
import Table from "@material-ui/core/Table";
import TableContainer from "@material-ui/core/TableContainer";
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TablePagination from '@material-ui/core/TablePagination';
import Paper from '@material-ui/core/Paper';
import StyledTableRow from "../StyledTableRow";
import TableSortLabel from '@material-ui/core/TableSortLabel';
import IconsTable from "../UI/IconsTable/IconsTable"
import Moment from 'react-moment';
import StyledTableCell from "../StyledTableCell/StyledTableCell"

import './AdminTable.scss';

const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
  cell: {
    width:'20%'
  },
});

const AdminTable = (props) => {
  const classes = useStyles();

  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [page, setPage] = useState(0);
  const [orderBy, setOrderBy] = useState();
  const [order, setOrder] = useState('asc');
  const [numeric, setNumeric] = useState(false);

  const handleChangePage = (_event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const createSortHandler = (property, column_numeric) => (event) => {
    column_numeric ? setNumeric(true) : setNumeric(false)
    handleRequestSort(event, property);
  };

  const handleRequestSort = (_event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const stableSort = (array, comparator) => {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
      const order = comparator(a[0], b[0]);
      if (order !== 0) return order;
      return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
  }

  const descendingComparator = (a, b, orderBy) => {
    if (orderBy) {
      if (numeric) {
        return b[orderBy] - a[orderBy]
      } else {
        if (b[orderBy].toLowerCase() < a[orderBy].toLowerCase()) {
          return -1;
        }
        if (b[orderBy].toLowerCase() > a[orderBy].toLowerCase()) {
          return 1;
        }
        return 0;
      }
    }
  }

  const getComparator = (order, orderBy) => {
    return order === 'desc'
      ? (a, b) => descendingComparator(a, b, orderBy)
      : (a, b) => -descendingComparator(a, b, orderBy);
  }

  return (
    <div className='w-100'>
      <TableContainer component={Paper} style={{marginTop: "20px"}}>
        <Table className={classes.table} aria-label="customized table">
          <TableHead>
            <TableRow>
              {
                props.columns.map(column => {
                  return <StyledTableCell key={column.id} align="left" color="secondary">
                          {
                            column.id === 'actions' ? column.label :
                            <TableSortLabel
                              active={orderBy === column.id}
                              direction={orderBy === column.id ? order : 'asc'}
                              onClick={createSortHandler(column.id, column.numeric)}
                            >
                              {column.label}
                            </TableSortLabel>
                          }
                        </StyledTableCell>
                })
              }
          </TableRow>
        </TableHead>
          <TableBody>
            {
              stableSort(props.rows, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => (
                  <StyledTableRow key={index}>
                    {
                      props.rowsItem.map((item) => {
                        if (item.title === 'datetime') {
                          return <StyledTableCell align="left" style={{width: item.width}} key={item.id}>
                            <Moment format="D MMM YYYY HH:MM" withTitle>{row[item.title]}</Moment>{}</StyledTableCell>
                        } else {
                          return <StyledTableCell align="left" style={{width: item.width}} key={item.id}>{item.title === 'cost' ? row[item.title].toLocaleString() : row[item.title]}</StyledTableCell>
                        }
                      })
                    }
                    {
                      props.hideIcons ?
                        null :
                          <StyledTableCell align="center" className={classes.cell}>
                            <IconsTable
                              deleteRow={props.deleteRow}
                              row_id={row._id}
                              route={props.route}
                              hideDeleteIcon={props.hideDeleteIcon}
                              hideShowIcon={props.hideShowIcon}
                              hideEditIcon={props.hideEditIcon}
                            />
                          </StyledTableCell>
                    }
                  </StyledTableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={props.rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </div>
  )
};

export default AdminTable;
