import React, {useState} from "react";
import {makeStyles} from '@material-ui/core/styles';
import Table from "@material-ui/core/Table";
import TableContainer from "@material-ui/core/TableContainer";
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TablePagination from '@material-ui/core/TablePagination';
import Paper from '@material-ui/core/Paper';
import StyledTableRow from "../StyledTableRow";
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Moment from 'react-moment';
import StyledTableCell from "../StyledTableCell/StyledTableCell"
import {NavLink} from "react-router-dom";
import Button from "@material-ui/core/Button";
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import VisibilityOutlinedIcon from '@material-ui/icons/VisibilityOutlined';
import TextField from "@material-ui/core/TextField";
import axios from "../../axios-api";
import TableCell from "@material-ui/core/TableCell";
import {loadFromLocalStorage} from "../../redux/localStorage";

const useStyles = makeStyles({
    table: {
        minWidth: '100%',
    },
    cell: {
        width: '20%',

    },
});

const CommentsTable = (props) => {
    const classes = useStyles();

    const [rowsPerPage, setRowsPerPage] = useState(5);
    const [page, setPage] = useState(0);
    const [orderBy, setOrderBy] = useState();
    const [order, setOrder] = useState('asc');
    const [numeric, setNumeric] = useState(false);

    const handleChangePage = (_event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const createSortHandler = (property, column_numeric) => (event) => {
        column_numeric ? setNumeric(true) : setNumeric(false)
        handleRequestSort(event, property);
    };

    const handleRequestSort = (_event, property) => {
        const isAsc = orderBy === property && order === 'asc';
        setOrder(isAsc ? 'desc' : 'asc');
        setOrderBy(property);
    };

    const stableSort = (array, comparator) => {
        const stabilizedThis = array.map((el, index) => [el, index]);
        stabilizedThis.sort((a, b) => {
            const order = comparator(a[0], b[0]);
            if (order !== 0) return order;
            return a[1] - b[1];
        });
        return stabilizedThis.map((el) => el[0]);
    }

    const descendingComparator = (a, b, orderBy) => {
        if (orderBy) {
            if (numeric) {
                return b[orderBy] - a[orderBy]
            } else {
                if (b[orderBy].toLowerCase() < a[orderBy].toLowerCase()) {
                    return -1;
                }
                if (b[orderBy].toLowerCase() > a[orderBy].toLowerCase()) {
                    return 1;
                }
                return 0;
            }
        }
    }

    const getComparator = (order, orderBy) => {
        return order === 'desc'
            ? (a, b) => descendingComparator(a, b, orderBy)
            : (a, b) => -descendingComparator(a, b, orderBy);
    }

    const [commentField, setCommentField] = useState('')
    const [replyComment, setReplyComment] = useState('')
    const [replyProgram, setReplyProgram] = useState('')

    const setReply = (parent, program) => {
        setReplyComment(parent)
        setReplyProgram(program)
    }
    const addReplyComment = () => {
        props.addReplyComment({
            message: commentField,
            program: replyProgram,
            parent: replyComment,
            individualProgram: replyProgram
        }).then(() => {
                setCommentField('')
            })


    }

    const changeCommentHandler = (e) => {
        setCommentField(e.target.value)
    };


    const replyEyeIconCurrent = (parent, thisId) => {
        setReplyComment(parent)
        setCurrentRow(thisId)
    }

    const [currentRow, setCurrentRow] = useState('')

    return (
        <div className='w-100'>
            <TableContainer component={Paper} style={{marginTop: "20px"}}>
                <Table className={classes.table} aria-label="customized table">
                    <TableHead>
                        <TableRow>
                            {
                                props.columns.map(column => {
                                    return <StyledTableCell key={column.id} align="center" color="secondary">
                                        {
                                            column.id === 'actions' ? column.label :
                                                <TableSortLabel
                                                    active={orderBy === column.id}
                                                    direction={orderBy === column.id ? order : 'asc'}
                                                    onClick={createSortHandler(column.id, column.numeric)}
                                                >
                                                    {column.label}
                                                </TableSortLabel>
                                        }
                                    </StyledTableCell>
                                })
                            }
                            <StyledTableCell align="center" color="secondary"/>
                            <StyledTableCell align="center" color="secondary"/>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            stableSort(props.rows, getComparator(order, orderBy))
                                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                .map((row, index) => {
                                    if (!row.program) {
                                        row.program = row.individualProgram
                                        console.log(props.rows)
                                    }
                                    return (
                                        <>
                                            <StyledTableRow className='vertical-align-top' key={row._id + index}>
                                                <StyledTableCell align="left" style={{width: '20%'}}>
                                                    <div> {row.author.name} {row.author.surname}</div>
                                                    {currentRow === row._id ?
                                                        <>
                                                            <div><b>Почта </b>: {row.author.email} </div>
                                                            <div><b>Пол</b>: {row.author.questionnaire.gender} </div>
                                                            <div><b>Возраст</b>: {row.author.questionnaire.age}</div>
                                                            <div><b>Вес</b>: {row.author.questionnaire.weight}КГ</div>
                                                            <div><b>Рост</b>: {row.author.questionnaire.height}СМ</div>
                                                            <div>
                                                                <b>Диагноз</b>: {row.author.questionnaire.diagnosis ? row.author.questionnaire.diagnosis :
                                                                <span>Нет</span>} </div>
                                                            <div><b>Род
                                                                деятельности</b>: {row.author.questionnaire.occupation}
                                                            </div>
                                                        </>
                                                        : null}
                                                </StyledTableCell>
                                                <StyledTableCell align="center" style={{width: '20%'}}>
                                                    <Moment format="D MMM YYYY HH:MM" withTitle>{row.title}</Moment>{}
                                                </StyledTableCell>
                                                <StyledTableCell align="center"
                                                                 style={{width: '20%'}}><NavLink
                                                    to={row.individualProgram ? `/programs/individual/${row.program._id}`
                                                        : `/programs/${row.program._id}`}>  {row.program.title} {row.individualProgram ?
                                                    <> (И) </> : <> (Б) </>}</NavLink></StyledTableCell>
                                                <StyledTableCell align="center" style={{width: '50%'}}
                                                                 key={row._id}>{row.message.length > 40 && !(currentRow === row._id) ?
                                                    <span> {[...row.message].slice(0, 37)}... </span> : row.message}
                                                    {row.parent ? <div className={'Comment_parent'}>
                                                        В ответ на
                                                        "<b>{row.parent.author.name} {row.parent.author.surname}:</b>
                                                        {row.parent.message}"</div> : null}
                                                </StyledTableCell>
                                                <StyledTableCell align="center" style={{width: '50%'}}
                                                >
                                                    <Button className={'mb-2'} variant='contained'
                                                            href={'#comment'}
                                                            onClick={() => setReply(row._id, row.program._id)}
                                                            color={'primary'}>Ответить
                                                    </Button>
                                                </StyledTableCell>
                                                <StyledTableCell align="center" style={{width: '50%'}}
                                                >
                                                    {!(currentRow === row._id) ?
                                                        <VisibilityOutlinedIcon
                                                            onClick={row.type === 'comment' ? () => setCurrentRow(row._id)
                                                                : () => replyEyeIconCurrent(row.parent, row._id)}/> :
                                                        <VisibilityOffIcon onClick={() => setCurrentRow('')}/>
                                                    }
                                                </StyledTableCell>
                                            </StyledTableRow>
                                            {(replyComment === row._id) ?
                                                <TableRow>
                                                    <TableCell colSpan={4} style={{width: '100%'}}>
                                                        <TextField
                                                            disabled={!replyProgram && !replyComment}
                                                            required
                                                            id="comment"
                                                            label="Напишите ваш комментарий"
                                                            multiline
                                                            rowsMax={4}
                                                            fullWidth
                                                            value={commentField}
                                                            onChange={changeCommentHandler}
                                                            variant="outlined"
                                                            margin={"normal"}
                                                        />
                                                        <Button disabled={!replyProgram && !replyComment}
                                                                variant={"contained"}
                                                                color={'primary'}
                                                                onClick={addReplyComment}>Отправить
                                                            комментарий</Button>
                                                    </TableCell>
                                                </TableRow> : null}
                                        </>)
                                }
                        )}
                    </TableBody>
                </Table>
            </TableContainer>
            <TablePagination
                rowsPerPageOptions={[5, 10, 25]}
                component="div"
                count={props.rows.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
            />
        </div>
    )
};

export default CommentsTable;

