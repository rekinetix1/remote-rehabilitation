import React from "react";
import { Form, FormGroup, Input, Label } from "reactstrap";
import "./OptionsMenu.css";

const OptionalMenu = props => {
    return ( 
        <>
            <h6 className="m-3 menuTitle"><b>Дополнительные возможности:</b></h6>
            <Form className="ml-3" style={{textAlign: "left"}}>
                {
                    props.options.map(option => {
                        return(
                            <FormGroup key={option._id} check className="OptionsMenu">
                                <Label check >
                                    <Input 
                                        type="checkbox" 
                                        id={option._id} 
                                        value={option.cost} 
                                        onClick={props.check}
                                    />
                                    <i className="OptionsMenu_input">{option.title}</i>
                                </Label>
                                <Label check for={`${option._id}`} className="float-right">
                                    <i>{option.cost.toLocaleString()} ₸</i>
                                </Label>
                            </FormGroup>
                        );
                    })
                }
            </Form>
        </>
    );
};

export default OptionalMenu;