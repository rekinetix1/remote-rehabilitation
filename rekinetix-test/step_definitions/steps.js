const {I} = inject();
const execSync = require('child_process').execSync


// Before(() => {
//     execSync("NODE_ENV=test node ../backend/fixtures.js")
// })

Given("я захожу на страницу {string}", (page) => {
    I.amOnPage(page)
});

When('я ввожу {string} в поле {string}', (text, fieldName) => {
    I.fillField({name: fieldName}, text);
});

When('я нажимаю на кнопку {string}', (button) => {
    I.click(button);
});

Then('я нахожусь на странице {string}', (page) => {
    I.seeInCurrentUrl(page);
});

Then('я вижу {string}', (text) => {
    I.see(text)
});

Then('в поле {string}, я вижу текст {string}', (fieldName, text) => {
    I.seeInField(fieldName, text);
})

Then("я выбираю в поле {string} значение {string}", (fieldName, option) => {
    I.selectOption({name: fieldName}, option)
})

Then('я не вижу {string}', text => {
    I.dontSee(text)
})

Then('я нажимаю интер', () => {
    I.pressKey('Enter')
})
Then('я очищаю поле {string}',  async fieldName  => {
    I.doubleClick({name:fieldName})
    I.pressKey("Backspace")
    I.doubleClick({name:fieldName})
    I.pressKey("Backspace")
    I.doubleClick({name:fieldName})
    I.pressKey("Backspace")
    I.doubleClick({name:fieldName})
})




