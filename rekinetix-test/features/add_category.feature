#language: ru


Функция: Добавление категории
  Сценарий: Успешное Добавление категории и валидация
    Допустим я захожу на страницу "/login"
    Если я ввожу "rina.saif221@gmail.com" в поле "email"
    И  я ввожу "123" в поле "password"
    И я нажимаю на кнопку "ВОЙТИ"
    То  я нахожусь на странице "/"
    Затем я захожу на страницу "/exercise-categories"
    И я нажимаю на кнопку "+ Добавить"
    Затем я нажимаю на кнопку "Создать"
    Тогда я вижу "Заполните заголовок области применения упражнений"
    Затем я ввожу "Ко" в поле "title"
    И я нажимаю на кнопку "Создать"
    Тогда я вижу "Минимальная длина заголовка 3 символа"
    Затем я очищаю поле "title"
    И я ввожу "Коленоффф" в поле "title"
    И я нажимаю на кнопку "Создать"
    Тогда я вижу "Такая область применения упражнений уже существует"
    Затем я очищаю поле "title"
    Затем я ввожу "Позвоночник" в поле "title"
    И я нажимаю на кнопку "Создать"
    Тогда я вижу "Успешно создано"
