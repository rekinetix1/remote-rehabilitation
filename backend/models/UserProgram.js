const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const UserProgramSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true
    },
    program: {
        type: Schema.Types.ObjectId,
        ref: "Program",
        required: true
    },
    options: [
        {
            type: Schema.Types.ObjectId,
            ref: "OptionsMenu",
        }
    ],
    program_rate: {
        type: Number,
        required: true
    },
    status: {
        type: String,
        required: true,
        enum: ["Оплачено", "Не оплачено"],
        default: "Оплачено"
    },
    datetime: Date
});

const UserProgram = mongoose.model("UserProgram", UserProgramSchema);
module.exports = UserProgram;