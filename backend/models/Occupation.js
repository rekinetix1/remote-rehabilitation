const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const OccupationSchema = new Schema({
    title: {
        type: String,
        required: [true, 'Заполните заголовок'],
        validate: {
            validator: async (value) => {
                const exerciseCategory = await Occupation.findOne({title: value});
                if (exerciseCategory) return false;
            },
            message: "Такой род деятельности уже существует"
        },
        minlength: [3,'Минимальная длина заголовка 3 символа']
    }
});

const Occupation = mongoose.model("Occupation", OccupationSchema);
module.exports = Occupation; 