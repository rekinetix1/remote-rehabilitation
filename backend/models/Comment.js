const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const CommentSchema = new Schema({
    message: {
        type: String,
        required: [true, 'Введите сообщение комментария'],
    },
    author: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true
    },
    program: {
        type: Schema.Types.ObjectId,
        ref: "Program"
    },
    parent: {
        type:Schema.Types.ObjectId,
        ref: 'Comment'
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    individualProgram: {
        type: Schema.Types.ObjectId,
        ref: "IndividualProgram"
    }
});

const Comment = mongoose.model("Comment", CommentSchema);
module.exports = Comment