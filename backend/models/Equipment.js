const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const EquipmentSchema = new Schema({
    title: {
        type: String,
        required: [true, 'Заполните заголовок инвентаря'],
        validate: {
            validator: async (value) => {
                const exerciseCategory = await Equipment.findOne({title: value});
                if (exerciseCategory) return false;
            },
            message: "Такой инвентарь уже существует"
        },
        minlength: [3,'Минимальная длина заголовка 3 символа']
    }
});

EquipmentSchema.index({title: 'text'});

const Equipment = mongoose.model("Equipment", EquipmentSchema);
module.exports = Equipment;