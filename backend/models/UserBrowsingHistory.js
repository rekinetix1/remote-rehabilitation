const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const UserBrowsingHistorySchema = new Schema ({
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true
    },
    program: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: "Program"
    },
    exercise: {
        type: Number,
        ref: "Exercise"
    },
    datetime: {
        type: Date,
        default: Date.now
    },
    day: {
        type: Number,
        required: true
    },
    type: {
        type: String,
        enum: ['day', 'exercise']
    }
});

const UserBrowsingHistory= mongoose.model("UserBrowsingHistory",UserBrowsingHistorySchema);

module.exports = UserBrowsingHistory;
