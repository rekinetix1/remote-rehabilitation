const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const OptionsMenuSchema = new Schema({
    title: {
        type: String,
        required: [true, 'Заполните заголовок'],
        validate: {
            validator: async (value) => {
                const exerciseCategory = await OptionsMenu.findOne({title: value});
                if (exerciseCategory) return false;
            },
            message: "Такая дополнительная функция уже существует"
        },
        minlength: [3, 'Минимальная длина заголовка 3 символа']
    },
    cost: {
        validate: {
            validator: async (value) => {
                console.log(value)
            },
            message: "Введите число"
        },
        error: async (value) => {
            console.log(value)
        },
        type: Number,
        min: 0,
        required: [true, 'Введите цену'],
    }
});


const OptionsMenu = mongoose.model("OptionsMenu", OptionsMenuSchema);
module.exports = OptionsMenu;