const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const TransactionSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true
    },
    invoiceId: String,
    program: {
        type: Schema.Types.ObjectId,
        ref: "Program",
        default: null
    },
    individualProgram: {
        type: Schema.Types.ObjectId,
        ref: "IndividualProgram",
        default: null
    },
    cost: {
        type: Number,
        required: true
    },
    status: {
        type: String,
        required: true,
        default: "Новая"
    },
    reason: String,
    datetime: Date
});

const Transaction = mongoose.model("Transaction", TransactionSchema);
module.exports = Transaction;