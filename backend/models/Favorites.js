const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const FavoritesSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true
    },
    program: {
        type: Schema.Types.ObjectId,
        ref: "Program",
        required: true
    }
});

const Favorites = mongoose.model("Favorites", FavoritesSchema);
module.exports = Favorites;