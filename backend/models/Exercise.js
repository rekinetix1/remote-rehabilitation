const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ExerciseSchema = new Schema({
    title: {
        type: String,
        required: [true, 'Заполните заголовок упражнения'],
        validate: {
            validator: async (value) => {
                const exerciseCategory = await Exercise.findOne({title: value});
                if (exerciseCategory) return false;
            },
            message: "Такое упражнение уже существует"
        },
        minlength: [3,'Минимальная длина заголовка 3 символа']
    },
    description: {
        type: String,
        required: [true, 'Заполните описание упражнения'],
        minlength: [10,'Минимальная длина описания 10 символов']
    },
    video_link: {
        type: String,
        required: [true, 'Заполните ссылку на видео'],
    },
    image: {
        type: String,
        default: "hands.png"
    },
    kinematic_chain: {
        type: String,
        enum: ["ОКЦ", "ЗКЦ"],
        required: [true, 'Заполните кинематическую цепь'],
    },
    complexity: {
        type: String,
        enum: ["Низкая", "Средняя", "Тяжелая"],
        required: [true, 'Заполните сложность'],
    },
    category: {
        type: Schema.Types.ObjectId,
        ref: "ExerciseCategory",
        required: [true, 'Выберете область применения упражнения'],
    },
    equipment: [
        {
            type: Schema.Types.ObjectId,
            ref: "Equipment",
            default: null
        }
    ],
    muscle: {
        type: Schema.Types.ObjectId,
        ref: "Muscle",
        default: null
    }
});
ExerciseSchema.index({title: 'text'});

const Exercise = mongoose.model("Exercise", ExerciseSchema);
module.exports = Exercise;