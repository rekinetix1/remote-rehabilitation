const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const QuestionnaireSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true
    },
    age: {
        type: Number,
        required: true
    },
    weight: {
        type: Number,
        required: true
    },
    height: {
        type: Number,
        required: true
    },
    gender: {
        type: String,
        required: true
    },
    occupation: {
        type: Schema.Types.ObjectId,
        ref: "Occupation",
        required: true,
    },
    physicalActivity: {
        type: Schema.Types.ObjectId,
        ref: "PhysicalActivity",
        required: true,
    },
    pains:
        [
            {
                type: Schema.Types.ObjectId,
                ref: "Pain",
                required: true
            }
        ],
    exacerbationsCountPerYear: {
        type: Number,
        required: true
    },
    painIntensity: {
        type: Number,
        required: true
    },
    diagnosis: String
});

const Questionnaire = mongoose.model("Questionnaire", QuestionnaireSchema);
module.exports = Questionnaire;
