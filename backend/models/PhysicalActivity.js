const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const PhysicalActivitySchema = new Schema({
    title: {
        type: String,
        required: [true, 'Заполните заголовок'],
        validate: {
            validator: async (value) => {
                const exerciseCategory = await PhysicalActivity.findOne({title: value});
                if (exerciseCategory) return false;
            },
            message: "Такой вид физической активости уже существует"
        },
        minlength: [3,'Минимальная длина заголовка 3 символа']
    }
});

const PhysicalActivity = mongoose.model("PhysicalActivity", PhysicalActivitySchema);
module.exports = PhysicalActivity; 