const mongoose = require("mongoose");
const {nanoid} = require("nanoid");
const Schema = mongoose.Schema;
const bcrypt = require("bcrypt");
const SALT_WORK_FACTOR = 10;

const UserSchema = new Schema ({
    email: {
        type: String,
        required: true,
        unique: true,
        validate: {
            validator: async function(value) {
                if (!this.isModified("email")) return;
                const user = await User.findOne({email: value});
                if (user) return false;
            },
            message: "Почтовый ящик уже используется"
        }
    },

    role: {
        type: String,
        required: true,
        enum: ["Пациент", "Терапевт", "Администратор", "Модератор", "Автор БП", "Менеджер"],
        default: "Пациент"
    },
    password: {
        type: String,
        required: [true, "заполните пароль"],
        minLength: [6, "Минимальная длина пароля 6 символов"]
    },
    status: {
        type: String,
        required: true,
        default: "Активный"
    },
    token: String,
    name: {
        type: String,
        required: [true, "Заполните имя"]
    },
    surname: {
        type: String,
        required: [true, "Заполните фамилию"]
    },
    verify: {
        type: Boolean,
        required: true,
        default: false
    },
    birth_Date: Date,
    country: {
        type: String,
        required: [true, "Заполните страну"]
    },
    city:{
        type: String,
        required: [true, "Заполните город"]
    },
    questionnaire: {
        type: Schema.Types.ObjectId,
        ref: "Questionnaire"
    }
});

UserSchema.pre("save", async function(next) {
    if (!this.isModified("password")) next();

    const salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
    this.password = await bcrypt.hash(this.password, salt);
    next();
});

UserSchema.set("toJSON", {
    transform: (doc, ret) => {
        delete ret.password;
        return ret;
    }
});

UserSchema.methods.checkPassword = function(password) {
    return bcrypt.compare(password, this.password);
};

UserSchema.methods.generateToken = function() {
    this.token = nanoid();
};

const User = mongoose.model("User", UserSchema);
module.exports = User;