const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const MuscleSchema = new Schema({
    title: {
        type: String,
        required: true,
        validate: {
            validator: async (value) => {
                const muscle = await Muscle.findOne({title: value});
                if (muscle) return false;
            },
            message: "Такой вид мышцы уже существует"
        },
        minlength: [3,'Минимальная длина заголовка 3 символа']
    }
});
MuscleSchema.index({title: 'text'});

const Muscle = mongoose.model("Muscle", MuscleSchema);
module.exports = Muscle;