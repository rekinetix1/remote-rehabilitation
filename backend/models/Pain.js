const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const PainSchema = new Schema({
    title: {
        type: String,
        required: true
    }
});

const Pain = mongoose.model("Pain", PainSchema);
module.exports = Pain; 