const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ProgramSchema = new Schema({
    title: {
        type: String,
        required: [true, 'Заполните заголовок программы'],
        minlength: [5,'Минимальная длина заголовка 5 символа']
    },
    short_description: {
        type: String,
        required: [true, 'Заполните описание'],
        minlength: [10,'Минимальная длина описания 10 символов']
    },
    full_description: {
        type: String,
        required: [true, 'Заполните описание'],
        minlength: [10,'Минимальная длина описания 10 символов']
    },
    complexity: {
        type: String,
        required: [true, 'Выберите сложность'],
        enum: ["Низкая", "Средняя", "Тяжелая"],
    },
    author: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true
    },
    moderator: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: [true, 'Выберите модератора'],
    },
    duration: {
        type: Number,
        required: true
    },
    image: {
        type: String
    },
    video: {
        type: String
    },
    category: {
        type: Schema.Types.ObjectId,
        ref: "ProgramCategory",
        required: [true, 'Выберите категорию программы'],
    },
    days: [
        [
            {
                exercise: {
                    type: Schema.Types.ObjectId,
                    ref: "Exercise",
                    required: true
                },
                repetitions: {
                    type: Number,
                    required: true
                },
                approaches: Number,
            },
        ],
    ],
    cost: {
        type: Number,
        required: [true, 'Заполните стоимость'],
    }

});
ProgramSchema.index({title: 'text'});

const Program = mongoose.model("Program", ProgramSchema);
module.exports = Program;