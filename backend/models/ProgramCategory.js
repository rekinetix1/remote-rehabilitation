const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ProgramCategorySchema = new Schema({
    title: {
        type: String,
        required: [true, 'Заполните заголовок категории'],
        validate: {
            validator: async (value) => {
                const exerciseCategory = await ProgramCategory.findOne({title: value});
                if (exerciseCategory) return z
            },
            message: "Такая категория уже существует"
        },
        minlength: [3,'Минимальная длина заголовка 3 символа']
    }
});
ProgramCategorySchema.index({title: 'text'});

const ProgramCategory = mongoose.model("ProgramCategory", ProgramCategorySchema);
module.exports = ProgramCategory;