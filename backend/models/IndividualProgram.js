const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const IndividualProgramSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true
    },
    title: {
        type: String,
        required: true
    },
    status: {
        type: String,
        required: true,
        default: "Подготовлена",
        enum: ["Подготовлена", "Действующая", "На корректировке", "Завершена"]
    },
    lifeTime: {
        type: Number,
        required: true
    },
    complexity: {
        type: String,
        required: true,
        enum: ["Низкая", "Средняя", "Тяжелая"]
    },
    moderator: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true
    },
    goal: {
        type: String,
        required: true
    },
    result: String,
    exercises: [
        {
            exercise: {
                type: Schema.Types.ObjectId,
                ref: "Exercise",
                required: true
            },
            repetitions: {
                type: Number,
                required: true
            },
            side: {
                type: String,
                enum: ["Правая", "Левая"]
            },
            reduction_type: {
                type: String,
                enum: ["Изометрическое", "Концентрическое"]
            },
            workouts: Number,
            approaches: Number,
        }
    ],
    cost: {
        type: Number,
        required: true
    },
    exercises_count: {
        type: Number
    },
    paymentStatus: {
        type: String,
        required: true,
        enum: ["Оплачено", "Не оплачено"]
    }
});

const IndividualProgram = mongoose.model("IndividualProgram", IndividualProgramSchema);
module.exports = IndividualProgram;