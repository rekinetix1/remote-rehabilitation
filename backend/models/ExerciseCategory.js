const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ExerciseCategorySchema = new Schema({
    title: {
        type: String,
        required: [true, 'Заполните заголовок области применения упражнений'],
        validate: {
            validator: async (value) => {
                const exerciseCategory = await ExerciseCategory.findOne({title: value});
                if (exerciseCategory) return false;
            },
            message: "Такая область применения упражнений уже существует"
        },
        minlength: [3,'Минимальная длина заголовка 3 символа']
    }
});


const ExerciseCategory = mongoose.model("ExerciseCategory", ExerciseCategorySchema);
module.exports = ExerciseCategory;