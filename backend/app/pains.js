const express = require("express");
const Pain = require("../models/Pain");
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const createRouter = () => {
    const router = express.Router();

    router.get("/", async (req, res) => {
        try {
            res.send(await Pain.find());
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.get("/:id", async (req, res) => {
        try {
            res.send(await Pain.findById(req.params.id));
        } catch (e) {
            res.status(400).send(e);
        }
     });

    router.post("/", [auth, permit("Администратор")], async (req, res) => {
        const user = new Pain(req.body);
        try {
            await user.save();
            res.send(user)
        } catch (e) {
            res.status(400).send(e)
        }
    });

    router.put("/:id", [auth, permit("Администратор")], async (req, res) => {
        try {
            await Pain.findByIdAndUpdate(req.params.id, req.body);
            res.send(await Pain.findById(req.params.id));
        } catch(e) {
            res.status(400).send(e);
        }
    });

    router.delete("/:id", [auth, permit("Администратор")], async (req, res) => {
        try {
            res.send(await Pain.findByIdAndRemove(req.params.id));
        } catch(e) {res.sendStatus(500).send(e)}
    });

    return router;
}

module.exports = createRouter;