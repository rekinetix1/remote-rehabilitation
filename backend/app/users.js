const express = require("express");
const User = require("../models/User");
const nodemailer = require('nodemailer');
const config = require('../config');
const bcrypt = require("bcrypt");
const SALT_WORK_FACTOR = 10;
let domen = process.env.NODE_ENV === "production" ? "https://rekinetix.sytes.net" : "http://localhost:3000";

const createRouter = () => {
    const router = express.Router();

    router.get("/", async (req, res) => {
        if (req.query.search) {
            let options = req.query.search.split(" ")
            try {
                let scope_users = [];
                let users_first_search = [];

                for (let i = 0; i < options.length; i++) {
                    // TODO отрефакторить поиск
                    if (i === 0) {
                        let by_email = await User.find({email: {'$regex' : options[i], '$options' : 'i'}})
                        let by_role = await User.find({role: {'$regex' : options[i], '$options' : 'i'}})
                        let by_name = await User.find({name: {'$regex' : options[i], '$options' : 'i'}})
                        let by_surname = await User.find({surname: {'$regex' : options[i], '$options' : 'i'}})
                        if (options.length === 1) {
                            scope_users.push(...by_email)
                            scope_users.push(...by_role)
                            scope_users.push(...by_name)
                            scope_users.push(...by_surname)
                        } else {
                            users_first_search.push(...by_email)
                            users_first_search.push(...by_role)
                            users_first_search.push(...by_name)
                            users_first_search.push(...by_surname)
                        }
                    } else {
                        let users = users_first_search.map(user => {
                            if (user.name === options[i] || user.surname === options[i] || user.email === options[i] || user.role === options[i]) {
                                return user
                            }
                        })
                        scope_users.push(...users)
                    }
                };

                let users = scope_users.filter((user, index, array) => {
                    return array.map((mapItem) => mapItem['email']).indexOf(user['email']) === index
                })
                res.send(users);
            } catch (e) {
                res.status(500).send(e);
            }
        } else {
            let role;
            if (req.query.role) {
                role = {role: req.query.role};
            }
            try {
                let users = await User.find(role)
                res.send(users);
            } catch (e) {
                res.status(500).send(e);
            }
        }
    });

    router.get("/verify/:id", async (req, res) => {
        try {
            const user = await User.findById(req.params.id);
            if (req.params.id === String(user._id)) {
                try {
                    await User.findByIdAndUpdate(req.params.id, {verify: true});
                    return res.send(await User.findById(req.params.id));
                } catch (e) {
                    return res.status(400).send(e);
                }
            }
            return res.send(user);
        } catch (e) {
            res.sendStatus(404)
        }
    });

    router.get("/send_verify/:id", async (req, res) => {
        let transporterSmtp = nodemailer.createTransport(config.smtpOptions)
        try {
            const user = await User.findById(req.params.id);
            if (user) {
                try {
                    await transporterSmtp.sendMail({
                        from: 'support@rekinetix.kz',
                        to: user.email,
                        subject: 'Верификация аккаунта',
                        text: `Приветствуем вас. Для верификации пройдите по ссылке ${domen}/users/verify/${req.params.id}`
                    })
                } catch (e) {
                    return res.status(400).send(e);
                }
            }
            return res.send(user);
        } catch (e) {
            res.sendStatus(404)
        }
    });

    router.get("/:id", async (req, res) => {
        try {
            const user = await User.findById(req.params.id);
            res.send(user);
        } catch (e) {
            res.sendStatus(404)
        }
    });

    router.get("/check_user/:email", async (req, res) => {
        let transporterSmtp = nodemailer.createTransport(config.smtpOptions)
        let haveUser = await User.find({email: req.params.email})

        if (haveUser.length !== 0) {
            try {
                await transporterSmtp.sendMail({
                    from: 'support@rekinetix.kz',
                    to: haveUser[0].email,
                    subject: 'Восстановление пароля',
                    text: `Здравствуйте вас приветствует REKINETIX.` +
                        `Чтобы восстановить пароль перейдите по ссылке. ${domen}/changePassword/${haveUser[0]._id}`
                })
                res.send(haveUser);
            } catch (e) {
                res.status(500).send(e);
            }
        } else {
            res.status(404).send({message: "пользователь не найден"});
        }

    });


    router.post("/", async (req, res) => {
        const user = new User(req.body);
        let transporterSmtp = nodemailer.createTransport(config.smtpOptions)

        try {
            user.generateToken();
            await user.save();
            res.send(user)
        } catch (e) {
            res.status(400).send(e)
        } finally {
            if (user._id) {
                // await transporterSmtp.sendMail({
                //     from: 'support@rekinetix.kz',
                //     to: req.body.email,
                //     subject: 'Добро пожаловать',
                //     text: "Вас приветствует REKINETIX. Добро пожаловать, вы успешно зарегистрировались."
                // })
                await transporterSmtp.sendMail({
                    from: 'support@rekinetix.kz',
                    to: req.body.email,
                    subject: 'Верификация аккаунта',
                    text: `Вас приветствует REKINETIX. Добро пожаловать. Для верификации пройдите по ссылке ${domen}/users/verify/${user._id}`
                })
            }
        }

    });

    router.put("/:id", async (req, res) => {
        try {
            await User.findByIdAndUpdate(req.params.id, req.body);
            res.send(await User.findById(req.params.id));
        } catch (e) {
            res.status(400).send(e);
        }
    });

    router.put("/reset_password/:id", async (req, res) => {
        if (req.body.oldPassword) {
            const user = await User.findOne({_id: req.params.id});
            const isMatch = await user.checkPassword(req.body.oldPassword);
            if (!isMatch) {
                return res.status(400).send({message: "Не верный старый пароль"});
            }
            if (req.body.password === req.body.confirmPassword) {
                const salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
                req.body.password = await bcrypt.hash(req.body.password, salt);
                try {
                    let resetPassword = await User.findByIdAndUpdate(req.params.id, {password: req.body.password})
                    resetPassword.generateToken()
                    res.send(resetPassword);
                } catch (e) {
                    res.status(400).send(e);
                }
            } else {
                res.status(404).send({message: "пароли не совпадают"})
            }
        } else {
            if (req.body.password === req.body.confirmPassword) {
                const salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
                req.body.password = await bcrypt.hash(req.body.password, salt);
                try {
                    let resetPassword = await User.findByIdAndUpdate(req.params.id, {password: req.body.password})
                    resetPassword.generateToken()
                    res.send(resetPassword);
                } catch (e) {
                    res.status(400).send(e);
                }
            } else {
                res.status(404).send({message: "пароли не совпадают"})
            }
        }
    });


    router.post("/sessions", async (req, res) => {
        const user = await User.findOne({email: req.body.email});
        if (!user) {
            return res.status(400).send({
                errors: {
                    email: "Почтовый ящик не найден"
                }
            });
        }

        const isMatch = await user.checkPassword(req.body.password);

        if (!isMatch) {
            return res.status(400).send({errors: {password: "Неверный пароль"}});
        }
        user.generateToken();

        await user.save()
        res.send(user);
    });

    router.delete("/sessions", async (req, res) => {
        const token = req.get("Token");
        const success = {message: "Logout success"};
        if (!token) return res.send(success);
        const user = await User.findOne({token});
        if (!user) return res.send(success);
        user.generateToken();
        try {
            await user.save();
        } catch (e) {
            res.status(500).send({message: "Logout failure"});
        }
        res.send(success);
    });

    router.delete("/:id", async (req, res) => {
        try {
            res.send(await User.findByIdAndRemove(req.params.id));
        } catch (e) {
            res.sendStatus(500).send(e)
        }
    });

    return router;
}

module.exports = createRouter;