const express = require("express");
const multer = require("multer");
const path = require("path");
const config = require("../config");
const {nanoid} = require("nanoid");
const Exercise = require("../models/Exercise");
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = () => {
    const router = express.Router();

    router.get("/", async (req, res) => {
        let category;
        if (req.query.category) {
            category = {category: req.query.category};
        }
        let search;
        if (req.query.search) {
            search = {$text: {$search: req.query.search}};
        }
        try {
            res.send(await Exercise.find(category).find(search).populate("category").populate("equipment").populate("muscle"));
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.get("/:id", async (req, res) => {
        try {
            const program = await (await Exercise.findById(req.params.id)
                .populate("category").populate("equipment"));
            res.send(program);
        } catch(e) {res.sendStatus(404)} 
    });

    router.post("/", [auth, permit("Администратор"), upload.single("image")], async (req, res) => {
        const exercise_data = req.body;
        exercise_data.equipment = JSON.parse(req.body.equipment)
        const exercise = new Exercise(exercise_data);

        if (req.file) {
            exercise.image = req.file.filename;
        }

        try {
            await exercise.save();
            res.send(exercise)
        } catch (e) {
            res.status(400).send(e)
        }
    });

    router.put("/:id", [auth, permit("Администратор")], async (req, res) => {
        const exercise_data = req.body;
        try {
            await Exercise.findByIdAndUpdate(req.params.id, exercise_data);
            res.send(await Exercise.findById(req.params.id));
        } catch(e) {
            res.status(400).send(e);
        }
    });

    router.delete("/:id", [auth, permit("Администратор")], async (req, res) => {
        try {
            res.send(await Exercise.findByIdAndRemove(req.params.id));
        } catch(e) {res.sendStatus(500).send(e)}
    });

    return router;
}

module.exports = createRouter;
