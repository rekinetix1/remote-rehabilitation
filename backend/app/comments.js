const express = require("express");
const Comment = require("../models/Comment");
const User = require("../models/User");
const auth = require('../middleware/auth');

const createRouter = () => {
    const router = express.Router();
    router.get("/", async (req, res) => {
        let program;
        if (req.query.program) {
            program = {program: req.query.program};
        }
        if (req.query.individualProgram) {
            program = {individualProgram: req.query.individualProgram};
        }
        try {
            res.send(await Comment.find({...program})
                .populate({
                    path: 'author', populate: {path: 'questionnaire'}
                })
                .populate('program').populate('individualProgram')
                .populate({path: 'parent', populate: {path: 'author'}})
                .populate({
                    path: 'parent', populate: {
                        path: 'reply', populate: {
                            path: 'comment', populate: {
                                path: 'author', populate: {path: 'questionnaire'}
                            }
                        }
                    }
                })
            )

        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.get('/user_answers', auth, async (req, res) => {
        try {
            let userComments = await Comment.find({author: req.user._id})
            let userCommentsIds = userComments.map(comment => String(comment._id))
            let allComments = await Comment.find().populate({
                path: 'parent',
                populate: {path: 'author'}
            }).populate('author')
            let sendComments = allComments.filter(comment => comment.parent && String(comment.author._id) !== String(req.user._id) && userCommentsIds.includes(String(comment.parent._id)))
            res.send(sendComments)
        } catch (e) {
            res.status(500).send(e);

        }
    })


    router.post("/", auth, async (req, res) => {
        req.body.author = req.user._id
        const comment = new Comment(req.body);
        try {
            await comment.save();
            res.send(comment)
        } catch (e) {
            res.status(400).send(e)
        }
    });

    router.delete("/:id", auth, async (req, res) => {
        try {
            const comment = await Comment.findById(req.params.id);
            const user = await User.findById(req.user._id)
            if (!comment) return res.sendStatus(404)
            if (comment.author.toString() !== user._id.toString() && user.role === 'Пациент') return res.sendStatus(403)
            await comment.remove();
            return res.send(comment)
        } catch (e) {
            return res.sendStatus(500)
        }
    });

    router.put("/:id", [auth], async (req, res) => {
        try {
            console.log(req.body)
            const comment = await Comment.findById(req.params.id);
            const user = await User.findById(req.user._id)
            console.log(comment)
            console.log(user)
            if (comment.author.toString() !== user._id.toString()) return res.sendStatus(403)
            await Comment.findByIdAndUpdate(req.params.id, req.body);
            res.send(await Comment.findById(req.params.id));
        } catch (e) {
            res.status(400).send(e);
        }
    });
    return router;
}

module.exports = createRouter;