const express = require("express");
const OptionsMenu = require("../models/OptionsMenu");
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const createRouter = () => {
    const router = express.Router();

    router.get("/", async (req, res) => {
        try {
            res.send(await OptionsMenu.find());
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.get("/:id", async (req, res) => {
        try {
            res.send(await OptionsMenu.findById(req.params.id));
        } catch (e) {
            res.status(400).send(e);
        }
     });

    router.post("/", [auth, permit("Администратор")], async (req, res) => {
        const option = new OptionsMenu(req.body);
        try {
            await option.save();
            res.send(option)
        } catch (e) {
            res.status(400).send(e)
        }
    });

    router.put("/:id", [auth, permit("Администратор")], async (req, res) => {
        try {
            await OptionsMenu.findByIdAndUpdate(req.params.id, req.body);
            res.send(await OptionsMenu.findById(req.params.id));
        } catch(e) {
            res.status(400).send(e);
        }
    });

    router.delete("/:id", [auth, permit("Администратор")], async (req, res) => {
        try {
            res.send(await OptionsMenu.findByIdAndRemove(req.params.id));
        } catch(e) {res.sendStatus(500).send(e)}
    });

    return router;
}

module.exports = createRouter;
