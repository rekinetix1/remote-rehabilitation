const express = require("express");
const multer = require("multer");
const path = require("path");
const config = require("../config");
const {nanoid} = require("nanoid");
const Program = require("../models/Program");
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = () => {
    const router = express.Router();

    router.get("/", async (req, res) => {
        let category;
        if (req.query.category) {
            category = {category: req.query.category};
        }
        let search;
        if (req.query.search) {
            search = {$text: {$search: req.query.search}};
        }
        try {
            res.send(await Program.find(category).find(search).populate("category").populate({path: 'days.exercise'}));
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.get("/:id", async (req, res) => {
        try {
            const program = await Program.findById(req.params.id).populate("moderator", "name");
            res.send(program);
        } catch(e) {res.sendStatus(404)}
    });

    router.post("/", [auth, permit("Администратор", "Автор БП"), upload.single("image")], async (req, res) => {
        const programData = req.body;
        programData.days = JSON.parse(programData.days);
        const program = new Program(programData);

        if (req.file) {
            program.image = req.file.filename;
        }

        try {
            await program.save();
            res.send(program)
        } catch (e) {
            res.status(400).send(e)
        }
    });

    router.put("/:id", [auth, permit("Администратор", "Автор БП"), upload.single("image")], async (req, res) => {
        const program = req.body;
        program.days = JSON.parse(program.days);
        if (req.file) {
            program.image = req.file.filename;
        }
        try {
            await Program.findByIdAndUpdate(req.params.id, program);
            res.send(await Program.findById(req.params.id));
        } catch(e) {
            res.status(400).send(e);
        }
    });

    router.delete("/:id", [auth, permit("Администратор", "Автор БП")], async (req, res) => {
        try {
            res.send(await Program.findByIdAndRemove(req.params.id));
        } catch(e) {res.sendStatus(500).send(e)}
    });

    return router;
}

module.exports = createRouter;
