const express = require("express");
const Occupation = require("../models/Occupation");
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const createRouter = () => {
    const router = express.Router();

    router.get("/", async (req, res) => {
        try {
            res.send(await Occupation.find());
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.get("/:id", async (req, res) => {
        try {
            res.send(await Occupation.findById(req.params.id));
        } catch (e) {
            res.status(400).send(e);
        }
     });

    router.post("/", [auth, permit("Администратор")], async (req, res) => {
        const user = new Occupation(req.body);
        try {
            await user.save();
            res.send(user)
        } catch (e) {
            res.status(400).send(e)
        }
    });

    router.put("/:id", [auth, permit("Администратор")], async (req, res) => {
        try {
            await Occupation.findByIdAndUpdate(req.params.id, req.body);
            res.send(await Occupation.findById(req.params.id));
        } catch(e) {
            res.status(400).send(e);
        }
    });

    router.delete("/:id", [auth, permit("Администратор")], async (req, res) => {
        try {
            res.send(await Occupation.findByIdAndRemove(req.params.id));
        } catch(e) {res.sendStatus(500).send(e)}
    });

    return router;
}

module.exports = createRouter;