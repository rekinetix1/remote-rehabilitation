const express = require("express");
const UserBrowsingHistory = require("../models/UserBrowsingHistory");
const User = require("../models/User");
const auth = require("../middleware/auth");

const createRouter = () => {
    const router = express.Router();
    router.post("/", auth, async (req, res) => {
        try {
            const userBrowsingHistory = new UserBrowsingHistory(req.body);
            userBrowsingHistory.user = req.user._id;
            res.send(await userBrowsingHistory.save());
        } catch (e) {
            res.status(400).send(e)
        }
    });

    router.get("/", async (req, res) => {
        let user;

        if (req.query.user) {
            user = {user: req.query.user};
        }

        let search;
        if (req.query.search) {
            let user_by_email = await User.find({email: {'$regex' : req.query.search, '$options' : 'i'}})
            if (user_by_email[0]) {
                search = {user: user_by_email[0]._id};
            }
        }
        try {
            let history;
            if (req.query.search && !search) {
                history = [];
            } else {
                history = await UserBrowsingHistory.find(user).find(search).populate("user").populate("program", "title");
            }
            res.send(history);
        } catch (e) {
            res.status(500).send(e);
        }
    });
    return router;
};

module.exports = createRouter;
