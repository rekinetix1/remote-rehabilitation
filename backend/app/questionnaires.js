const express = require("express");
const Questionnaire = require("../models/Questionnaire");
const auth = require('../middleware/auth');

const createRouter = () => {
    const router = express.Router();

    router.get("/", async (req, res) => {
        let user;
        if (req.query.user) {
            user = {user: req.query.user};
        }

        try {
            res.send(await Questionnaire.find(user));
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.get("/:id", async (req, res) => {
        console.log(req.params.id)
        try {
            res.send(await Questionnaire.findOne({user: req.params.id}));
        } catch (e) {
            res.status(400).send(e);
        }
    });

    router.post("/", auth, async (req, res) => {
        req.body.user = req.user._id
        const searchQuestionnaire= await Questionnaire.find({user:req.user._id})
        console.log(searchQuestionnaire.length)
        if (searchQuestionnaire.length !== 0){
            res.status(400).send({message:"Такая анкета уже существует"})
        }
        const questionnaire = new Questionnaire(req.body);
        console.log(questionnaire)
        try {
            await questionnaire.save();
            console.log("cool")
            res.send(questionnaire)
        } catch (e) {
            console.log("error")
            res.status(400).send({message:"error"})
        }

    });

    router.put("/:id", auth, async (req, res) => {
        console.log(req.body);
        try {
            await Questionnaire.findByIdAndUpdate(req.params.id, req.body);
            res.send(await Questionnaire.findById(req.params.id));
        } catch (e) {
            res.status(400).send(e);
        }
    });

    router.delete("/:id", async (req, res) => {
        try {
            res.send(await Questionnaire.findByIdAndRemove(req.params.id));
        } catch (e) {
            res.sendStatus(500).send(e)
        }
    });

    return router;
}

module.exports = createRouter;