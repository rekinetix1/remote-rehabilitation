const express = require("express");
const ExerciseCategory = require("../models/ExerciseCategory");
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const createRouter = () => {
    const router = express.Router();

    router.get("/", async (req, res) => {
        try {
            res.send(await ExerciseCategory.find());
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.get("/:id", async (req, res) => {
        try {
            res.send(await ExerciseCategory.findById(req.params.id));
        } catch (e) {
            res.status(400).send(e);
        }
    });

    router.post("/", [auth, permit("Администратор")], async (req, res) => {
        const category = new ExerciseCategory(req.body);
        try {
            await category.save();
            res.send(category)
        } catch (e) {
            res.status(400).send(e)
        }
    });

    router.put("/:id", [auth, permit("Администратор")], async (req, res) => {
            try {
                let categoryTitle
                await ExerciseCategory.findOne({_id: req.params.id}).then(r => {
                    categoryTitle = r.title
                })
                await ExerciseCategory.findByIdAndUpdate(req.params.id, req.body, {runValidators: !(req.body.title === categoryTitle)})
                res.send(await ExerciseCategory.findById(req.params.id));
            } catch
                (e) {
                res.status(400).send(e);
            }
        }
    )
    ;

    router.delete("/:id", [auth, permit("Администратор")], async (req, res) => {
        try {
            res.send(await ExerciseCategory.findByIdAndRemove(req.params.id));
        } catch (e) {
            res.sendStatus(500).send(e)
        }
    });

    return router;
}

module.exports = createRouter;
