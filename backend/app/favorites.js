const express = require("express");
const Favorites = require("../models/Favorites");
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const createRouter = () => {
    const router = express.Router();

    router.get("/", async (req, res) => {
        let user;
        if (req.query.user) {
            user = {user: req.query.user};
        }

        try {
            res.send(await Favorites.find(user).populate("program"));
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.get("/:id", async (req, res) => {
        try {
            res.send(await Favorites.findById(req.params.id));
        } catch (e) {
            res.status(400).send(e);
        }
     });

    router.post("/", [auth, permit("Пациент")], async (req, res) => {
        const favorite = new Favorites(req.body);
        try {
            await favorite.save();
            res.send(favorite)
        } catch (e) {
            res.status(400).send(e)
        }
    });

    router.put("/:id", [auth, permit("Пациент")], async (req, res) => {
        try {
            await Favorites.findByIdAndUpdate(req.params.id, req.body);
            res.send(await Favorites.findById(req.params.id));
        } catch(e) {
            res.status(400).send(e);
        }
    });

    router.delete("/:id", [auth, permit("Пациент")], async (req, res) => {
        try {
            res.send(await Favorites.findByIdAndRemove(req.params.id));
        } catch(e) {res.sendStatus(500).send(e)}
    });

    return router;
}

module.exports = createRouter;