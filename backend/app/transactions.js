const express = require("express");
const Transaction = require("../models/Transaction");
const nodemailer = require('nodemailer');
const User = require("../models/User");
const config = require('../config')
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const createRouter = () => {
    const router = express.Router();

    router.get("/", async (req, res) => {
        let user;
        if (req.query.user) {
            user = {user: req.query.user};
        }

        let search;
        if (req.query.search) {
            let user_by_email = await User.find({email: {'$regex' : req.query.search, '$options' : 'i'}})
            if (user_by_email[0]) {
                search = {user: user_by_email[0]._id};
            }
        }

        try {
            let transactions;
            if (req.query.search && !search) {
                history = [];
            } else {
                transactions = await Transaction.find(user).find(search).populate("user", "email").populate("program").populate("individualProgram", "title").sort({datetime: -1})
            }
            res.send(transactions);
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.get("/:id", async (req, res) => {
        try {
            res.send(await Transaction.findById(req.params.id).populate("user", "email").populate("program", "title").populate("individualProgram", "title"));
        } catch (e) {
            res.status(400).send(e);
        }
     });

    router.post("/", [auth, permit("Пациент")], async (req, res) => {
        let invoiceId = 1;
        const transactions = await Transaction.find();
        if (transactions.length > 0) invoiceId = parseInt(transactions[transactions.length - 1].invoiceId) + 1;
        const TransactionDate = req.body;

        TransactionDate.datetime = new Date().toISOString();
        TransactionDate.invoiceId = invoiceId;
        let transporterSmtp=nodemailer.createTransport(config.smtpOptions)


        const transaction = new Transaction(TransactionDate);
        try {
            const user = await User.findById(req.body.user);

            await transaction.save();
            await transporterSmtp.sendMail( {
                from: 'support@rekinetix.kz',
                to: user.email,
                subject: 'Добро пожаловать',
                text: "Здравствуйте вас приветствует REKINETIX.Вы купили программу. Теперь программа доступна в вашем личном кабинете в разделе мои программы." +
                    "Можете приступать к занятиям."
            })
            res.send(transaction)
        } catch (e) {
            res.status(400).send(e)
        }
    });

    router.put("/:id", [auth, permit("Пациент", "Администратор", "Менеджер")], async (req, res) => {
        try {
            await Transaction.findByIdAndUpdate(req.params.id, req.body);
            res.send(await Transaction.findById(req.params.id));
        } catch(e) {
            res.status(400).send(e);
        }
    });

    router.delete("/:id", async (req, res) => {
        try {
            res.send(await Transaction.findByIdAndRemove(req.params.id));
        } catch(e) {res.sendStatus(500).send(e)}
    });

    return router;
}

module.exports = createRouter;