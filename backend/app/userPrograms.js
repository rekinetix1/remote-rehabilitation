const express = require("express");
const UserProgram = require("../models/UserProgram");
const { ObjectID } = require("mongodb");

const createRouter = () => {
    const router = express.Router();

    router.get("/", async (req, res) => {
        let user;
        let program;
        if (req.query.user) {
            user = {user: req.query.user};
        }
        if (req.query.program){
            program = {program: req.query.program}
        }

        try {
            res.send(await UserProgram.find({...user, ...program}).populate("user").populate('options').populate("program").sort({datetime: -1}));
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.get("/:id", async (req, res) => {
        try {
            res.send(await UserProgram.findById(req.params.id).populate("user").populate("program", "title"));
        } catch (e) {
            res.status(400).send(e);
        }
     });

    router.post("/", async (req, res) => {
        const userProgramDate = req.body;
        const programs = await UserProgram.find({user: userProgramDate.user});
        
        userProgramDate.datetime = new Date().toISOString();

        const userProgram = new UserProgram(userProgramDate);
        try {
            let program = programs.find(program => (userProgramDate.program).toString() === ObjectID(program.program).toString())
            if (program) return res.status(500).send("The program is already assigned");
            await userProgram.save();
            res.send(userProgram)
        } catch (e) {
            res.status(400).send(e)
        }
    });

    router.put("/:id", async (req, res) => {
        try {
            await UserProgram.findByIdAndUpdate(req.params.id, req.body);
            res.send(await UserProgram.findById(req.params.id));
        } catch(e) {
            res.status(400).send(e);
        }
    });

    router.delete("/:id", async (req, res) => {
        try {
            res.send(await UserProgram.findByIdAndRemove(req.params.id));
        } catch(e) {res.sendStatus(500).send(e)}
    });

    return router;
}

module.exports = createRouter;