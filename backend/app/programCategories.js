const express = require("express");
const ProgramCategory = require("../models/ProgramCategory");
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const createRouter = () => {
    const router = express.Router();

    router.get("/", async (req, res) => {
        let search;
        if (req.query.search) {
            search = {$text: {$search: req.query.search}};
        }
        try {
            res.send(await ProgramCategory.find(search));
        } catch (e) {
            res.status(500).send(e);
        }
    });
    
    router.get("/:id", async (req, res) => {
        try {
            res.send(await ProgramCategory.findById(req.params.id));
        } catch (e) {
            res.status(400).send(e);
        }
     });

    router.post("/", [auth, permit("Администратор")], async (req, res) => {
        const programCategory = new ProgramCategory(req.body);
        try {
            await programCategory.save();
            res.send(programCategory)
        } catch (e) {
            res.status(400).send(e)
        }
    });

    router.put("/:id", [auth, permit("Администратор")], async (req, res) => {
        try {
            await ProgramCategory.findByIdAndUpdate(req.params.id, req.body);
            res.send(await ProgramCategory.findById(req.params.id));
        } catch(e) {
            res.status(400).send(e);
        }
    });

    router.delete("/:id", [auth, permit("Администратор")], async (req, res) => {
        try {
            res.send(await ProgramCategory.findByIdAndRemove(req.params.id));
        } catch(e) {res.sendStatus(500).send(e)}
    });

    return router;
}

module.exports = createRouter;
