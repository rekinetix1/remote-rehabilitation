const express = require("express");
const PhysicalActivity = require("../models/PhysicalActivity");
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const createRouter = () => {
    const router = express.Router();

    router.get("/", async (req, res) => {
        try {
            res.send(await PhysicalActivity.find());
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.get("/:id", async (req, res) => {
        try {
            res.send(await PhysicalActivity.findById(req.params.id));
        } catch (e) {
            res.status(400).send(e);
        }
     });

    router.post("/", [auth, permit("Администратор")], async (req, res) => {
        const user = new PhysicalActivity(req.body);
        try {
            await user.save();
            res.send(user)
        } catch (e) {
            res.status(400).send(e)
        }
    });

    router.put("/:id", [auth, permit("Администратор")], async (req, res) => {
        try {
            await PhysicalActivity.findByIdAndUpdate(req.params.id, req.body);
            res.send(await PhysicalActivity.findById(req.params.id));
        } catch(e) {
            res.status(400).send(e);
        }
    });

    router.delete("/:id", [auth, permit("Администратор")], async (req, res) => {
        try {
            res.send(await PhysicalActivity.findByIdAndRemove(req.params.id));
        } catch(e) {res.sendStatus(500).send(e)}
    });

    return router;
}

module.exports = createRouter;