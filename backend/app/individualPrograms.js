const express = require("express");
const config = require("../config");
const nodemailer = require('nodemailer');
const IndividualProgram = require("../models/IndividualProgram");
const User = require("../models/User");
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const createRouter = () => {
    const router = express.Router();

    router.get("/", [auth, permit("Пациент", "Терапевт", "Администратор", "Модератор", "Менеджер")], async (req, res) => {
        let user;
        if (req.query.user) {
            user = {user: req.query.user};
        }
        if (req.query.search) {
            let user_by_email = await User.find({email: {'$regex' : req.query.search, '$options' : 'i'}})
            if (user_by_email[0]) {
                user = {user: user_by_email[0]._id};
            }
        }
        try {
            res.send(await IndividualProgram.find(user).populate("user").sort({_id: -1}));
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.get("/:id",  async (req, res) => {
        try {
            const program = await IndividualProgram.findById(req.params.id)
                .populate("moderator")
                .populate("user")
                .populate({
                path: 'exercises',
                populate: {
                    path: 'exercise',
                    populate: {
                        path: 'equipment',
                        populate: {
                            path:'muscle'
                        }
                    }
                }})
            res.send(program);
        } catch(e) {res.sendStatus(404)}
    });

    router.post("/", [auth, permit("Терапевт", "Администратор")], async (req, res) => {
        let transporterSmtp = nodemailer.createTransport(config.smtpOptions)
        const programData = req.body;
        const program = new IndividualProgram(programData);
        const user = await User.findById(req.body.user);
        let text = `Здравствуйте вас приветствует REKINETIX.` +
        `Вам назначена индивидуальная программа`;
        if (req.body.status === "Не оплачено"){
            text = `Здравствуйте вас приветствует REKINETIX.` +
            `Вам назначена индивидуальная программа, для активации необходимо провести оплату`;
        }

        try {
            await program.save();
            await transporterSmtp.sendMail({
                from: 'support@rekinetix.kz',
                to: user.email,
                subject: 'Индивидуальная программа',
                text: text
            })
            res.send(program)
        } catch (e) {
            res.status(400).send(e)
        }
    });

    router.put("/:id", [auth, permit("Терапевт", "Администратор", "Менеджер", "Пациент")], async (req, res) => {
        const program = req.body;
        try {
            await IndividualProgram.findByIdAndUpdate(req.params.id, program);
            res.send(await IndividualProgram.findById(req.params.id));
        } catch(e) {
            res.status(400).send(e);
        }
    });

    router.delete("/:id", [auth, permit("Терапевт", "Администратор")], async (req, res) => {
        try {
            res.send(await IndividualProgram.findByIdAndRemove(req.params.id));
        } catch(e) {res.sendStatus(500).send(e)}
    });

    return router;
}

module.exports = createRouter;
