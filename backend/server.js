const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const app = express();
const config = require("./config");
const users = require("./app/users");
const exercises = require("./app/exercises");
const exerciseCategories = require("./app/exerciseCategories");
const equipment = require("./app/equipment");
const muscles = require("./app/muscles");
const programs = require("./app/programs");
const programCategories = require("./app/programCategories");
const optionsMenus = require("./app/optionsMenus");
const questionnaires = require("./app/questionnaires");
const userPrograms = require("./app/userPrograms");
const favorites = require("./app/favorites");
const occupation = require("./app/occupation");
const physicalActivities = require("./app/physicalActivities");
const pains = require("./app/pains");
const transaction = require("./app/transactions");
const userBrowsingHistories=require('./app/userBrowsingHistories')
const individualPrograms = require("./app/individualPrograms");
const comments = require("./app/comments");
let PORT = process.env.NODE_ENV === "test" ? 8010 : 8000;

app.use(cors());
app.use(express.static("public"));
app.use(express.json());

mongoose.connect(`${config.db.url}/${config.db.name}`, {

  useFindAndModify: false
}).then(() => {
  console.log('Mongoose connected!');

  app.use('/users', users());
  app.use('/exercises', exercises());
  app.use('/exercise_categories', exerciseCategories());
  app.use('/equipment', equipment());
  app.use('/muscles', muscles());
  app.use('/programs', programs());
  app.use('/program_categories', programCategories());
  app.use('/options_menus', optionsMenus());
  app.use('/questionnaires', questionnaires());
  app.use('/user_programs', userPrograms());
  app.use('/favorites', favorites());
  app.use('/occupation', occupation());
  app.use('/physical_activities', physicalActivities());
  app.use('/pains', pains());
  app.use('/transactions', transaction());
  app.use('/userBrowsingHistories', userBrowsingHistories());
  app.use('/individual_programs', individualPrograms());
  app.use('/comments', comments());

  app.listen(PORT, () => {
    console.log(`Server started on ${PORT} port!`);
  });
});
