const mongoose = require("mongoose");
const config = require("./config");
const User = require("./models/User");
const Equipment = require("./models/Equipment");
const ExerciseCategory = require("./models/ExerciseCategory");
const Exercise = require("./models/Exercise");
const ProgramCategory = require("./models/ProgramCategory");
const Program = require("./models/Program");
const UserProgram = require("./models/UserProgram");
const Questionnaire = require("./models/Questionnaire");
const OptionsMenu = require("./models/OptionsMenu");
const Favorites = require("./models/Favorites");
const Occupation = require("./models/Occupation");
const PhysicalActivity = require("./models/PhysicalActivity");
const Pain = require("./models/Pain");
const Muscle = require("./models/Muscle");

mongoose.connect(`${config.db.url}/${config.db.name}`);

const db = mongoose.connection;

db.once("open", async () => {
    try {
        await db.dropCollection("equipment");
        await db.dropCollection("exerciseCategories");
        await db.dropCollection("exercises");
        await db.dropCollection("programCategories");
        await db.dropCollection("programs");
        await db.dropCollection("users");
        await db.dropCollection("questionnaires");
        await db.dropCollection("optionsMenus");
        await db.dropCollection("favorites");
        await db.dropCollection("occupation");
        await db.dropCollection("physicalActivities");
        await db.dropCollection("pains");
        await db.dropCollection("muscle");
    } catch (e) {
        console.log("Collection were not presented. Skipping drop...");
    }

  const [user, admin, moderator, doctor, author, manager] = await User.create({
    email: "user1@mail.ru",
    password: "123123",
    role: "Пациент",
    status: "Активный",
    name: "Кайрат",
    surname: "Нуртас",
    city: "Astana",
    country: "Kazakhstan",
    verify: true,
  }, {
    email: "admin@mail.ru",
    password: "123123",
    role: "Администратор",
    status: "Активный",
    name: "Администратор",
    surname: "Рекинетикса",
    city: "Almaty",
    country: "Kazakhstan",
    verify: true
  }, {
    email: "moderator@mail.ru",
    password: "123123",
    role: "Модератор",
    status: "Активный",
    name: "Модератор",
    surname: "Рекинетикса",
    city: "Almaty",
    country: "Kazakhstan",
    verify: true
  }, {
    email: "doctor@mail.ru",
    password: "123123",
    role: "Терапевт",
    status: "Активный",
    name: "Терапевт",
    surname: "Рекинетикса",
    city: "Almaty",
    country: "Kazakhstan",
    verify: true
  }, {
    email: "author@mail.ru",
    password: "123123",
    role: "Автор БП",
    status: "Активный",
    name: "Автор БП",
    surname: "Рекинетикса",
    city: "Almaty",
    country: "Kazakhstan",
    verify: true
  }, {
    email: "manager@mail.ru",
    password: "123123",
    role: "Менеджер",
    status: "Активный",
    name: "Менеджер",
    surname: "Рекинетикса",
    city: "Almaty",
    country: "Kazakhstan",
    verify: true
  }, );

    const [dumbbells, fitball, hoop, gymnasticSticks, weight, skippingRope, medBall, Expander] = await Equipment.create(
        {
            title: "Гантели"
        }, {
            title: "Фитбол"
        }, {
            title: "Обруч"
        }, {
            title: "Гимнастические палки"
        }, {
            title: "Гиря"
        }, {
            title: "Скакалка"
        }, {
            title: "Медбол"
        }, {
            title: "Эспандер"
        },
    );


    const [general, lower_limbs, upper_limbs] = await ExerciseCategory.create({
        title: "Общеразвивающие упражнения"
    }, {
        title: "Упражнения для нижних конечностей"
    }, {
        title: "Упражнения для верхних конечностей"
    });

    const [general_programm, spine, leg, chest] = await ProgramCategory.create({
        title: "Общее"
    }, {
        title: "Позвоночник"
    }, {
        title: "Ноги"
    }, {
        title: "Грудь"
    });

    const [quadriceps, hip_biceps, pectoral_muscle, delta_muscle, abs_muscle] = await Muscle.create({
        title: "Квадрицепс"
    }, {
        title: "Бицепс бедра"
    }, {
        title: "Грудная мышца"
    }, {
        title: "Дельты"
    }, {
        title: "Пресс"
    });


    const [hand_circles, squat, plow_pose, press_weight, swing_weights, jumping_rope, torsion_of_the_hoop, push_up, twisting_the_body, lying_leg_curl] = await Exercise.create(
        {
            title: "Круги руками",
            description: "Встаньте прямо, поднимите прямые руки в стороны на уровень плеч, ладони смотрят вверх. Медленно описывайте руками небольшие круги диаметром около 15 сантиметров. В основном движение исходит от плеч, напряжение чувствуется в их задней части. Выполните пять кругов вперёд и пять назад.",
            category: general,
            video_link: "https://www.youtube.com/embed/-O-Jf31xLV8",
            equipment: dumbbells,
            image: "hands.png",
            complexity: "Средняя",
            kinematic_chain: "ОКЦ",
            muscle: delta_muscle

        }, {
            title: "Приседание",
            description: "Одно из базовых силовых упражнений. Дополняющие упражнение приседает и затем встаёт, возвращаясь в положение стоя.",
            category: lower_limbs,
            video_link: "https://www.youtube.com/embed/t8fUlq53Z8s",
            equipment: dumbbells,
            image: "sit.jpg",
            complexity: "Тяжелая",
            kinematic_chain: "ЗКЦ",
            muscle: quadriceps
        },
        {
            title: "Поза плуга",
            description: "В положении лежа на коврике сделайте медленны вдох и выдох. Со следующим вдохом оторвите ноги от коврика, сначала согните в коленях и затем плавно заведите за голову. Избегайте рывков, а также ерзаний в перевернутом положении.",
            category: lower_limbs,
            video_link: "https://www.youtube.com/embed/MLkrUUQEFi0",
            equipment: fitball,
            image: "plug.jpeg",
            complexity: "Низкая",
            kinematic_chain: "ОКЦ",
            muscle: hip_biceps
        }, {
            title: "Жим гири",
            description: "Исходное положение: стоя, гиря поднята на грудь, она удобно лежит на предплечье с внешней стороны руки. Запястье при этом повернуто тыльной стороной к телу и составляет одну прямую линию с предплечьем. Кисть не сгибается ни внутрь, ни наружу. Мышцы всего тела при этом в тонусе, пресс подтянут, стопы на ширине плеч, спина прямая.",
            category: upper_limbs,
            video_link: "https://www.youtube.com/watch?v=ypTH3k23uPk",
            equipment: weight,
            image: "press_weght.jpg",
            complexity: "Средняя",
            kinematic_chain: "ОКЦ",
            muscle: delta_muscle
        },
        {
            title: "Махи гирей",
            description: "Ноги чуть шире плеч. Носки расставлены в стороны под 45 градусов.  Ступни плотно прижаты к полу.  Центр тяжести лежит на пятках.  Таз отведен назад, спина идеально прямая.  Не наклоняем голову вниз и не выгибаем шею назад, взгляд должен быть направлен строго перед собой. Гиря стоит на полу между Ваших ног.",
            category: upper_limbs,
            video_link: "https://youtu.be/HSTreJWu6N0",
            equipment: weight,
            image: "swing_weights.jpg",
            complexity: "Низкая",
            kinematic_chain: "ОКЦ",
            muscle: delta_muscle
        },
        {
            title: "Прыжки на скакалке",
            description: "Ноги чуть шире плеч. Носки расставлены в стороны под 45 градусов.  Ступни плотно прижаты к полу.  Центр тяжести лежит на пятках.  Таз отведен назад, спина идеально прямая.  Не наклоняем голову вниз и не выгибаем шею назад, взгляд должен быть направлен строго перед собой. Гиря стоит на полу между Ваших ног.",
            category: general,
            video_link: "https://youtu.be/HSTreJWu6N0",
            equipment: skippingRope,
            image: "jump_skippingRope.jpg",
            complexity: "Средняя",
            kinematic_chain: "ОКЦ",
            muscle: delta_muscle
        },
        {
            title: "Кручиние обруча",
            description: "Крутить обруч нужно строго по часовой стрелке (а не куда придется). Исходное положение: ноги вместе (!!!), руки за головой, спина прямая.",
            category: upper_limbs,
            video_link: "https://www.youtube.com/watch?v=HHDcUuVjlmQ",
            equipment: hoop,
            image: "twist wrap.jpg",
            complexity: "Низкая",
            kinematic_chain: "ОКЦ",
            muscle: abs_muscle
        },
        {
            title: "Жим гантелей лежа ",
            description: "упражнение для прокачки всех частей груди. Чтобы полноценно задействовать верх, " +
                "низ и середину грудных мышц следует выполнять его под разными углами.",
            category: upper_limbs,
            video_link: "https://www.youtube.com/watch?v=mXdyLcQ_VZU",
            equipment: dumbbells,
            image: "dumbbell_bench_press.jpg",
            complexity: "Низкая",
            kinematic_chain: "ОКЦ",
            muscle: pectoral_muscle
        },
        {
            title: "Отжимания от пола",
            description: "Базовое физическое упражнение, выполняемое в планке и представляющее собой опускание-поднятие тела с помощью рук от пола.",
            category: upper_limbs,
            video_link: "https://www.youtube.com/watch?v=EZf7IDkxnLc",
            equipment: fitball,
            image: "Pushups.jpg",
            complexity: "Средняя",
            kinematic_chain: "ОКЦ",
            muscle: pectoral_muscle
        },
        {
            title: "Скручивание корпуса",
            description: "Лёжа на спине, согните ноги в коленях и поставьте на пол. Палку поднимите над собой на уровне коленей, руки полусогнуты.",
            category: upper_limbs,
            video_link: "https://www.youtube.com/watch?v=HHDcUuVjlmQ",
            equipment: gymnasticSticks,
            image: "gymnastic_stick_press.jpg",
            complexity: "Средняя",
            kinematic_chain: "ОКЦ",
            muscle: abs_muscle
        },
        {
            title: "Сгибание ног лежа",
            description: "Установите фиксатор в нижнем положении, трубку (трубки) эспандера просуньте через петлю фиксатора и пристегните к манжетам на ногах. Лягте на живот спиной к стене или двери. В начальном положении эспандер должен " +
                "быть в небольшом натяжении. На выдохе, плавно начинайте сгибание. На пике сделайте паузу и медленно" +
                " возвращайтесь в исходное положение. Старайтесь не отрывать таз от пола, выполняйте с максимальной амплитудой.",
            category: lower_limbs,
            video_link: "https://www.youtube.com/watch?v=RfBrPF5rxlg",
            equipment: Expander,
            image: "Lying_Leg_Curl.jpg",
            complexity: "Тяжелая",
            kinematic_chain: "ЗКЦ",
            muscle: hip_biceps
        },
        {
            title: "Планка с медболом",
            description: "Положите медбол на пол и упритесь на него обеими руками. Держите руки прямо, так же как и ноги, как в классическом положении планки," +
                " поддерживая баланс на медболе. Напрягите мышцы живота и держите тело в одной линии. ",
            category: upper_limbs,
            video_link: "https://www.youtube.com/watch?v=hjfXKQWtA9Q",
            equipment: medBall,
            image: "Plank_with_medball.png",
            complexity: "Низкая",
            kinematic_chain: "ЗКЦ",
            muscle: abs_muscle
        },

        {
            title: "Приседание с гирей между ног ",
            description: "Берем гантель обеими руками так, чтобы держать ее за один из краев.Ставим ноги носками врозь, будто вы балерина." +
                "Спину выпрямляем, таз немного отводим назад, создавая естественный прогиб в пояснице. Начинаем опускать таз вниз. После достижения нижней точки начинаем вставать обратно.",
            category: lower_limbs,
            video_link: "https://www.youtube.com/watch?v=ziXoI_ghgz8",
            equipment: weight,
            image: "sit_without_weight.jpg",
            complexity: "Низкая",
            kinematic_chain: "ЗКЦ",
            muscle: hip_biceps
        },
    );


    const [first_program, second_program] = await Program.create({
        title: "Комплекс упражнений для позвоночника",
        short_description: "Комплексы упражнений увеличивают подвижность суставов, растяжение мышц, тренируют работу сердечно-сосудистой и дыхательной системы.",
        full_description: "Комплексы упражнений увеличивают подвижность суставов, растяжение мышц, тренируют работу сердечно-сосудистой и дыхательной системы. Они улучшают обмен веществ, повышают устойчивость к физическим нагрузкам. Лечебная физкультура ускоряет выздоровление, повышает эффективность комплексного лечения, предупреждает дальнейшее прогрессирование хронических болезней.",
        complexity: "Низкая",
        author: doctor,
        moderator: moderator,
        duration: 3,
        image: "Spine.jpeg",
        category: spine,
        video: 'https://www.youtube.com/embed/OeuUrUuDvq0',
        days: [
            [
                {
                    exercise: hand_circles,
                    repetitions: 10,
                    approaches: 0,
                }, {
                exercise: squat,
                repetitions: 15,
                approaches: 4,
            }
            ], [
                {
                    exercise: hand_circles,
                    repetitions: 10,
                    approaches: 0,
                }, {
                    exercise: squat,
                    repetitions: 15,
                    approaches: 4,
                }
            ], [
                {
                    exercise: squat,
                    repetitions: 15,
                    approaches: 3,
                }, {
                    exercise: plow_pose,
                    repetitions: 20,
                    approaches: 0,
                },
            ]
        ],
        cost: 5000
    }, {
        title: "Лечебная физкультура (ЛФК)",
        short_description: "Упражнения на развитие силы сначала выполняют с собственным весом в исходном положении лежа на спине, на боку, на животе.",
        full_description: "Упражнения на развитие силы сначала выполняют с собственным весом в исходном положении лежа на спине, на боку, на животе. При положительной динамике довольно быстро можно начинать выполнять упражнения с отягощением (утяжелители) и с сопротивлением (с эластичными лентами или эспандером). Постепенно в комплекс упражнений вводят новые исходные положения — сидя и стоя. Заниматься необходимо несколько раз в день: начинают с одного-двух раз и в течение нескольких дней доводят до 3–4 занятий в день.",
        complexity: "Средняя",
        author: admin,
        moderator: moderator,
        duration: 2,
        image: "lfk.jpg",
        category: general_programm,
        video: 'https://www.youtube.com/embed/D5VYidrEllU',
        days: [
            [
                {
                    exercise: hand_circles,
                    repetitions: 10,
                    approaches: 0,
                }, {
                exercise: squat,
                repetitions: 15,
                approaches: 0,
            }
            ], [
                {
                    exercise: hand_circles,
                    repetitions: 25,
                    approaches: 0,
                }, {
                    exercise: plow_pose,
                    repetitions: 20,
                    approaches: 0,
                },
            ]
        ],
        cost: 7000
    }, {
        title: "Комплексная тренировка",
        short_description: " Подготовка  тела к интенсивным тренировкам.  ",
        full_description: "Данная программа  подготавливает ваше тело к полноценным тренировкам, так как если вы сразу приступите  к интенсивным тренировкам могут быть травмы",
        complexity: "Низкая",
        author: admin,
        moderator: moderator,
        duration: 2,
        image: "lfk.jpg",
        category: general_programm,
        video: 'https://www.youtube.com/embed/D5VYidrEllU',
        days: [
            [
                {
                    exercise: hand_circles,
                    repetitions: 10,
                    approaches: 0,
                }, {
                exercise: squat,
                repetitions: 15,
                approaches: 0,
            }
            ], [
                {
                    exercise: hand_circles,
                    repetitions: 25,
                    approaches: 0,
                }, {
                    exercise: plow_pose,
                    repetitions: 20,
                    approaches: 0,
                },
            ]
        ],
        cost: 5000
    }, {
        title: "Тренировка ног",
        short_description: " Энтенсивная тренировка ног.",
        full_description: "Данная программа  делает большой упор на ноги, так как ноги сама большая мышца ",
        complexity: "Тяжелая",
        author: admin,
        moderator: moderator,
        duration: 2,
        image: "legs.jpg",
        category: leg,
        video: 'https://www.youtube.com/watch?v=5E0n8bDkU4o',
        days: [
            [
                {
                    exercise: squat,
                    repetitions: 10,
                    approaches: 0,
                }, {
                exercise: squat,
                repetitions: 15,
                approaches: 0,
            }
            ], [
                {
                    exercise: press_weight,
                    repetitions: 15,
                    approaches: 0,
                }, {
                    exercise: press_weight,
                    repetitions: 20,
                    approaches: 0,
                },
            ],
        ],
        cost: 5000
    }, {
        title: "Тренировка груди",
        short_description: " Энтенсивная тренировка груди.",
        full_description: "Данная программа  делает большой упор на грудь, так как грудь важная часть тела ",
        complexity: "Средняя",
        author: admin,
        moderator: moderator,
        duration: 2,
        image: "chest.jpg",
        category: chest,
        video: 'https://www.youtube.com/watch?v=g-_avpQWyI8',
        days: [
            [
                {
                    exercise: push_up,
                    repetitions: 5,
                    approaches: 3,
                }, {
                exercise: twisting_the_body,
                repetitions: 20,
                approaches: 4,
            }
            ], [
                {
                    exercise: push_up,
                    repetitions: 15,
                    approaches: 3,
                }, {
                    exercise: push_up,
                    repetitions: 20,
                    approaches: 0,
                },
            ],
        ],
        cost: 5000
    });


    const [option1, option2, option3] = await OptionsMenu.create({
        title: "Обратная связь",
        cost: 500
    }, {
        title: "Индивидуальные консультации",
        cost: 1000
    }, {
        title: "Индивидуальная программа",
        cost: 2000
    });

    await UserProgram.create({
        user: user,
        program: first_program,
        program_rate: 7500,
        options: [option1._id, option2._id, option3._id],
        status: "Оплачено",
        datetime: "2020-09-17T17:41:31.868Z"
    }, {
        user: user,
        program: second_program,
        program_rate: 5000,
        status: "Оплачено",
        datetime: "2020-09-17T17:41:31.868Z"
    });

    await Favorites.create({
        user: user._id,
        program: second_program
    }, {
        user: user._id,
        program: first_program
    });


    const [dom, office, walk, onePose, sport] = await Occupation.create({
        title: "Домохозяин (-ка)"
    }, {
        title: "Офисный работник (-ца), сидячая работа"
    }, {
        title: "Работа связана с длительной ходьбой"
    }, {
        title: "Длительное пребывание в однотипной позе"
    }, {
        title: "Профессиональный спортсмен"
    });

    const [lessAct, normalAct, act] = await PhysicalActivity.create({
        title: "Малоподвижная"
    }, {
        title: "Умеренно активная"
    }, {
        title: "Активная"
    });

    const [head, neck, loin, shoulder, hip, knee] = await Pain.create({
        title: "Частые головные боли"
    }, {
        title: "Боли в шейном отделе"
    }, {
        title: "Боли в пояснице"
    }, {
        title: "Боли в плече"
    }, {
        title: "Боли в тазобедренном суставе"
    }, {
        title: "Боли в коленном суставе"
    });

    await Questionnaire.create({
        user: user,
        age: 34,
        weight: 95,
        height: 175,
        gender: "Мужчина",
        occupation: walk._id,
        physicalActivity: act._id,
        pains: [head._id, neck._id],
        exacerbationsCountPerYear: 2,
        painIntensity: 4,
        diagnosis: "Черепное давление"
    }, {
        user: admin,
        age: 22,
        weight: 75,
        height: 185,
        gender: "Мужчина",
        occupation: office._id,
        physicalActivity: lessAct._id,
        pains: [loin._id],
        exacerbationsCountPerYear: 90,
        painIntensity: 5,
        diagnosis: "Остеохондроз"
    });

    db.close();
});

