const path = require("path");

const rootPath = __dirname;

let dbUrl = "mongodb://localhost";
let dbName = "rekinetix";

if (process.env.NODE_ENV === 'test') {
    dbName = "rekinetix-test";
}

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, "public", "uploads"),
    db: {
        url: dbUrl,
        name: dbName
    },
    mailOptions: {
        service: 'mail.ru',
        auth: {
            user: 'rekinetixesdp@mail.ru',
            pass: '1qaz@WSX29'
        }
    },
    smtpOptions:{
        service:'Yandex',
        auth:{
            user:"support@rekinetix.kz",
            pass:'p^4X$L1nbJ7w'
        }
    }
}
